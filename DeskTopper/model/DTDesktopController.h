//
//  DTDesktopController.h
//  DeskTopper
//
//  Created by David Shea on 12/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DTDesktop.h"

//-----------------------------------------------------------------
enum DTDesktopIndex
{
	DTDesktopIndexBin	= 0,
	DTDesktopIndexScrap	= 1,
	DTDesktopIndexUser	= 2,
};


//-----------------------------------------------------------------
@interface DTDesktopController : NSObject// <NSCoding>

@property (nonatomic, strong)	NSMutableArray*	desktops;
@property (nonatomic)			int				currentDesktopIndex;
@property (nonatomic)			int				index;
@property (nonatomic, weak)		FMDatabase*		database;

+ (DTDesktopController*) createDesktopControllerFromDatabase:(FMDatabase*)database;

- (void)		addDesktop:(DTDesktop*)desktop;
- (void)		removeDesktop:(DTDesktop*)desktop;
- (DTDesktop*)	getDesktopFromIndex:(int)index;
- (DTDesktop*)	getCurrentDesktop;
- (int)			getDesktopCount;
- (void)		updateDatabaseFromDesktopController:(FMDatabase*)database;

//-----------------------------------------------------------------
@end

