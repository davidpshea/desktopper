//
//  Desktop.h
//  2ndProject
//
//  Created by David Shea on 1/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTWidget.h"
#import "DTShortcutWidget.h"

@class FMDatabase;

@interface DTDesktop : NSObject //<NSCoding>

@property (nonatomic)			BOOL			isEditing;
@property (nonatomic, strong)	id				selectedWidget;
@property (nonatomic, strong)	NSMutableArray*	widgets;
@property (nonatomic, strong)	UIImage*		snapshot;
@property (nonatomic, strong)	UIImage*		backgroundImage;
@property (nonatomic)			int				index;
@property (nonatomic)			BOOL			hasLoadedFromDatabase;
@property (nonatomic)			BOOL			isDirty;
@property (nonatomic)			BOOL			thumbnailIsDirty;
@property (nonatomic)			BOOL			backgroundImageActive;


- (NSString*)	getName;
- (void)		addWidget:(id)widget;
- (void)		removeWidget:(id)widget;
- (id)			getWidgetFromIndex:(int)Index;
- (int)			getWidgetCount;
- (void)    	selectWidgetForEdit:(id)pWidget;
- (void)		orderWidgetsByZ;
- (BOOL)		isAnyWidgetDirty;

- (NSMutableArray*)	getWidgetsOrderedBySize;
//- (DTWidget*)	findWidgetClosestToPoint:(CGPoint)point;
- (void)		updateDatabaseFromDesktop:(FMDatabase*)database;
- (void)		clearBackgroundImage;

@end

