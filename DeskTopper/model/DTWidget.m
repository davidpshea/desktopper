
//
//  Widget.m
//  2ndProject
//
//  Created by David Shea on 1/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import "DTWidget.h"
#import "UINavigationController+Extras.h"
#import "DTWidgetImageView.h"
#import "QuickDialogHelper.h"
#import "UIImagePicker.h"
#import "UIApplication+Helpers.h"
#import "DTOptionsControllerWithDone.h"
#import "QImageElement.h"
#import "DTWidgetStyle.h"
#import "UIView+Animation.h"
#import "DTAppDelegate.h"
#import "DTViewController.h"
#import "SVProgressHUD.h"
#import "UIImageExtras.h"
#import "UIPopoverController+Stack.h"

@implementation DTWidget

//-----------------------------------------------------------------
@synthesize image = _image;

//-----------------------------------------------------------------
/*
 -(void) setIsDirty:(BOOL)isDirty
{
	_isDirty = isDirty;
}
*/
//-----------------------------------------------------------------
static __weak DTWidget* activeWidget = nil;

+ (DTWidget*) getActiveWidget
{
	return activeWidget;
}

//-----------------------------------------------------------------
static __weak QRootElement* activeMenuRoot = nil;

+ (QRootElement*) getActiveMenuRoot
{
	return activeMenuRoot;
}

//-----------------------------------------------------------------
static __weak DTWidgetQuickDialogController* activeMenuController = nil;

+ (DTWidgetQuickDialogController*) getActiveMenuController
{
	return activeMenuController;
}

//-----------------------------------------------------------------
+ (void) activeMenuUpdateQMultilineElement:(NSString*)string withKey:(NSString*)key
{
	if (activeMenuRoot) {
		QMultilineElement* element = (QMultilineElement*)[activeMenuRoot elementWithKey:key];

		if (element) {
			[element setTextValue:string];
			[[activeMenuController quickDialogTableView] reloadData];
		}
	}
}

//-----------------------------------------------------------------
+ (void) activeMenuReloadData
{
	if (activeMenuController) {
		[[activeMenuController quickDialogTableView] reloadData];
	}
}

//-----------------------------------------------------------------
+ (DTWidget*) createWidgetFromClass:(Class)class
{
	return [[class alloc] init];
}

//-----------------------------------------------------------------
#if 0
- (void) setIndex:(int)index
{
	DT_ASSERT(index > 0);
	_index = index;
}
#endif

//-----------------------------------------------------------------
//#ifdef DEBUG
#if 0
- (void) setDesktopIndex:(int)index
{
	DT_DEBUG_LOG(@"Set desktop index %d:", index);
	DT_ASSERT(index > 0);
	_desktopIndex = index;
}
#endif
//------------------------------------------------------------------
- (void) setIsProcessing:(BOOL)isProcessing
{
//	static BOOL own = NO;
/*
	if ([SVProgressHUD isVisible] && isProcessing) {
		return;
	}
	*/
	_isProcessing = isProcessing;
/*
	if (isProcessing) {
		[SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
	}
	else {
		[SVProgressHUD dismiss];
	}
*/
}

//-----------------------------------------------------------------
- (id) copyWithZone:(NSZone*)zone
{
	DTWidget* another = [[[self class] alloc] init];

	[another setImage:				nil];
	[another setZPosition:			[self zPosition]];
	[another setScreenPosition:		[self screenPosition]];
	[another setPreviousScreenPosition: [self previousScreenPosition]];
	[another setView:				nil];
	[another setScale:				[self scale]];
	[another setSize:				[self size]];
	[another setPreviousSize:		[self previousSize]];
	[another setKeepAspectRatio:	[self keepAspectRatio]];
	[another setStyle:				[[self style] copy]];
	[another setLoadingOperation:	nil];
	[another setIndex:				0];
	[another setDesktopIndex:		[self desktopIndex]];
	[another setIsDirty:YES];

	NSString* oldFilePath = [self imageFilePath];
	if (oldFilePath) {
		// Copy desktop image to new file
		NSString* fileName = [UIImage addDefaultImageFileExtension:[FileHelper getUniqueFileNameFromTime:@"image%@"]];
		NSString* filePath = [FileHelper getPathForFileInDocuments:fileName];
		[FileHelper copyFile:oldFilePath toPath:filePath];

		// Create a new image object from the new file
		UIImage* image = [UIImage createFromImageFile:filePath];

		// give new widget pathname and image data
		[another setImageFilePath:filePath withImage:image];
	}
/*	else {
		[another setIconIndex:[self iconIndex]];
	}
*/
	return another;
}

//-----------------------------------------------------------------
+ (NSArray*) getWidgetClassNameArray
{
	NSArray* classNames = [[NSArray alloc] initWithObjects:
		@"DTShortcutWidget",
		@"DTPictureWidget",
		@"DTPostItNoteWidget",
		@"DTPhotoWidget",
//		@"DTTimerWidget",
						   //		@"DTOptionsWidget",
		nil
	];

	return classNames;
}

//-----------------------------------------------------------------
- (CGRect) getBounds
{
	return CGRectMake(_screenPosition.x, _screenPosition.y, _size.width, _size.height);
}

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
	return @"system";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Not Set";
}

//-----------------------------------------------------------------
- (void) setLoadingOperation:(NSOperation*)operation
{
//	LOG_FN();
	_loadingOperation = operation;
	if (operation) {
		[UIApplication addOperationToMyOperationQueue:operation];
	}
}

//-----------------------------------------------------------------
- (void) refreshImageFromFilePathB
{
	// Icons are numbered from 1..N, with 0 being no icon.
	if (_imageFilePath && (! _loadingOperation)) {
//		DT_DEBUG_LOG(@"refreshImageFromFilePath: %@", _imageFilePath);
		NSBlockOperation* loadOperation = [
			NSBlockOperation
			blockOperationWithBlock:^{
				[self setImage:[UIImage createFromImageFile:_imageFilePath]];
			}
		];
		[self setLoadingOperation:loadOperation];
	}
}

//-----------------------------------------------------------------
- (void) refreshImageFromFilePath
{
	// Icons are numbered from 1..N, with 0 being no icon.
	if (_imageFilePath && (! _loadingOperation)) {
//		DT_DEBUG_LOG(@"refreshImageFromFilePath: %@", _imageFilePath);
		[self setImage:[UIImage createFromImageFile:_imageFilePath]];
	}
}

//-----------------------------------------------------------------
- (void) refreshImageFromIconIndex
{
	// Icons are numbered from 1..N, with 0 being no icon.
	if ((_iconIndex > 0) && (! _loadingOperation)) {
//		DT_DEBUG_LOG(@"refreshImageFromIconIndex");
		NSBlockOperation* loadOperation = [
			NSBlockOperation
			blockOperationWithBlock:^{
				// Icons are numbered from 1..N, with 0 being no icon.
				NSArray* icons     = [FileHelper getIconFileArray];
				NSString* pathName = [icons objectAtIndex:(_iconIndex - 1)];
				[self setImage:[[UIImage alloc] initWithContentsOfFile:pathName]];
			}
		];
		[self setLoadingOperation:loadOperation];
	}
}

//-----------------------------------------------------------------
- (UIImage*) image
{
	// Note use of variables rather than setter/getter

	// We already have an image, use it...
	if (_image) {
		return _image;
	}

	// no image, but have a filename, so load the image back in
	if (_imageFilePath != nil) {
		[self refreshImageFromFilePath];
		return _image;
	}

	// ok, so perhaps we have an icon number?
	// Icons are numbered from 1..N, with 0 being no icon.
	if (_iconIndex > 0) {
		[self refreshImageFromIconIndex];
		return _image;
	}

	// Failed to create any kind of image...
	return nil;
}

//-----------------------------------------------------------------
- (void) setViewSizeFromWidgetSize
{
	DTWidgetImageView* view = [self view];

	if (view) {
		CGRect bounds      = [view bounds];
		bounds.size.width  = _size.width;
		bounds.size.height = _size.height;
//		DT_DEBUG_LOG(@"SetSize:%@", NSStringFromCGRect(bounds));

		[view setBounds:bounds];
	}
}

//-----------------------------------------------------------------
- (void) setImage:(UIImage*)image
{
	if (image != _image) {
		_image = image;

		// reset scale so it's displayed at its correct size
//		[self setScale:1.0f];

		DTWidgetImageView* view = [self view];
		if (view) {
			// Make view aware of new image, re-size view to fit it
			[view setImage:image];
			[self setViewSizeFromWidgetSize];

			// Make any sub classes aware of the change
			[self hasUpdatedImage];
		}
	}
}

//-----------------------------------------------------------------
- (void) setHighResolutionImage:(UIImage*)image
{
}

//-----------------------------------------------------------------
- (void) setHighResolutionImageBackground:(UIImage*)image
{
}

//-----------------------------------------------------------------
- (UIImage*) getHighResolutionImage
{
	return [self image];
}

//-----------------------------------------------------------------
- (NSString*) getHighResolutionImageFilePath
{
	return [self imageFilePath];
}

//-----------------------------------------------------------------
- (void) setImageFilePath:(NSString*)newPath withImage:(UIImage*)image
{
	LOG_FN();
	if (! [_imageFilePath isEqualToString:newPath]) {

		//		DT_ASSERT_NOT_NIL(newPath);
		//		DT_ASSERT([newPath length] < 0);
		
		if (_imageFilePath) {
			[FileHelper sendFileToBin:_imageFilePath];
		}

		_imageFilePath = newPath;
		if (image == nil) {
			[self refreshImageFromFilePath];
		}
		else {
			[self setImage:image];
		}
		_isDirty = YES;
	}
}

//-----------------------------------------------------------------
- (void) setImageFilePath:(NSString*)newPath
{
	[self setImageFilePath:newPath withImage:nil];
}

//-----------------------------------------------------------------
- (void) setImageFilePathNoUpdate:(NSString*)newPath
{
	_imageFilePath = newPath;
}

//-----------------------------------------------------------------
- (void) setIconIndex:(int)iconIndex
{
	LOG_FN();
	if (_iconIndex != iconIndex) {
		_iconIndex = iconIndex;
		_isDirty   = YES;
		[self refreshImageFromIconIndex];
	}
}

//-----------------------------------------------------------------
- (void) setView:(DTWidgetImageView*)view
{
	if (_view != view) {
		_view = view;

		if (view) {
			if (_image) {
				// Make view aware of new image, re-size view to fit it
				[view setImage:_image];
			}
			[self setViewSizeFromWidgetSize];
		}
		else {
			// View is nil, so we don't need the underlying image either
			_image = nil;
		}
	}
}

//-----------------------------------------------------------------
- (void) enableForViewing
{
	(void) [self image];
	[[self view] setHidden:NO];
}

//-----------------------------------------------------------------
- (void) disableForViewing
{
	_image = nil;
	[[self view] setHidden:YES];
}

//-----------------------------------------------------------------
- (void) setScale:(CGFloat)scale
{
//	if (_scale != scale) {
		_scale   = scale;
		_isDirty = YES;
		[[self view] setScale:scale];
//	}
}

//-----------------------------------------------------------------
- (void) setScreenPosition:(CGPoint)position
{
	//	_previousScreenPosition = _screenPosition;

	_screenPosition = position;
	_isDirty        = YES;
	[[self view] setCenter:position];
}

//-----------------------------------------------------------------
- (void) setSize:(CGSize)size
{
	if ((size.width <= 0) || (size.height <= 0)) {
		DT_DEBUG_LOG(@"WIDGET ZERO SIZE! %f,%f", size.width, size.height);
		size.width  = 150;
		size.height = 150;
	}

	//	_previousSize = _size;
	
	_size	  = size;
	_isDirty  = YES;
	[self setViewSizeFromWidgetSize];
}

//-----------------------------------------------------------------
- (void) hasFinishedResizing:(CGSize)size
{
	[self setSize:size];
}

//-----------------------------------------------------------------
- (DTWidget*) init
{
	LOG_FN();
	self = [super init];
	if (self) {
//		_isDirty = YES;
		[self setScale:1.0];
		[self setIconIndex:0];
		[self setSize:CGSizeMake(320/2, 480/2)];
		[self setPreviousSize:_size];

		[self setStyle:[[DTWidgetStyle alloc] init]];
	}
	return self;
}

//-----------------------------------------------------------------
- (void) destroy:(BOOL)animated
{
#if 0
	// Do the delete on an operation, since deleting a
	// hi-res image can be a bit slow (or so it seams)
	NSBlockOperation* deleteOperation = [
		NSBlockOperation
		blockOperationWithBlock:^{
			[self setHighResolutionImage:nil];
			[self setImageFilePath:nil];
		}
	];
	[UIApplication addOperationToMyOperationQueue:deleteOperation];
#endif

	// remove any standard image...
	[self setImageFilePath:nil];

	// Then any hi-res version. We do this after the standard one since sometimes
	// These are one-and-the-same
	// We also don't set it to nil since this would trigger a database
	// update that we don't want
	if ([self getHighResolutionImageFilePath]) {
		[FileHelper sendFileToBin:[self getHighResolutionImageFilePath]];
	}
	
	// Animate the delete
	if (animated) {
		[[self view] drainAway:1.0];
	}
	else {
		[[self view] removeFromSuperview];
		[self setView:nil];
	}
}

//-----------------------------------------------------------------
#define kZPositionKey		@"zPosition"
#define kCenterXKey			@"centerx"
#define kCenterYKey			@"centery"
#define kPreviousCenterXKey	@"previouscenterx"
#define kPreviousCenterYKey	@"previouscentery"
#define kWidthKey			@"width"
#define kHeightKey			@"height"
#define kPreviousWidthKey	@"previouswidth"
#define kPreviousHeightKey	@"previousheight"
#define kScaleKey			@"scale"
#define kFilePathKey		@"filepath"
#define kIconIndexKey		@"iconindex"
#define kStyleKey			@"style"
#define kIndexKey			@"index"
#define kDesktopIndexKey	@"desktopIndex"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	LOG_FN();

	[encoder encodeFloat:_screenPosition.x forKey:kCenterXKey];
	[encoder encodeFloat:_screenPosition.y forKey:kCenterYKey];

	[encoder encodeFloat:_previousScreenPosition.x forKey:kPreviousCenterXKey];
	[encoder encodeFloat:_previousScreenPosition.y forKey:kPreviousCenterYKey];

	[encoder encodeInt:[self zPosition] forKey:kZPositionKey];

	[encoder encodeFloat:_size.width  forKey:kWidthKey];
	[encoder encodeFloat:_size.height forKey:kHeightKey];

	[encoder encodeFloat:_previousSize.width  forKey:kPreviousWidthKey];
	[encoder encodeFloat:_previousSize.height forKey:kPreviousHeightKey];

	[encoder encodeFloat:_scale forKey:kScaleKey];

	[encoder encodeObject:[self imageFilePath] forKey:kFilePathKey];
	[encoder encodeInt:[self iconIndex] forKey:kIconIndexKey];

//	[encoder encodeInt:_index        forKey:kIndexKey];
	[encoder encodeInt:_desktopIndex forKey:kDesktopIndexKey];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	LOG_FN();
	self = [super init];

	if (self) {

		float x    = [decoder decodeFloatForKey:kCenterXKey];
		float y    = [decoder decodeFloatForKey:kCenterYKey];
		[self setScreenPosition:CGPointMake(x, y)];

		x = [decoder decodeFloatForKey:kPreviousCenterXKey];
		y = [decoder decodeFloatForKey:kPreviousCenterYKey];
		[self setPreviousScreenPosition:CGPointMake(x, y)];

		[self setZPosition:[decoder decodeIntForKey:kZPositionKey]];

		float width  = [decoder decodeFloatForKey:kWidthKey];
		float height = [decoder decodeFloatForKey:kHeightKey];
		[self setSize:CGSizeMake(width, height)];

		width  = [decoder decodeFloatForKey:kPreviousWidthKey];
		height = [decoder decodeFloatForKey:kPreviousHeightKey];
		[self setPreviousSize:CGSizeMake(width, height)];

		CGFloat scale = [decoder decodeFloatForKey:kScaleKey];
		[self setScale:scale];

		NSString* p = [decoder decodeObjectForKey:kFilePathKey];
//		DT_DEBUG_LOG(@"FPath:%@",p);
		if (! [FileHelper doesFileExist:p]) {
			p = [FileHelper getPathForFileInDocuments:[p lastPathComponent]];
		}
		_imageFilePath = p;

		_iconIndex = [decoder decodeIntForKey:kIconIndexKey];

//		_index		  = [decoder decodeIntForKey:kIndexKey];
		_desktopIndex = [decoder decodeIntForKey:kDesktopIndexKey];

		DTWidgetStyle* style = [decoder decodeObjectForKey:kStyleKey];
		if (style) {
			[self setStyle:style];
		}
		else {
			[self setStyle:[[DTWidgetStyle alloc] init]];
		}

		_isDirty = NO;
	}
	return self;
}

//-----------------------------------------------------------------
- (BOOL) updateDatabaseFromWidget:(FMDatabase*)database
{
	BOOL wasDirty = _isDirty;

	if (_isDirty) {
		LOG_FN();
		DT_DEBUG_LOG(@"widget %d dirty..", [self index]);

		// Mark it as not dirty before saving it to the database
		// Otherwise it always comes in "dirty"
		_isDirty = NO;

		// Create a binary copy of the widget
		NSData* data = [NSKeyedArchiver archivedDataWithRootObject:self];

		if ([self index]) {
			DT_DEBUG_LOG(@"UPDATE WIDGET %d, %d", [self index], [self desktopIndex]);
			[database executeUpdate:
				@"UPDATE widgets SET widgetData=?,desktopID=? WHERE widgetID=?",
				data,
				[NSNumber numberWithInt:[self desktopIndex]],
				[NSNumber numberWithInt:[self index]]
			];
		}
		else {
			[database executeUpdate:
				@"INSERT INTO widgets (widgetData, desktopID) VALUES(?,?)",
				data,
				[NSNumber numberWithInt:[self desktopIndex]]
			];
			[self setIndex:(int)[database lastInsertRowId]];
			DT_DEBUG_LOG(@"INSERT WIDGET %d, %d", [self index], [self desktopIndex]);
		}
	}
	return wasDirty;
}

//-----------------------------------------------------------------
- (BOOL) updateDatabaseFromWidget
{
	return [self updateDatabaseFromWidget:[UIApplication myDatabase]];
}

//-----------------------------------------------------------------
-(void) handleDoubleTap:(UIViewController*) controller
{
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(UIViewController*) controller
{
	return YES;
}

//-----------------------------------------------------------------
- (void) showCreationSettings:(UIViewController*)parentViewController fromSender:(id)sender
{
	[self showSettingsModal:parentViewController fromSender:sender];
}

//-----------------------------------------------------------------
- (void) createSettingsMenuWithRoot:(QRootElement*)pRoot withViewController:(UIViewController*)parentViewController
{
}

//-----------------------------------------------------------------
- (void) saveSettingsFromMenu:(QRootElement*)optionsRoot
{
}

//-----------------------------------------------------------------
//- (void) showActionMenu:(UIViewController*)parentViewController
- (void) actionButtonAction:(UIBarButtonItem*)button
{
}

//-----------------------------------------------------------------
- (void) hasUpdatedImage
{
#if 0
//	LOG_FN();
	if (activeMenuRoot) {
		QImageElement* element = (QImageElement*)[activeMenuRoot elementWithKey:@"thumbnail"];

		if (element) {
//			LOG_FN();
			[element setImage:[self image]];
		}
	}
#endif
}

//-----------------------------------------------------------------
- (void) cancelButtonPressed
{
//	LOG_FN();
	[[UIApplication myPresentedViewController] dismissViewControllerAnimated:YES completion:nil];

	activeWidget		 = nil;
	activeMenuRoot		 = nil;
	activeMenuController = nil;

	[DTWidgetQuickDialogController setGlobalStyleProvider:nil];
}

//-----------------------------------------------------------------
- (void) doneButtonPressed
{
//	LOG_FN();
	[self saveSettingsFromMenu:activeMenuRoot];
	[self cancelButtonPressed];
}

//-----------------------------------------------------------------
- (void) cell:(UITableViewCell*)cell willAppearForElement:(QElement*)element atIndexPath:(NSIndexPath*)indexPath
{
//	LOG_FN();
//    cell.backgroundColor = [UIColor colorWithRed:0.9582 green:0.9104 blue:0.2991 alpha:1.0000];
}


//-----------------------------------------------------------------
//- (void) showModalMenu:(QRootElement*)root onViewController:(UIViewController*)parentViewController
//{
//	LOG_FN();
//
//	activeWidget = self;
//
//	// Let the widget create the options menu structure
//	//	QRootElement* root = [[QRootElement alloc] init];
//	activeMenuRoot = root;
//
//	[root setPresentationMode:QPresentationModeNavigationInPopover];
//	// Get widget specific menus....
//	//	[self createSettingsMenuWithRoot:root withViewController:parentViewController];
//
//	// Create the enclosing navigation controller
//	UINavigationController* 		navigationController = [DTWidgetQuickDialogController controllerWithNavigationForRoot:root];
//	DTWidgetQuickDialogController*  viewController		 = (DTWidgetQuickDialogController*)[navigationController topViewController];
//	[viewController setStyleProvider:self];
//
//	activeMenuController = viewController;
//
//	// Customise
//	[navigationController style];
//	[navigationController addDoneButton:nil   forTarget:self];
//	[navigationController addCancelButton:nil forTarget:self];
//
//	[navigationController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
//
//	// display options menu - displays nav controller that displays everything else
//	QuickDialogController* p = (QuickDialogController*) [QuickDialogController controllerWithNavigationForRoot:root];
//
//
//	[p displayViewControllerInPopover:nil withNavigation:YES];
//
//#if 0
//	UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
//	[DTWidget setActiveController:popover];
//	[popover setDelegate:self];
//
//	CGRect screen = [UIApplication myMainScreenBounds];
//    CGSize size = CGSizeMake(320, 480); // size of view in popover
//
//	CGRect popupPosition = CGRectMake(screen.size.width / 2, 0, 1, 1);
///*
//	size.size.height -= 1;
//	size.size.width -= 80;
//	size.origin.x += 40;
//	size.origin.y += 40;
//*/
//	[popover setPopoverContentSize:CGSizeMake(size.size.width, size.size.height)];
//
//	[popover presentPopoverFromRect:popupPosition inView:[parentViewController view]
//		   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES
//	 ];
//
//#endif
///*
//	[parentViewController presentViewController:navigationController animated:YES completion:nil];
//*/
//}

//-----------------------------------------------------------------
- (void) showSettingsModal:(UIViewController*)parentViewController fromSender:(id)sender
{
	LOG_FN();

	if ([UIPopoverController dismissActiveController]) {
		return;
	}

	activeWidget = self;

	// Let the widget create the options menu structure
	QRootElement* root = [[QRootElement alloc] init];
	activeMenuRoot = root;

	[root setPresentationMode:QPresentationModeNavigationInPopover];
	[root setControllerName:@"DTWidgetQuickDialogController"];
	
	// Always add a thumbnail...
//	QSection* section = [root AddNewSection];
//	[QImageElement addThumbnailElementToSection:section forWidget:self];

	// Get widget specific menus....
	[self createSettingsMenuWithRoot:root withViewController:parentViewController];

//	+ (QuickDialogController *)controllerForRoot:(QRootElement *)root {

	if ([UIApplication isPad]) {

//		QuickDialogController* 		dialogController = [DTWidgetQuickDialogController controllerForRoot:root];

		UINavigationController* 		navigationController = [DTWidgetQuickDialogController controllerWithNavigationForRoot:root];
		DTWidgetQuickDialogController*  viewController		 = (DTWidgetQuickDialogController*)[navigationController topViewController];
		[viewController setStyleProvider:self];
		
		[DTWidgetQuickDialogController setGlobalStyleProvider:self];
		activeMenuController = viewController;
		
		// Customise
		[navigationController setPreferredContentSize:[UIImage getStandardPopoverSize]];

		UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
		[UIPopoverController setActiveController:popover];
		[popover setDelegate:self];
		[popover setPopoverContentSize:[UIImage getStandardPopoverSize]];

		[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
	else {
		// Create the enclosing navigation controller
		UINavigationController* 		navigationController = [DTWidgetQuickDialogController controllerWithNavigationForRoot:root];
		DTWidgetQuickDialogController*  viewController		 = (DTWidgetQuickDialogController*)[navigationController topViewController];
		[viewController setStyleProvider:self];

		[DTWidgetQuickDialogController setGlobalStyleProvider:self];
		activeMenuController = viewController;

		// Customise
		[navigationController style];
		[navigationController addDoneButton:@selector(doneButtonPressed)   		forTarget:self];
		[navigationController addCancelButton:@selector(cancelButtonPressed) 	forTarget:self];

		[navigationController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];

		// display options menu - displays nav controller that displays everything else
		[parentViewController presentViewController:navigationController animated:YES completion:nil];
	}
}

@end

//-----------------------------------------------------------------

