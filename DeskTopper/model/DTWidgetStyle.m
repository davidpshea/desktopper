//
//  DTWidgetStyle.m
//  DeskTopper
//
//  Created by David Shea on 27/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidgetStyle.h"

@implementation DTWidgetStyle

@synthesize borderWidth		= _borderWidth;
@synthesize shadowSize		= _shadowSize;

//-----------------------------------------------------------------
- (DTWidgetStyle*) init
{
	self = [super init];
	if (self) {
		[self setBorderWidth:0.0];
		[self setShadowSize:12.0];
	}
	return self;
}

//-----------------------------------------------------------------
- (id) copyWithZone:(NSZone*)zone
{
	DTWidgetStyle* another = [[DTWidgetStyle alloc] init];

	[another setBorderWidth:[self borderWidth]];
	[another setShadowSize:	[self shadowSize]];

	return another;
}

//-----------------------------------------------------------------
#define kBorderWidth		@"borderwidth"
#define kShadowSize			@"shadowsize"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	[encoder encodeFloat:_borderWidth forKey:kBorderWidth];
	[encoder encodeFloat:_shadowSize  forKey:kShadowSize];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	LOG_FN();
	self = [super init];

	if (self) {
		_borderWidth	= [decoder decodeFloatForKey:kBorderWidth];
		_shadowSize		= [decoder decodeFloatForKey:kShadowSize];
	}
	return self;
}

//-----------------------------------------------------------------
@end

