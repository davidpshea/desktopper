//
//  DTDesktopController.m
//  DeskTopper
//
//  Created by David Shea on 12/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FMDatabase.h"
#import "DTDesktopController.h"
#import "UIApplication+Helpers.h"
#import "FMDatabase+Variables.h"

//-----------------------------------------------------------------
@implementation DTDesktopController

@synthesize	desktops 			= _desktops;
@synthesize	currentDesktopIndex	= _currentDesktopIndex;
@synthesize	index				= _index;
@synthesize	database			= _database;

//-----------------------------------------------------------------
- (DTDesktopController*) init
{
	LOG_FN();
	self = [super init];

	return self;
}

//-----------------------------------------------------------------
- (NSMutableArray*) desktops
{
	if (! _desktops) {
		_desktops = [[NSMutableArray alloc] init];
	}
	return _desktops;
}

//-----------------------------------------------------------------
- (void) addDesktop:(DTDesktop*)desktop
{
	[[self desktops] addObject:desktop];
}

//-----------------------------------------------------------------
- (void) removeDesktop:(DTDesktop*)desktop
{
	[[self desktops] removeObject:desktop];
}

//-----------------------------------------------------------------
- (DTDesktop*) getDesktopFromIndex:(int)index
{
	if ((index < 0) || (index >= [[self desktops] count])) {
		return nil;
	}

	DTDesktop* newDesktop = [[self desktops] objectAtIndex:index];

	// Extract and create widgets for the desktop we are moving to
	// We only do this the first time the desktop is accessed - after that, it's
	// held in memory, and written straight to the database when changes occur
	if (! [newDesktop hasLoadedFromDatabase]) {

		//TODO		DT_ASSERT(![[self database] isExecutingStatement]);
		FMResultSet* widgetResults = [
			[self database]
			executeQuery:@"SELECT widgetID,widgetData FROM widgets WHERE desktopID=? ORDER BY widgetID",
			[NSNumber numberWithInt:index]
		];
		//TODO		DT_ASSERT(![[self database] isExecutingStatement]);

		while ([widgetResults next]) {
			LOG_FN();
			int widgetIndex	 = [widgetResults intForColumn:@"widgetID"];
			NSData* data	 = [widgetResults dataForColumn:@"widgetData"];

            DTWidget* widget = nil;

            @try
            {
                widget = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            }
            @catch (NSException *exception)
            {
                NSLog(@"Failed to create widget on desktop %d, index %d", index, widgetIndex);
            }
            @finally
            {
            }

            if (widget != nil)
            {
                [widget setIndex:widgetIndex];
                [newDesktop addWidget:widget];
                NSLog(@"created widget. desktop %d, index %d", index, widgetIndex);
            }
		}

		[newDesktop setHasLoadedFromDatabase:YES];

		// Make sure the widgets are in the correct order for drawing
		[newDesktop orderWidgetsByZ];
	}

	return newDesktop;
}

//-----------------------------------------------------------------
- (DTDesktop*) getCurrentDesktop
{
	return [self getDesktopFromIndex:_currentDesktopIndex];
}

//-----------------------------------------------------------------
- (int) getDesktopCount
{
	return (int)[[self desktops] count];
}

//-----------------------------------------------------------------
- (void) setCurrentDesktopIndex:(int)desktopIndex
{
	LOG_FN();
	if (_currentDesktopIndex != desktopIndex) {

		DTDesktop* newDesktop = [self getDesktopFromIndex:desktopIndex];

		if (newDesktop) {
			_currentDesktopIndex = desktopIndex;
			[[self database] setIntVariable:@"currentDesktopIndex" withValue:desktopIndex];
		}
	}
}

//-----------------------------------------------------------------
- (void) updateDatabaseFromDesktopController:(FMDatabase*)database
{
	for (DTDesktop* desktop in [self desktops]) {
		[desktop updateDatabaseFromDesktop:database];
	}

	[database setIntVariable:@"currentDesktopIndex" withValue:_currentDesktopIndex];
}

//-----------------------------------------------------------------
+ (DTDesktopController*) createDesktopControllerFromDatabase:(FMDatabase*)database
{
	LOG_FN();
	DTDesktopController* controller = [[DTDesktopController alloc] init];
	[controller setDatabase:database];

	// Extract and create desktops
	//TODO	DT_ASSERT(![database isExecutingStatement]);
	FMResultSet* results = [database executeQuery:@"SELECT desktopID,snapshot FROM desktops ORDER BY desktopID"];
	//TODO	DT_ASSERT(![database isExecutingStatement]);

	while ([results next]) {
		int index			= [results intForColumn:@"desktopID"];
		NSData* data		= [results dataForColumn:@"snapshot"];
		UIImage* snapshot	= (UIImage*) [NSKeyedUnarchiver unarchiveObjectWithData:data];

		DTDesktop* desktop = [[DTDesktop alloc] init];
		[desktop setSnapshot:snapshot];
		// Needed since setsnapshot sets this - but it doesn't make sense at this stage
		[desktop setThumbnailIsDirty:NO];
		[desktop setIndex:index];
		[controller addDesktop:desktop];
	}

	// If no desktops loaded, create some blank ones
	if ([controller getDesktopCount] == 0) {
		for (int i = 0 ; i < 12; i++) {
			DTDesktop* desktop = [[DTDesktop alloc] init];
			[desktop setIndex:i];
			[desktop setIsDirty:YES];
			[controller addDesktop:desktop];

			//TODO			DT_ASSERT(![database isExecutingStatement]);
			[database executeUpdate:@"INSERT OR REPLACE INTO desktops (desktopID, snapshot, background) VALUES(?,?, ?)",
				 [NSNumber numberWithInt:i],
				 nil,
				 nil
			 ];
			//TODO			DT_ASSERT(![database isExecutingStatement]);
			[desktop updateDatabaseFromDesktop:database];
		}
	}

	[controller setCurrentDesktopIndex:[database getIntVariable:@"currentDesktopIndex" withDefault:DTDesktopIndexUser]];

	return controller;
}

//-----------------------------------------------------------------
@end

