//
//  Widget.h
//  2ndProject
//
//  Created by David Shea on 1/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "FileHelper.h"
#import "FMDatabase.h"

@class QRootElement;
@class QuickDialogController;
@class DTViewController;
@class DTWidgetImageView;
@class DTWidgetStyle;
@class FMDatabase;
@class DTWidgetQuickDialogController;

//-----------------------------------------------------------------
/*
  @protocol DTWidgetDeligate <NSObject>

@optional
- (void) hasUpdatedImage:(DTWidget*)widget;

@end
*/
//-----------------------------------------------------------------
@interface DTWidget: NSObject <
	NSCoding,
	NSCopying,
//	QuickDialogStyleProvider,
	UIPopoverControllerDelegate
>
@property (nonatomic, strong)	UIImage*			image;
@property (nonatomic)			int					zPosition;
//@property (nonatomic)			int					page;
@property (nonatomic)			CGPoint				screenPosition;
@property (nonatomic)			CGPoint				previousScreenPosition;
@property (nonatomic)			CGFloat				scale;
@property (nonatomic)			CGSize				size;
@property (nonatomic)			CGSize				previousSize;
@property (nonatomic, weak)		DTWidgetImageView*	view;
@property (nonatomic, strong)	NSString*			imageFilePath;
@property (nonatomic)			int					iconIndex;
@property (nonatomic)			BOOL				keepAspectRatio;
@property (nonatomic, strong)	DTWidgetStyle*		style;
@property (nonatomic, weak)		NSOperation*		loadingOperation;
@property (nonatomic)			int					index;
@property (nonatomic)			int					desktopIndex;
@property (nonatomic)			BOOL				isProcessing;
@property (nonatomic)			BOOL				isDirty;

+ (NSArray*)	getWidgetClassNameArray;
+ (DTWidget*)	createWidgetFromClass:(Class)class;
+ (NSString*)	getIconName;
+ (NSString*)	getUserName;

// class functions for dealing with the active (settings) menu
+ (DTWidget*)				getActiveWidget;
+ (QRootElement*)			getActiveMenuRoot;
+ (void)					activeMenuUpdateQMultilineElement:(NSString*)string withKey:(NSString*)key;
+ (void)					activeMenuReloadData;

+ (DTWidgetQuickDialogController*)	getActiveMenuController;

- (CGRect)		getBounds;
- (void)		setImageFilePath:			(NSString*)newPath withImage:(UIImage*)image;
- (void)		setImageFilePathNoUpdate:	(NSString*)newPath;
- (BOOL)		handleSingleTap:			(UIViewController*)controller;
- (void)		handleDoubleTap:			(UIViewController*)controller;
- (void)		createSettingsMenuWithRoot:	(QRootElement*)root withViewController:(UIViewController*)parentViewController;
- (void)		showCreationSettings:		(UIViewController*)parentViewController fromSender:(id)sender;
- (void)		showSettingsModal:			(UIViewController*)parentViewController fromSender:(id)sender;
- (void)		saveSettingsFromMenu:		(QRootElement*)optionsRoot;
//- (void)		showModalMenu:				(QRootElement*)root onViewController:(UIViewController*)parentViewController;
- (void)		actionButtonAction:			(UIBarButtonItem*)button;
- (void)		hasFinishedResizing:		(CGSize)size;

- (void)		setHighResolutionImage:				(UIImage*)image;
- (void)		setHighResolutionImageBackground:	(UIImage*)image;
- (NSString*)	getHighResolutionImageFilePath;
- (UIImage*)	getHighResolutionImage;
- (void)		hasUpdatedImage;

- (void)		destroy:(BOOL)animated;
- (id)			copyWithZone:(NSZone*)zone;

- (BOOL)		updateDatabaseFromWidget:(FMDatabase*)database;
- (BOOL)		updateDatabaseFromWidget;
/*
- (void)		enableForViewing;
- (void)		disableForViewing;
*/

@end
//-----------------------------------------------------------------

