//
//  DTOptions.h
//  DeskTopper
//
//  Created by David Shea on 1/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QuickDialogHelper.h"

@class QRootElement;
@class DTPopupTextController;

//-----------------------------------------------------------------
@interface DTShortcut : NSObject

@property (nonatomic)			int 		index;
@property (nonatomic)			BOOL 		isDirty;
@property (nonatomic, strong)	NSString*	label;
@property (nonatomic, strong)	NSString*	text;

- (DTShortcut*) initWithLabel:(NSString*)label index:(int)index andShortcut:(NSString*)text;
- (void)		updateDatabaseFromShortcut:	(FMDatabase*)database withTableName:(NSString*)databaseTableName;

@end

//-----------------------------------------------------------------
@interface DTOptionArray : NSObject

@property (nonatomic, strong)	NSString*				databaseTableName;
@property (nonatomic, strong)	NSMutableArray*			array;
@property (nonatomic, strong)	FMDatabase*				database;
@property (nonatomic, strong)	DTPopupTextController*	popupTextController;

- (NSMutableArray*) asArray;
- (void)			updateDatabase;
- (int)				count;
- (void)			addObject:(DTShortcut*)object;
- (DTShortcut*)		getShortcutFromIndex:(int)index;

@end

//-----------------------------------------------------------------
@interface DTOptions : NSObject <NSCoding>

@property (nonatomic)			BOOL			constrainSize;
@property (nonatomic)			BOOL 			alignIcons;
@property (nonatomic, strong)	QRootElement*	menuRoot;
@property (nonatomic, strong)	DTOptionArray*	shortcuts;
@property (nonatomic, strong)	DTOptionArray*	searchEngines;
@property (nonatomic, strong)	DTOptionArray*	styleSheets;

- (QRootElement*)		createOptionsMenu;
- (void)				saveSettingsFromMenu;
/*- (DTOptionArray*) 		shortcuts;
- (DTOptionArray*) 		searchEngines;
- (DTOptionArray*) 		styleSheets;
*/
//-----------------------------------------------------------------
@end
