//
//  DTWidgetStyle.h
//  DeskTopper
//
//  Created by David Shea on 27/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DTWidgetStyle : NSObject <NSCoding, NSCopying>

@property (nonatomic)	float	borderWidth;
@property (nonatomic)	float	shadowSize;

- (id)			copyWithZone:(NSZone*)zone;


@end
