//
//  Desktop.m
//  2ndProject
//
//  Created by David Shea on 1/03/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "DTDesktop.h"
#import "UIImageView+ImageViewBorder.h"
#import "FMDatabase.h"
#import "UIImageExtras.h"
#import "UIApplication+Helpers.h"
#import "macros.h"

//-----------------------------------------------------------------
//static const int InitialSizeOfWidgetArray = 40;

@implementation DTDesktop

//-----------------------------------------------------------------
@synthesize isEditing				= _isEditing;
@synthesize selectedWidget			= _selectedWidget;
@synthesize widgets					= _widgets;
@synthesize snapshot				= _snapshot;
@synthesize backgroundImage			= _backgroundImage;
@synthesize	index					= _index;
@synthesize hasLoadedFromDatabase	= _hasLoadedFromDatabase;
@synthesize isDirty					= _isDirty;
@synthesize thumbnailIsDirty		= _thumbnailIsDirty;
@synthesize backgroundImageActive	= _backgroundImageActive;


//-----------------------------------------------------------------
- (void) setThumbnailIsDirty:(BOOL)isDirty
{
	_thumbnailIsDirty = isDirty;
}

//-----------------------------------------------------------------
- (void) setIsDirty:(BOOL)isDirty
{
	_isDirty = isDirty;
}

//-----------------------------------------------------------------
- (id) init
{
	LOG_FN();
	self = [super init];
	return self;
}

//-----------------------------------------------------------------
- (UIImage*) snapshot
{
	if (!_snapshot) {
		CGSize size	= CGSizeMake(86.0, 86.0 * 3.0 / 2.0);
		_snapshot = [UIImage createColouredRectangle:size colour:[UIColor blackColor]];
	}
	return _snapshot;
}

//-----------------------------------------------------------------
- (void) setSnapshot:(UIImage*)snapshot
{
	_snapshot		  = snapshot;
	_thumbnailIsDirty = YES;
}

//-----------------------------------------------------------------
- (void) setBackgroundImage:(UIImage*)backgroundImage
{
	_backgroundImage = backgroundImage;

	NSData* data = [NSKeyedArchiver archivedDataWithRootObject:backgroundImage];
	
	[[UIApplication myDatabase] executeUpdate:@"UPDATE desktops SET background=? WHERE desktopID=?",
		data,
		[NSNumber numberWithInt:[self index]]
	 ];
}

//-----------------------------------------------------------------
- (void) clearBackgroundImage
{
	_backgroundImage = nil;
}

//-----------------------------------------------------------------
- (UIImage*) backgroundImage
{
	if (! _backgroundImage) {

		FMDatabase* database = [UIApplication myDatabase];

		//TODO		DT_ASSERT(![database isExecutingStatement]);
		FMResultSet* results = [database
			executeQuery:@"SELECT background FROM desktops WHERE desktopID=?",
			[NSNumber numberWithInt:[self index]]
		];
		//TODO		DT_ASSERT(![database isExecutingStatement]);

		if (!results) {
			DT_DEBUG_LOG(@"Background image - no results: %@", [database lastErrorMessage]);
		}

		while ([results next]) {
			NSData* data		= [results dataForColumn:@"background"];
			_backgroundImage	= (UIImage*) [NSKeyedUnarchiver unarchiveObjectWithData:data];
		}
	}
	return _backgroundImage;
}

//-----------------------------------------------------------------
- (NSMutableArray*) widgets
{
	if (! _widgets) {
		_widgets = [[NSMutableArray alloc] init];
	}
	return _widgets;
}

//-----------------------------------------------------------------
- (void) addWidget:(id)widget
{
	[[self widgets] addObject:widget];
	[widget setDesktopIndex:[self index]];
}

//-----------------------------------------------------------------
- (void) removeWidget:(id)widget
{
	[[self widgets] removeObject:widget];
	[widget setDesktopIndex:-1];
}

//-----------------------------------------------------------------
- (id) getWidgetFromIndex:(int)index
{
	NSUInteger c = [[self widgets] count];
	if (index >= c) {
		return nil;
	}
	return [[self widgets] objectAtIndex:index];
}

//-----------------------------------------------------------------
- (int) getWidgetCount
{
	return (int)[[self widgets] count];
}

//-----------------------------------------------------------------
- (NSString*) getName
{
	NSString* name = [NSString stringWithFormat:NSLocalizedString(@"spaceGenericName", nil), [self index] - 2];
	return name;
}

//-----------------------------------------------------------------
- (void) selectWidgetForEdit:(id)widget
{
	if ([self selectedWidget]) {
		[self setIsEditing:NO];
		[self setSelectedWidget:nil];
	}

	if (widget) {
		[self setIsEditing:YES];
		[self setSelectedWidget:widget];
	}
}

//-----------------------------------------------------------------
- (void) orderWidgetsByZ
{
	[[self widgets] sortUsingComparator: (NSComparator)^(id obj1, id obj2) {

		DTWidget* widget1 = (DTWidget*) obj1;
		DTWidget* widget2 = (DTWidget*) obj2;

		if ([widget1 zPosition] > [widget2 zPosition]) {
			return (NSComparisonResult)NSOrderedDescending;
		}

		if ([widget1 zPosition] < [widget2 zPosition]) {
			return (NSComparisonResult)NSOrderedAscending;
		}
		return (NSComparisonResult)NSOrderedSame;
	}];
}

//-----------------------------------------------------------------
- (NSMutableArray*) getWidgetsOrderedBySize
{
	NSMutableArray* sortedWidgets = [[NSMutableArray alloc] initWithArray:[self widgets] copyItems:NO];

	[sortedWidgets sortUsingComparator: (NSComparator)^(id obj1, id obj2) {

		DTWidget* widget1 = (DTWidget*) obj1;
		DTWidget* widget2 = (DTWidget*) obj2;

		CGSize size1 = [widget1 size];
		CGSize size2 = [widget2 size];

		if (size1.width > size2.width) {
			return (NSComparisonResult)NSOrderedAscending;
		}

		if (size1.height > size2.height) {
			return (NSComparisonResult)NSOrderedAscending;
		}

		return (NSComparisonResult)NSOrderedDescending;
	}];

	return sortedWidgets;
}

//-----------------------------------------------------------------
- (BOOL) isAnyWidgetDirty
{
	for (DTWidget* widget in [self widgets]) {

		if ([widget isDirty]) {
			return YES;
		}
	}
	return NO;
}

//-----------------------------------------------------------------

#if 0
- (DTWidget*) findWidgetClosestToPoint:(CGPoint)point
{
	float currentDistance   = 1e6;
	DTWidget* closestWidget = nil;

	for (DTWidget* widget in [self widgets]) {

//		CGPoint widgetPosition	= [widget screenPosition];
		CGRect widgetRect		= [widget getBounds];

		// If point is directly over a widget, use that...
		if (CGRectContainsPoint(widgetRect, point)) {
			currentDistance = 0.0;
			closestWidget   = widget;
			break;
		}
#if 0
		// otherwise we're looking for a widget near the point
		float xDiff    = (point.x - widgetPosition.x);
		float yDiff    = (point.y - widgetPosition.y);
		float distance = sqrt((xDiff * xDiff) + (yDiff * yDiff));

		if (distance < currentDistance) {
			currentDistance = distance;
			closestWidget   = widget;
		}
#endif
	}

	return closestWidget;
}
#endif

//-----------------------------------------------------------------
- (void) updateDatabaseFromDesktop:(FMDatabase*)database
{
	LOG_FN();

	DT_DEBUG_LOG(@"desktop %d dirty..", [self index]);
	// Create a binary copy of the snapshot
	if (_thumbnailIsDirty) {
		NSData* snapshotData = [NSKeyedArchiver archivedDataWithRootObject:[self snapshot]];
		[
			database executeUpdate:@"UPDATE desktops SET snapshot=? WHERE desktopID=?",
			snapshotData,
			[NSNumber numberWithInt:[self index]]
		];
		_thumbnailIsDirty = NO;
	}

	if (_isDirty) {
		// Archive all widgets in this desktop - if they've changed (see dirty flag in widget)
		for (DTWidget* widget in [self widgets]) {
			[widget setDesktopIndex:[self index]];
			[widget updateDatabaseFromWidget:database];
		}

		_isDirty = NO;
	}
}

//-----------------------------------------------------------------
- (void) dealloc
{
	LOG_FN();
	[[self widgets] removeAllObjects];
}

//-----------------------------------------------------------------
@end

