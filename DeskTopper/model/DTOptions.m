//
//  DTOptions.m
//  DeskTopper
//
//  Created by David Shea on 1/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QWebElement.h"
#import "DTOptions.h"
#import "DTPopupTextController.h"
#import "UIApplication+Helpers.h"
#import "DTViewController.h"
#import "UIImageExtras.h"
#import "MBProgressHUD.h"
#import "SVProgressHUD.h"
#import "MBProgressHUD+Extras.h"
#import "DTDocumentSharing.h"
#import "DTHelpViewController.h"
#import "NSString+Markdown.h"

//-----------------------------------------------------------------
@interface DTLabelElement : QLabelElement

@property (nonatomic, strong)	DTShortcut*	shortcut;
@property (nonatomic, strong)	DTPopupTextController* popupTextController;

@end

//-----------------------------------------------------------------
@implementation DTLabelElement

//-----------------------------------------------------------------
- (void) selectedAccessory:(QuickDialogTableView*)tableView controller:(QuickDialogController*)controller indexPath:(NSIndexPath*)indexPath
{
    if ([self controllerAccessoryAction])
    {
		#pragma clang diagnostic push
		#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
		[self performSelector:NSSelectorFromString([self controllerAccessoryAction])];
		#pragma clang diagnostic pop
	}
}

//-----------------------------------------------------------------
- (void) selected:(QuickDialogTableView*)tableView controller:(QuickDialogController*)controller indexPath:(NSIndexPath*)indexPath
{
    if ([self controllerAction] && !controller.quickDialogTableView.editing)
    {
		#pragma clang diagnostic push
		#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:NSSelectorFromString([self controllerAction])];
		#pragma clang diagnostic pop
    }
}

//-----------------------------------------------------------------
- (void) popupTextControllerCancelButtonPressed
{
	[[UIApplication myPresentedViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) popupTextControllerSaveButtonPressed
{
	LOG_FN();
	UINavigationController* controller     = [UIApplication myPresentedViewController];
	QuickDialogController*  viewController = (QuickDialogController*)[controller topViewController];

	DTPopupTextController* editController = [self popupTextController];

	DTShortcut* shortcut = [self shortcut];

	if ([editController userObject]) {
		[shortcut setLabel:[editController text]];
		[self setTitle:[editController text]];
	}
	else {
		[shortcut setText:[editController text]];
		[self setValue:[editController text]];
	}

	[shortcut setIsDirty:YES];

	QuickDialogTableView* tableView = [viewController quickDialogTableView];

	[tableView reloadCellForElements:self, nil];

	[controller dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) editShortcutLabel
{
	DTShortcut* shortcut = [self shortcut];

	DTPopupTextController* editController = [[DTPopupTextController alloc] init];
	[editController setUserObject:self];
	[editController setText:[shortcut label]];
	[editController setTarget:self];
	[editController setEnabledKeyboards:(keyboardIDControl|keyboardIDURL)];
	[self setPopupTextController:editController];

	//	[editController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];

	[[UIApplication myPresentedViewController] presentViewController:editController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) editShortcutText
{
	DTShortcut* shortcut = [self shortcut];

	DTPopupTextController* editController = [[DTPopupTextController alloc] init];
	[editController setUserObject:nil];
	[editController setText:[shortcut text]];
	[editController setTarget:self];
	[self setPopupTextController:editController];

	//	[editController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];

	[[UIApplication myPresentedViewController] presentViewController:editController animated:YES completion:nil];
}

@end

//-----------------------------------------------------------------
@implementation DTShortcut

//-----------------------------------------------------------------
- (void) updateDatabaseFromShortcut:(FMDatabase*)database withTableName:(NSString*)databaseTableName
{
	if (_isDirty) {
		NSString* query = [NSString
			stringWithFormat:@"INSERT OR REPLACE INTO %@ (shortcutID,shortcutLabel,shortcutText) VALUES(?,?,?)",
			databaseTableName
		];

		//TODO		DT_ASSERT(![database isExecutingStatement]);
		[database executeUpdate:query,
			[NSNumber numberWithInt:[self index]],
			[self label],
			[self text]
		 ];
		//TODO		DT_ASSERT(![database isExecutingStatement]);

		_isDirty = NO;
	}
}

//-----------------------------------------------------------------
- (DTShortcut*) initWithLabel:(NSString*)label index:(int)index andShortcut:(NSString*)text
{
	if (self = [super init]) {
		[self setLabel:label];
		[self setText:text];
		[self setIndex:index];
		[self setIsDirty:YES];
	}

	return self;
}

//-----------------------------------------------------------------
- (DTShortcut*) initWithIndex:(int)index
{
	if (self = [super init])
	{
		[self setLabel:@""];
		[self setText:@""];
		[self setIndex:index];
		[self setIsDirty:YES];
	}

	return self;
}

//-----------------------------------------------------------------
@end


@implementation DTOptionArray

//-----------------------------------------------------------------
- (void) updateDatabase
{
	NSMutableArray* array	= [self array];
	FMDatabase* database	= [self database];

	for (DTShortcut* shortcut in array) {
		[shortcut updateDatabaseFromShortcut:database withTableName:[self databaseTableName]];
	}
}

//-----------------------------------------------------------------
- (int) count
{
	return (int)[[self array] count];
}

//-----------------------------------------------------------------
- (void) addObject:(DTShortcut*)object
{
	[[self array] addObject:object];
}

//-----------------------------------------------------------------
- (void) fillWithBlanksFromIndex:(int)index totalSize:(int)totalSize
{
	while (index < totalSize)
	{
		DTShortcut* shortcut = [[DTShortcut alloc] initWithIndex:index];
		[self addObject:shortcut];
		index++;
	}
}

//-----------------------------------------------------------------
- (DTShortcut*) getShortcutFromIndex:(int)index
{
	if ((index < 0) || (index >= [[self array] count])) {
		return nil;
	}
	return [[self array] objectAtIndex:index];
}

//-----------------------------------------------------------------
- (NSMutableDictionary*) asDictionary
{
	NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];

	for (DTShortcut* shortcut in [self array]) {
		[dictionary setValue:[shortcut text] forKey:[shortcut label]];
	}
	return dictionary;
}

//-----------------------------------------------------------------
- (NSMutableArray*) asArray
{
	NSMutableArray* array = [[NSMutableArray alloc] init];

	for (DTShortcut* shortcut in [self array]) {
		[array addObject:[shortcut label]];
	}
	return array;
}

//-----------------------------------------------------------------
- (NSMutableArray*) array
{
	if (! _array) {
		FMDatabase* database = [self database];

		_array = [[NSMutableArray alloc] init];

		NSString* query = [NSString
			stringWithFormat:@"SELECT shortcutID,shortcutLabel,shortcutText FROM %@ ORDER BY shortcutID",
			[self databaseTableName]
		];

		//TODO		DT_ASSERT(![database isExecutingStatement]);
		FMResultSet* results = [database executeQuery:query];
		//TODO		DT_ASSERT(![database isExecutingStatement]);

		while ([results next]) {
			int index	 		= [results intForColumn:@"shortcutID"];
			NSString* label		= [results stringForColumn:@"shortcutLabel"];
			NSString* text		= [results stringForColumn:@"shortcutText"];

			DTShortcut* shortcut = [[DTShortcut alloc] initWithLabel:label index:index andShortcut:text];
			[_array addObject:shortcut];
		}
	}

	return _array;
}

//-----------------------------------------------------------------
-(void) addToMenu:(QSection*)section
{
	for (DTShortcut* shortcut in [self array]) {

		int i = [shortcut index];
		DTLabelElement* labelEdit = [[DTLabelElement alloc] initWithTitle:[shortcut label] Value:[shortcut text]];
		[labelEdit setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
		[labelEdit setKey:[NSString stringWithFormat:@"l%d", i]];
		[labelEdit setShortcut:shortcut];
		[labelEdit setLabelingPolicy:QLabelingPolicyTrimValue];

		[labelEdit setControllerAction:@"editShortcutLabel"];
		[labelEdit setControllerAccessoryAction:@"editShortcutText"];
		[section addElement:labelEdit];
	}
}

@end

//-----------------------------------------------------------------
@implementation DTOptions

@synthesize		constrainSize	= _constrainSize;
@synthesize		alignIcons		= _alignIcons;
@synthesize		menuRoot		= _menuRoot;
@synthesize		shortcuts		= _shortcuts;
@synthesize		searchEngines	= _searchEngines;
@synthesize		styleSheets		= _styleSheets;

//-----------------------------------------------------------------
#define alignIconsKey		@"alignIcons"
#define constrainSizeKey	@"constrainSize"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	[encoder encodeBool:[self constrainSize]	forKey:constrainSizeKey];
	[encoder encodeBool:[self alignIcons]		forKey:alignIconsKey];
}

//-----------------------------------------------------------------
- (DTOptionArray*) shortcuts
{
	if (! _shortcuts)
	{
		_shortcuts = [[DTOptionArray alloc] init];
		[_shortcuts setDatabaseTableName:@"shortcuts"];
		[_shortcuts setDatabase:[UIApplication myDatabase]];

		int index = [_shortcuts count];
		if (index == 0)
		{
			[_shortcuts addObject:[[DTShortcut alloc] initWithLabel:@"TimeStamp" index:index++
				andShortcut:@"$datelong $timeshort"]];
		}

		[_shortcuts fillWithBlanksFromIndex:index totalSize:16];
		[_shortcuts updateDatabase];
	}

	return _shortcuts;
}

//-----------------------------------------------------------------
- (DTOptionArray*) searchEngines
{
	if (! _searchEngines)
	{
		_searchEngines = [[DTOptionArray alloc] init];
		[_searchEngines setDatabaseTableName:@"search"];
		[_searchEngines setDatabase:[UIApplication myDatabase]];

        int index = [_searchEngines count];
		if (index == 0)
		{
			[_searchEngines addObject:[[DTShortcut alloc] initWithLabel:@"g" index:index++
				andShortcut:@"https://www.google.com/search?q=$s"]];

			[_searchEngines addObject:[[DTShortcut alloc] initWithLabel:@"b" index:index++
				andShortcut:@"http://www.bing.com/?q=$s"]];

			[_searchEngines addObject:[[DTShortcut alloc] initWithLabel:@"y" index:index++
				andShortcut:@"http://search.yahoo.com/search?p=$s"]];

			[_searchEngines addObject:[[DTShortcut alloc] initWithLabel:@"i" index:index++
				andShortcut:@"http://m.imdb.com/find?q=$s"]];

			[_searchEngines addObject:[[DTShortcut alloc] initWithLabel:@"w" index:index++
				andShortcut:@"http://en.wikipedia.org/w/index.php?search=$s"]];
		}

		[_searchEngines fillWithBlanksFromIndex:index totalSize:16];
		[_searchEngines updateDatabase];
	}

	return _searchEngines;
}

//-----------------------------------------------------------------
- (DTOptionArray*) styleSheets
{
	if (! _styleSheets)
    {
		_styleSheets = [[DTOptionArray alloc] init];
		[_styleSheets setDatabaseTableName:@"styles"];
		[_styleSheets setDatabase:[UIApplication myDatabase]];

        int index = [_styleSheets count];
		if (index == 0)
		{
			[_styleSheets addObject:[[DTShortcut alloc] initWithLabel:@"none" index:index++
				andShortcut:[NSString stringWithContentsOfFile:[FileHelper getCSSFileNamed:@"none"] encoding:NSUTF8StringEncoding error:nil]] ];

			[_styleSheets addObject:[[DTShortcut alloc] initWithLabel:@"corporate" index:index++
				andShortcut:[NSString stringWithContentsOfFile:[FileHelper getCSSFileNamed:@"corporate"] encoding:NSUTF8StringEncoding error:nil]] ];

			[_styleSheets addObject:[[DTShortcut alloc] initWithLabel:@"tidy" index:index++
				andShortcut:[NSString stringWithContentsOfFile:[FileHelper getCSSFileNamed:@"tidy"] encoding:NSUTF8StringEncoding error:nil]] ];

			[_styleSheets addObject:[[DTShortcut alloc] initWithLabel:@"note" index:index++
				andShortcut:[NSString stringWithContentsOfFile:[FileHelper getCSSFileNamed:@"note"] encoding:NSUTF8StringEncoding error:nil]] ];

			[_styleSheets addObject:[[DTShortcut alloc] initWithLabel:@"list" index:index++
				andShortcut:[NSString stringWithContentsOfFile:[FileHelper getCSSFileNamed:@"list"] encoding:NSUTF8StringEncoding error:nil]] ];

			[_styleSheets addObject:[[DTShortcut alloc] initWithLabel:@"funky" index:index++
				andShortcut:[NSString stringWithContentsOfFile:[FileHelper getCSSFileNamed:@"funky"] encoding:NSUTF8StringEncoding error:nil]] ];
		}

		[_styleSheets fillWithBlanksFromIndex:index totalSize:8];
		[_styleSheets updateDatabase];
	}

	return _styleSheets;
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super init];

	if (self) {
		//[self setConstrainSize:[decoder decodeBoolForKey:constrainSizeKey]];
		//		[self setAlignIcons:[decoder    decodeBoolForKey:alignIconsKey]];
	}
	return self;
}

//-----------------------------------------------------------------
/*
 - (QRootElement*) createSupportSection
{
	QRootElement* root	= [[QRootElement alloc] init];
	root.title			= @"Get support";
	root.grouped		= YES;

	QSection* supportSection = [root addNewSection];
	QLabelElement* helpLabel = [[QLabelElement alloc] initWithTitle:@"About" Value:@""];
	[supportSection addElement:helpLabel];

	return root;
}
*/
//-----------------------------------------------------------------
- (QElement*) createAboutSection
{
	NSString* html = NSLocalizedString(@"helpCSS", nil);
	html = [html stringByAppendingString:[NSLocalizedString(@"aboutApp", nil) createHTMLFromMarkdown]];
	QWebElement* helpText = [[QWebElement alloc] initWithTitle:NSLocalizedString(@"About", nil) HTML:html];

	return helpText;
}

//-----------------------------------------------------------------
/*
 - (QRootElement*) createHelpSection
{
	QRootElement* root	= [[QRootElement alloc] init];
	root.title			= @"Help";
	root.grouped		= YES;

	QSection* helpSection = [root addNewSection];
	QTextElement* text = [[QTextElement alloc] initWithText:@"help is on it's way"];
	[helpSection addElement:text];

	return root;
}
*/

//-----------------------------------------------------------------
/*
 - (QRootElement*) createAppOptionsSection
{
	QRootElement* root	= [[QRootElement alloc] init];
	root.title			= @"App Options";
	[root setGrouped:YES];

	QSection* helpSection = [root addNewSection];
	QLabelElement* helpLabel = [[QLabelElement alloc] initWithTitle:@"Help" Value:@""];
	[helpSection addElement:helpLabel];

	return root;
}
*/
//-----------------------------------------------------------------
- (void) handleBackgroundImage
{
	[[UIApplication myRootViewController] showBackgroundImageSelector:nil];
}

//-----------------------------------------------------------------
- (void) refreshButton:(QButtonElement*)button
{
	UINavigationController* controller     = [UIApplication myPresentedViewController];
	QuickDialogController*  viewController = (QuickDialogController*)[controller topViewController];
	QuickDialogTableView*   tableView      = [viewController quickDialogTableView];

	[tableView reloadCellForElements:button, nil];
}

//-----------------------------------------------------------------
- (void) updateImageButton:(QButtonElement*)button
{
	DTViewController* rootController = [UIApplication myRootViewController];
	DTDesktop*        desktop        = [rootController desktop];

	if ([desktop backgroundImage]) {
		[button setEnabled:YES];
		[button setTitle:@"Clear Image"];
	}
	else {
		[button setEnabled:NO];
		[button setTitle:@"No Image"];
	}
}

//-----------------------------------------------------------------
- (QSection*) createDesktopOptionsSection
{
	QSection* desktopOptionsSection = [[QSection alloc] initWithTitle:@"This Space"];

	//-----------------
	// Button to handle background image selection
	QButtonElement* backgroundImage = [[QButtonElement alloc] initWithTitle:@"Set Background Image"];

	//-----------------
	// Button to handle resetting the image to nothing
	QButtonElement* resetButton = [[QButtonElement alloc] initWithTitle:@"Clear Image"];
	__weak QButtonElement* weakResetButton = resetButton;

	// Initial value of botton - enabled or not?
	[self updateImageButton:weakResetButton];

	backgroundImage.onSelected = ^{
		[self handleBackgroundImage];
		// Assume we have an image (though the handle.. returns before the image is ready)
		[weakResetButton setEnabled:YES];
		[weakResetButton setTitle:@"Clear Image"];
		[self refreshButton:weakResetButton];
	};
	[desktopOptionsSection addElement:backgroundImage];

	resetButton.onSelected = ^{
		// Kill any existing image...
		DTViewController* controller = [UIApplication myRootViewController];
		DTDesktop* desktop = [controller desktop];
		[controller updateDesktopBackgroundImage:nil];
		[desktop setBackgroundImage:nil];

		// Let user know...
		[MBProgressHUD showDoneOnView:[[UIApplication myPresentedViewController] view]];

		// Update the button status
		[self updateImageButton:weakResetButton];
		[self refreshButton:weakResetButton];

	};
	[desktopOptionsSection addElement:resetButton];

	//	return root;
	return desktopOptionsSection;
}

//-----------------------------------------------------------------
- (void) addHelp:(NSString*)helpLabel forRoot:(QRootElement*)root
{
	QSection* helpSection = [root addNewSection];
	NSString* html = NSLocalizedString(@"helpCSS", nil);

	html = [html stringByAppendingString:[NSLocalizedString(helpLabel, nil) createHTMLFromMarkdown]];
	QWebElement* helpText = [[QWebElement alloc] initWithTitle:NSLocalizedString(@"help", nil) HTML:html];

	[helpSection addElement:helpText];
}

//-----------------------------------------------------------------
- (QWebElement*) createMarkdownHelp
{
	//	QSection* helpSection = [root addNewSection];
	//	NSString* html = NSLocalizedString(@"helpCSS", nil);

	//	html = [html stringByAppendingString:[NSLocalizedString(helpLabel, nil) createHTMLFromMarkdown]];

	QWebElement* helpText = [[QWebElement alloc]
		initWithTitle:NSLocalizedString(@"markdownHelp", nil)
		url:@"http://daringfireball.net/projects/markdown/basics"
	];

	return helpText;
}

//-----------------------------------------------------------------
- (QRootElement*) createShortcutOptionsSection
{
	QRootElement* root	= [QRootElement rootWithTitle:@"Shortcuts"];

	// The help text...
	[self addHelp:@"shortcutHelp" forRoot:root];

	QSection* shortcutSection = [root addNewSection];
	[shortcutSection setTitle:@"Shortcuts"];
	[[self shortcuts] addToMenu:shortcutSection];

	return root;
}

//-----------------------------------------------------------------
- (QRootElement*) createSearchEngineOptionsSection
{
	QRootElement* root	= [QRootElement rootWithTitle:@"Search Engines"];

	[self addHelp:@"searchEngineHelp" forRoot:root];

	QSection* searchSection = [root addNewSection];
	[searchSection setTitle:@"Internet Search Engines"];
	[[self searchEngines] addToMenu:searchSection];

	return root;
}

//-----------------------------------------------------------------
- (QRootElement*) createStyleSheetsOptionsSection
{
	QRootElement* root	= [QRootElement rootWithTitle:@"Style Sheets"];

	[self addHelp:@"styleHelp" forRoot:root];

	QSection* styleSection = [root addNewSection];
	[styleSection setTitle:@"Style Sheets"];
	[[self styleSheets] addToMenu:styleSection];

	return root;
}

//-----------------------------------------------------------------
- (QRootElement*) createBookMarkletOptionsSection
{
	QRootElement* root = [QRootElement rootWithTitle:@"Adding a Safari bookmark"];

	QSection* bookmarkSection = [root addNewSection];

	NSString* html = NSLocalizedString(@"bookmarkHelp", nil);
	QTextElement* text = [[QTextElement alloc] initWithText:html];
	[bookmarkSection addElement:text];

	//-----------------
	// Button to handle copying bookmark to clipboard
	QButtonElement* copyBookmark = [[QButtonElement alloc] initWithTitle:@"Copy Bookmark to clipboard"];

	copyBookmark.onSelected = ^{
		UIPasteboard* clipboard = [UIPasteboard generalPasteboard];
		[clipboard setString:@"javascript:window.location='totalnotes://http?'+location.href"];
		[MBProgressHUD showDoneOnView:[[UIApplication myPresentedViewController] view]];
	};
	[bookmarkSection addElement:copyBookmark];

	return root;
}

//-----------------------------------------------------------------
- (QRootElement*) createOptionsMenu
{
	// Let the widget create the options menu structure
	QRootElement* root = [QRootElement rootWithTitle:@"Settings"];
	[self setMenuRoot:root];

	//----------------
	[root addSection:[self createDesktopOptionsSection]];

	//----------------
	QSection* optionsSection = [root addNewSectionWithTitle:@"Options"];
	//	[optionsSection addElement:[self createAppOptionsSection]];
	[optionsSection addElement:[self createShortcutOptionsSection]];
	[optionsSection addElement:[self createStyleSheetsOptionsSection]];
	[optionsSection addElement:[self createSearchEngineOptionsSection]];

	//----------------
	QSection* helpSection = [root addNewSectionWithTitle:@"Help"];

	//----------------
	QButtonElement* help = [[QButtonElement alloc] initWithTitle:@"Help"];
	help.onSelected = ^{
		[DTHelpViewController showAllHelp:[UIApplication myPresentedViewController]];
	};
	[helpSection addElement:help];

	//----------------
	[helpSection addElement:[self createMarkdownHelp]];

	[helpSection addElement:[self createBookMarkletOptionsSection]];

	//----------------
	QSection* aboutSection = [root addNewSectionWithTitle:@"Support"];

	NSString* versionString = [NSString stringWithFormat:@"App version: %@", [UIApplication myAppVersion]];
	[aboutSection setFooter:versionString];

	//----------------
	#ifdef USE_TESTFLIGHT_FEEBACK
	QButtonElement* testFlightButton = [[QButtonElement alloc] initWithTitle:@"Feedback"];
	testFlightButton.onSelected = ^{
		[TestFlight openFeedbackView];
	};
	[aboutSection addElement:testFlightButton];
	#endif

	//----------------
	QButtonElement* emailMe = [[QButtonElement alloc] initWithTitle:@"Contact Us"];
	[emailMe setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
	
	emailMe.onSelected = ^{
		[DTDocumentSharing emailSupport:@"Support query for Total Notes" body:@""];
	};
	[aboutSection addElement:emailMe];

	//----------------
	QButtonElement* itunes = [[QButtonElement alloc] initWithTitle:NSLocalizedString(@"itunes", nil)];

	NSString* itunesLink = @"itms-apps://itunes.apple.com/us/app/total-notes/id598501523?ls=1&mt=8";

	itunes.onSelected = ^{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:itunesLink]];
	};

	[aboutSection addElement:itunes];

	//----------------
	[aboutSection addElement:[self createAboutSection]];

	return root;
}

//-----------------------------------------------------------------
- (void) saveSettings
{
	QRootElement* root = [self menuRoot];

	id element = [root elementWithKey:@"alignIcons"];
	[self setAlignIcons:[element boolValue]];

	[[self shortcuts]     updateDatabase];
	[[self searchEngines] updateDatabase];
	[[self styleSheets]   updateDatabase];

	[MBProgressHUD hideAllOnTopWindow];
	[[UIApplication myPresentedViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
#if 1
- (void) saveSettingsFromMenu
{
/*
	(void) [
			NSTimer
			scheduledTimerWithTimeInterval:0.005
			target:self
			selector:@selector(saveSettings)
			userInfo:nil
			repeats:NO
			];
*/
	(void) [MBProgressHUD showProgressWhilePerformingSelector:@selector(saveSettings)
				  onTarget:self usingData:nil onView:[UIApplication myWindow]];

}
#endif

//-----------------------------------------------------------------
@end

