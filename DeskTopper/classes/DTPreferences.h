//
//  DTPreferences.h
//  DeskTopper
//
//  Created by David Shea on 28/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#if 0
#import <Foundation/Foundation.h>

@interface DTPreferences : NSObject
{
	int		Country;
	bool	Enabled;
	float	Slider;
}

- (id) initFromUserDefaults;

@property int	Country;
@property bool	Enabled;
@property float	Slider;

@end

#endif
