//
//  macros.h
//  DeskTopper
//
//  Created by David Shea on 21/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef DeskTopper_macros_h
#define DeskTopper_macros_h

//-----------------------------------------------------------------
// Define to use testflight API
//#define USE_TESTFLIGHT

// Add a button in the settings that provides app feedback
#ifdef USE_TESTFLIGHT
#define USE_TESTFLIGHT_FEEBACK
#warning Using testflight

// Allow logging to go to testflight
#define NSLog TFLog

#define TESTFLIGHT_CHECKPOINT(x)	[TestFlight passCheckpoint:x];

#else
#define TESTFLIGHT_CHECKPOINT(x)

#endif

//-----------------------------------------------------------------
// Define to force help pages to appear every time the app starts
//#define DEBUG_HELP

//-----------------------------------------------------------------
// Define to enable detailed SQL logging
//#define DEBUG_SQL

//-----------------------------------------------------------------
#define TABLE_CELL_DEFAULT_IMAGE_WIDTH	40
#define TABLE_CELL_DEFAULT_IMAGE_HEIGHT	40

#define COLOUR_ICON_IMAGE_WIDTH		(40-16)
#define COLOUR_ICON_IMAGE_HEIGHT	(40-16)

//-----------------------------------------------------------------
#define DEGREES_TO_RADIANS(x) (M_PI * x / 180.0)

//-----------------------------------------------------------------
// Flags to control debugging macros
#ifdef DEBUG
#define USE_BREAK_ON_DEBUGGER
#define USE_ALERT_BOX
#endif

//-----------------------------------------------------------------
// Create a DebugBreak() function to stop execution if the debugger is attached
#ifdef USE_BREAK_ON_DEBUGGER
#define DebugBreak() { __builtin_trap(); }
#else
	#define DebugBreak()
#endif

//-----------------------------------------------------------------
// Regular log to debugger with file and line number
#ifdef DEBUG
#define DT_DEBUG_LOG(fmt, ...) NSLog((@"%s [%d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DT_DEBUG_LOG(...)
#endif

//-----------------------------------------------------------------
// log to debugger with file and line number even in release
#define DT_RELEASE_LOG(fmt, ...) NSLog((@"%s [%d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

//-----------------------------------------------------------------
// Log to an alert box
#ifdef USE_ALERT_BOX
#define DT_ALERT_LOG(fmt, ...)\
{\
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%s [%d]", __PRETTY_FUNCTION__, __LINE__] \
	message:[NSString stringWithFormat:fmt, ##__VA_ARGS__]  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];\
	[alert show];\
}
#else
#define DT_ALERT_LOG(...)
#endif

//-----------------------------------------------------------------
// Output function name and line number only to debug log file
#ifdef DEBUG
#define LOG_FN()  DT_DEBUG_LOG(@"")
#else
#define LOG_FN()
#endif

//-----------------------------------------------------------------
// Debug assert, with formatting message and arguments - output to alert box
#ifdef DEBUG
#define DT_ASSERT_MSG(condition, fmt, ...) \
{\
	if (!(condition)) {\
		NSString* message = [NSString stringWithFormat:fmt, ##__VA_ARGS__];\
		DT_DEBUG_LOG(@"** ASSERT FAILED: %@", message);\
		DebugBreak();\
		NSString* __title = [NSString stringWithFormat:@"Assert failed\n%s [%d] %@", __PRETTY_FUNCTION__, __LINE__, @#condition];\
		UIAlertView *__alert = [[UIAlertView alloc] initWithTitle:__title \
			message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];\
		[__alert show];\
	}\
}
#else
#define DT_ASSERT_MSG(...)
#endif

//-----------------------------------------------------------------
// Debug assert with condition only - output to alert box
#ifdef DEBUG
#define DT_ASSERT(condition)  DT_ASSERT_MSG(condition, @#condition)
#else
#define DT_ASSERT(...)
#endif

//-----------------------------------------------------------------
// Assert a pointer is not nil...
#ifdef DEBUG
#define DT_ASSERT_NOT_NIL(pointer)  DT_ASSERT(pointer != nil)
#else
#define DT_ASSERT_NOT_NIL(...)
#endif

//-----------------------------------------------------------------
#endif

