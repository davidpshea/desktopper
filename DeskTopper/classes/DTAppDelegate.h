//
//  DTAppDelegate.h
//  DeskTopper
//
//  Created by David Shea on 15/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DTViewController;

@interface DTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow*         window;
@property (strong, nonatomic) DTViewController* viewController;
@property (strong, nonatomic) NSOperationQueue* operationQueue;

@end

