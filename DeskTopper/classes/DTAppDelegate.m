//
//  DTAppDelegate.m
//  DeskTopper
//
//  Created by David Shea on 15/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTAppDelegate.h"
#import "DTViewController.h"
#import "UIApplication+Helpers.h"

@implementation DTAppDelegate

//-----------------------------------------------------------------
@synthesize window			= _window;
@synthesize viewController	= _viewController;
@synthesize operationQueue	= _operationQueue;

//-----------------------------------------------------------------
- (NSOperationQueue*) operationQueue
{
	if (! _operationQueue) {
		_operationQueue = [[NSOperationQueue alloc] init];
	}
	return _operationQueue;
}

//-----------------------------------------------------------------
- (BOOL) application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
	#ifdef USE_TESTFLIGHT
	[TestFlight takeOff:@"373d27f9307fd571f86add47bc132563_MTIzMzU1MjAxMy0wMS0yNCAyMToyNToyNC45NzI1MDM"];
	#endif

	NSLog(@"Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);

	NSString* bin  = [FileHelper getPathForFileInDocuments:@"bin"];
	[FileHelper createDirectory:bin];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    self.viewController = [[DTViewController alloc] initWithNibName:@"DTViewController_iPhone" bundle:nil];
	} else {
	    self.viewController = [[DTViewController alloc] initWithNibName:@"DTViewController_iPad" bundle:nil];
	}
	self.window.rootViewController = self.viewController;

	//IOS7 TEST!!
	if ([UIApplication isIOS7]) {
		[[self window] setTintColor:[UIColor blueColor]];

		[[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
		[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithWhite:0.0f alpha:0.5f]];
		[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
	}

    [self.window makeKeyAndVisible];

	return YES;
}

//-----------------------------------------------------------------
- (BOOL) application:(UIApplication *)application openURL:(NSURL *)url
	sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{    
    if (url != nil) {
		[(DTViewController*)[self viewController] handleDocumentOpenURL:url];
	}

    return YES;
}

//-----------------------------------------------------------------
//- (void)applicationWillResignActive:(UIApplication *)application
//{
//	[self.viewController saveState];
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
//}

//-----------------------------------------------------------------
- (void)applicationDidEnterBackground:(UIApplication *)application
{
	[self.viewController saveState];
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
}

//-----------------------------------------------------------------
- (void)applicationWillEnterForeground:(UIApplication *)application
{
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
}

//-----------------------------------------------------------------
- (void)applicationDidBecomeActive:(UIApplication *)application
{
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
}

//-----------------------------------------------------------------
- (void)applicationWillTerminate:(UIApplication *)application
{
	/*
	 Called when the application is about to terminate.
	 Save data if appropriate.
	 See also applicationDidEnterBackground:.
	 */
}

@end

