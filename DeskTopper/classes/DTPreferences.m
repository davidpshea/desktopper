//
//  DTPreferences.m
//  DeskTopper
//
//  Created by David Shea on 28/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#if 0
#import "DTPreferences.h"

@implementation DTPreferences

@synthesize	Country;
@synthesize	Enabled;
@synthesize	Slider;

- (id) initFromUserDefaults
{
	self = [super init];
	if (self) {
		NSUserDefaults* pDefaults = [NSUserDefaults standardUserDefaults];

		[self setCountry:[pDefaults integerForKey:	@"country"]];
		[self setEnabled:[pDefaults boolForKey:  	@"enabled_preference"]];
		[self setSlider: [pDefaults floatForKey:	@"slider_preference"]];
	}
	return self;
}

@end
#endif
