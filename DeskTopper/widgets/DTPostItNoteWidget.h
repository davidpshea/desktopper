//
//  DTPostItNoteWidget.h
//  DeskTopper
//
//  Created by David Shea on 23/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidget.h"
//#import "YIPopupTextView.h"
#import "CustomImagePickerController.h"
#import "DTPopupTextController.h"

@class DTPopupTextController;
@class DTHTMLViewController;
@class DTBrowserViewController;

@interface DTPostItNoteWidget : DTWidget <
	NSCopying,
// 	YIPopupTextViewDelegate,
	DTPopupTextControllerDelegate,
	QuickDialogEntryElementDelegate,
	CustomImagePickerDelegate,
	UIActionSheetDelegate,
///	QuickDialogStyleProvider,
	UIPopoverControllerDelegate
>
@property (strong, nonatomic)	NSString*		text;
@property (strong, nonatomic)	NSString*		HTML;
@property (nonatomic, strong)	UIColor*		backgroundColour;
@property (nonatomic, strong)	UIColor*		textColour;
@property (nonatomic, strong)	UIColor*		linkColour;
@property (nonatomic, strong)	NSString*		fontName;
@property (nonatomic, strong)	NSString*		fontSize;
@property (nonatomic)			int				styleSheetIndex;
@property (nonatomic)			float			textScale;
@property (nonatomic)			int				cursorPosition;
@property (nonatomic, weak)		NSOperation*	conversionOperation;
@property (nonatomic)			BOOL			imageNeedsGenerating;

@property (nonatomic, weak) DTHTMLViewController*		HTMLController;
@property (nonatomic, weak) DTBrowserViewController*	browserController;
@property (nonatomic, weak) DTPopupTextController*		popupTextController;
@property (nonatomic, weak)	UIViewController*			parentViewController;

+ (NSArray*) getFontNameArray;

- (void)	displayHTMLNote:(UIViewController*)controller animated:(BOOL)animated;
- (id)		copyWithZone:(NSZone*)zone;

@end

