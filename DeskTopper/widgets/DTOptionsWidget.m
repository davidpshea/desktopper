//
//  DTOptionsWidget.m
//  DeskTopper
//
//  Created by David Shea on 30/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTOptionsWidget.h"
#import "DTAppDelegate.h"
#import "DTViewController.h"
#import "UIApplication+Helpers.h"
#import "UIImageExtras.h"
#import "QuickDialogHelper.h"

#if 0

//-----------------------------------------------------------------
@implementation DTOptionsWidget

@synthesize		myOptions	= _MyOptions;

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
//	return @"20-gear2";
	return @"system";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Options";
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UIViewController*) controller
{
	[self showSettingsModal:controller];
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(UIViewController*) controller
{
	[self showSettingsModal:controller];
	return YES;
}

//-----------------------------------------------------------------
- (void) createSettingsMenuWithRoot:(QRootElement*)root withViewController:(UIViewController*)parentViewController
{
	[root setTitle:@"Options"];
	[root setGrouped:YES];

	QSection*		 section;
	QBooleanElement* boolElement;

	//----------------
	section = [root addNewSection];
	[section setTitle:@"Icons"];
//	[section setFooter:@"How you wish the icons to be displayed"];
	boolElement = [[QBooleanElement alloc] initWithTitle:@"Constrain size" BoolValue:[[self options] constrainSize]];
	[boolElement setKey:@"constrainSize"];
	[section addElement:boolElement];

	//----------------
	section = [root addNewSection];
	[section setTitle:@"Desktop"];
//	[section setFooter:@"How icons are aligned"];
	boolElement = [[QBooleanElement alloc] initWithTitle:@"Align icons to grid" BoolValue:[[self options] alignIcons]];
	[boolElement setKey:@"alignIcons"];
	[section addElement:boolElement];

	//----------------
}

//-----------------------------------------------------------------
- (void) updateViewControllerWithOptions
{
	DTViewController* controller = [UIApplication myRootViewController];
	[controller setMyOptions:[self myOptions]];
}

//-----------------------------------------------------------------
- (void) saveSettingsFromMenu:(QRootElement*)optionsRoot
{
	id element = [optionsRoot elementWithKey:@"alignIcons"];
	[[self options] setAlignIcons:[element boolValue]];

	element = [optionsRoot elementWithKey:@"constrainSize"];
	[[self options] setConstrainSize:[element boolValue]];

	[self updateViewControllerWithOptions];
}

//-----------------------------------------------------------------
#define kOptionsKey	@"options"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	[super encodeWithCoder:encoder];
	[encoder encodeObject:[self options] forKey:kOptionsKey];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super initWithCoder:decoder];

	if (self) {
		[self setOptions:[decoder decodeObjectForKey:kOptionsKey]];
	}
	return self;
}

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	if (self) {
		[self setIconIndex:32];
		[self setScale:1.0f];
		[self setSize:CGSizeMake(320/4, 240/4)];

		_options = [[DTOptions alloc] init];

		[self updateViewControllerWithOptions];
	}
	return self;
}

//-----------------------------------------------------------------
@end
#endif

