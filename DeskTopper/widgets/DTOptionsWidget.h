//
//  DTOptionsWidget.h
//  DeskTopper
//
//  Created by David Shea on 30/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidget.h"
#import "DTOptions.h"

@interface DTOptionsWidget : DTWidget <NSCoding>

@property (nonatomic, strong) DTOptions*	options;

- (void)		handleDoubleTap:(UIViewController*) controller;

@end

