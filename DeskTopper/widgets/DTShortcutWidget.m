//
//  DTShortcutWidget.m
//  DeskTopper
//
//  Created by David Shea on 21/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "macros.h"
#import "DTShortcutWidget.h"
#import "QuickDialogHelper.h"
#import "UIImageExtras.h"
#import "DTViewController.h"
//#import "SVModalWebViewController.h"
#import "UIApplication+Helpers.h"
#import "DTWidgetStyle.h"
//#import "SVWebViewController.h"
#import "DTBrowserController.h"
#import "DTNavigationController.h"
#import "DTPopupTextController.h"
#import "NSCoder+DefaultDecoding.h"
#import "DTDocumentSharing.h"
#import "UIApplication+Helpers.h"
#import "NSString+Word.h"
#import "MBProgressHUD+Extras.h"
#import "NSString+URL.h"
#import "UIPopoverController+Stack.h"
#import "UIActionSheet+Tools.h"

//-----------------------------------------------------------------
@implementation DTShortcutWidget

@synthesize URL						= _URL;
@synthesize popupTextController		= _popupTextController;
@synthesize browserViewController	= _browserViewController;
@synthesize updateURL				= _updateURL;
@synthesize updateImage				= _updateImage;
@synthesize isReadOnly				= _isReadOnly;
@synthesize parentViewController	= _parentViewController;

static BOOL ShowingCreationSettings = NO;

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
	return @"safari";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Web page";
}


//-----------------------------------------------------------------
#if 1
static UIImage* placeholder = nil;

- (UIImage*) image
{
	UIImage* image = [super image];

	if (!image) {
		if (! placeholder) {
			CGSize size = [UIImage getThumbnailPhotoSize];
			placeholder = [UIImage createColouredRectangle:size colour:[UIColor lightGrayColor]];
		}
		return placeholder;
	}

	return image;
}
#endif

//-----------------------------------------------------------------
- (void) setURL:(NSString*)URL
{
	_URL = URL;

	[self setIsDirty:YES];

	[DTWidget activeMenuUpdateQMultilineElement:URL withKey:@"url"];
	[DTWidget activeMenuReloadData];
}

//-----------------------------------------------------------------
- (NSString*) getURLForExport
{
	NSString* browserURL = [[self browserViewController] getURL];
	BOOL hasURL			 = [browserURL length] > 0;

	// If we update the URL, get latest from web browser
	// Otherwise just use what was last typed in
	return (hasURL && [self updateURL]) ? browserURL : [self URL];
}

//------------------------------------------------------------------
- (id) copyWithZone:(NSZone*)zone
{
	DTShortcutWidget* another = [super copyWithZone:zone];

	[another setPopupTextController:nil];
	[another setBrowserViewController:nil];

	[another setURL:			[[self URL] copy]];
	[another setUpdateURL:		[self updateURL]];
	[another setUpdateImage:	[self updateImage]];

	return another;
}

//-----------------------------------------------------------------
- (void) saveViewForWidget:(DTShortcutWidget*)otherWidget
{
	WKWebView* webView	= [[self browserViewController] webView];
	UIImage* viewImage = [UIImage createFromView:webView];
	NSString* fileName = [viewImage saveToUniqueImageFile:nil];

	[otherWidget setImageFilePath:fileName withImage:viewImage];

	CGSize webpageSize = [viewImage size];
//	DT_DEBUG_LOG(@"viewSize:%@", NSStringFromCGSize(webpageSize));
	webpageSize.width  /= 2;
	webpageSize.height /= 2;
	[otherWidget setSize:webpageSize];

	// ensure that the database stays up to date with new filename/image size
	[otherWidget updateDatabaseFromWidget];
}

//-----------------------------------------------------------------
- (void) saveView
{
	[self saveViewForWidget:self];
}

//-----------------------------------------------------------------
- (void) displayWebPageDoneButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"display web page - done pressed");

	WKWebView* webView	= [[self browserViewController] webView];
	NSString* URL		= [[self browserViewController] getURL];
	BOOL hadURL			= [URL length] > 0;
	
	if ( ((![webView isLoading]) && (![self isReadOnly])) || (!hadURL) ) {
		if ([self updateURL]) {
			[self setURL:URL];
		}
	}
	
	//	if ([self updateImage] || (! hadURL)) {
	if ([self updateImage]) {
		[self saveView];
	}
	
	[self updateDatabaseFromWidget];

	// We've created a new image, so the desktop will need regenerating
	DTDesktop* desktop = [[UIApplication myRootViewController] desktop];
	[desktop setThumbnailIsDirty:YES];

	[[self browserViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) displayWebPageCancelButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"display web page - cancel pressed");
	[[self browserViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) displayWebPage:(NSString*)URLAsString parentController:(UIViewController*)parentController animated:(BOOL)animated
{
	TESTFLIGHT_CHECKPOINT(@"display web page");

	// Create view controller for web page
	DTBrowserViewController* browseController = [[DTBrowserViewController alloc] init];
	[browseController setIsReadOnly:[self isReadOnly]];

	[self setBrowserViewController:browseController];

    [browseController setLoadingImage: [self image]];
	// Start loading the URL...
	[browseController setPageSourceURL:URLAsString];

	// Create a navigation wrapper around web controller
	DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:browseController];

	if ([self isReadOnly]) {
		[navigationController addDoneButton:@selector(displayWebPageCancelButtonPressed) forTarget:self];
	}
	else {
		[navigationController addSaveButton:@selector(displayWebPageDoneButtonPressed)         forTarget:self];
		[navigationController addLeftCancelButton:@selector(displayWebPageCancelButtonPressed) forTarget:self];
		[navigationController addEditButton:@selector(displayWebPageEditButtonPressed)         forTarget:self];
	}

	[navigationController setTranslucent:[UIApplication isIOS7]];
	[navigationController setNavigationBarHidden:NO animated:YES];
	[navigationController setWidget:self];

	[browseController setNavigationController:navigationController];

	ShowingCreationSettings = NO;

	// Show it...
	[parentController presentViewController:navigationController animated:animated completion:nil];

	[self setParentViewController:parentController];

}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UIViewController*)controller
{
	[self showCreationSettings:controller fromSender:nil];
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(DTViewController*)controller
{
	[self displayWebPage:_URL parentController:controller animated:YES];
	return YES;
}

//-----------------------------------------------------------------
- (void) actionButtonAction:(id)sender
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"Web page - action menu");

	if ([UIPopoverController dismissActiveController]) {
		return;
	}

	// We only allow the creation of duplicate link if the action button
	// is pressed while viewing the webpage, not from the desktop.
	// This is because the new link assumes the web widget is already running and has
	// All it needs. You can still tap "duplicate" from the desktop - which
	// is consistent with the other widget types.
	BOOL AllowNewLink = YES;

	if ([sender isKindOfClass:[UIBarButtonItem class]]) {

		if ([[sender target] isKindOfClass:[DTViewController class]]) {

			AllowNewLink = NO;
		}
	}

	UIActionSheet* sheet =
	[
		[UIActionSheet alloc] initWithTitle:nil
		delegate:self
		cancelButtonTitle:		nil
		destructiveButtonTitle:	nil
		otherButtonTitles:		nil
	];
	[sheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];

	if ([MFMailComposeViewController canSendMail]) {
		[sheet addButtonWithTitle:@"Email link" menuIndex:2];
	}

	[sheet addButtonWithTitle:@"Open in..." menuIndex:0];

	if ([DTDocumentSharing isChromeInstalled]) {
		[sheet addButtonWithTitle:@"Open in Chrome" menuIndex:3];
	}

	[sheet addButtonWithTitle:@"Open in Safari"		menuIndex:1];
	[sheet addButtonWithTitle:@"Copy To Clipboard"	menuIndex:6];

	if (AllowNewLink) {
		[sheet addButtonWithTitle:@"Create New Link" menuIndex:4];
	}

	[sheet addButtonWithTitle:@"Cancel" menuIndex:5];
	[sheet setCancelButtonIndex:[sheet getButtonIndexFromMenuIndex:5]];

	[UIPopoverController setActiveController:sheet];

	//    [sheet showFromBarButtonItem:sender animated:YES];

    [sheet launchFrom:sender animated:YES];
}

//-----------------------------------------------------------------
- (void) actionSheet:(UIActionSheet*)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	NSURL*				URL;
	DTViewController*	controller;
	DTShortcutWidget*	newWidget;
	NSString*			imageName;
	UIPasteboard*		clipboard;

	[UIPopoverController setActiveController:nil];

	LOG_FN();
	switch ([actionSheet getMenuIndexFromButtonIndex:(int)buttonIndex]) {
		case 0:
			TESTFLIGHT_CHECKPOINT(@"Web page - action menu - open in");
			[DTDocumentSharing exportString:[self getURLForExport] as:@"public.url"];
			break;

		case 1:
			TESTFLIGHT_CHECKPOINT(@"Web page - action menu - open URL");
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self getURLForExport]]];
			break;

		case 2:
			TESTFLIGHT_CHECKPOINT(@"Web page - action menu - email link");
			[DTDocumentSharing sendEmail:@"A web link from Total Notes" body:[self getURLForExport] isHTML:NO imageFileName:nil];
			break;

		case 3:
			TESTFLIGHT_CHECKPOINT(@"Web page - action menu - open with chrome");
			URL = [NSURL URLWithString:[self getURLForExport]];
			[DTDocumentSharing openURLWithChrome:URL];
			break;

		case 6:
			TESTFLIGHT_CHECKPOINT(@"Web page - action menu - clipboard");
			clipboard = [UIPasteboard generalPasteboard];
			//			[clipboard setString:[self getURLForExport]];
			[clipboard setURL:[NSURL URLWithString:[self getURLForExport]]];
			break;
			
		case 4:
			TESTFLIGHT_CHECKPOINT(@"Web page - action menu - save link");
			[MBProgressHUD showProgressOnView:[[self browserViewController] view]];

			controller = [UIApplication myRootViewController];
			imageName  = [self imageFilePath];
			[self setImageFilePathNoUpdate:nil];

			newWidget = (DTShortcutWidget*)[controller duplicateWidget:self];
			[newWidget setUpdateURL:YES];
			[newWidget setUpdateImage:YES];

			[self saveViewForWidget:newWidget];
			[self setImageFilePathNoUpdate:imageName];

			[newWidget setURL:[self getURLForExport]];

			NSString* browserURL = [[self browserViewController] getURL];
			browserURL =  [browserURL length] ? browserURL : [self URL];
			[newWidget setURL:browserURL];
			
			[MBProgressHUD showDoneOnView:[[self browserViewController] view]];
			break;
	}
}

//-----------------------------------------------------------------
- (void) popupTextControllerSaveButtonPressed
{
	LOG_FN();

	TESTFLIGHT_CHECKPOINT(@"Web page - URL edit - save");
	DTPopupTextController* editController = [self popupTextController];
	//	YIPopupTextView* popup	= [editController popupView];
	NSString* text			= [editController text];

	if (ShowingCreationSettings) {
		[[UIApplication myRootViewController] completeWidgetInProgress];
	}

	ShowingCreationSettings = NO;

	NSString* URL = [NSString createValidURLFromString:text];
	[self setURL:URL];
	[self updateDatabaseFromWidget];

	// We've created a new image, so the desktop will need regenerating
	DTDesktop* desktop = [[UIApplication myRootViewController] desktop];
	[desktop setThumbnailIsDirty:YES];

	if ([editController userObject]) {

		[[self browserViewController] setPageSourceURL:URL];
		[[self browserViewController] refresh];
		[[self popupTextController] dismissViewControllerAnimated:NO completion:nil];
	}
	else {

		[editController dismissViewControllerAnimated:NO completion:^{
			[self displayWebPage:[self URL] parentController:[self parentViewController] animated:NO];
		}];
	}
}

//-----------------------------------------------------------------
- (void) popupTextControllerCancelButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"Web page - URL edit - cancel");

	if (ShowingCreationSettings) {
		[[UIApplication myRootViewController] destroyWidgetInProgress];
	}

	ShowingCreationSettings = NO;
	if ([[self popupTextController] userObject]) {
		[[self popupTextController] dismissViewControllerAnimated:NO completion:nil];
	}
	else {
		[UIApplication dismissToRootViewController:YES];
	}
}

//-----------------------------------------------------------------
- (void) popupTextController:(DTPopupTextController*)controller textDidChange:(NSString*)text
{
	// bit of a cludge - if user presses enter while editing
	// a URL, treat it as end of editing
	if ([text length] > 0) {

		NSRange range = [text rangeOfString:@"\x0A"];

		if (range.location != NSNotFound) {

			text = [text stringByReplacingOccurrencesOfString:@"\x0A" withString:@""];
			[controller setText:text];

			ShowingCreationSettings = NO;
			[self popupTextControllerSaveButtonPressed];
		}
	}
}

//-----------------------------------------------------------------
- (void) displayWebPageEditButtonPressed
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"Web page - edit button");
	DTPopupTextController* editController = [[DTPopupTextController alloc] init];
	[editController setEnabledKeyboards:(keyboardIDShortcut | keyboardIDControl | keyboardIDURL)];
	[editController setText:[self getURLForExport]];
	[editController setTarget:self];
	[editController setUserObject:self];
	[editController setPopupTextControllerDelegate:self];

	[self setPopupTextController:editController];

	//YIPopupTextView* popup = [editController popupView];

	UITextView* textView = [editController getTextView];
	
	[textView setAutocorrectionType:UITextAutocorrectionTypeNo];
	[textView setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[textView setEnablesReturnKeyAutomatically:YES];
	[textView setReturnKeyType:UIReturnKeyGo];

	DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:editController];
	//	[navigationController addSaveButton:@selector(editFromDisplayWebPageButtonSaveButtonPressed) forTarget:self];
	//	[navigationController addCancelButton:@selector(editFromDisplayWebPageCancelButtonPressed)   forTarget:self];
	[navigationController setTranslucent:NO];
	[navigationController setNavigationBarHidden:YES animated:YES];

	// Show it...
	[[self browserViewController] presentViewController:navigationController animated:NO completion:nil];
}

//-----------------------------------------------------------------
- (void) showCreationSettings:(UIViewController*)parentViewController fromSender:(id)sender
{
	LOG_FN();
	[self setParentViewController:parentViewController];

	// Create view controller for web page
	DTPopupTextController* editController = [[DTPopupTextController alloc] init];
	[editController setEnabledKeyboards:(keyboardIDShortcut | keyboardIDControl | keyboardIDURL)];
	[editController setText:[self URL]];
	[editController setTarget:self];
	[editController setUserObject:nil];
	[editController setPopupTextControllerDelegate:self];

	[self setPopupTextController:editController];

	UITextView* textView = [editController getTextView];

	[textView setAutocorrectionType:UITextAutocorrectionTypeNo];
	[textView setAutocapitalizationType:UITextAutocapitalizationTypeNone];
	[textView setEnablesReturnKeyAutomatically:YES];
	[textView setReturnKeyType:UIReturnKeyGo];

	DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:editController];
	//	[navigationController addSaveButton:@selector(showCreationSettingsSaveButtonPressed)     forTarget:self];
	//	[navigationController addCancelButton:@selector(showCreationSettingsCancelButtonPressed) forTarget:self];
	[navigationController setTranslucent:NO];
	[navigationController setNavigationBarHidden:YES animated:YES];

	ShowingCreationSettings = YES;

	// Show it...
//	[parentViewController presentViewController:navigationController animated:YES completion:nil];
	[parentViewController presentViewController:navigationController animated:NO completion:nil];
}

//-----------------------------------------------------------------
- (void) createSettingsMenuWithRoot:(QRootElement*)root withViewController:(UIViewController*)parentViewController
{
	root.title    = @"Web Page";
	root.grouped  = YES;

	//----------------
	QSection* optionsSection = [root addNewSection];
	[optionsSection setFooter:@"What happens when you finish browsing"];
/*
	QLabelElement* editButton = [[QLabelElement alloc] initWithTitle:@"Edit URL" Value:@""];
	[editButton setControllerAction:@"dummy"];
	editButton.onSelected = ^{
		[self editFromMenuButton];
	};
	[editSection addElement:editButton];
*/
	QBooleanElement* updateURL = [[QBooleanElement alloc] initWithTitle:@"Update URL" BoolValue:[self updateURL]];
	[updateURL setKey:@"updateURL"];
	[optionsSection addElement:updateURL];

	QBooleanElement* updateImage = [[QBooleanElement alloc] initWithTitle:@"Update image" BoolValue:[self updateImage]];
	[updateImage setKey:@"updateImage"];
	[optionsSection addElement:updateImage];

	//----------------
	QSection* aboutSection = [root addNewSection];

	QLabelElement* help = [[QLabelElement alloc] initWithTitle:@"Help" Value:@""];
	[help setControllerAction:@"dummy"];
	help.onSelected = ^{
	};
	[aboutSection addElement:help];
}

//-----------------------------------------------------------------
- (void) saveSettingsFromMenu:(QRootElement*)optionsRoot
{
	id element = [optionsRoot elementWithKey:@"updateURL"];
	[self setUpdateURL:[element boolValue]];

	element = [optionsRoot elementWithKey:@"updateImage"];
	[self setUpdateImage:[element boolValue]];

	[super saveSettingsFromMenu:optionsRoot];

	[self updateDatabaseFromWidget];
}

//-----------------------------------------------------------------
#define kURLKey			@"url"
#define kupdateURL		@"updateURL"
#define kupdateImage	@"updateImage"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	[super encodeWithCoder:encoder];

	[encoder encodeObject:	[self URL]			forKey:kURLKey];
	[encoder encodeBool:	[self updateURL]	forKey:kupdateURL];
	[encoder encodeBool:	[self updateImage]	forKey:kupdateImage];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super initWithCoder:decoder];

	if (self) {
		[self setURL:[decoder decodeObjectForKey:kURLKey]];

		[self setUpdateURL:  [decoder decodeBOOL:kupdateURL   withDefault:YES]];
		[self setUpdateImage:[decoder decodeBOOL:kupdateImage withDefault:YES]];

		[[self style] setBorderWidth:0.0];
	}
	return self;
}

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	if (self) {
		[self setUpdateURL:  YES];
		[self setUpdateImage:YES];

		[[self style] setBorderWidth:0.0];
	}
	return self;
}

//-----------------------------------------------------------------
@end

