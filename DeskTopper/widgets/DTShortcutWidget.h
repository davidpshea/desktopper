//
//  DTShortcutWidget.h
//  DeskTopper
//
//  Created by David Shea on 21/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidget.h"
//#import "YIPopupTextView.h"
#import "DTPopupTextController.h"

@class DTPopupTextController;
@class DTBrowserViewController;

@interface DTShortcutWidget : DTWidget <
	NSCopying,
	UIActionSheetDelegate,
	DTPopupTextControllerDelegate
>
@property (strong, nonatomic)	NSString*					URL;
@property (nonatomic, weak)		DTPopupTextController*		popupTextController;
@property (nonatomic, weak)		DTBrowserViewController*	browserViewController;
@property (nonatomic)			BOOL						updateURL;
@property (nonatomic)			BOOL						updateImage;
@property (nonatomic)			BOOL						isReadOnly;
@property (nonatomic, weak)		UIViewController*			parentViewController;

//+ (NSArray*)  getQueryArray;
//+ (NSString*) checkForValidURL:(NSString*)stringToCheck;
//+ (NSString*) createValidURLFromString:(NSString*)stringToProcess;

- (void)	handleDoubleTap:(UIViewController*) controller;
- (id)		copyWithZone:(NSZone*)zone;
//- (void)	showCreationSettingsSaveButtonPressed;

- (void) displayWebPage:(NSString*)URLAsString parentController:(UIViewController*)parentController animated:(BOOL)animated;

@end

