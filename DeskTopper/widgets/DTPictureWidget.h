//
//  DTPictureWidget.h
//  DeskTopper
//
//  Created by David Shea on 21/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidget.h"
#import "CLImageEditor.h"

@class DTPictureViewerController;

@interface DTPictureWidget : DTWidget <
	NSCopying,
	UINavigationControllerDelegate,
	UIImagePickerControllerDelegate,
	UIActionSheetDelegate,
	UIPopoverControllerDelegate,
	CLImageEditorDelegate
>

@property (nonatomic, strong)	DTPictureViewerController*		photoViewController;
@property (nonatomic)			BOOL							keepAspectRatio;
@property (nonatomic, strong)	UIImagePickerController*		imagePickerController;
@property (nonatomic, strong)	NSString*						highResolutionImageFilePath;
@property (nonatomic, strong)	UIImage*						scaledImage;

- (void)	handleDoubleTap:(UIViewController*) controller;
- (id)		copyWithZone:(NSZone*)zone;


@end
