//
//  DTPhotoWidget.m
//  DeskTopper
//
//  Created by David Shea on 20/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTPhotoWidget.h"
#import "UIImagePicker.h"
#import "UIApplication+Helpers.h"
#import "UIImageExtras.h"
#import "UIImagePickerController+Helper.h"

@implementation DTPhotoWidget

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
	return @"camera";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Camera";
}

//-----------------------------------------------------------------
- (id) copyWithZone:(NSZone*)zone
{
	return [super copyWithZone:zone];
}

//-----------------------------------------------------------------
- (void) showCreationSettings:(UIViewController*)parentViewController fromSender:(id)sender
{
	// give me a view controller for my camera
	UIImagePickerController* pickerController = [UIImagePickerController initCameraViewController:self];

	[self setImagePickerController:pickerController];

	if (pickerController) {
		// show camera...
		[parentViewController presentViewController:pickerController animated:NO completion:nil];
	}
	else {
		[super showCreationSettings:parentViewController  fromSender:sender];
	}
}

//-----------------------------------------------------------------
@end

