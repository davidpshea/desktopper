//
//  DTPictureWidget.m
//  DeskTopper
//
//  Created by David Shea on 21/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTPictureWidget.h"
#import "QuickDialogHelper.h"
#import "UIImageExtras.h"
#import "UIApplication+Helpers.h"
#import "UIImagePicker.h"
#import "UIImagePickerController+Helper.h"
#import "DTPictureViewerController.h"
#import "DTOptionsControllerWithDone.h"
#import "DTNavigationController.h"
#import "DTDocumentSharing.h"
#import "DTAppDelegate.h"
#import "SVProgressHUD.h"
#import "DTWidgetControl.h"
#import "DTDesktop.h"
#import "DTViewController.h"
#import "UIPopoverController+Stack.h"
#import "UIActionSheet+Tools.h"

@implementation DTPictureWidget

//-----------------------------------------------------------------
@synthesize photoViewController   		= _photoViewController;
@synthesize keepAspectRatio       		= _keepAspectRatio;
@synthesize imagePickerController		= _imagePickerController;
@synthesize highResolutionImageFilePath = _highResolutionImageFilePath;
@synthesize scaledImage					= _scaledImage;

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
	return @"pictures";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Photo Roll";
}

//------------------------------------------------------------------
- (void) setIsProcessing:(BOOL)isProcessing
{
	[super setIsProcessing:isProcessing];

	DTPictureViewerController* pictureViewer	 = [self photoViewController];
	DTNavigationController* navigationController = [pictureViewer navigationController];

	if (navigationController) {
		if (isProcessing) {
			[navigationController startActivityIndicator];
		}
		else {
			[navigationController stopActivityIndicator];
		}
	}
}

//------------------------------------------------------------------
- (id) copyWithZone:(NSZone*)zone
{
	DTPictureWidget* another = [super copyWithZone:zone];

	[another setPhotoViewController:nil];
	[another setImagePickerController:nil];
	[another setKeepAspectRatio:[self keepAspectRatio]];

	NSString* oldFilePath = [self highResolutionImageFilePath];
	if (oldFilePath) {
		NSString* fileName = [UIImage addDefaultImageFileExtension:[FileHelper getUniqueFileNameFromTime:@"full_image%@"]];
		NSString* filePath = [FileHelper getPathForFileInDocuments:fileName];

		[self setIsProcessing:YES];
		NSBlockOperation* copyOperation = [
			NSBlockOperation
			blockOperationWithBlock:^{
				[FileHelper copyFile:oldFilePath toPath:filePath];
				[self setIsProcessing:NO];
				[another setHighResolutionImageFilePath:filePath];
			}
		];
		[UIApplication addOperationToMyOperationQueue:copyOperation];
	}

	return another;
}

//-----------------------------------------------------------------
static UIImage* placeholder = nil;

- (UIImage*) image
{
	if ([self scaledImage]) {
		return [self scaledImage];
	}

	UIImage* image = [super image];

	if (!image) {
		if (! placeholder) {
			CGSize size = [UIImage getThumbnailPhotoSize];
			placeholder = [UIImage createColouredRectangle:size colour:[UIColor grayColor]];
		}
		return placeholder;
	}

	return image;
}

//-----------------------------------------------------------------
-(NSString*) getHighResolutionImageFilePath
{
	return _highResolutionImageFilePath ?
		_highResolutionImageFilePath : [super imageFilePath];
}

//-----------------------------------------------------------------
-(void) setHighResolutionImageFilePath:(NSString*)newPath
{
	if ([self highResolutionImageFilePath]) {
		[FileHelper sendFileToBin:[self highResolutionImageFilePath]];
	}

	_highResolutionImageFilePath = newPath;
	[self updateDatabaseFromWidget];
}

//-----------------------------------------------------------------
/*
  - (void) setHighResolutionImageOperation:(id)object
{
	UIImage* image = (UIImage*) object;
	NSString* fullSizeFilePath = [image saveToUniquePNG:@"full_image%@.png"];
	[self setHighResolutionImageFilePath:fullSizeFilePath];
}
*/
//-----------------------------------------------------------------
- (void) setHighResolutionImage:(UIImage*)image
{
	if (image == nil) {
		[self setHighResolutionImageFilePath:nil];
	}
	else {
		[self setIsProcessing:YES];

		NSString* fullSizeFilePath = [image saveToUniqueImageFile:@"full_image%@"];
		[self setHighResolutionImageFilePath:fullSizeFilePath];

		DTPictureViewerController* pictureController = [self photoViewController];
		if (pictureController) {
//			[pictureController setImage:[self getHighResolutionImage]];
			[pictureController setImage:image];
		}

		[self setIsProcessing:NO];
	}
}

//-----------------------------------------------------------------
- (void) setHighResolutionImageBackground:(UIImage*)image
{
	if (image == nil) {
		[self setHighResolutionImageFilePath:nil];
	}
	else {
		[self setIsProcessing:YES];
		NSBlockOperation* saveOperation = [
			NSBlockOperation
			blockOperationWithBlock:^{
				NSString* fullSizeFilePath = [image saveToUniqueImageFile:@"full_image%@"];
				[self setHighResolutionImageFilePath:fullSizeFilePath];
				DTPictureViewerController* pictureController = [self photoViewController];
				if (pictureController) {
//					[pictureController setImage:[self getHighResolutionImage]];
					[pictureController setImage:image];
				}
				[self setIsProcessing:NO];
			}
		];
		[UIApplication addOperationToMyOperationQueue:saveOperation];
	}
}

//-----------------------------------------------------------------
- (UIImage*) getHighResolutionImage
{
	UIImage* imageToView = nil;

	// Read the hi-res version off disk
	if ([self highResolutionImageFilePath]) {
		[self setIsProcessing:YES];
		imageToView = [UIImage createFromImageFile:[self highResolutionImageFilePath]];
		[self setIsProcessing:NO];
	}

	// if no hi-res version, drop down to the low-res desktop version
	if (!imageToView) {
		imageToView = [self image];
	}

	return imageToView;
}

//-----------------------------------------------------------------
- (void) actionButtonAction:(UIBarButtonItem*)button
{
	TESTFLIGHT_CHECKPOINT(@"picture - actionsheet");

	if ([UIPopoverController dismissActiveController]) {
		return;
	}

	UIActionSheet* sheet =
	[
		[UIActionSheet alloc] initWithTitle:nil
		delegate:self
		cancelButtonTitle:		nil
		destructiveButtonTitle:	nil
		otherButtonTitles:		nil
	];
	[sheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];

	if ([MFMailComposeViewController canSendMail]) {
		[sheet addButtonWithTitle:@"Email picture" menuIndex:0];
	}

	[sheet addButtonWithTitle:@"Open in..."				menuIndex:1];
	[sheet addButtonWithTitle:@"Save to Camera Roll"	menuIndex:2];
	[sheet addButtonWithTitle:@"Copy"					menuIndex:3];
	[sheet addButtonWithTitle:@"Cancel"					menuIndex:4];
	[sheet setCancelButtonIndex:[sheet getButtonIndexFromMenuIndex:4]];

	[UIPopoverController setActiveController:sheet];
	[sheet showFromBarButtonItem:button animated:YES];
}

//-----------------------------------------------------------------
- (void) actionSheet:(UIActionSheet*)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	UIPasteboard*		clipboard;

	[UIPopoverController setActiveController:nil];
	
	switch (buttonIndex) {
		case 0:
			TESTFLIGHT_CHECKPOINT(@"picture - send email");
			[DTDocumentSharing sendEmail:@"A Picture from Total Notes" body:@""
				isHTML:NO imageFileName:[self getHighResolutionImageFilePath]];
			break;

		case 1:
			TESTFLIGHT_CHECKPOINT(@"picture - export photo");
			[DTDocumentSharing exportPhoto:[self getHighResolutionImageFilePath]];
			break;

		case 2:
			TESTFLIGHT_CHECKPOINT(@"picture - save to photos");
			[[self getHighResolutionImage] saveToCameraRoll];
			break;

		case 3:
			TESTFLIGHT_CHECKPOINT(@"picture - copy");
			clipboard = [UIPasteboard generalPasteboard];
			[clipboard setImage:[self getHighResolutionImage]];
			break;

		default:
			break;
	}
}

//-----------------------------------------------------------------
#if 0
- (void) hasFinishedResizing:(CGSize)size
{
	//UIImage* image = [self image];
	//	UIImage* scaledImage = [image imageFromImageByScaling:size keepAspectRatio:YES];

	CGRect rect = CGRectMake(0, 0, size.width, size.height);

	CGImageRef imageRef = CGImageCreateWithImageInRect([[self image] CGImage], rect);
	// or use the UIImage wherever you like
	[self setScaledImage:[UIImage imageWithCGImage:imageRef]];
	CGImageRelease(imageRef);


	//	[self setScaledImage:scaledImage];
}
#endif

//-----------------------------------------------------------------
//- (void) photoEditorCanceled:(AFPhotoEditorController*)editor
- (void)imageEditor:(CLImageEditor*)editor didDismissWithImageView:(UIImageView *)imageView canceled:(BOOL)canceled
{
	// Find out if this widget was selected
	DTViewController* rootController = [UIApplication myRootViewController];
	DTDesktop* desktop = [rootController desktop];

	if ([desktop selectedWidget] == self) {
		// if so, we update the edit controls in case the image has changed size
		[DTWidgetControl updateEditControlsForWidget:self];
		[DTWidgetControl showEditControls];
	}

	[editor dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
#pragma mark- CLImageEditor delegate
- (void)imageEditor:(CLImageEditor*)editor didFinishEdittingWithImage:(UIImage*)image
{
	[self setHighResolutionImage:image];

	// Scale the camera/photo image to something a bit more usable for the desktop
	CGSize scaledSize		 = [image getSizeToFitIntoSize:[UIImage getScaledPhotoSize]];
	UIImage* scaledImage	 = [image imageFromImageByScaling:scaledSize keepAspectRatio:YES];
	NSString* scaledFilePath = [scaledImage saveToUniqueImageFile:nil];

	// Send the correct image to the widget being edited
	[self setImageFilePath:scaledFilePath withImage:scaledImage];

	CGSize thumbnailSize = [image getSizeToFitIntoSize:[UIImage getThumbnailPhotoSize]];
	[self setSize:thumbnailSize];

	// Call common code...
	[self imageEditor:editor didDismissWithImageView:nil canceled:NO];

	//	[editor dismissViewControllerAnimated:YES completion:nil];
}


//-----------------------------------------------------------------
- (void) showImageEditor:(UIViewController*)parentController
{
	TESTFLIGHT_CHECKPOINT(@"picture - edit");
/*
	[[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
	[[UINavigationBar appearance] setBarTintColor:[UIColor redColor]];
	[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
*/
	//	[[UINavigationBar appearance] setB]


	UIImage* image = [self getHighResolutionImage];

	CLImageEditorTheme* theme = [CLImageEditorTheme theme];
	[theme setToolbarColor:[UIColor blackColor]];
	[theme setToolbarTextColor:[UIColor whiteColor]];
	[theme setToolIconColor:@"white"];
	[theme setBackgroundColor:[UIColor blackColor]];


	CLImageEditor *editor = [[CLImageEditor alloc] initWithImage:image];
	[editor setDelegate:self];


	[parentController presentViewController:editor animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) editButtonPressed
{
	[self showImageEditor:[UIApplication myPresentedViewController]];
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UIViewController*)controller
{
	[self showImageEditor:controller];
}

//-----------------------------------------------------------------
- (void) viewImage:(UIViewController*)controller
{
//	[SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
	TESTFLIGHT_CHECKPOINT(@"picture - view");

	// Initialise the picture viewer (scrolling photo view)
	DTPictureViewerController* pictureViewer = [[DTPictureViewerController alloc]
		initWithImage:[self getHighResolutionImage]];

	// Create the navigation controller wrapper
	DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:pictureViewer];

	[navigationController addDoneButton:NSSelectorFromString(@"doneButtonPressed") forTarget:pictureViewer];
	[navigationController addEditButton:@selector(editButtonPressed) forTarget:self];

	[navigationController setTranslucent:YES];
//	[navigationController setTimeTillHide:5.0];
	[navigationController setWidget:self];

	// let viewer know about controller
	[pictureViewer setNavigationController:navigationController];

	// Widget need to know about the photo viewer
	[self setPhotoViewController:pictureViewer];

//	[SVProgressHUD dismiss];

	// Show it...
	[controller presentViewController:navigationController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(UIViewController*)controller
{
	[self viewImage:controller];
	return YES;
}

//-----------------------------------------------------------------
- (void) hasUpdatedImage
{
	[super hasUpdatedImage];

	DTPictureViewerController* pictureController = [self photoViewController];
	if (pictureController) {
		[pictureController setImage:[self getHighResolutionImage]];
	}
}

//-----------------------------------------------------------------
- (void) showCreationSettings:(UIViewController*)parentViewController fromSender:(id)sender
{
	if ([UIPopoverController dismissActiveController]) {
		[[UIApplication myRootViewController] destroyWidgetInProgress];
		return;
	}

	// give me a view controller for my camera
	UIImagePickerController* pickerController = [UIImagePickerController initPhotoAlbumViewController:self];

	// none? - use normal settings
	if (pickerController != nil) {

		if ([UIApplication isPad]) {
			// Thumbnails exist in a popover
			[self setImagePickerController:pickerController];

			UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:pickerController];
			[UIPopoverController setActiveController:popover];

//			CGRect size = [UIApplication myMainScreenBounds];
			//			[popover setPopoverContentSize:viewSize];
			[popover setDelegate:self];

			[popover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
/*

			CGRect position = CGRectMake(size.size.width / 2.0, 120.0, 1, 1);

			[popover presentPopoverFromRect:position inView:[parentViewController view]
				   permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
*/
		}
		else {
			[UIApplication PushNavigationBarDarkGreyColour];
			[parentViewController presentViewController:pickerController animated:YES completion:nil];
		}
	}
}

//-----------------------------------------------------------------
- (void) imagePickerController:(UIImagePickerController*)imagePicker
						didFinishPickingMediaWithInfo:(NSDictionary*)MediaInfo
{
	[self setIsProcessing:YES];
	[[UIApplication myRootViewController] completeWidgetInProgress];

	[UIImagePickerController didFinishPickingMediaWithWidget:MediaInfo widget:self];
	[self updateDatabaseFromWidget];

	if ([UIApplication isPad]) {
		[UIPopoverController dismissActiveController];
	}
	else {
		[[UIApplication myRootViewController] dismissViewControllerAnimated:NO completion:nil];
	}
	[UIApplication PopNavigationBarColour];
	[[UIApplication sharedApplication] setStatusBarHidden:YES];

	[self setIsProcessing:NO];
}

//-----------------------------------------------------------------
- (void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	if ([UIApplication isPad]) {
		[UIPopoverController dismissActiveController];
	}
	else {
		[picker dismissViewControllerAnimated:NO completion:nil];
	}

	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[UIApplication PopNavigationBarColour];

	[[UIApplication myRootViewController] destroyWidgetInProgress];
}

//-----------------------------------------------------------------
- (void) popoverControllerDidDismissPopover:(UIPopoverController*)popoverController
{
	[[UIApplication sharedApplication] setStatusBarHidden:YES];

	[UIApplication PopNavigationBarColour];
	[[UIApplication myRootViewController] destroyWidgetInProgress];

	[UIPopoverController dismissActiveController];
}

//-----------------------------------------------------------------
- (void) createSettingsMenuWithRoot:(QRootElement*)root withViewController:(UIViewController*)parentViewController
{
	root.title    = @"Photo";
	root.grouped  = YES;

	//----------------
/*	QSection* iconSection = [root AddNewSection];

	QButtonElement* editButton = [[QButtonElement alloc] initWithTitle:@"Edit"];
	[editButton setControllerAction:@"showImageEditor"];
	[iconSection addElement:editButton];
*/
	//----------------
#if 0
		QSection* imageSection = [root addNewSection];

//	[imageSection setTitle:@"Image"];
	[imageSection setFooter:@"Select the image for this widget"];

	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

		QLabelElement* cameraLabel = [[QLabelElement alloc] initWithTitle:@"Take photo" Value:@""];
		[cameraLabel setControllerAction:@"showCamera"];
		[imageSection addElement:cameraLabel];
	}

	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {

		QLabelElement* photoRollLabel = [[QLabelElement alloc] initWithTitle:@"Choose from library" Value:@""];
		[photoRollLabel setControllerAction:@"showPhotoPicker:"];
		[imageSection addElement:photoRollLabel];
	}

	QLabelElement* iconLabel = [[QLabelElement alloc] initWithTitle:@"Choose a preset icon" Value:@""];
	[iconLabel setControllerAction:@"showIconPicker"];
	[imageSection addElement:iconLabel];
#endif

	//----------------
	QSection* optionsSection = [root addNewSection];
	QBooleanElement* aspectRatio = [[QBooleanElement alloc] initWithTitle:@"Maintain aspect ratio" BoolValue:[self keepAspectRatio]];
	[aspectRatio setKey:@"aspectRatio"];
	[optionsSection addElement:aspectRatio];
}

//-----------------------------------------------------------------
- (void) saveSettingsFromMenu:(QRootElement*)optionsRoot
{
	id element = [optionsRoot elementWithKey:@"aspectRatio"];
	[self setKeepAspectRatio:[element boolValue]];

	[super saveSettingsFromMenu:optionsRoot];
	[self setIsDirty:YES];
	[self updateDatabaseFromWidget];
}

//-----------------------------------------------------------------
#define kAspectRatioKey					@"aspect"
#define khighResolutionImageFilePath	@"highResolutionImageFilePath"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	[super encodeWithCoder:encoder];

	[encoder encodeBool:	[self keepAspectRatio] 				forKey:kAspectRatioKey];
	[encoder encodeObject:	[self highResolutionImageFilePath]	forKey:khighResolutionImageFilePath];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super initWithCoder:decoder];

	if (self) {
		BOOL keepAspectRatio = [decoder decodeBoolForKey:kAspectRatioKey];
		[self setKeepAspectRatio:keepAspectRatio];

		NSString* p = [decoder decodeObjectForKey:khighResolutionImageFilePath];
		_highResolutionImageFilePath = p;
	}
	return self;
}

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	if (self) {
		[self setScale:1.0f];
		[self setSize:[UIImage getThumbnailPhotoSize]];
		[self setKeepAspectRatio:YES];
	}
	return self;
}

//-----------------------------------------------------------------
@end

