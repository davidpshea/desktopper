//
//  DTTimerWidget.m
//  DeskTopper
//
//  Created by David Shea on 20/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#if 0

#import "QuickDialogHelper.h"
//#import "UIImageExtras.h"
#import "DTViewController.h"
#import "DTTimerWidget.h"

@implementation DTTimerWidget

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
//	return @"78-stopwatch";
	return @"movies";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Timer";
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UIViewController*) controller
{
//	[self showSettingsModal:controller];
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(UIViewController*) controller
{
	return YES;
}

//-----------------------------------------------------------------
- (void) createSettingsMenuWithRoot:(QRootElement*)root withViewController:(UIViewController*)parentViewController
{
	root.title    = @"Timer";
	root.grouped  = YES;

	//----------------
	QSection* pIconSection = [root addNewSection];

    [pIconSection setTitle:@"Timer"];
}

//-----------------------------------------------------------------
- (void) saveSettingsFromMenu:(QRootElement*)optionsRoot
{
}

//-----------------------------------------------------------------
- (void) encodeWithCoder:(NSCoder*)encoder
{
	[super encodeWithCoder:encoder];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super initWithCoder:decoder];

	if (self) {
	}
	return self;
}

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	if (self) {
		[self setScale:1.0f];
		[self setIconIndex:1];
		[self setSize:CGSizeMake(320/2, 240/2)];
	}
	return self;
}

//-----------------------------------------------------------------
@end

#endif
