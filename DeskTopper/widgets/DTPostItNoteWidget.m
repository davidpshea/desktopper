//
//  DTPostItNoteWidget.m
//  DeskTopper
//
//  Created by David Shea on 23/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTPostItNoteWidget.h"
#import "QuickDialogHelper.h"
#import "UIImageExtras.h"
#import "DTViewController.h"
#import "UIApplication+Helpers.h"
#import "DTNavigationController.h"
#import "DTWidgetStyle.h"
#import "NSString+Markdown.h"
#import "DTPopupTextController.h"
#import "DTHTMLViewController.h"
#import "UIColor+StandardColours.h"
#import "DTColor+HTML.h"
#import "NSCoder+DefaultDecoding.h"
#import "DTDocumentSharing.h"
#import "DTWidgetImageView.h"
#import "DTOptions.h"
#import "NSString+Macros.h"
#import "UIPopoverController+Stack.h"
#import "UIActionSheet+Tools.h"

//-----------------------------------------------------------------
@implementation DTPostItNoteWidget

@synthesize text					= _text;
@synthesize HTML					= _HTML;
@synthesize HTMLController			= _HTMLController;
@synthesize browserController		= _browserController;
@synthesize popupTextController		= _popupTextController;
@synthesize backgroundColour		= _backgroundColour;
@synthesize textColour				= _textColour;
@synthesize linkColour				= _linkColour;
@synthesize textScale				= _textScale;
@synthesize cursorPosition			= _cursorPosition;
@synthesize fontName				= _fontName;
@synthesize fontSize				= _fontSize;
@synthesize styleSheetIndex			= _styleSheetIndex;
@synthesize conversionOperation		= _conversionOperation;
@synthesize parentViewController	= _parentViewController;
@synthesize imageNeedsGenerating	= _imageNeedsGenerating;


//-----------------------------------------------------------------
- (void) setStyleSheetIndex:(int)styleSheetIndex
{
	_styleSheetIndex = styleSheetIndex;
}

//-----------------------------------------------------------------
- (int) styleSheetIndex
{
	return _styleSheetIndex;
}

//-----------------------------------------------------------------
+ (NSString*) getIconName
{
	return @"notes";
}

//-----------------------------------------------------------------
+ (NSString*) getUserName
{
	return @"Note";
}

//------------------------------------------------------------------
static NSArray* staticFontNames = nil;

+ (NSArray*) getFontNameArray
{
	if (! staticFontNames) {
		staticFontNames = [
			[NSArray alloc] initWithObjects:
			@"Times New Roman",
			@"Courier New",
			@"Helvetica",
			@"Marker Felt",
			@"Noteworthy",
			@"Trebuchet MS",
			@"SnellRoundhand",
			@"Papyrus",
			@"Palatino",
			@"Copperplate",
			@"Baskerville",
			@"ArialMT",
			@"AmericanTypewriter",
			nil
		];
	}

	return staticFontNames;
}

//------------------------------------------------------------------
- (id) copyWithZone:(NSZone*)zone
{
	DTPostItNoteWidget* another = [super copyWithZone:zone];

	[another setHTML:				nil];
	[another setHTMLController:		nil];
	[another setPopupTextController:nil];
	[another setBackgroundColour:	[[self backgroundColour] copy]];
	[another setTextColour:			[[self textColour] copy]];
	[another setLinkColour:			[[self linkColour] copy]];
	[another setTextScale:			[self textScale]];
	[another setFontName:			[self fontName]];
	[another setFontSize:			[self fontSize]];
	[another setStyleSheetIndex:	[self styleSheetIndex]];
	[another setText:				[[self text] copy]];
	[another setIsDirty:			YES];
	[another setImageNeedsGenerating: YES];

	return another;
}

//-----------------------------------------------------------------
static UIImage* placeholder = nil;

- (UIImage*) image
{
	UIImage* image = [super image];

	if (!image) {
		if (! placeholder) {
			CGSize size = [UIImage getThumbnailPhotoSize];
			placeholder = [UIImage createColouredRectangle:size colour:[UIColor lightGrayColor]];
		}
		return placeholder;
	}

	return image;
}

//-----------------------------------------------------------------
- (void) setFontName:(NSString*)fontName
{
	// Don't allow a nil font name - IOS doesn't like it...
	if (fontName) {
		[self setIsDirty:YES];
		[self setImageNeedsGenerating:YES];

		_fontName = fontName;
	}
}

//-----------------------------------------------------------------
- (void) setFontSize:(NSString*)fontSize
{
	if ([fontSize compare:_fontSize] != NSOrderedSame) {
		_fontSize = fontSize;
		[self setImageNeedsGenerating:YES];
	}
}

//-----------------------------------------------------------------
- (void) updateImageFromText
{
	BOOL hasImage = ([self imageFilePath] != nil);

	if ([self imageNeedsGenerating]) {

		UIWebView* webView	= [[self HTMLController] webView];
		UIImage* viewImage  = [UIImage createFromView:webView];
		NSString* fileName  = [viewImage saveToUniqueImageFile:nil];

		[self setImageFilePath:fileName withImage:viewImage];
		[self setImageNeedsGenerating:NO];
		
		// If this is the first time it has an image...
		if (! hasImage) {
			CGSize imageSize = [viewImage size];
			imageSize.width  /= 2;
			imageSize.height /= 2;
			[self setSize:imageSize];
		}

		// Update widget with new image name as possibly size too
		[self updateDatabaseFromWidget];
	}
}

//-----------------------------------------------------------------
#if 0
- (void) hasFinishedResizing:(CGSize)size
{

	CGRect rect = CGRectMake(0, 0, size.width, size.height);
	UIImage* clippedImage = [[self image] maskImageWithArea:rect];

	[self setImage:clippedImage];
}
#endif

//-----------------------------------------------------------------
- (void) popupTextControllerCancelButtonPressed
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"text - cancel");
	
	DTPopupTextController* editController = [self popupTextController];

	[self setCursorPosition:[editController cursorPosition]];
	[editController dismissKeyboard];

	[[UIApplication myRootViewController] destroyWidgetInProgress];

	[self setPopupTextController:nil];
	[editController dismissViewControllerAnimated:NO completion:nil];
}

//-----------------------------------------------------------------
- (void) popupTextControllerSaveButtonPressed
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"text - save");

	DTPopupTextController* editController = [self popupTextController];

	[self setText:[editController text]];

	[self setCursorPosition:[editController cursorPosition]];

	[[UIApplication myRootViewController] completeWidgetInProgress];

	[self setIsDirty:YES];
	[self updateDatabaseFromWidget];
	[self setImageNeedsGenerating:YES];

	// We've created a new image, so the desktop will need regenerating
	DTDesktop* desktop = [[UIApplication myRootViewController] desktop];
	[desktop setThumbnailIsDirty:YES];

	//	[self setPopupTextController:nil];

	if ([self HTMLController]) {
		[[self popupTextController] dismissViewControllerAnimated:NO completion:nil];
	}
	else {
		[editController dismissViewControllerAnimated:NO completion:^{
			[self displayHTMLNote:[self parentViewController] animated:NO];
		}];
	}
}

//-----------------------------------------------------------------
- (void) popupTextController:(DTPopupTextController*)controller willDismissWithText:(NSString*)text
{
	DT_DEBUG_LOG("Leaving text edit");
	//[self popupTextControllerSaveButtonPressed];
}

//-----------------------------------------------------------------
- (DTPopupTextController*) createPopupTextControllerCommon
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"text - edit popup");

	// Create view controller for web page
	DTPopupTextController* editController = [[DTPopupTextController alloc] init];
	[editController setText:[self text]];
	[editController setCursorPosition:[self cursorPosition]];
	[editController setTarget:self];
	[editController setPopupTextControllerDelegate:self];
	[editController setTextColour:[self textColour]];
	[editController setBackgroundColour:[self backgroundColour]];
	[self setPopupTextController:editController];

	if ([self fontName]) {
		UIFont* font = [UIFont fontWithName:[self fontName] size: 17.0];
		[editController setFont:font];
	}

	return editController;
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UIViewController*)controller
{
	LOG_FN();
	[self showCreationSettings:controller  fromSender:nil];
}

//-----------------------------------------------------------------
- (void) editFromMenuButton
{
	DTPopupTextController* controller = [self createPopupTextControllerCommon];
	[[UIApplication myPresentedViewController] presentViewController:controller animated:NO completion:nil];
}

//-----------------------------------------------------------------
- (void) HTMLViewDoneButtonPressed
{
	LOG_FN();
	DTHTMLViewController* HTMLController = [self HTMLController];
/*
	float scale = [HTMLController textScale];
	[self setTextScale:scale];
*/
	// Update font size from controller in case user changed it on the fly
	[self setFontSize:[HTMLController fontSize]];
	[self updateImageFromText];
	[HTMLController removeLinkWidget];

	[HTMLController dismissViewControllerAnimated:YES completion:^{
		[self setHTMLController:nil];
	}];
}

//-----------------------------------------------------------------
- (void) HTMLViewEditButtonPressed
{
	LOG_FN();
	[self handleDoubleTap:[UIApplication myPresentedViewController]];
}

//-----------------------------------------------------------------
- (void) displayHTMLNote:(UIViewController*)controller animated:(BOOL)animated
{
	LOG_FN();
	// Create view controller for web page
	DTHTMLViewController* HTMLController = [[DTHTMLViewController alloc] initWithString:[self HTML]];
	[self setHTMLController:HTMLController];

	DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:HTMLController];

	[navigationController addDoneButton:@selector(HTMLViewDoneButtonPressed) forTarget:self];
	[navigationController addEditButton:@selector(HTMLViewEditButtonPressed) forTarget:self];
	[navigationController setTranslucent: [UIApplication isIOS7]];
	[navigationController setWidget:self];
	[navigationController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];

	// let viewer know about controller
	[HTMLController setNavigationController:navigationController];

	// View background colour
	[[HTMLController webView] setBackgroundColor:[UIColor blackColor]];
	// HTML background colour
	[HTMLController setBackgroundColour:[self backgroundColour]];
	[HTMLController setTextColour:[self textColour]];
	[HTMLController setLinkColour:[self linkColour]];
	[HTMLController setTextScale:[self textScale]];
	[HTMLController setFontName:[self fontName]];
	[HTMLController setFontSize:[self fontSize]];

	if ([self styleSheetIndex] != NSNotFound) {
		DTOptionArray* array = [[UIApplication myOptions] styleSheets];
		DTShortcut* style    = [array getShortcutFromIndex:[self styleSheetIndex]];
		[HTMLController setCSS:[style text]];
	}
	[HTMLController setText:[self HTML]];

	// Show it...
	[controller presentViewController:navigationController animated:animated completion:nil];
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(UIViewController*)controller
{
	[self displayHTMLNote:controller animated:YES];
	return YES;
}


//-----------------------------------------------------------------
- (void) setText:(NSString*)text
{
	_text = text;

	// Expand macros in text...
	NSString* translatedText = [text expandMacros];

	// Create HTML text for view...
	[self setHTML:[translatedText createHTMLFromMarkdown]];

	[[self HTMLController] setText:[self HTML]];
}

//-----------------------------------------------------------------
- (void) actionButtonAction:(UIBarButtonItem*)button
{
	TESTFLIGHT_CHECKPOINT(@"text - action button");

	// If pressing button and it's already active, simply dismiss the actionsheet
	if ([UIPopoverController dismissActiveController]) {
		return;
	}

	UIActionSheet* sheet =
	[
		[UIActionSheet alloc] initWithTitle:nil
		delegate:self
		cancelButtonTitle:nil
		destructiveButtonTitle:nil
		otherButtonTitles:
		nil
	];
	[sheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];

	[sheet addButtonWithTitle:@"Email text"		menuIndex:1];
	[sheet addButtonWithTitle:@"Email HTML"		menuIndex:2];
	[sheet addButtonWithTitle:@"Copy text"		menuIndex:3];
	[sheet addButtonWithTitle:@"Copy HTML"		menuIndex:4];
	[sheet addButtonWithTitle:@"Open in..."		menuIndex:5];
	[sheet addButtonWithTitle:@"Cancel"			menuIndex:6];
	[sheet setCancelButtonIndex:[sheet getButtonIndexFromMenuIndex:6]];

	[UIPopoverController setActiveController:sheet];
	[sheet showFromBarButtonItem:button animated:YES];
}

//-----------------------------------------------------------------
- (void) actionSheet:(UIActionSheet*)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	UIPasteboard* clipboard;

	DT_DEBUG_LOG("%d", [actionSheet getMenuIndexFromButtonIndex:(int)buttonIndex]);

	[UIPopoverController setActiveController:nil];

	switch ([actionSheet getMenuIndexFromButtonIndex:(int)buttonIndex]) {

		case 1:
			TESTFLIGHT_CHECKPOINT(@"text - email text");
			[DTDocumentSharing sendEmail:@"A Note from Total Notes (as text)" body:[self text] isHTML:NO imageFileName:nil];
			break;

		case 2:
			TESTFLIGHT_CHECKPOINT(@"text - email HTML");
			[DTDocumentSharing sendEmail:@"Note from Total Notes (as HTML)" body:[self HTML] isHTML:YES imageFileName:nil];
			break;

		case 3:
			TESTFLIGHT_CHECKPOINT(@"text - copy text");
			clipboard = [UIPasteboard generalPasteboard];
			[clipboard setString:[self text]];
			break;

		case 4:
			TESTFLIGHT_CHECKPOINT(@"text - copy HTML");
			clipboard = [UIPasteboard generalPasteboard];
			[clipboard setString:[self HTML]];
			break;

		case 5:
			TESTFLIGHT_CHECKPOINT(@"text - export");
			[DTDocumentSharing exportString:[self text] as:@"public.text"];
			break;

		default:
			break;
	}
}

//-----------------------------------------------------------------
- (void) popoverControllerDidDismissPopover:(UIPopoverController*)popoverController
{
	[UIPopoverController setActiveController:nil];
}

//-----------------------------------------------------------------
enum colourIdentifier
{
	backgroundColourIdentifier,
	textColourIdentifier,
	linkColourIdentifier
};

- (BOOL) iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender
{
	LOG_FN();
	UIButton* button			 = (UIButton*)sender;
	int tag			 			 = (int)[button tag];
	QRootElement* activeMenuRoot = [DTWidget getActiveMenuRoot];

	UIColor* colour  = [[UIColor getStandardColours] objectAtIndex:tag];
	CGSize size		 = CGSizeMake(COLOUR_ICON_IMAGE_WIDTH, COLOUR_ICON_IMAGE_HEIGHT);

	QLabelElement*	element = nil;
	UIImage*		image	= nil;

	switch ([[imagePicker userObject] intValue]) {

		case backgroundColourIdentifier:
			TESTFLIGHT_CHECKPOINT(@"text - bk colour");
			[self setBackgroundColour:colour];
			element	= (QLabelElement*)[activeMenuRoot elementWithKey:@"backgroundColour"];
			image	= [UIImage createColouredRectangleWithContrastingBorder:size colour:[self backgroundColour]];
			break;

		case textColourIdentifier:
			TESTFLIGHT_CHECKPOINT(@"text - text colour");
			[self setTextColour:colour];
			element	= (QLabelElement*)[activeMenuRoot elementWithKey:@"textColour"];
			image	= [UIImage createColouredRectangleWithContrastingBorder:size colour:[self textColour]];
			break;

		case linkColourIdentifier:
			TESTFLIGHT_CHECKPOINT(@"text - link colour");
			[self setLinkColour:colour];
			element	= (QLabelElement*)[activeMenuRoot elementWithKey:@"linkColour"];
			image	= [UIImage createColouredRectangleWithContrastingBorder:size colour:[self linkColour]];
			break;

		default:
			break;
	}

	if (element && image) {
		[element setImage:image];

		if ([UIApplication isPad]) {
			[DTWidget activeMenuReloadData];

			// iPad gets a live update
			[self saveSettingsFromMenu:activeMenuRoot];
		}
	}

	[[UIPopoverController getActivePopoverController] popViewController];
	[[UIPopoverController getActivePopoverController] setPopoverContentSize:[UIImage getStandardPopoverSize] animated:NO];

	return YES;
}

//-----------------------------------------------------------------
- (void) colourViewCancelButtonPressed
{
	[[UIApplication myPresentedViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) colourFromMenuButton:(int)identifier
{
	BOOL isPad = [UIApplication isPad];
	
	NSString* title;
	switch (identifier) {
		case backgroundColourIdentifier:
			title = @"Background Colour";
			break;

		default:
		case textColourIdentifier:
			title = @"Text Colour";
			break;

		case linkColourIdentifier:
			title = @"Link Colour";
			break;
	}
	
	CustomImagePicker* picker = [[CustomImagePicker alloc] init];
	[picker setIconWidth:48.0];
	[picker setIconHeight:48.0];
	[picker setIconMargin:6.0];

	CGRect screen = [UIApplication myMainScreenBoundsRotated];

	CGSize size = [UIApplication isPad] ?
		[picker sizeForIconGridWithWidth:11 height:10] :
		CGSizeMake(screen.size.width, screen.size.height)
	;

	[picker createFromColourListWithTitle:title andViewSize:size];
	[picker setDelegate:self];

	// We use the user object to distinguish between text and background colour
	// Background colour has this set, text colour it's left as nil
	[picker setUserObject:[NSNumber numberWithInt:identifier]];

	if (isPad) {
		[[UIPopoverController getActivePopoverController] pushViewController:picker];

		[picker setViewSize:size];
		[[UIPopoverController getActivePopoverController] setPopoverContentSize:size animated:NO];
	}
	else {		
		DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:picker];
		
		[navigationController addCancelButton:@selector(colourViewCancelButtonPressed) forTarget:self];
		[navigationController setTranslucent:NO];
		[navigationController setWidget:self];
		//	[[navigationController bottomToolbar] setHidden:YES];
		[navigationController setBottomToolbarHidden:YES animated:NO];
		
		[[DTWidget getActiveMenuController]
		 presentViewController:navigationController animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) setCellFontForLabel:(UILabel*)label
{
	NSString* fontName = [label text];
	UIFont* font = nil;

	if (fontName) {
		font = [UIFont fontWithName:fontName size: 17.0];
	}

	if (!font) {
		// Regular fonts for other cells - needed since cells are
		// reused, and if we don't set it, it takes old cell values
		font = [UIFont systemFontOfSize:17];
	}

	if (font) {
		[label setFont:font];
	}
}

//-----------------------------------------------------------------
-(void) cell:(QTableViewCell*)cell willAppearForElement:(QElement*)element atIndexPath:(NSIndexPath*)indexPath
{
	LOG_FN();

	NSInteger section = [indexPath indexAtPosition:0];

	if (section != 99) {
		[self setCellFontForLabel:[cell textLabel]];
		[self setCellFontForLabel:[cell detailTextLabel]];
	}
}

//-----------------------------------------------------------------
- (void) createSettingsMenuWithRoot:(QRootElement*)root withViewController:(UIViewController*)parentViewController
{
	root.title    = @"Settings";
	root.grouped  = YES;

	TESTFLIGHT_CHECKPOINT(@"text - settings");

	//----------------
	QSection* colourSection = [root addNewSection];
	[colourSection setTitle:@"Colours"];

	CGSize size = CGSizeMake(COLOUR_ICON_IMAGE_WIDTH, COLOUR_ICON_IMAGE_HEIGHT);
	QLabelElement* backgroundColour = [[QLabelElement alloc] initWithTitle:@"Background Colour" Value:@""];
	UIImage* backgroundColourImage  = [UIImage createColouredRectangleWithContrastingBorder:size colour:[self backgroundColour]];
	[backgroundColour setImage:backgroundColourImage];
	[backgroundColour setKey:@"backgroundColour"];
	[backgroundColour setControllerName:@"DTWidgetQuickDialogController"];
	backgroundColour.onSelected = ^{
		[self colourFromMenuButton:backgroundColourIdentifier];
	};
	[colourSection addElement:backgroundColour];

	QLabelElement* textColour = [[QLabelElement alloc] initWithTitle:@"Text Colour" Value:@""];
	UIImage* textColourImage  = [UIImage createColouredRectangleWithContrastingBorder:size colour:[self textColour]];
	[textColour setImage:textColourImage];
	[textColour setKey:@"textColour"];
	textColour.onSelected = ^{
		[self colourFromMenuButton:textColourIdentifier];
	};
	[colourSection addElement:textColour];

	QLabelElement* linkColour = [[QLabelElement alloc] initWithTitle:@"Link Colour" Value:@""];
	UIImage* linkColourImage  = [UIImage createColouredRectangleWithContrastingBorder:size colour:[self linkColour]];
	[linkColour setImage:linkColourImage];
	[linkColour setKey:@"linkColour"];
	linkColour.onSelected = ^{
		[self colourFromMenuButton:linkColourIdentifier];
	};
	[colourSection addElement:linkColour];

	//----------------
	QSection* styleSection = [root addNewSection];
	[styleSection setTitle:@"Styles"];

	// Give everything in this section black lettering
	QAppearance* appearance = [[styleSection rootElement] appearance];
	[appearance setValueColorEnabled:[UIColor blackColor]];

	DTOptionArray* styles      = [[UIApplication myOptions] styleSheets];
	NSMutableArray* styleArray = [styles asArray];

	QRadioElement* styleElement = [[QRadioElement alloc] initWithItems:styleArray selected:[self styleSheetIndex]];
	[styleElement setTitle:@"Style"];
	[styleElement setKey:@"style"];
	[styleElement setSelected:[self styleSheetIndex]];
	[styleElement setControllerName:@"DTWidgetQuickDialogController"];
	[styleElement setPresentationMode:QPresentationModeNormal];

	if ([UIApplication isPad]) {
		// Live updating of style...
		[styleElement setOnSelected: ^{
			[self saveSettingsFromMenu:root];
		}];
	}

	[styleSection addElement:styleElement];

	//----------------
	NSArray* fontNames = [DTPostItNoteWidget getFontNameArray];

	// Try and find font name in font list
	NSUInteger selectedIndex = [fontNames indexOfObject:[self fontName]];

	QRadioElement* fontElement = [[QRadioElement alloc] initWithItems:fontNames selected:selectedIndex];
	[fontElement setTitle:@"Font"];
	[fontElement setKey:@"font"];
	[fontElement setSelected:selectedIndex];
	[fontElement setPresentationMode:QPresentationModeNormal];
	[fontElement setControllerName:@"DTWidgetQuickDialogController"];

	QSection* itemsSection = [fontElement getVisibleSectionForIndex:0];
	int fontIndex = 0;

	for (QElement* item in itemsSection.elements) {

		QAppearance* fontAppearance = [[QAppearance alloc] init];

		UIFont* font = [UIFont fontWithName:[fontNames objectAtIndex:fontIndex] size: 17.0];
		[fontAppearance setLabelFont:font];

		[item setAppearance:fontAppearance];
		fontIndex++;
	}

	if ([UIApplication isPad]) {
		// Live updating of font...
		fontElement.onSelected = ^{
			[self saveSettingsFromMenu:root];
		};
	}

	[styleSection addElement:fontElement];

	//----------------
	NSMutableArray* sizeNames = [[NSMutableArray alloc] init];
	for (int fontSize = 50; fontSize < 400; fontSize += 50) {
		NSString* option = [NSString stringWithFormat:@"%d%%", fontSize];
		[sizeNames addObject:option];
	}
	
	selectedIndex = [sizeNames indexOfObject:[self fontSize]];

	QRadioElement* sizeElement = [[QRadioElement alloc] initWithItems:sizeNames selected:selectedIndex];

	[sizeElement setTitle:@"Font Size"];
	[sizeElement setKey:@"fontSize"];
	[sizeElement setSelected:selectedIndex];
	[sizeElement setPresentationMode:QPresentationModeNormal];
	[sizeElement setControllerName:@"DTWidgetQuickDialogController"];

	if ([UIApplication isPad]) {
		// Live updating of font size...
		sizeElement.onSelected = ^{
			[self saveSettingsFromMenu:root];
		};
	}

	[styleSection addElement:sizeElement];
}

//-----------------------------------------------------------------
- (void) saveSettingsFromMenu:(QRootElement*)optionsRoot
{
	// Get changed options
	QRadioElement* fontSection = (QRadioElement*)[optionsRoot elementWithKey:@"font"];
	NSString*      fontName    = (NSString*)[fontSection selectedItem];

	if (fontName) {
		TESTFLIGHT_CHECKPOINT(@"text - font changed");
		[self setFontName:fontName];
	}

	QRadioElement* sizeSection = (QRadioElement*)[optionsRoot elementWithKey:@"fontSize"];
	NSString*      sizeName    = (NSString*)[sizeSection selectedItem];
	
	if (sizeName) {
		TESTFLIGHT_CHECKPOINT(@"text - font size changed");
		[self setFontSize:sizeName];
	}

	QRadioElement* styleElement = (QRadioElement*)[optionsRoot elementWithKey:@"style"];
	NSInteger styleIndex = [styleElement selected];

	if (styleIndex != NSNotFound) {
		TESTFLIGHT_CHECKPOINT(@"text - style changed");
		[self setStyleSheetIndex:(int)styleIndex];
	}

	// If we have an underlying HTML view, update that as well...
	DTHTMLViewController* HTMLController = [self HTMLController];

	if (HTMLController) {
		[HTMLController setBackgroundColour:[self backgroundColour]];
		[HTMLController setTextColour:[self textColour]];
		[HTMLController setLinkColour:[self linkColour]];
		[HTMLController setFontName:[self fontName]];
		[HTMLController setFontSize:[self fontSize]];

		if ([self styleSheetIndex] != NSNotFound) {
			DTOptionArray* array = [[UIApplication myOptions] styleSheets];
			DTShortcut* style    = [array getShortcutFromIndex:[self styleSheetIndex]];
			[HTMLController setCSS:[style text]];
		}
		
		[HTMLController setText:[self HTML]];

		[HTMLController refreshView];
	}

	// Update views...
	[self setImageNeedsGenerating:YES];
	[self setIsDirty:YES];
	[self updateDatabaseFromWidget];
}

//-----------------------------------------------------------------
- (void) showCreationSettings:(UIViewController*)parentViewController fromSender:(id)sender
{
	LOG_FN();
	[self setParentViewController:parentViewController];
	DTPopupTextController* controller = [self createPopupTextControllerCommon];
	[parentViewController presentViewController:controller animated:NO completion:nil];
}

//-----------------------------------------------------------------
#define kTextKey			@"text"
#define kBackgroundColour	@"backgroundColour"
#define kTextColour			@"textColour"
#define kLinkColour			@"linkColour"
#define kTextScale			@"TextScale"
#define kCursorPosition		@"cursorPosition"
#define kFontName			@"fontName"
#define kFontSize			@"fontSize"
#define kStyleSheetIndex	@"styleSheetIndex"

- (void) encodeWithCoder:(NSCoder*)encoder
{
	[super encodeWithCoder:encoder];

	[encoder encodeObject:	[self text]				forKey:kTextKey];
	[encoder encodeObject:	[self backgroundColour] forKey:kBackgroundColour];
	[encoder encodeObject:	[self textColour] 		forKey:kTextColour];
	[encoder encodeObject:	[self linkColour] 		forKey:kLinkColour];
	[encoder encodeObject:	[self fontName] 		forKey:kFontName];
	[encoder encodeObject:	[self fontSize] 		forKey:kFontSize];
	[encoder encodeFloat:	[self textScale] 		forKey:kTextScale];
	[encoder encodeInt:		[self cursorPosition] 	forKey:kCursorPosition];
	[encoder encodeInt:		[self styleSheetIndex] 	forKey:kStyleSheetIndex];
}

//-----------------------------------------------------------------
- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super initWithCoder:decoder];

	if (self) {
		[self setBackgroundColour:	[decoder decodeObject:kBackgroundColour withDefault:[UIColor colorWithHTMLName:@"Yellow"]]];
		[self setTextColour:		[decoder decodeObject:kTextColour withDefault:		[UIColor colorWithHTMLName:@"Black"]]];
		[self setLinkColour:		[decoder decodeObject:kLinkColour withDefault:		[UIColor colorWithHTMLName:@"Blue"]]];
		[self setText:				[decoder decodeObjectForKey:kTextKey]];
		//		[self setFontName:			[decoder decodeObjectForKey:kFontName]];
		//		[self setFontSize:			[decoder decodeObject:kFontSize withDefault:@"100%"]];
		[self setTextScale:			[decoder decodeFloat:kTextScale withDefault:1.0]];
		[self setCursorPosition:	[decoder decodeInt:kCursorPosition withDefault:0]];

		int index = [decoder decodeInt:kStyleSheetIndex withDefault:0];
		if ((index < 0) || (index > 255))
		{
			index = 0;
		}

		[self setStyleSheetIndex:index];

		// Set these directly to prevent image regeneration
		_fontSize = [decoder decodeObject:kFontSize withDefault:@"100%"];
		_fontName = [decoder decodeObjectForKey:kFontName];

		if (! _fontName) {
			NSArray* fonts = [DTPostItNoteWidget getFontNameArray];
			_fontName = [fonts objectAtIndex:0];
		}
		[self setIconIndex:0];
		[[self style] setBorderWidth:0.0];
	}
	return self;
}

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	if (self) {
		[self setSize:				CGSizeMake(96, 96)];
		[self setBackgroundColour:	[UIColor colorWithHTMLName:@"Yellow"]];
		[self setTextColour:		[UIColor colorWithHTMLName:@"Black"]];
		[self setLinkColour:		[UIColor colorWithHTMLName:@"Blue"]];
		[self setFontSize:			@"100%"];
		[self setText:				@""];
		[self setTextScale:			1.0f];
		[self setStyleSheetIndex:	0];

		[self setIsDirty:				YES];
		[self setImageNeedsGenerating:	YES];

		NSArray* fonts = [DTPostItNoteWidget getFontNameArray];
		[self setFontName:[fonts objectAtIndex:0]];

		[[self style] setBorderWidth:0.0];
	}
	return self;
}

//-----------------------------------------------------------------
@end


