//
//  settingsbundle.h
//  DeskTopper
//
//  Created by David Shea on 28/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#if 0

#ifndef DeskTopper_settingsbundle_h
#define DeskTopper_settingsbundle_h

// Keys for the settings bundle
#define kPreferencesCountryKey		@"country"
#define kPreferencesCommentKey		@"comment"
#define kPreferencesNameKey			@"name_preference"
#define kPreferencesEnabledKey		@"enabled_preference"
#define kPreferencesSliderKey		@"slider_preference"

#endif
#endif
