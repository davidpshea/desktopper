//
//  YIPopupTextView.m
//  YIPopupTextView
//
//  Created by Yasuhiro Inami on 12/02/01.
//  Copyright (c) 2012 Yasuhiro Inami. All rights reserved.
//

#import "YIPopupTextView.h"
#import "UIApplication+Helpers.h"

//-----------------------------------------------------------------
#define IS_ARC              (__has_feature(objc_arc))
#define IS_IPAD             (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

//-----------------------------------------------------------------
//#define TEXTVIEW_INSETS     (IS_IPAD ? UIEdgeInsetsMake(30, 30, 30, 30) : UIEdgeInsetsMake(15, 15, 15, 15))
#define TEXTVIEW_INSETS     UIEdgeInsetsMake(0, 0, 0, 0)
#define TEXT_SIZE           (IS_IPAD ? 32 : 16)

#define ANIMATION_DURATION  0.25

//-----------------------------------------------------------------
typedef enum {
    CaretShiftDirectionNone,
    CaretShiftDirectionLeft,
    CaretShiftDirectionRight,
} CaretShiftDirection;


//-----------------------------------------------------------------
@interface YIPopupTextView () <UIGestureRecognizerDelegate>

- (void) startObservingNotifications;
- (void) stopObservingNotifications;

- (void) startCaretShiftTimer;
- (void) stopCaretShiftTimer;
- (void) shiftCaret;

@end

//-----------------------------------------------------------------
@implementation YIPopupTextView
{
    UIView*     _backgroundView;
    UIView*     _popupView;
	//    BOOL        _shouldAnimate;

    UIPanGestureRecognizer* _panGesture;
    CGPoint                 _panStartLocation;
    NSTimer*                _caretShiftTimer;
    CaretShiftDirection     _caretShiftDirection;
}

@synthesize	popupView				= _popupView;
@synthesize	respondsToNotifications = _respondsToNotifications;
@synthesize keyboardFrame			= _keyboardFrame;

@dynamic delegate;

//-----------------------------------------------------------------
- (id) initWithPlaceHolder:(NSString*)placeHolder maxCount:(NSUInteger)maxCount
{
    self = [super init];
    if (self) {
		//        _shouldAnimate = YES;



		CGRect frame = [UIApplication myMainScreenBounds];

        _backgroundView = [[UIView alloc] initWithFrame:frame];
		//        _backgroundView.backgroundColor = [UIColor redColor];
        _backgroundView.backgroundColor = [UIColor blackColor];
        _backgroundView.alpha = 1.0;
		_backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

		_popupView = [[UIView alloc] initWithFrame:frame];


			_popupView.autoresizingMask = UIViewAutoresizingFlexibleWidth; // height will be set at KeyboardWillShow
        [_backgroundView addSubview:_popupView];

        self.frame = UIEdgeInsetsInsetRect(_popupView.frame, TEXTVIEW_INSETS);
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

		//_popupView.edgesForExtendedLayout = UIRectEdgeNone;

        [self setKeyboardType:UIKeyboardTypeDefault];
        [self setAutocorrectionType:UITextAutocorrectionTypeYes];
        [self setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
		[self setFont:[UIFont systemFontOfSize:TEXT_SIZE]];
		[self setBackgroundColor:[UIColor whiteColor]];
		[self setTextColor:[UIColor blackColor]];

		[_popupView addSubview:self];

		[self setCaretShiftGestureEnabled:YES];
    }
    return self;
}

//-----------------------------------------------------------------
- (void) dealloc
{
	[self setRespondsToNotifications:NO];
	[self resignFirstResponder];

	[self setInputView:nil];
	[self setPopupView:nil];

    [self stopObservingNotifications];
}

//-----------------------------------------------------------------
- (BOOL) caretShiftGestureEnabled
{
    return _panGesture != nil;
}

//-----------------------------------------------------------------
- (void) setCaretShiftGestureEnabled:(BOOL)caretShiftGestureEnabled
{
    if (caretShiftGestureEnabled) {
        if (!_panGesture) {
            _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
            [_backgroundView addGestureRecognizer:_panGesture];
        }
    }
    else {
        if (_panGesture) {
            [_backgroundView removeGestureRecognizer:_panGesture];
            _panGesture = nil;
        }
    }

}

//-----------------------------------------------------------------
- (void) scrollCaretToVisible
{
	[self scrollRangeToVisible:[self selectedRange]];
}

//-----------------------------------------------------------------
#pragma mark -
#pragma mark Show/Dismiss

- (void) showInView:(UIView*)view HTMLView:(UIView*)HTMLView
{
	//    _shouldAnimate = YES;

    // TODO: show in window + orientation handling
    if (!view || [view isKindOfClass:[UIWindow class]]) {
        NSLog(@"Warning: show in window currently doesn't support orientation.");
    }

    UIView* targetView = nil;
    CGRect frame;
    if (!view) {
        targetView = [UIApplication sharedApplication].keyWindow;
        frame = [UIScreen mainScreen].applicationFrame;
    }
    else {
        targetView = view;
        frame = view.bounds;
    }

    _backgroundView.frame = frame;

	if (HTMLView) {
		[targetView addSubview:HTMLView];
		[HTMLView addSubview:_backgroundView];
	}
	else {
		[targetView addSubview:_backgroundView];
	}

    [self startObservingNotifications];
    [self becomeFirstResponder];
}

//-----------------------------------------------------------------
- (void) dismiss
{
    if ([self isFirstResponder]) {
        [self resignFirstResponder];
    }
}

//-----------------------------------------------------------------
#pragma mark -
#pragma mark Notifications

- (void) startObservingNotifications
{
    [[NSNotificationCenter defaultCenter]
		 addObserver:self
		selector:@selector(didReceiveKeyboardWillHideNotification:)
		name:UIKeyboardWillHideNotification
		object:nil
	 ];

    [[NSNotificationCenter defaultCenter]
		addObserver:self
		selector:@selector(didReceiveKeyboardWillShowNotification:)
		name:UIKeyboardWillShowNotification
		object:nil
	 ];

    [[NSNotificationCenter defaultCenter]
		addObserver:self
		selector:@selector(didReceiveTextDidChangeNotification:)
		name:UITextViewTextDidChangeNotification
		object:nil
	 ];

    [[NSNotificationCenter defaultCenter]
	 	addObserver:self
		selector:@selector(didReceiveTextDidEndEditingNotification:)
		name:UITextViewTextDidEndEditingNotification
	   object:nil
	 ];

	[self setRespondsToNotifications:YES];
}

//-----------------------------------------------------------------
- (void) stopObservingNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidEndEditingNotification object:nil];

	[self setRespondsToNotifications:NO];
}

//-----------------------------------------------------------------
- (void) didReceiveKeyboardWillHideNotification:(NSNotification*)notification
{
    if (!_backgroundView.superview) {
		return;
	}

	if (! [self respondsToNotifications]) {
		return;
	}

	NSDictionary* userInfo = [notification userInfo];

	DT_DEBUG_LOG("Will hide...");
    CGRect keyboardRect = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    DT_DEBUG_LOG(@"UIKeyboardFrameBeginUserInfoKey = %f %f %f %f",
				 keyboardRect.origin.x,keyboardRect.origin.y,keyboardRect.size.width,keyboardRect.size.height);

    keyboardRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    DT_DEBUG_LOG(@"UIKeyboardFrameEndUserInfoKey = %f %f %f %f",
				 keyboardRect.origin.x,keyboardRect.origin.y,keyboardRect.size.width,keyboardRect.size.height);

	[self setKeyboardFrame:keyboardRect];
}

//-----------------------------------------------------------------
/*
 NSDictionary *userInfo = [notification userInfo];
NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
CGFloat keyboardHeight = [keyboardBoundsValue CGRectValue].size.width;

CGFloat duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
NSInteger animationCurve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];

[UIView animateWithDuration:duration delay:0. options:animationCurve animations:^{
    [[self textView] setContentInset:UIEdgeInsetsMake(0., 0., keyboardHeight, 0.)];
    [[self view] layoutIfNeeded];
} completion:nil];

*/


- (void) didReceiveKeyboardWillShowNotification:(NSNotification*)notification
{
    if (!_backgroundView.superview) {
		return;
	}

	if (! [self respondsToNotifications]) {
		return;
	}

	NSDictionary* userInfo = [notification userInfo];

	DT_DEBUG_LOG("Will show...");
	#ifdef DEBUG
    CGRect keyboardRect = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
	#endif
    DT_DEBUG_LOG(@"UIKeyboardFrameBeginUserInfoKey = %f %f %f %f",
				 keyboardRect.origin.x,keyboardRect.origin.y,keyboardRect.size.width,keyboardRect.size.height);

	//    keyboardRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

    CGRect keyboardEndRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    DT_DEBUG_LOG(@"UIKeyboardFrameEndUserInfoKey = %f %f %f %f",
				 keyboardEndRect.origin.x,keyboardEndRect.origin.y,keyboardEndRect.size.width,keyboardEndRect.size.height);

	[self setKeyboardFrame:keyboardEndRect];

    CGPoint origin = [self.window convertPoint:_backgroundView.frame.origin fromView:_backgroundView];

    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

    CGFloat popupViewHeight = 0;

    switch (orientation) {
        case UIInterfaceOrientationPortrait:
            popupViewHeight = keyboardEndRect.origin.y - origin.y;
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            popupViewHeight = origin.y - keyboardEndRect.size.height;
            break;
        case UIInterfaceOrientationLandscapeLeft:
            popupViewHeight = keyboardEndRect.origin.x - origin.x;
            break;
        case UIInterfaceOrientationLandscapeRight:
            popupViewHeight = origin.x - keyboardEndRect.size.width;
            break;
        default:
            break;
    }

	//    CGRect frame = _backgroundView.bounds;


#if 1
	CGRect frame = [_popupView bounds];
	//    frame.size.height = (keyboardTopY - 12);
    frame.size.height = popupViewHeight;
	[_popupView setFrame:frame];

	frame = [_backgroundView bounds];
    frame.size.height = popupViewHeight;
	[_backgroundView setFrame:frame];

#else
	//	[self setContentInset: UIEdgeInsetsMake(50, 50, 100/*keyboardHeight*/, 0)];

	keyboardHeight = 300;

		UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0,  keyboardHeight, 0.0);
	//	UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, -100, 0.0);
	self.contentInset = contentInsets;
	// self.scrollIndicatorInsets = contentInsets;

#endif

	[self layoutIfNeeded];
	[self scrollCaretToVisible];

	//    if (_shouldAnimate) {

        // NOTE: textView popup animation doesn't work when using UIView animation
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
        animation.delegate = self;
        animation.duration = ANIMATION_DURATION / 2;
        animation.repeatCount = 0;
        animation.fromValue =[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 0.1)];
        animation.toValue =[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)];
        [_popupView.layer addAnimation:animation forKey:@"popupAnimation"];

        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            _backgroundView.alpha = 1;
        } completion:^(BOOL finished) {
			//            _shouldAnimate = NO;
			[self scrollCaretToVisible];

			[self  layoutIfNeeded];
		}];
	//    }

}

//-----------------------------------------------------------------
- (void) didReceiveTextDidChangeNotification:(NSNotification*)notification
{
	if ([notification object] != self) {
		return;
	}

	if (! [self respondsToNotifications]) {
		return;
	}

	//	NSRange range = self.selectedRange;
	[self scrollRangeToVisible:[self selectedRange]];

	if ([[self textViewDelegate] respondsToSelector:@selector(popupTextView:textDidChange:)]) {
        [[self textViewDelegate] popupTextView:self textDidChange:self.text];
	}
}

//-----------------------------------------------------------------
- (void) didReceiveTextDidEndEditingNotification:(NSNotification*)notification
{
	if ([notification object] != self) {
		return;
	}

	if (! [self respondsToNotifications]) {
		return;
	}

    [self stopObservingNotifications];

    if ([self.textViewDelegate respondsToSelector:@selector(popupTextView:willDismissWithText:)]) {
        [self.textViewDelegate popupTextView:self willDismissWithText:self.text];
    }
/*
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{

			_backgroundView.alpha = 0;
		}
		completion:^(BOOL finished) {

			if (finished) {
*/				if ([self.textViewDelegate respondsToSelector:@selector(popupTextView:didDismissWithText:)]) {
					[self.textViewDelegate popupTextView:self didDismissWithText:self.text];
				}

				[_backgroundView removeFromSuperview];
				_backgroundView = nil;
				_popupView = nil;
	//			}
	//		}
	//	];
}

//-----------------------------------------------------------------
- (void) handlePanGesture:(UIPanGestureRecognizer*)gesture
{
    if (self.dragging || self.decelerating) {
        [self stopCaretShiftTimer];
        return;
    }

    switch (gesture.state) {

        case UIGestureRecognizerStateBegan:
			{
				CGPoint translation = [gesture translationInView:gesture.view];
				if (translation.x > 0) {
					_caretShiftDirection = CaretShiftDirectionRight;
				}
				else if (translation.x < 0) {
					_caretShiftDirection = CaretShiftDirectionLeft;
				}
				else {
					_caretShiftDirection = CaretShiftDirectionNone;
				}

				if (_caretShiftDirection != CaretShiftDirectionNone) {
					_panStartLocation = [gesture locationInView:gesture.view];
					[self shiftCaret];
				}

				[self performSelector:@selector(startCaretShiftTimer) withObject:nil afterDelay:0.5];
			}
			break;

		case UIGestureRecognizerStateChanged:

            if (_caretShiftTimer) {
                CGPoint velocity = [gesture velocityInView:gesture.view];
                if (velocity.x > 0) {
                    _caretShiftDirection = CaretShiftDirectionRight;
                }
                else if (velocity.x < 0) {
                    _caretShiftDirection = CaretShiftDirectionLeft;
                }
            }

            break;

        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:

            [self stopCaretShiftTimer];
            break;

        default:
            break;
    }
}

//-----------------------------------------------------------------
- (void) startCaretShiftTimer
{
    if (!_caretShiftTimer) {
        _caretShiftTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(shiftCaret) userInfo:nil repeats:YES];
    }
}

//-----------------------------------------------------------------
- (void) stopCaretShiftTimer
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(startCaretShiftTimer) object:nil];

    [_caretShiftTimer invalidate];
    _caretShiftTimer = nil;

    _caretShiftDirection = CaretShiftDirectionNone;
    _panStartLocation = CGPointZero;
}

//-----------------------------------------------------------------
- (void) shiftCaret
{
    NSRange range = self.selectedRange;
    if (range.length == 0) {
        if (_caretShiftDirection == CaretShiftDirectionRight && range.location < self.text.length) {
            range.location += 1;
        }
        else if (_caretShiftDirection == CaretShiftDirectionLeft && range.location > 0) {
            range.location -= 1;
        }
    }
    else {
        // right caret
        if (_panStartLocation.x > _panGesture.view.frame.size.width/2) {
            if (_caretShiftDirection == CaretShiftDirectionRight && range.location+range.length < self.text.length) {
                range.length += 1;
            }
            else if (_caretShiftDirection == CaretShiftDirectionLeft && range.length > 0) {
                range.length -= 1;
            }
        }
        // left caret
        else {
            if (_caretShiftDirection == CaretShiftDirectionRight && range.length > 0) {
                range.location +=1;
                range.length -= 1;
            }
            else if (_caretShiftDirection == CaretShiftDirectionLeft && range.location > 0) {
                range.location -=1;
                range.length += 1;
            }
        }
    }

    self.selectedRange = range;
}

//-----------------------------------------------------------------

@end
