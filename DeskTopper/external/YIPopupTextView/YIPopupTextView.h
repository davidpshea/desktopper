//
//  YIPopupTextView.h
//  YIPopupTextView
//
//  Created by Yasuhiro Inami on 12/02/01.
//  Copyright (c) 2012 Yasuhiro Inami. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class YIPopupTextView;

//-----------------------------------------------------------------
@protocol YIPopupTextViewDelegate <UITextViewDelegate>

@optional
- (void)popupTextView:(YIPopupTextView*)textView willDismissWithText:(NSString*)text;
- (void)popupTextView:(YIPopupTextView*)textView didDismissWithText:(NSString*)text;
- (void)popupTextView:(YIPopupTextView*)textView textDidChange:(NSString*)text;

@end

//-----------------------------------------------------------------
@interface YIPopupTextView : UITextView

@property (nonatomic, weak) id <YIPopupTextViewDelegate> textViewDelegate;

@property (nonatomic, assign) BOOL		caretShiftGestureEnabled;
@property (nonatomic, strong) UIView*	popupView;
@property (nonatomic) 		  BOOL		respondsToNotifications;
@property (nonatomic) 		  CGRect	keyboardFrame;

//@property (nonatomic, strong) UIFont*	font;
//@property (nonatomic, strong) UIColor*	backgroundColour;
//@property (nonatomic, strong) UIColor*	textColour;

- (id)		initWithPlaceHolder:(NSString*)placeHolder maxCount:(NSUInteger)maxCount;
- (void)	showInView:(UIView*)view HTMLView:(UIView*)HTMLView;
- (void)	dismiss;

//-----------------------------------------------------------------
@end

