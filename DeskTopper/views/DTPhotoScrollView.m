//
//  DTPhotoScrollView.m
//  DeskTopper
//
//  Created by David Shea on 11/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTPhotoScrollView.h"

//-----------------------------------------------------------------
@implementation DTPhotoScrollView

- (void) layoutSubviews
{
	[super layoutSubviews];

	// Center the image as it becomes smaller than the size of the screen.
	UIView* zoomingSubview	= [[self delegate] viewForZoomingInScrollView:self];
	CGSize boundsSize		= self.bounds.size;
	CGRect frameToCenter	= [zoomingSubview frame];

	// Center horizontally.
	if (frameToCenter.size.width < boundsSize.width) {
		frameToCenter.origin.x = floorf((boundsSize.width - frameToCenter.size.width) / 2);
	} else {
		frameToCenter.origin.x = 0;
	}

	// Center vertically.
	if (frameToCenter.size.height < boundsSize.height) {
		frameToCenter.origin.y = floorf((boundsSize.height - frameToCenter.size.height) / 2);
	} else {
		frameToCenter.origin.y = 0;
	}

	[zoomingSubview setFrame:frameToCenter];
}

//-----------------------------------------------------------------
@end

