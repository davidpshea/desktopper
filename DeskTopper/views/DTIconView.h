//
//  DTIconView.h
//  DeskTopper
//
//  Created by David Shea on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SPUserResizableView.h"
@class DTViewController;

typedef BOOL(^singleTapCallbackType)(DTViewController* controller);

//-----------------------------------------------------------------
@interface DTIconView : UIImageView
//@interface DTIconView : SPUserResizableView

@property (nonatomic, copy) singleTapCallbackType singleTapCallback;

- (BOOL) handleSingleTap:(DTViewController*)controller;

@end
//-----------------------------------------------------------------

