//
//  DTIconView.m
//  DeskTopper
//
//  Created by David Shea on 10/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

#import "DTIconView.h"

@implementation DTIconView

@synthesize singleTapCallback = _singleTapCallback;

//-----------------------------------------------------------------
- (DTIconView*) init
{
	self = [super init];
	if (self) {
	}
	return self;
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(DTViewController*)controller
{
	DT_DEBUG_LOG(@"DTIconView:handleSingleTap");
	if (_singleTapCallback) {
		return _singleTapCallback(controller);
	}
	return NO;
}

//-----------------------------------------------------------------
//- (void)layoutSubviews
//{
//	CALayer* layer = [self layer];

//	NSArray* p = [layer sublayers];



//	DT_DEBUG_LOG(@"layer %f,%f", [layer

//    layer.bounds = self.bounds;
//    [layer setFrame:self.frame];
//}

//-----------------------------------------------------------------
@end

