//
//  DTWidgetImageView.h
//  DeskTopper
//
//  Created by David Shea on 23/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageExtras.h"
#import "DTWidget.h"
#import "DTIconView.h"

@class DTWidgetStyle;
@class DTWidgetControl;

@interface DTWidgetImageView : DTIconView

@property (nonatomic, weak) DTWidget*		widget;
@property (nonatomic, weak) DTWidgetStyle*	style;

- (DTWidgetImageView*)	initWithWidget:(DTWidget*)widget;

- (BOOL) handleSingleTap:(DTViewController*)controller;

- (void) removeBorderAndShadow;
- (void) addBorderAndShadow;

- (void) setBounds:(CGRect)bounds;

- (void) selectForDragging;
- (void) selectForEdit;
- (void) unSelect;

@end

