//
//  DTWidgetControl.h
//  DeskTopper
//
//  Created by David Shea on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTWidget.h"
#import "DTIconView.h"

//-----------------------------------------------------------------
enum ControlTag {
	resizeTopLeftTag = 2,
	resizeTopRightTag,
	resizeBottomLeftTag,
	resizeBottomRightTag,
	resizeLeftTag,
	resizeRightTag,
	resizeTopTag,
	resizeBottomTag
};

//-----------------------------------------------------------------
@interface DTWidgetControl : DTIconView

@property (nonatomic, weak) DTWidget* widget;

+ (void) createEditControls:(UIView*)containingView;
+ (void) updateEditControls:(DTWidget*)widget;
+ (void) updateEditControlsForWidget:(DTWidget*)widget;
+ (void) hideEditControls;
+ (void) showEditControls;
+ (void) removeEditControls;
//+ (void) setZoomScale:(float)zoomScale;
//+ (void) setZoomScaleFromScrollView:(UIScrollView*)scrollView;

+ (void) resizeWidget:(DTWidget*)widget usingControl:(id)objectHit
			sizeOffset:(CGPoint)difference initialBounds:(CGRect)bounds;

- (void) selectForEdit;
- (void) unselectForEdit;

//-----------------------------------------------------------------
@end

