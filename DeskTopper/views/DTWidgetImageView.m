//
//  DTWidgetImageView.m
//  DeskTopper
//
//  Created by David Shea on 23/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidgetImageView.h"
#import "DTWidgetControl.h"
#import "DTViewController.h"
//#import "UIImageView+Curled.h"
#import "UIImageView+ImageViewBorder.h"
#import "UIImageView+ImageViewShadow.h"
#import "DTWidgetStyle.h"
//#import "SPUserResizableView.h"

//-----------------------------------------------------------------
/*
 static const UIViewAnimationOptions animationOptions =
	UIViewAnimationOptionAllowUserInteraction  |
	UIViewAnimationOptionBeginFromCurrentState |
	UIViewAnimationOptionCurveLinear;
;
*/

//-----------------------------------------------------------------
@implementation DTWidgetImageView

@synthesize widget	= _widget;
@synthesize style	= _style;

//-----------------------------------------------------------------
- (DTWidgetImageView*) init
{
	self = [super init];
	if (self) {
	}
	return self;
}

//-----------------------------------------------------------------
- (void) updateShadow
{
	DTWidgetStyle* style = [self style];
	if (style) {
		[self addImageShadow:[style shadowSize]];
	}
}

//-----------------------------------------------------------------
- (void) addBorderAndShadow
{
	DTWidgetStyle* style = [self style];
	if (style) {
		[self configureImageViewBorder:[style borderWidth]];
		[self addImageShadow:[style shadowSize]];
	}
}

//-----------------------------------------------------------------
- (void) removeBorderAndShadow
{
//	[self configureImageViewBorder:0.0];
//	[self removeImageShadow];
}

//-----------------------------------------------------------------
- (DTWidgetImageView*) initWithWidget:(DTWidget*)widget
{
	// Initialise the image view
	self = [super initWithImage:[widget image]];

	[self setContentMode:UIViewContentModeScaleToFill];

	// Using stuff from the widget
	[self setCenter:[widget screenPosition]];
	[self setStyle:[widget style]];
	[self setUserInteractionEnabled:YES];

	// Link widget and view
	[self setWidget:widget];
	[widget setView:self];

	[self addBorderAndShadow];

	return self;
}

//-----------------------------------------------------------------
- (void) setBounds:(CGRect)bounds
{
	[super setBounds:bounds];
	// Update the shadow size to match the new image size
	[self updateShadow];
}

//-----------------------------------------------------------------
- (BOOL) handleSingleTap:(DTViewController*)controller
{
	DT_DEBUG_LOG(@"DTWidgetImageView:handleSingleTap");

	BOOL hasHandled = [super handleSingleTap:controller];

	if (! hasHandled) {
		DTDesktop* desktop  = [controller desktop];
		DTWidget* oldWidget = [desktop selectedWidget];

		if ([desktop isEditing]) {
			if (oldWidget == [self widget]) {
				// Single click on selected widget sends it to the back
				[[self superview] sendSubviewToBack:self];
			}

			[controller selectWidgetForEdit:[self widget]];
			hasHandled = YES;
		}
		else {
			hasHandled = [[self widget] handleSingleTap:controller];
		}
	}
	return hasHandled;
}

//-----------------------------------------------------------------
- (void) selectForDragging
{
	[self setAlpha:0.6];
	[self configureImageViewBorder:6 withColour:[UIColor colorWithRed:0.5 green:0.0 blue:0.0 alpha:1.0]];
}

//-----------------------------------------------------------------
- (void) selectForEdit
{
	[self setAlpha:0.75];
	[self configureImageViewBorder:4 withColour:[UIColor redColor]];
}

//-----------------------------------------------------------------
- (void) unSelect
{
	[self setAlpha:1.0];
	[self addBorderAndShadow];
}

@end
//-----------------------------------------------------------------

