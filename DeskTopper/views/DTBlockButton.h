//
//  DTBlockButton.h
//  DeskTopper
//
//  Created by David Shea on 8/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef BOOL(^touchCallbackType)(void);
typedef void(^touchCallbackType)(void);

//-----------------------------------------------------------------
@interface DTBlockButton : UIButton

@property (nonatomic, copy) touchCallbackType touchCallback;

+ (DTBlockButton*)	buttonWithType:(UIButtonType)type;

@end
//-----------------------------------------------------------------

