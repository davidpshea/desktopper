//
//  DTWidgetControl.m
//  DeskTopper
//
//  Created by David Shea on 2/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidgetControl.h"
#import "UIImageExtras.h"

//-----------------------------------------------------------------
/*
 static const UIViewAnimationOptions animationOptions =
	UIViewAnimationOptionAllowUserInteraction  |
	UIViewAnimationOptionBeginFromCurrentState |
	UIViewAnimationOptionCurveLinear;
;
*/

static const float widgetControlIconSize     = (52.0 / 2.0);
//static const float halfWidgetControlIconSize = (widgetControlIconSize / 2.0);

static const float minimumWidgetWidth  = 56.0;
static const float minimumWidgetHeight = 56.0;

//-----------------------------------------------------------------
static UIView*		_viewContainingEditControls;
static float		_zoomScale;

#if 0
static UIImageView* horizontalLineView = nil;
static UIImageView* verticalLineView   = nil;
#endif

//-----------------------------------------------------------------
static NSMutableArray* controls;

struct controlData {
	const char* 	imageName;
	enum ControlTag	tag;
	int				xOffset;
	int				yOffset;
};

static struct controlData controlDataArray[] = {
	{ "/resource/badge.png", resizeTopLeftTag,	  -1, -1 },
	{ "/resource/badge.png", resizeTopTag,		   0, -1 },
	{ "/resource/badge.png", resizeTopRightTag,	   1, -1 },
	{ "/resource/badge.png", resizeLeftTag,		  -1,  0 },
	{ "/resource/badge.png", resizeRightTag,  	   1,  0 },
	{ "/resource/badge.png", resizeBottomLeftTag, -1,  1 },
	{ "/resource/badge.png", resizeBottomTag,  	   0,  1 },
	{ "/resource/badge.png", resizeBottomRightTag, 1,  1 }
};

//-----------------------------------------------------------------
@implementation DTWidgetControl

@synthesize widget = _widget;

//-----------------------------------------------------------------
+ (void) createEditControls:(UIView*)containingView
{
	_viewContainingEditControls = containingView;
	_zoomScale = 1.0f;

	controls = [[NSMutableArray alloc] init];

	for (int i = 0; i < (sizeof(controlDataArray) / sizeof(controlDataArray[0])); i++) {

		NSString* imageName = [NSString stringWithUTF8String:(controlDataArray[i].imageName)];
		UIImage* image      = [UIImage imageFromResource:imageName];

		DTWidgetControl* control  = [[DTWidgetControl alloc] initWithImage:image];

		[control setCenter:CGPointMake(0 , 0)];
		[control setUserInteractionEnabled:YES];
		[control setHidden:YES];
		[control setTag:controlDataArray[i].tag];
		[containingView addSubview:control];

		[controls addObject:control];
	}
}

//-----------------------------------------------------------------
/*
  + (void) setZoomScale:(float)zoomScale
{
	_zoomScale = zoomScale;
}
*/
//-----------------------------------------------------------------
/*
  + (void) setZoomScaleFromScrollView:(UIScrollView*)scrollView
{
	float zoom = [scrollView zoomScale];
	if (zoom < 1.0) {
		_zoomScale = 1.0 / zoom;
	}
	else {
		_zoomScale = 1.0; //;
	}

	DT_DEBUG_LOG(@"Scroll zoom:%f, my zoom:%f", zoom, _zoomScale);
}
*/
//-----------------------------------------------------------------
/*
+ (BOOL) canDisplayScaleMarkers
{
	float buttonWidthX3 	  = widgetControlIconSize * 3;
	float actualButtonWidthX3 = (buttonWidthX3 * 3) * _zoomScale;

	return actualButtonWidthX3 < buttonWidthX3;
}
*/

//-----------------------------------------------------------------
+ (void) updateEditControls:(DTWidget*)widget
{
	if (! widget) {
		return;
	}
//	DT_DEBUG_LOG(@"updateEditControls with widget");

	CGPoint centre = [widget screenPosition];

	float iconSize = (_zoomScale) * widgetControlIconSize;
	CGSize size = [widget size];
	float width  = size.width;
	float height = size.height;

	if (width < widgetControlIconSize) {
		width = widgetControlIconSize;
	}
	if (height < widgetControlIconSize) {
		height = widgetControlIconSize;
	}

	int i = 0;
	for (DTWidgetControl* control in controls) {

		float x = centre.x + (((float)controlDataArray[i].xOffset) * (width / 2.0));
		float y = centre.y + (((float)controlDataArray[i].yOffset) * (height / 2.0));

		CGRect newFrame = CGRectMake(
			x - (iconSize / 2.0),
			y - (iconSize / 2.0),
			iconSize,
			iconSize
		);
		[control setFrame:newFrame];
		[control setWidget:widget];

//		[_viewContainingEditControls bringSubviewToFront:[widget view]];

//		[control setHidden:NO];
//		[control setAlpha:1.0];
		[_viewContainingEditControls bringSubviewToFront:control];

		i++;
	}
}

//-----------------------------------------------------------------
+ (void) removeEditControls
{
	LOG_FN();
	for (DTWidgetControl* control in controls) {
/*		[UIView animateWithDuration:0.2
			delay: 0.0f
			options:animationOptions
			animations:^{
				[control setAlpha:0];
			}
			completion:^(BOOL finished) {
				[control setHidden:YES];
			}
		];
*/
		[control setHidden:YES];
	}
}

//-----------------------------------------------------------------
+ (void) updateEditControlsForWidget:(DTWidget*)widget
{
	if (widget) {
		[self updateEditControls:widget];
	}
	else {
		[self removeEditControls];
	}
}

//-----------------------------------------------------------------
+ (void) hideEditControls
{
	LOG_FN();
	[DTWidgetControl removeEditControls];
//	for (DTWidgetControl* control in controls) {
//		[control setHidden:YES];
//	}
}

//-----------------------------------------------------------------
+ (void) showEditControls
{
	LOG_FN();
/*	for (DTWidgetControl* control in controls) {
		[UIView animateWithDuration:0.15
			delay: 0.0f
			options:animationOptions
			animations:^{
				[control setAlpha:1.0];
			}
			completion:^(BOOL finished) {
				[control setHidden:NO];
			}
		];
	}
*/
	for (DTWidgetControl* control in controls) {
		[control setHidden:NO];
		[control setAlpha:1.0];
	}
}

//-----------------------------------------------------------------
+ (void) resizeWidget:(DTWidget*)widget usingControl:(id)objectHit sizeOffset:(CGPoint)difference initialBounds:(CGRect)initialBounds
{
	DTWidgetControl* control = (DTWidgetControl*) objectHit;

	CGPoint position			= [widget screenPosition];
	CGSize  size				= [widget size];
	BOOL	keepAspectRatio		= NO;

	switch ([control tag]) {

		case resizeTopLeftTag:
			size.width  = initialBounds.size.width  + difference.x;
			size.height = initialBounds.size.height + difference.y;
			keepAspectRatio = YES;
			break;

		case resizeTopRightTag:
			size.width  = initialBounds.size.width  - difference.x;
			size.height = initialBounds.size.height + difference.y;
			keepAspectRatio = YES;
			break;

		case resizeBottomLeftTag:
			size.width  = initialBounds.size.width  + difference.x;
			size.height = initialBounds.size.height - difference.y;
			keepAspectRatio = YES;
			break;

		case resizeBottomRightTag:
			size.width  = initialBounds.size.width  - difference.x;
			size.height = initialBounds.size.height - difference.y;
			keepAspectRatio = YES;
			break;

		case resizeLeftTag:
			size.width = initialBounds.size.width + difference.x;
			position.x = initialBounds.origin.x - (difference.x / 2.0);
			break;

		case resizeTopTag:
			size.height = initialBounds.size.height + difference.y;
			position.y  = initialBounds.origin.y - (difference.y / 2.0);
			break;

		case resizeRightTag:
			size.width = initialBounds.size.width - difference.x;
			position.x = initialBounds.origin.x - (difference.x / 2.0);
			break;

		case resizeBottomTag:
			size.height = initialBounds.size.height - difference.y;
			position.y  = initialBounds.origin.y - (difference.y / 2.0);
			break;

		default:
			break;
	}

	if (keepAspectRatio) {

		float aspectRatio = initialBounds.size.width / initialBounds.size.height;

		// Keep size maintained to the aspect ratio - scale the other side depending
		// on which side moved the most - the dominant side
		if ((difference.x) > (difference.y)) {
			size.height = size.width / aspectRatio;
		}
		else {
			size.width = size.height * aspectRatio;
		}

		// size is smaller than we can handle - don't do anything
		if ((size.width < minimumWidgetWidth) || (size.height < minimumWidgetHeight)) {
			return;
		}

		// Find out how much size has actually changed after aspect ratio calculation
		float offsetX = (size.width -  initialBounds.size.width)  / 2.0;
		float offsetY = (size.height - initialBounds.size.height) / 2.0;

		// and apply this change to the positions to keep image in correct
		// place on screen as it's resized
		switch ([control tag]) {
			case resizeTopLeftTag:
				position.x = initialBounds.origin.x - offsetX;
				position.y = initialBounds.origin.y - offsetY;
				break;

			case resizeTopRightTag:
				position.x = initialBounds.origin.x + offsetX;
				position.y = initialBounds.origin.y - offsetY;
				break;

			case resizeBottomLeftTag:
				position.x = initialBounds.origin.x - offsetX;
				position.y = initialBounds.origin.y + offsetY;
				break;

			case resizeBottomRightTag:
				position.x = initialBounds.origin.x + offsetX;
				position.y = initialBounds.origin.y + offsetY;
				break;
		}
	}
	else {
		// No aspect ratio checks, just don't get too small...
		if (size.width < minimumWidgetWidth) {
			size.width = minimumWidgetWidth;
		}

		if (size.height < minimumWidgetHeight) {
			size.height = minimumWidgetHeight;
		}
	}

	// Finally actually update the widget
	//	UIView* view = [widget view];
	//	[view setContentMode:UIViewContentModeCenter];
	//	[view setContentMode:UIViewContentModeScaleAspectFill];
	//	[view setClipsToBounds:YES];
	
	[widget setSize:size];
	[widget setScreenPosition:position];
}

//-----------------------------------------------------------------
- (void) selectForEdit
{
	for (DTWidgetControl* control in controls) {
		[control setAlpha:(control == self) ? 1.0 : 0.2];
	}

#if 0
	if (! verticalLineView) {
		CGSize size = CGSizeMake(2, 200);
		UIImage* image = [UIImage createColouredRectangle:size colour:[UIColor whiteColor]];
		verticalLineView = [[UIImageView alloc] initWithImage:image];
	}

//	[verticalLineView setCenter:[self center]];
	[self addSubview:verticalLineView];

	if (! horizontalLineView) {
		CGSize size = CGSizeMake(200, 2);
		UIImage* image = [UIImage createColouredRectangle:size colour:[UIColor whiteColor]];
		horizontalLineView = [[UIImageView alloc] initWithImage:image];
	}

//	[horizontalLineView setCenter:[self center]];
	[self addSubview:horizontalLineView];
#endif
}

//-----------------------------------------------------------------
- (void) unselectForEdit
{
	for (DTWidgetControl* control in controls) {
		[control setAlpha:1.0];
	}
}

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	if (self) {
	}
	return self;
}

@end

//-----------------------------------------------------------------

