//
//  DTBlockButton.m
//  DeskTopper
//
//  Created by David Shea on 8/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTBlockButton.h"

//-----------------------------------------------------------------
@implementation DTBlockButton

@synthesize touchCallback = _touchCallback;

//-----------------------------------------------------------------
+ (DTBlockButton*) buttonWithType:(UIButtonType)type
{
	DTBlockButton* button = (DTBlockButton*)[UIButton buttonWithType:type];
	return button;
}

@end
//-----------------------------------------------------------------

