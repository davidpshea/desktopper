//
//  DTHelpViewController.h
//  DeskTopper
//
//  Created by David Shea on 13/12/12.
//
//

#import <UIKit/UIKit.h>

@interface DTHelpViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) UIPageControl* 	pageControl;
@property (nonatomic, strong) NSArray*			pageNameArray;

//+ (void) showHelp:		 (UIViewController*)parentController pageNameArray:(NSArray*)pages;
+ (void) showMainHelp:	 (UIViewController*)parentController;
+ (void) showAllHelp:    (UIViewController*)parentController;
+ (void) showNoteHelp:	 (UIViewController*)parentController;
+ (void) showBrowserHelp:(UIViewController*)parentController;

@end
