//
//  DTOptionsControllerWithDone.h
//  DeskTopper
//
//  Created by David Shea on 5/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#if 0
#import <Foundation/Foundation.h>
#import "CustomImagePickerController.h"
#import "AFPhotoEditorController.h"


@class DTWidget;

@interface DTOptionsControllerWithDone : QuickDialogController <
	UINavigationControllerDelegate,
	UIImagePickerControllerDelegate,
	CustomImagePickerDelegate,
	AFPhotoEditorControllerDelegate
>

@property (nonatomic, weak) DTWidget* widget;

//- (void) viewDidLoad;
- (void) doneButtonPressed;
- (void) cancelButtonPressed;

- (void) showPhotoPicker:(QElement*)element;
- (void) showCamera;
- (void) showImageEditor;
- (void) showIconPicker;
- (void) displayWebPage:(QElement*)element;

- (void) displayWebPage:(NSString*)URL parentController:(UIViewController*)parentController;

@end

#endif

// not used
