//
//  DTBrowserController.m
//  DeskTopper
//
//  Created by David Shea on 19/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTBrowserController.h"
#import "UIApplication+Helpers.h"
#import "DTNavigationController.h"
#import "SVProgressHUD.h"
#import "UIBarButtonItem+StandardButtons.h"
#import "UIImageExtras.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+Extras.h"
#import "UIGlossyButton.h"
#import "FMDatabase+Variables.h"
#import "DTHelpViewController.h"

//-----------------------------------------------------------------
@implementation DTBrowserViewController
/*
@synthesize pageSource				= _pageSource;
@synthesize pageSourceURL			= _pageSourceURL;
@synthesize navigationController	= _navigationController;
@synthesize isReadOnly				= _isReadOnly;
@synthesize finishedTimer			= _finishedTimer;
@synthesize singleTapRecognizer		= _singleTapRecognizer;
@synthesize fullScreenButton		= _fullScreenButton;
*/
//-----------------------------------------------------------------
- (id) init
{
	if (self = [super init]) {
	}
	return self;
}

//-----------------------------------------------------------------
- (void) dealloc
{
    [[self webView] setNavigationDelegate:nil];

	if ([self finishedTimer]) {
		[[self finishedTimer] invalidate];
		[self setFinishedTimer:nil];
	}
}

//-----------------------------------------------------------------
- (void) displayNotConnected
{
	NSString* notAvailable = NSLocalizedString(@"internetNotAvailable", "displayed when internet cannot be connected to");
	[MBProgressHUD showInformation:notAvailable onView:[self webView] withDelay:6.0];
}

//-----------------------------------------------------------------
- (NSString*) getURL
{
	WKWebView* webView = [self webView];

    NSString* URL = [[webView URL] absoluteString];

	// none available, fall back to the last URL set...
	if ([URL length] == 0) {
		return [self pageSourceURL];
	}

	return URL;
}

//-----------------------------------------------------------------
- (NSString*) getImageFromView
{
	WKWebView* webView = [self webView];

	UIImage* viewImage = [UIImage createFromView:webView];
	NSString* fileName = [viewImage saveToUniqueImageFile:nil];

	return fileName;
}

//-----------------------------------------------------------------
- (void) loadView
{
	CGRect frame = [UIApplication myMainScreenBounds];

	// the base view for this view controller
	UIView* contentView = [[UIView alloc] initWithFrame:frame];
	[contentView setBackgroundColor:[UIColor blackColor]];
	[contentView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
	[contentView setAutoresizesSubviews:YES];
	[self setView:contentView];

	WKWebView* webView = [[WKWebView alloc] initWithFrame:frame];
	[self setWebView:webView];

    [webView setNavigationDelegate:self];
	[webView setAutoresizesSubviews:YES];
	[webView setAutoresizingMask:(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)];

	if ([self pageSource]) {
		[webView loadHTMLString:[self pageSource] baseURL:nil];
	}
	else {
		if ([self pageSourceURL]) {
			if ([UIApplication isConnectedToNetwork]) {
				NSURL* URL = [NSURL URLWithString:[self pageSourceURL]];
				[webView loadRequest:[NSURLRequest requestWithURL:URL]];
			}
			else {
				[self displayNotConnected];
			}
		}
	}
	//add the web view to the content view
	[contentView addSubview:webView];

    _imageBeforeLoadView = [[UIImageView alloc] initWithFrame:frame];
    [contentView addSubview:_imageBeforeLoadView];
    [contentView bringSubviewToFront:_imageBeforeLoadView];
    [_imageBeforeLoadView setImage:[_loadingImage greyScaleImage]];
    [_imageBeforeLoadView setAlpha:1.0f];
    [_imageBeforeLoadView setHidden:NO];

	UIButton* fullScreenButton = [UIButton createFullScreenButton:@selector(toggleFullScreenButtonAction:) forTarget:self];
	[contentView addSubview:fullScreenButton];
	[fullScreenButton setHidden:YES];
	[self setFullScreenButton:fullScreenButton];

	// Gesture recogniser -------
/*	UITapGestureRecognizer* doubleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleDoubleTap:)
	];
	[doubleTapRecognizer setNumberOfTapsRequired:2];
	[doubleTapRecognizer setNumberOfTouchesRequired:2];
	[contentView addGestureRecognizer:doubleTapRecognizer];
*/
	// Gesture recogniser -------
	UITapGestureRecognizer* singleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSingleTap:)
	];
	[singleTapRecognizer setNumberOfTapsRequired:1];
	[singleTapRecognizer setNumberOfTouchesRequired:2];
	[singleTapRecognizer setDelegate:self];
	[contentView addGestureRecognizer:singleTapRecognizer];
	//	[self setSingleTapRecognizer:singleTapRecognizer];

	// Gesture recogniser -------
	UISwipeGestureRecognizer* swipeLeftRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeLeft:)
	];
	[swipeLeftRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[swipeLeftRecognizer setNumberOfTouchesRequired:2];
	[swipeLeftRecognizer setDelegate:self];
	[contentView addGestureRecognizer:swipeLeftRecognizer];

	// Gesture recogniser -------
	UISwipeGestureRecognizer* swipeRightRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeRight:)
	];
	[swipeRightRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	[swipeRightRecognizer setNumberOfTouchesRequired:2];
	[swipeRightRecognizer setDelegate:self];
	[contentView addGestureRecognizer:swipeRightRecognizer];

	// Only pan around with a single finger
	UIPanGestureRecognizer* scrollGesture = [[[self webView] scrollView] panGestureRecognizer];
	//	UIPinchGestureRecognizer* zoomGesture = [[[self webView] scrollView] pinchGestureRecognizer];

	[scrollGesture setMaximumNumberOfTouches:1];
	[scrollGesture setMinimumNumberOfTouches:1];

	[scrollGesture requireGestureRecognizerToFail:swipeLeftRecognizer];
	[scrollGesture requireGestureRecognizerToFail:swipeRightRecognizer];

	//	[singleTapRecognizer requireGestureRecognizerToFail:zoomGesture];
	[singleTapRecognizer requireGestureRecognizerToFail:scrollGesture];
}

//-----------------------------------------------------------------
- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	FMDatabase* database = [UIApplication myDatabase];

	BOOL displayHelp = [database getBOOLVariable:@"showBrowserHelp" withDefault:YES];

	if (displayHelp) {
		[DTHelpViewController showBrowserHelp:self];
		[database setBOOLVariable:@"showBrowserHelp" withValue:NO];
	}
}

//-----------------------------------------------------------------
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
	shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

//-----------------------------------------------------------------
- (void) CheckForLoadingAndUpdateToolbar:(BOOL)checkLoadingFlag
{
	WKWebView* webView	= [self webView];

	BOOL isLoading = checkLoadingFlag ? [webView isLoading] : NO;

	UIBarButtonItem* backBarButtonItem		 = [UIBarButtonItem createBackButton:@selector(backButtonAction:)       		forTarget:self];
	UIBarButtonItem* forwardBarButtonItem	 = [UIBarButtonItem createForwardButton:@selector(forwardButtonAction:)    		forTarget:self];
	UIBarButtonItem* fullScreenBarButtonItem = [UIBarButtonItem createFullScreenButton:@selector(fullScreenButtonAction:)	forTarget:self];
	UIBarButtonItem* actionBarButtonItem	 = [UIBarButtonItem createActionButton:@selector(actionButtonAction:)     		forTarget:self];

	UIBarButtonItem* refreshAndStopButtonItem = isLoading ?
		[UIBarButtonItem createStopButton:@selector(stopButtonAction:)    		forTarget:self] :
		[UIBarButtonItem createRefreshButton:@selector(refreshButtonAction:)	forTarget:self]
	;

	UIBarButtonItem* fixedSpace    = [UIBarButtonItem createFixedSpace:5.0f];
	UIBarButtonItem* flexibleSpace = [UIBarButtonItem createFlexibleSpace];

	[backBarButtonItem    	 setEnabled:[webView canGoBack]];
	[forwardBarButtonItem 	 setEnabled:[webView canGoForward]];
	[actionBarButtonItem  	 setEnabled:(! isLoading)];
	[fullScreenBarButtonItem setEnabled:(! isLoading)];

	NSArray* items = nil;
	items = [NSArray arrayWithObjects:
		fixedSpace,
		backBarButtonItem,
		flexibleSpace,
		forwardBarButtonItem,
		flexibleSpace,
		refreshAndStopButtonItem,
		flexibleSpace,
		fullScreenBarButtonItem,
		flexibleSpace,
		actionBarButtonItem,
		fixedSpace,
		nil
	];

	//	[[self navigationController] setToolbarItems:items animated:NO];
	[self setToolbarItems:items animated:NO];
}

//-----------------------------------------------------------------
- (void) refresh
{
	if ([self pageSource]) {
		[[self webView] loadHTMLString:[self pageSource] baseURL:nil];
	}
	else {
		if ([self pageSourceURL]) {
			if ([UIApplication isConnectedToNetwork]) {
				NSURL* URL = [NSURL URLWithString:[self pageSourceURL]];
				[[self webView] loadRequest:[NSURLRequest requestWithURL:URL]];
			}
			else {
				[self displayNotConnected];
			}
		}
	}

	[self CheckForLoadingAndUpdateToolbar:YES];
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
	[super viewDidLoad];
	[self CheckForLoadingAndUpdateToolbar:YES];
}

//-----------------------------------------------------------------
- (void) toggleFullScreenButtonAction:(UIButton*)sender
{
	[[self navigationController] toggleToolbars];

	[[self fullScreenButton] setHidden:![[self navigationController] isNavigationBarHidden]];
}

//-----------------------------------------------------------------
- (void) backButtonAction:(UIBarButtonItem*)sender
{
	[[self webView] goBack];
	[self CheckForLoadingAndUpdateToolbar:YES];
}

//-----------------------------------------------------------------
- (void) forwardButtonAction:(UIBarButtonItem*)sender
{
    [[self webView] goForward];
	[self CheckForLoadingAndUpdateToolbar:YES];
}

//-----------------------------------------------------------------
- (void) refreshButtonAction:(UIBarButtonItem*)sender
{
    [[self webView] reload];
	[self CheckForLoadingAndUpdateToolbar:YES];
}

//-----------------------------------------------------------------
- (void) stopButtonAction:(UIBarButtonItem*)sender
{
    [[self webView] stopLoading];
	[self CheckForLoadingAndUpdateToolbar:YES];
}

//-----------------------------------------------------------------
- (void) fullScreenButtonAction:(UIBarButtonItem*)sender
{
	[self toggleFullScreenButtonAction:nil];
}

//-----------------------------------------------------------------
- (void) settingsButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];
	[widget showSettingsModal:self fromSender:sender];
}

//-----------------------------------------------------------------
- (void) actionButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];
	[widget actionButtonAction:sender];
}

//-----------------------------------------------------------------
- (void) handleSingleTap:(UITapGestureRecognizer*)recogniser
{
	if ([recogniser state] == UIGestureRecognizerStateEnded) {
		[self toggleFullScreenButtonAction:nil];
	}
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UITapGestureRecognizer*)recogniser
{
	if ([recogniser state] == UIGestureRecognizerStateEnded) {
		[[self navigationController] toggleToolbars];
	}
}

//-----------------------------------------------------------------
- (void) handleSwipeLeft:(UITapGestureRecognizer*)recogniser
{
	if ([[self webView] canGoBack]) {
		[MBProgressHUD showGesture:navigateBack onView:[self webView]];
		[[self webView] goBack];
		[self CheckForLoadingAndUpdateToolbar:YES];
	}
}

//-----------------------------------------------------------------
- (void) handleSwipeRight:(UITapGestureRecognizer*)recogniser
{
	if ([[self webView] canGoForward]) {
		[MBProgressHUD showGesture:navigateForward onView:[self webView]];
		[[self webView] goForward];
		[self CheckForLoadingAndUpdateToolbar:YES];
	}
}

//-----------------------------------------------------------------
- (void) timerFire:(NSTimer*)timer
{
	[self setFinishedTimer:nil];

	[[self navigationController] stopActivityIndicator];
	[SVProgressHUD dismiss];
	[self CheckForLoadingAndUpdateToolbar:NO];
	
	DT_DEBUG_LOG(@"Web: stopped...");
}

//-----------------------------------------------------------------
- (void) stopActivityIndicator
{
	if (! [self finishedTimer]) {
		
		[self setFinishedTimer:
		 [
		  NSTimer
		  scheduledTimerWithTimeInterval:0.3
		  target:self
		  selector:@selector(timerFire:)
		  userInfo:nil
		  repeats:NO
		  ]
		 ];
		DT_DEBUG_LOG(@"Web: About to stop...");
	}
}

//-----------------------------------------------------------------
- (void) fadeOutLoadingImage
{
    [UIView
     animateWithDuration:0.5
     animations:^(void)
     {
         [_imageBeforeLoadView setAlpha:0.0f];
     }

     completion:^(BOOL finished)
     {
         [_imageBeforeLoadView setHidden:YES];
     }
     ];
}

//-----------------------------------------------------------------
- (void) fadeIncolouredLoadingImage
{
    CGRect frame = [[self view] frame];

    UIView* contentView = [self view];

    UIImageView* colouredView = [[UIImageView alloc] initWithFrame:frame];
    [colouredView setAlpha:0.0f];
    [colouredView setHidden:NO];
    [contentView addSubview:colouredView];
    [contentView bringSubviewToFront:colouredView];
    [colouredView setImage:_loadingImage];


    [
         UIView
         animateWithDuration:4.5
         animations:^(void)
         {
             [_imageBeforeLoadView setAlpha:0.0f];
             [colouredView setAlpha:1.0f];
         }

         completion:^(BOOL finished)
         {
             [_imageBeforeLoadView setHidden:YES];
             [_imageBeforeLoadView setImage:_loadingImage];
             [_imageBeforeLoadView setHidden:NO];
             [_imageBeforeLoadView setAlpha:1.0];

             [colouredView removeFromSuperview];
         }
     ];
}

//-----------------------------------------------------------------
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [self stopActivityIndicator];
    //    [self fadeIncolouredLoadingImage];

    NSString* s;

    UIView* viewToShowOn = _imageBeforeLoadView; //[[[self view] subviews] lastObject];

    switch ([error code]) {
        case -1003:
            s = NSLocalizedString(@"siteNotFound", nil);
            [MBProgressHUD showInformation:s onView:viewToShowOn withDelay:6.0];
            break;

        default:
            [MBProgressHUD showInformation:@"Cannot load web site" onView:viewToShowOn withDelay:6.0];
            //			[MBProgressHUD showInformation:[error localizedDescription] onView:[self webView] withDelay:6.0];
            break;
    }

    DT_DEBUG_LOG(@"webFailError: %ld, %@", (long)[error code], [error localizedDescription]);
}

//-----------------------------------------------------------------
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    [self webView:webView didFailNavigation:navigation withError:error];
}

//-----------------------------------------------------------------
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [self stopActivityIndicator];
    [self fadeOutLoadingImage];
}

//-----------------------------------------------------------------
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	LOG_FN();
	return YES;
}

//-----------------------------------------------------------------
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[[self fullScreenButton] repositionFullScreenButton];
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return YES;
}

@end
//-----------------------------------------------------------------

