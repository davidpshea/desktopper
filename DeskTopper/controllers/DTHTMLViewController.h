//
//  DTHTMLViewController.h
//  DeskTopper
//
//  Created by David Shea on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DTPostItNoteWidget;
@class DTNavigationController;
@class DTBrowserViewController;
@class DTShortcutWidget;
@class DTAttributedTextView;
@class DTAttributedTextViewDelegate;

//-----------------------------------------------------------------
@interface DTHTMLViewController : UIViewController <
	UIGestureRecognizerDelegate,
	UIWebViewDelegate
>

@property (nonatomic, strong)	UIWebView*						webView;
@property (nonatomic, strong)	DTAttributedTextView*			textView;
@property (nonatomic, strong)	DTAttributedTextViewDelegate*	attributedTextViewDelegate;
@property (nonatomic, strong)	NSString*						HTML;
@property (nonatomic, strong)	NSString*						CSS;
@property (nonatomic, strong)	NSString*						generatedCSS;
@property (nonatomic, strong)	NSString*						shortString;
@property (nonatomic) 			float							scale;
@property (nonatomic) 			float							textScale;
@property (nonatomic) 			BOOL							isResizing;
@property (nonatomic, weak)		DTNavigationController*			navigationController;
@property (nonatomic, strong)	UIColor*						backgroundColour;
@property (nonatomic, strong)	UIColor*						textColour;
@property (nonatomic, strong)	UIColor*						linkColour;
@property (nonatomic, strong)	NSString*						fontName;
@property (nonatomic, strong)	NSString*						fontSize;
@property (nonatomic, weak)		DTBrowserViewController*		browserViewController;
@property (nonatomic, strong)	DTShortcutWidget*				linkWidget;
@property (nonatomic) 			BOOL							clickedLink;
@property (nonatomic, strong)	UIButton*						fullScreenButton;

- (id)		initWithString:	(NSString*)HTMLText;
- (void)	setText:		(NSString*)HTMLText;
- (void)	refreshView;
- (void)	removeLinkWidget;

//-----------------------------------------------------------------
@end

