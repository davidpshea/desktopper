//
//  DTViewController.h
//  DeskTopper
//
//  Created by David Shea on 15/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTDesktop.h"
#import "DTPreferences.h"
#import "DTOptions.h"
#import "DTWidgetControl.h"
#import "CustomImagePickerController.h"

@class DTPhotoScrollView;
@class DTDesktopController;
@class FMDatabase;

@interface DTViewController : UIViewController <
	UIScrollViewDelegate,
	CustomImagePickerDelegate,
	UINavigationControllerDelegate,
	UIImagePickerControllerDelegate,
	UIActionSheetDelegate,
	UIGestureRecognizerDelegate,
	UIPopoverControllerDelegate
>
{
	CGFloat _beginX;
	CGFloat _beginY;
	CGFloat _lastScale;
}

@property (nonatomic, strong)	DTDesktop* 				desktop;
@property (nonatomic, strong)	DTDesktopController*	desktopController;
@property (nonatomic, strong)	UIView*   				zoomView;
@property (nonatomic, strong)	UIImageView*			backgroundView;
@property (nonatomic, strong)	UIToolbar*				toolbar;
@property (nonatomic, strong)	NSArray*				editToolbarItems;
@property (nonatomic, strong)	NSArray*				editInboxToolbarItems;
@property (nonatomic, strong)	NSArray*				viewToolbarItems;
@property (nonatomic, strong)	DTOptions*				myOptions;
@property (nonatomic)			BOOL					isZoomedOut;
@property (nonatomic)			float					lastZoomLevel;
@property (nonatomic)			CGRect					lastZoomRect;
@property (nonatomic)			CGRect					thisZoomRect;
@property (nonatomic, strong)	NSURL*					inputFileURL;
@property (nonatomic, strong)	FMDatabase*				database;
@property (nonatomic)			int						lastDesktopIndex;
@property (nonatomic, strong)	UIImageView*			desktopSwitchArrow;
@property (nonatomic, weak)		UIBarButtonItem*		cameraButton;
@property (nonatomic, weak)		UIBarButtonItem*		selectDesktopButton;
@property (nonatomic, weak)		DTWidget*				widgetInProgress;
@property (nonatomic)			UIInterfaceOrientation	lastOrientation;

- (void) saveState;
- (void) restoreState;

- (void)		selectWidgetForEdit:(DTWidget*)widget;
- (id)			createWidgetFromClassName:(NSString*)className;
- (void)		deleteWidget:(DTWidget*)widget animated:(BOOL)animated;
- (id)			duplicateWidget:(id)fromWidget;
- (BOOL)		iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender;
- (void)		handleDocumentOpenURL:(NSURL*)url;
- (void)		showBackgroundImageSelector:(id)sender;
- (void)		updateDesktopBackgroundImage:(UIImage*)newImage;

- (void)		destroyWidgetInProgress;
- (void)		completeWidgetInProgress;

@end

