//
//  DTOptionsControllerWithDone.m
//  DeskTopper
//
//  Created by David Shea on 5/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#if 0

import "QuickdialogHelper.h"
#import "DTOptionsControllerWithDone.h"
#import "UIImagePickerController+Helper.h"
#import "CustomImagePickerController.h"
#import "UIApplication+Helpers.h"
#import "UIImageExtras.h"
#import "SVModalWebViewController.h"

@implementation DTOptionsControllerWithDone
//-----------------------------------------------------------------

@synthesize widget = _widget;

//-----------------------------------------------------------------
- (void) doneButtonPressed
{
	LOG_FN();
	[[self widget] saveSettingsFromMenu:[self root]];

    [UIApplication dismissToRootViewController:YES];
}

//-----------------------------------------------------------------
- (void) cancelButtonPressed
{
	LOG_FN();
    [UIApplication dismissToRootViewController:YES];
}

//-----------------------------------------------------------------
- (void) showPhotoPicker:(QElement*)element
{
	// give me a view controller for my photo album
	UIImagePickerController* pickerController = [UIImagePickerController initPhotoAlbumViewController:self];

	// none? - use normal settings
	if (pickerController != nil) {
		// show photo album...
		[self presentViewController:pickerController animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) showCamera
{
	// give me a view controller for my photo album
	UIImagePickerController* pickerController = [UIImagePickerController initCameraViewController:self];

	// none? - use normal settings
	if (pickerController != nil) {
		// show photo album...
		[self presentViewController:pickerController animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (BOOL) iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender
{
	DT_DEBUG_LOG(@"icon is selected in view controller");

	UIButton* button = (UIButton*)sender;
	int tag			 = [button tag] + 1;

	[[self widget] setImageFilePath:nil];
	[[self widget] setSize:CGSizeMake(64, 64)];
	[[self widget] setIconIndex:tag];

	return YES;
}

//-----------------------------------------------------------------
- (void) showIconPicker
{
	CustomImagePicker* iconPicker = [[CustomImagePicker alloc] init];
	[iconPicker setDelegate:self];

	// Initialize image picker
	[iconPicker setTitle:@"Choose Icon"];

	NSArray* icons = [FileHelper getIconFileArray];
	for (NSString* path in icons) {
		UIImage* iconImage = [[UIImage alloc] initWithContentsOfFile:path];
		[iconPicker addImage:nil withThumbnail:iconImage];
	}

	// Create the enclosing navigation controller, so we get the 'cancel' button
//	UINavigationController* navigationController = [UINavigationController styledControllerWithRoot:iconPicker];
//	[self presentViewController:navigationController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) imagePickerController:(UIImagePickerController*)pImagePicker
            didFinishPickingMediaWithInfo:(NSDictionary*)MediaInfo
{
	[UIImagePickerController didFinishPickingMediaWithWidget:MediaInfo widget:[self widget]];

	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
-(void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) showImageEditor
{
	DTWidget* widget = [self widget];

	AFPhotoEditorController* editorController = [[AFPhotoEditorController alloc] initWithImage:[widget image]];
	[editorController setDelegate:self];

	[self presentViewController:editorController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) photoEditor:(AFPhotoEditorController*)editor finishedWithImage:(UIImage*) image
{
	DTWidget* widget = [self widget];

	// Send the correct image to the widget being edited
	NSString* newFilePath = [image saveToUniquePNG:nil];
	[widget setImageFilePath:newFilePath withImage:image];

	[editor dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) photoEditorCanceled:(AFPhotoEditorController*)editor
{
	[editor dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) displayWebPage:(NSString*)URL parentController:(UIViewController*)parentController
{
	SVModalWebViewController* webViewController = [[SVModalWebViewController alloc] initWithAddress:URL];

	[webViewController setWidget:[self widget]];
	[parentController presentViewController:webViewController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) displayWebPage:(QElement*)element
{
	NSString* URL = (NSString*)[element object];

	SVModalWebViewController* webViewController = [[SVModalWebViewController alloc] initWithAddress:URL];

	[webViewController setWidget:[self widget]];
	[self presentViewController:webViewController animated:YES completion:nil];
}

//-----------------------------------------------------------------

@end

#endif


