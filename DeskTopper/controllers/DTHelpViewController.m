//
//  DTHelpViewController.m
//  DeskTopper
//
//  Created by David Shea on 13/12/12.
//
//

#import "DTHelpViewController.h"
#import "UIApplication+Helpers.h"
#import "DTNavigationController.h"
#import "UIImageExtras.h"

@interface DTHelpViewController ()

@end

//-----------------------------------------------------------------
@implementation DTHelpViewController

@synthesize pageControl		= _pageControl;
@synthesize pageNameArray	= _pageNameArray;

//-----------------------------------------------------------------
+ (void) showMainHelp:(UIViewController*)parentController
{
	NSArray* pages = [
		[NSArray alloc] initWithObjects:
			@"/resource/help/toolbar1.png",
			@"/resource/help/toolbar_edit.png",
			@"/resource/help/toolbar_swipe.png",
		nil
	];

	[DTHelpViewController showHelp:parentController pageNameArray:pages];
}

//-----------------------------------------------------------------
+ (void) showAllHelp:(UIViewController*)parentController
{
	NSArray* pages = [
		[NSArray alloc] initWithObjects:
		@"/resource/help/toolbar1.png",
		@"/resource/help/toolbar_edit.png",
		@"/resource/help/toolbar_swipe.png",
		@"/resource/help/browser_help.png",
		@"/resource/help/note_help.png",
		nil
	];

	[DTHelpViewController showHelp:parentController pageNameArray:pages];
}

//-----------------------------------------------------------------
+ (void) showBrowserHelp:(UIViewController*)parentController
{
	NSArray* pages = [
	[NSArray alloc] initWithObjects:
		@"/resource/help/browser_help.png",
		nil
	];

	[DTHelpViewController showHelp:parentController pageNameArray:pages];
}

//-----------------------------------------------------------------
+ (void) showNoteHelp:(UIViewController*)parentController
{
	NSArray* pages = [
		[NSArray alloc] initWithObjects:
		@"/resource/help/note_help.png",
		nil
	];

	[DTHelpViewController showHelp:parentController pageNameArray:pages];
}

//-----------------------------------------------------------------
+ (void) showHelp:(UIViewController*)parentController pageNameArray:(NSArray*)pages
{
	DTHelpViewController* helpController = [[DTHelpViewController alloc] init];

	[helpController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
	[helpController setPageNameArray:pages];

	[parentController presentViewController:helpController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) scrollViewDidScroll:(UIScrollView*)scrollView
{
    CGRect frame = [[UIScreen mainScreen] applicationFrame];
    float roundedValue = round(scrollView.contentOffset.x / frame.size.width);
    [[self pageControl] setCurrentPage:roundedValue];
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
    [super viewDidLoad];

	NSMutableArray* pages = [[NSMutableArray alloc] init];
	UIImage* image		  = nil;
	NSUInteger pageCount  = [[self pageNameArray] count];

	CGRect fullScreenRect = [UIApplication myMainScreenBounds];
	CGRect frame		  = CGRectMake(0, 0.0, fullScreenRect.size.width, fullScreenRect.size.height);

	// Create images for each pages
	for (NSString* imageName in [self pageNameArray]) {

		image = [UIImage imageFromResource:imageName];

		UIImage* scaled = [image imageFromImageByScaling:frame.size keepAspectRatio:NO];

		[pages addObject:[[UIImageView alloc] initWithImage:scaled]];
	}

	//CGSize imageSize			= [image size];
	UIScrollView* scrollView	= [[UIScrollView alloc] initWithFrame:fullScreenRect];

	CGSize contentSize = CGSizeMake(fullScreenRect.size.width * pageCount, fullScreenRect.size.height);

	[scrollView setContentSize:contentSize];
	[scrollView setPagingEnabled:YES];
	[scrollView setBounces:YES];
	[scrollView setDelegate:self];
	[scrollView setDecelerationRate:UIScrollViewDecelerationRateFast];
	[scrollView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];

	// Add each help image to the scroll view
	for (UIImageView* view in pages)
	{
		[view setFrame:frame];
		[scrollView addSubview:view];
		frame.origin.x += frame.size.width;
	}

	[[self view] addSubview:scrollView];

	//--------------------------
	if (pageCount > 1) {
		// Add a page control if we need one
		UIPageControl* pageControl = [[UIPageControl alloc] init];
		CGPoint centre = CGPointMake(fullScreenRect.size.width / 2.0, 16);
		frame = CGRectMake(0, 0, fullScreenRect.size.width, 50);

		[pageControl setFrame:frame];
		[pageControl setCenter:centre];
		[pageControl setNumberOfPages:pageCount];

		[[self view] addSubview:pageControl];

		[self setPageControl:pageControl];
	}

	//--------------------------
	UITapGestureRecognizer* singleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(dispatchSingleTap:)
	];
	[scrollView addGestureRecognizer:singleTapRecognizer];
}

//-----------------------------------------------------------------
- (void) doneButtonPressed
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) dispatchSingleTap:(UILongPressGestureRecognizer*)recogniser
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return NO;
}

//-----------------------------------------------------------------
/*
 - (void) dealloc
{
	DT_DEBUG_LOG(@"Destroying help controller...");
}
*/
//-----------------------------------------------------------------
@end
