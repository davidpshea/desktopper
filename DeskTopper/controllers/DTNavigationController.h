//
//  DTNavigationController.h
//  DeskTopper
//
//  Created by David Shea on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "CustomImagePickerController.h"

@class DTWidget;

@interface DTNavigationController : UINavigationController <
	UINavigationControllerDelegate
>

@property (nonatomic, weak)		DTWidget*	widget;
//@property (nonatomic, strong)	id			strongSelf;
//@property (nonatomic, strong)	UIToolbar*	bottomToolbar;
@property (nonatomic)			float		timeTillHide;
@property (nonatomic)			BOOL		bottomToolbarHidden;

@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;

- (DTNavigationController*) initWithRootViewController:(UIViewController*)viewController;

- (void) setTranslucent:(BOOL)translucent;
//- (void) setToolbarItems:(NSArray*)items animated:(BOOL)animated;
- (void) showToolbars;
- (void) hideToolbars;
- (void) toggleToolbars;
- (void) setBottomToolbarHidden:(BOOL)hidden animated:(BOOL)animated;

//- (void) showImageEditor:(UIViewController*)parentController;

- (void) addEditButton:			(SEL)action forTarget:(id)buttonTarget;
- (void) addCancelButton:		(SEL)action forTarget:(id)buttonTarget;
- (void) addLeftCancelButton:	(SEL)action forTarget:(id)buttonTarget;
- (void) addDoneButton:			(SEL)action forTarget:(id)buttonTarget;
- (void) addSaveButton:			(SEL)action forTarget:(id)buttonTarget;

- (void) startActivityIndicator;
- (void) stopActivityIndicator;

- (void) addDefaultButtonsForTarget:(id)buttonTarget;

@end

