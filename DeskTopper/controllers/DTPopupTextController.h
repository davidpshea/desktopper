//
//  DTPopupTextController.h
//  DeskTopper
//
//  Created by David Shea on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YIPopupTextView.h"

@class DTCustomKeyboard;
@class DTPopupTextController;

//-----------------------------------------------------------------
typedef enum keyboardID
{
	keyboardIDRegular	= 1 << 0,
	keyboardIDHTML		= 1 << 1,
	keyboardIDMarkdown	= 1 << 2,
	keyboardIDControl	= 1 << 3,
	keyboardIDShortcut	= 1 << 4,
	keyboardIDURL		= 1 << 5

} keyboardID;

//-----------------------------------------------------------------
@protocol DTPopupTextControllerDelegate <NSObject>

@optional

- (void) popupTextController:(DTPopupTextController*)textView willDismissWithText:(NSString*)text;
- (void) popupTextController:(DTPopupTextController*)textView didDismissWithText:(NSString*)text;
- (void) popupTextController:(DTPopupTextController*)textView textDidChange:(NSString*)text;

@end

//-----------------------------------------------------------------
@interface DTPopupTextController : UIViewController <YIPopupTextViewDelegate>

@property (nonatomic, weak) id <DTPopupTextControllerDelegate> popupTextControllerDelegate;

@property (nonatomic)		  NSString*				text;
@property (nonatomic, strong) UIColor*				backgroundColour;
@property (nonatomic, strong) UIColor*				textColour;
@property (nonatomic, strong) UIFont*				font;
@property (nonatomic)		  float					textScale;
@property (nonatomic, strong) id					target;
@property (nonatomic, strong) id					userObject;
@property (nonatomic)		  int					enabledKeyboards;
@property (nonatomic)		  int					cursorPosition;

- (UITextView*) getTextView;
- (void)		dismissKeyboard;

//-----------------------------------------------------------------
@end

