//
//  DTOptionsController.h
//  DeskTopper
//
//  Created by David Shea on 2/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <UIImagePickerController/UIImagePickerController.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "DTWidget.h"

@interface DTOptionsController : QuickDialogController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
}

@property (nonatomic, weak) DTWidget* pWidget;

+ (NSArray*) getIconFileList;

@end

