//
//  DTPictureViewerController.m
//  DeskTopper
//
//  Created by David Shea on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "UIApplication+Helpers.h"
#import "DTPictureViewerController.h"
#import "QuickdialogHelper.h"
#import "DTPhotoScrollView.h"
#import "DTNavigationController.h"
#import "UIBarButtonItem+StandardButtons.h"
#import "SVProgressHUD.h"
#import "UIView+Animation.h"

@implementation DTPictureViewerController

//-----------------------------------------------------------------
@synthesize image					= _image;
@synthesize imageView				= _imageView;
@synthesize scrollView				= _scrollView;
@synthesize toolbarHidden			= _toolbarHidden;
@synthesize navigationController	= _navigationController;

//-----------------------------------------------------------------
- (float) calculateMinimumZoom
{
	UIImage* image		= [self image];
	UIView* view		= [self view];
	CGSize boundsSize	= [view bounds].size;
	CGSize imageSize	= [image size];

	float widthRatio  = boundsSize.width / imageSize.width;
	float heightRatio = boundsSize.height / imageSize.height;
//	DT_DEBUG_LOG(@"W:%f x H:%f", widthRatio, heightRatio);

	float zoom = (widthRatio < heightRatio) ? widthRatio : heightRatio;
//	DT_DEBUG_LOG(@"zoom:%f", zoom);
	return zoom;
}

//-----------------------------------------------------------------
- (void) setImage:(UIImage*)image
{
	_image = image;

	UIImageView*  view		 = [self imageView];
	UIScrollView* scrollView = [self scrollView];

	CGSize imageSize   = [image size];
	CGSize contentSize = CGSizeMake(imageSize.width, imageSize.height);
	[scrollView setContentSize:contentSize];

	float minimumZoom = [self calculateMinimumZoom];
	[scrollView setMinimumZoomScale:minimumZoom];
	[scrollView setZoomScale:minimumZoom];

	CGRect bounds = CGRectMake(0.0, 0.0, imageSize.width, imageSize.height);
	[view setBounds:bounds];
	[view setImage:image];
}

//-----------------------------------------------------------------
- (DTPictureViewerController*) initWithImage:(UIImage*)image
{
	self = [super init];
	if (self) {
		_image = image;
	}

	return self;
}

//-----------------------------------------------------------------
- (void) updateToolbar
{
	// bottom Toolbar
    UIBarButtonItem* fixedSpace    = [UIBarButtonItem createFixedSpace:5.0f];
	UIBarButtonItem* flexibleSpace = [UIBarButtonItem createFlexibleSpace];

	UIBarButtonItem* fullScreenBarButtonItem = [UIBarButtonItem createFullScreenButton:@selector(fullScreenButtonAction:)	forTarget:self];
	UIBarButtonItem* actionBarButtonItem	 = [UIBarButtonItem createActionButton:@selector(actionButtonAction:)     		forTarget:self];

	NSArray* items = [NSArray arrayWithObjects:
		fixedSpace,
		fullScreenBarButtonItem,
		flexibleSpace,
		actionBarButtonItem,
		fixedSpace,
		nil
	];

	[self setToolbarItems:items animated:NO];
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
    [super viewDidLoad];

	UIImage* image = [self image];
	UIView*  view  = [self view];

	UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
	[self setImageView:imageView];

	UIColor* black = [UIColor blackColor];
	[view setBackgroundColor: black];

	CGRect fullScreenRect    		= [UIApplication myMainScreenBounds];
	DTPhotoScrollView* scrollView	= [[DTPhotoScrollView alloc] initWithFrame:fullScreenRect];
	CGSize imageSize		 		= [image size];

	CGSize contentSize = CGSizeMake(imageSize.width, imageSize.height);

	[scrollView setContentSize:contentSize];
	[scrollView setPagingEnabled:NO];
	[scrollView setBounces:YES];
	[scrollView setMultipleTouchEnabled:YES];
	[scrollView setDecelerationRate:UIScrollViewDecelerationRateFast];
	[scrollView setDelegate:self];
	[scrollView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];

	// Zoom parameters
	[scrollView setBouncesZoom:NO];
	[scrollView setMaximumZoomScale:4.0];
	float minimumZoom = [self calculateMinimumZoom];
	[scrollView setMinimumZoomScale:minimumZoom];
	[scrollView setZoomScale:minimumZoom];

	scrollView.contentInset          = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
	scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);

	// ScreenView --> scrollView --> zoomView
	[view addSubview:scrollView];
	[scrollView addSubview:imageView];

	// and save it in the controller for easy access
	[self setScrollView:scrollView];

	// Gesture recognisers
	UITapGestureRecognizer* doubleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleDoubleTap:)
	];
	[doubleTapRecognizer setNumberOfTapsRequired:2];
	[view addGestureRecognizer:doubleTapRecognizer];

	UITapGestureRecognizer* singleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSingleTap:)
	];
	[singleTapRecognizer setNumberOfTapsRequired:1];
	[singleTapRecognizer requireGestureRecognizerToFail:doubleTapRecognizer];
	[view addGestureRecognizer:singleTapRecognizer];

#if 0
	UISwipeGestureRecognizer* swipeLeftRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeLeft:)
	];
	[swipeLeftRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[swipeLeftRecognizer setNumberOfTouchesRequired:1];
	[imageView addGestureRecognizer:swipeLeftRecognizer];

	UISwipeGestureRecognizer* swipeRightRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeRight:)
	];
	[swipeRightRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	[swipeRightRecognizer setNumberOfTouchesRequired:1];
	[imageView addGestureRecognizer:swipeRightRecognizer];
#endif

	[self updateToolbar];

	[SVProgressHUD dismiss];
}

//-----------------------------------------------------------------
- (void) fullScreenButtonAction:(UIBarButtonItem*)sender
{
	[[self navigationController] toggleToolbars];
}

//-----------------------------------------------------------------
- (void) actionButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];
	[widget actionButtonAction:sender];
}

//-----------------------------------------------------------------
/*- (void) settingsButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];
	[widget showSettingsModal:self];
}
*/
//-----------------------------------------------------------------
- (UIView*) viewForZoomingInScrollView:(UIScrollView*)scrollView
{
	return [self imageView];
}

//-----------------------------------------------------------------
- (void) doneButtonPressed
{
	[[UIApplication myRootViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) cancelButtonPressed
{
	[[UIApplication myRootViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (CGRect) rectAroundPoint:(CGPoint)point atZoomScale:(CGFloat)zoomScale
{
	// Define the shape of the zoom rect.
	UIView* view      = [self view];
	CGSize boundsSize = [view bounds].size;

	// Modify the size according to the requested zoom level.
	// For example, if we're zooming in to 0.5 zoom, then this will increase the bounds size
	// by a factor of two.
	CGSize scaledBoundsSize = CGSizeMake(
		boundsSize.width  / zoomScale,
		boundsSize.height / zoomScale
	);

	CGRect rect = CGRectMake(
		point.x - scaledBoundsSize.width / 2,
		point.y - scaledBoundsSize.height / 2,
		scaledBoundsSize.width,
		scaledBoundsSize.height
	);

	// When the image is zoomed out there is a bit of empty space around the image due
	// to the fact that it's centered on the screen. When we created the rect around the
	// point we need to take this "space" into account.

	// 1: get the frame of the image in this view's coordinates.
	CGRect imageScaledFrame = [view convertRect:[[self imageView] frame] toView:view];

	// 2: Offset the frame by the excess amount. This will ensure that the zoomed location
	//    is always centered on the tap location. We only allow positive values because a
	//    negative value implies that there isn't actually any offset.
	rect = CGRectOffset(rect, -MAX(0, imageScaledFrame.origin.x), -MAX(0, imageScaledFrame.origin.y));

	return rect;
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UITapGestureRecognizer*)recogniser
{
	DTPhotoScrollView* scrollView = [self scrollView];

	BOOL isCompletelyZoomedIn = ([scrollView maximumZoomScale] <= [scrollView zoomScale] + FLT_EPSILON);

	if (isCompletelyZoomedIn) {
		// Zoom the photo back out.
		[scrollView setZoomScale:[scrollView minimumZoomScale] animated:YES];

	} else {
		// Zoom into the tap point.
		CGPoint tapCenter = [recogniser locationInView:[self imageView]];

		CGRect maxZoomRect = [self rectAroundPoint:tapCenter atZoomScale:[scrollView maximumZoomScale]];
		[scrollView zoomToRect:maxZoomRect animated:YES];
	}
}

//-----------------------------------------------------------------
#if 0
- (void) handleSwipeLeft:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	UIView* view			= [self view];
	UIImageView* imageView	= [self imageView];

//	CGPoint point = CGPointMake(0.0, 0.0);
//	[imageView raceTo:point withSnapBack:YES];


	/*
   CATransition* transition = [CATransition animation];
   transition.duration = 10.0;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [imageView.layer addAnimation:transition forKey:@"push-transition"];
*/
//	if ([recogniser state] == UIGestureRecognizerStateEnded) {
#if 1
		[
			UIView transitionWithView:view
			duration:0.5
			options:UIViewAnimationOptionTransitionFlipFromRight
			animations:^{
				[imageView removeFromSuperview];
				[view addSubview:imageView];
			}
			completion:NULL
		];
//	}
#endif
}
#endif

//-----------------------------------------------------------------
#if 0
- (void) handleSwipeRight:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	UIView* view			= [self view];
	UIImageView* imageView	= [self imageView];

	[
		UIView transitionWithView:view
		duration:0.5
		options:UIViewAnimationOptionTransitionFlipFromLeft
		animations:^{
			[imageView removeFromSuperview];
			[view addSubview:imageView];
		}
		completion:NULL
	];
}
#endif

//-----------------------------------------------------------------
- (void) handleSingleTap:(UITapGestureRecognizer*)recogniser
{
	LOG_FN();
	[[self navigationController] toggleToolbars];
}

//-----------------------------------------------------------------
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
#if 0
	LOG_FN();
    // Return YES for supported orientations
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    return YES;//(interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
	} else {
	    return YES;
	}
#endif
}

//-----------------------------------------------------------------
@end

