//
//  DTOptionsController.m
//  DeskTopper
//
//  Created by David Shea on 2/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//#import <NSObjCRuntime.h>
#import "DTOptionsController.h"
#import "CustomImagePickerController.h"

@implementation DTOptionsController

@synthesize pWidget;

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	return self;
}

//-----------------------------------------------------------------
+ (NSArray*) getIconFileList
{
	NSBundle* pMainBundle = [NSBundle mainBundle];

	NSArray* icons       = [pMainBundle pathsForResourcesOfType:@"png" inDirectory:@"resource/icons"];
	NSArray* sortedIcons = [icons sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

	return sortedIcons;
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
	// Add a 'done' button to the root right-side edge
    UIBarButtonItem* pDoneButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Done"
		style:UIBarButtonItemStyleDone
		target:self
		action:@selector(CloseOptionsMenu)
	];

	self.navigationItem.rightBarButtonItem = pDoneButton;

	// Add a 'cancel' button to the root left-side edge
/*	UIBarButtonItem* pCancelButton = [
		[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
		target:self
		action:@selector(CancelOptionsMenu)
	];
*/
//	self.navigationItem.leftBarButtonItem = pCancelButton;
}

//-----------------------------------------------------------------
- (void)CloseOptionsMenu
{
	[[self pWidget] saveSettingsFromMenu:[self root]];
    [self dismissModalViewControllerAnimated:YES];
}

//-----------------------------------------------------------------
/*
- (void)CancelOptionsMenu
{
    [self dismissModalViewControllerAnimated:YES];
}
*/

//-----------------------------------------------------------------
- (void) CameraAction
{
	DT_DEBUG_LOG(@"Select camera");
	// Need a camera...
	if (! [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
		return;
	}

	// Need a rear facing camera...
	if (! ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear])) {
		return;
	}

	UIImagePickerController* pImagePicker = [[UIImagePickerController alloc] init];

	[pImagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
	[pImagePicker setAllowsEditing:YES];
	[pImagePicker setDelegate: self];

	[self presentModalViewController:pImagePicker animated:YES];
}

//-----------------------------------------------------------------
- (void) PhotoRollAction
{
	DT_DEBUG_LOG(@"Select photo roll");
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] == NO) {
		return;
	}

	UIImagePickerController* pImagePicker = [[UIImagePickerController alloc] init];

	[pImagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
	[pImagePicker setAllowsEditing:YES];
	[pImagePicker setDelegate: self];

	[self presentModalViewController:pImagePicker animated:YES];

}
//-----------------------------------------------------------------
- (void) imagePickerController:(UIImagePickerController*)pImagePicker
            didFinishPickingMediaWithInfo:(NSDictionary*)MediaInfo
{
	// Make sure we have an image...
	NSString* mediaType = [MediaInfo objectForKey: UIImagePickerControllerMediaType];

	if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {

		// Edited or original copy?
		UIImage* pEditedImage   = (UIImage*) [MediaInfo objectForKey:UIImagePickerControllerEditedImage];
		UIImage* pOriginalImage = (UIImage*) [MediaInfo objectForKey:UIImagePickerControllerOriginalImage];

		UIImage* pImageToUse = pEditedImage ? pEditedImage : pOriginalImage;

		// Send the correct image to the widget being edited

		NSString* pFileName = [UIImage addDefaultImageFileExtension:[FileHelper GetUniqueFileNameFromTime:@"image%@"]];
		NSString* pFilePath = [FileHelper GetPathForFileInDocuments:pFileName];

		[FileHelper SaveImageAsImageFile:pImageToUse UsingPath:pFilePath];

//		[[self pWidget] updateWithNewImage:pImageToUse UsingPath:pFilePath];
		[[self pWidget] setImageFilePath:pFilePath];
		[[self pWidget] setImage:pImageToUse];
	}

	[self dismissModalViewControllerAnimated:YES];
}

//-----------------------------------------------------------------
-(void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[self dismissModalViewControllerAnimated:YES];
}

//-----------------------------------------------------------------
- (void) IconAction
{
	DT_DEBUG_LOG(@"Select Icon");

	// Initialize image picker
	CustomImagePicker* imagePicker = [[CustomImagePicker alloc] init];
	imagePicker.title = @"Choose Icon";

	[imagePicker setWidget:[self pWidget]];

	NSArray* icons = [DTOptionsController getIconFileList];

	for (NSString* path in icons) {

		UIImage* iconImage = [[UIImage alloc] initWithContentsOfFile:path];
		[imagePicker addImage:nil withThumbnail:iconImage];
	}

	UINavigationController* pNavigationController =
		[[UINavigationController alloc] initWithRootViewController:imagePicker];

	[self presentModalViewController:pNavigationController animated:YES];
}

//-----------------------------------------------------------------
@end

