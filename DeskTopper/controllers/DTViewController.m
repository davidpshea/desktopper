//
//  DTViewController.m
//  DeskTopper
//
//  Created by David Shea on 15/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "macros.h"
#import <MessageUI/MessageUI.h>

#import "DTViewController.h"
#import "DTDesktop.h"
#import "DTDesktopController.h"
#import "UIImageExtras.h"
#import "QuickDialogHelper.h"
#import "UIImageView+ImageViewShadow.h"
#import "DTWidgetImageView.h"
#import "FileHelper.h"
#import "SVProgressHUD.h"
#import "UIView+Animation.h"
#import "CustomImagePickerController.h"
#import "UIScrollView+ZoomToPoint.h"
#import "UIApplication+Helpers.h"
#import "DTWidgetImageView.h"
#import "DTPictureWidget.h"
#import "DTPhotoScrollView.h"
#import "DTNavigationController.h"
#import "UIBarButtonItem+StandardButtons.h"
#import "NSCoder+DefaultDecoding.h"
#import "UIImagePickerController+Helper.h"
#import "UINavigationController+Extras.h"
#import "DTPostItNoteWidget.h"
#import "FMDatabase.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+Extras.h"
#import "FMDatabase+Variables.h"
#import "UIImageView+Animation.h"
#import "DTPopupTextController.h"
#import "DTHelpViewController.h"
#import "DTDocumentSharing.h"
#import "UIPopoverController+Stack.h"
#import "UIActionSheet+Tools.h"

//-----------------------------------------------------------------
// View tags
static const int desktopTag	= -1;
//static const int widgetTag	= 0;

//-----------------------------------------------------------------
typedef enum swipeDirection
{
	swipeDirectionNone,
	swipeDirectionUp,
	swipeDirectionDown,
	swipeDirectionLeft,
	swipeDirectionRight,

	swipeDirectionUpDown,
	swipeDirectionLeftRight

} swipeDirection;

//-----------------------------------------------------------------
static inline float roundToNearest(const float N, const float roundTo)
{
	return ((int)((N + (roundTo / 2)) / roundTo)) * roundTo;
}

//-----------------------------------------------------------------
@implementation DTViewController

//-----------------------------------------------------------------
- (void) didReceiveMemoryWarning
{
	LOG_FN();
    [super didReceiveMemoryWarning];
}


//-----------------------------------------------------------------
- (void) handleHTTPURL:(NSURL*)url
{
	DT_DEBUG_LOG(@"url recieved: %@", [url absoluteString]);

	if ([[url scheme] isEqualToString:@"totalnotes"]) {

		NSString* s = [url absoluteString];

		// remove totalnotes://
		s = [s substringFromIndex:13];

		NSRange hostRange = [s rangeOfString:@"//"]; //location length

		if (hostRange.location != NSNotFound) {

			NSString* host = [s substringToIndex:hostRange.location];

			if ([host isEqualToString:@"newnote"]) {
				// TODO: Add more commands here...
			}
			else {

				NSString* rest = [s substringFromIndex:hostRange.location];

				NSString* newURL = [NSString stringWithFormat:@"%@:%@", host, rest];

				[DTWidgetControl hideEditControls];
				DTShortcutWidget* widget = [self createWidgetFromClassName:@"DTShortcutWidget"];

				// And give it the URL
				[widget setURL:newURL];
				[widget handleSingleTap:self];
			}
		}
	}
/*
	DT_DEBUG_LOG(@"url recieved: %@", [url absoluteString]);
	DT_DEBUG_LOG(@"query string: %@", [url query]);
	DT_DEBUG_LOG(@"host: %@", [url host]);
	DT_DEBUG_LOG(@"url path: %@", [url path]);
*/
}

//-----------------------------------------------------------------
- (void) handleFileURL:(NSURL*)url
{
	// We asume to get here we do have a desktop to put it on
	LOG_FN();

	[self setInputFileURL:nil];
	DT_DEBUG_LOG(@"URL: %@", [url absoluteString]);

	NSString* extension = [url pathExtension];

	BOOL isImage =
		([extension compare:@"jpg"]  ==  NSOrderedSame) ||
		([extension compare:@"jpeg"] ==  NSOrderedSame) ||
		([extension compare:@"png"]  ==  NSOrderedSame)
	;

	if (isImage) {

		NSData* data = [NSData dataWithContentsOfURL:url];
		if (data) {

			UIImage* image = [UIImage imageWithData:data];
			if (image) {
				TESTFLIGHT_CHECKPOINT(@"import photo");

				// Create a blank picture widget...
				[DTWidgetControl hideEditControls];
				DTPictureWidget* widget = [self createWidgetFromClassName:@"DTPictureWidget"];

				// Scale the camera/photo image to something a bit more usable for the desktop
				CGSize hiResScaledSize	= [image getSizeToFitIntoSize:[UIImage getScaledPhotoSize]];
				UIImage* hiResImage 	= [image imageFromImageByScaling:hiResScaledSize keepAspectRatio:YES];
				[widget setHighResolutionImage:hiResImage];

				// Create a thumbnail as well...
				CGSize thumbnailSize		= [hiResImage getSizeToFitIntoSize:[UIImage getThumbnailPhotoSize]];
				UIImage* scaledImage		= [hiResImage imageFromImageByScaling:thumbnailSize keepAspectRatio:YES];
				NSString* thumbnailFilePath = [scaledImage saveToUniqueImageFile:nil];

				[widget setImageFilePath:thumbnailFilePath withImage:scaledImage];
				[widget setSize:thumbnailSize];

				// Bring it up for the user
				[widget handleSingleTap:self];
			}
		}
	}

	if ([extension compare:@"url"] ==  NSOrderedSame) {
		// Handle url files..
		TESTFLIGHT_CHECKPOINT(@"import URL");

		NSError* error = nil;
		NSString* fileContents = [FileHelper readStringFromURL:url error:&error];
		DT_DEBUG_LOG(@"File: %@", fileContents);

		if (fileContents) {
			// Create a blank note widget...
			[DTWidgetControl hideEditControls];
			DTShortcutWidget* widget = [self createWidgetFromClassName:@"DTShortcutWidget"];
			// And fill it with the contents of the file
			[widget setURL:fileContents];
			[widget handleSingleTap:self];
		}
	}

	else {

		// Handle text files..
		DT_DEBUG_LOG(@"URL: %@", [url absoluteString]);
		NSError* error = nil;
		NSString* fileContents = [FileHelper readStringFromURL:url error:&error];
		DT_DEBUG_LOG(@"File: %@", fileContents);

		if (fileContents) {

			TESTFLIGHT_CHECKPOINT(@"import text file");

			// Create a blank note widget...
			[DTWidgetControl hideEditControls];
			DTPostItNoteWidget* widget = [self createWidgetFromClassName:@"DTPostItNoteWidget"];
			// And fill it with the contents of the file
			[widget setText:fileContents];
			[widget handleSingleTap:self];
		}
	}
}


//-----------------------------------------------------------------
- (void) handleDocumentOpenURL:(NSURL*)url
{
	LOG_FN();

	// We can't create a widget until we have a desktop to put it on
	if ([self desktop]) {

		if (url) {
			// Handle files being passed in...
			if ([url isFileURL]) {
				[self handleFileURL:url];
			}
			else {
				// Handle totalnotes://
				[self handleHTTPURL:url];
			}
		}
	}
	else {
		// No desktop yet, store URL away until we can use it
		[self setInputFileURL:url];
	}
}

//-----------------------------------------------------------------
- (void) showSettingsDoneButton
{
	[[self myOptions] saveSettingsFromMenu];
}

//-----------------------------------------------------------------
- (void) showSettingsCancelButton
{
	[[UIApplication myPresentedViewController] dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) showSettings
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"show settings");

	QRootElement* root = [[self myOptions] createOptionsMenu];

	// Create the enclosing navigation controller
	UINavigationController* navigationController = [QuickDialogController controllerWithNavigationForRoot:root];

	// Customise
	[navigationController style];
	[navigationController addDoneButton:@selector(showSettingsDoneButton)     forTarget:self];

	[navigationController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];

	// display options menu - displays nav controller that displays everything else
	[[UIApplication myRootViewController] presentViewController:navigationController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (BOOL) calculateZPositionForWidgets
{
	int z = 0;
	BOOL orderHasChanged = NO;

	for (UIView* view in [[self zoomView] subviews]) {

		if ([view isKindOfClass:[DTWidgetImageView class]]) {
			DTWidget* widget = [(DTWidgetImageView*)view widget];

			if ([widget zPosition] != z) {

				[widget setZPosition:z];
				[widget setIsDirty:YES];

				orderHasChanged = YES;
			}
			z++;
		}
	}

	return orderHasChanged;
}

//-----------------------------------------------------------------
- (void) createSnapshotForCurrentDesktop
{
	DTDesktop* desktop = [self desktop];
	
	if ([desktop thumbnailIsDirty] || [desktop isAnyWidgetDirty]) {
		UIImage* fullSizeSnapshot = [UIImage createFromViewWithScale:[self backgroundView] scaledBy:1.0/4.0];
		[desktop setSnapshot:fullSizeSnapshot];
		// Update database to match desktop state
		[desktop updateDatabaseFromDesktop:[self database]];
	}
}

//-----------------------------------------------------------------
- (void) addWidgetToView:(DTWidget*)widget
{
	DTWidgetImageView* view = [[DTWidgetImageView alloc] initWithWidget:widget];
	[widget setView:view];

	[[self zoomView] addSubview:view];
}

//-----------------------------------------------------------------
- (void) createViewsFromWidgets
{
	DTDesktop* desktop	= [self desktop];
	int cWidgets		= [desktop getWidgetCount];

	for (int i = 0; i < cWidgets; i++) {
		DTWidget* widget = [desktop getWidgetFromIndex:i];
		[self addWidgetToView:widget];
	}
}

//-----------------------------------------------------------------
- (void) removeWidgetFromView:(DTWidget*)widget
{
	[[widget view] removeFromSuperview];
	[widget setView:nil];
}

//-----------------------------------------------------------------
- (void) destroyViewsFromWidgets
{
	int cWidgets = [[self desktop] getWidgetCount];

	for (int i = 0; i < cWidgets; i++) {
		DTWidget* widget = [[self desktop] getWidgetFromIndex:i];
		[self removeWidgetFromView:widget];
	}
}

//-----------------------------------------------------------------
- (void) moveWidget:(DTWidget*)widget toDesktop:(int)desktopIndex
{
	// Remove from this desktop
	DTDesktop* currentDesktop = [self desktop];

	[currentDesktop removeWidget:widget];
	[widget setView:nil];

	DTDesktop* newDesktop = [[self desktopController] getDesktopFromIndex:desktopIndex];
	DT_ASSERT([newDesktop hasLoadedFromDatabase]);
	[newDesktop addWidget:widget];

	[widget setIsDirty:YES];
	[currentDesktop setThumbnailIsDirty:YES];
	[widget updateDatabaseFromWidget:[self database]];
}

//-----------------------------------------------------------------
- (void) showDesktopName:(int)desktopIndex
{
	// Remove loading...
	[MBProgressHUD hideAllHUDsForView:[self view] animated:YES];

	NSString* name = nil;

	switch (desktopIndex) {

		case DTDesktopIndexScrap:
			name = NSLocalizedString(@"scrapSpace", "Name of scrap space");
			break;
		case DTDesktopIndexBin:
			name = NSLocalizedString(@"trashSpace", "Name of trash space");
			break;
		default:
			name = NSLocalizedString(@"spaceGenericName", "Name of space X");
			name = [NSString stringWithFormat:name, (desktopIndex - DTDesktopIndexUser) + 1];
			break;
	}
	// Add information on new desktop
	(void) [MBProgressHUD showInformation:name];
}

//-----------------------------------------------------------------
- (void) updateDatabaseFromCurrentDesktop
{
	DTDesktop* desktop = [self desktop];

	// Give each widget a Z position based on the view order
	BOOL orderChanged = [self calculateZPositionForWidgets];

	if (orderChanged) {
		// Re-order the widgets to match the Z order of the views
		[desktop orderWidgetsByZ];
		[desktop setIsDirty:YES];
		[desktop setThumbnailIsDirty:YES];
		DT_DEBUG_LOG(@"Widget order changed...");
	}

	// Create a thumbnail of current desktop
	[self createSnapshotForCurrentDesktop];

	// Update database to match desktop state
	[desktop updateDatabaseFromDesktop:[self database]];
}

//-----------------------------------------------------------------
- (void) moveToDesktopBG:(int)desktopIndex withSwipeDirection:(swipeDirection)swipeDirection
{
	DTDesktopController* desktopController = [self desktopController];

	[self selectWidgetForEdit:nil];

	//	UIView* zoomView 	= [self zoomView];
	UIView* baseView	= [self backgroundView];
	CGPoint destination = [baseView center];
	CGRect bounds		= [baseView bounds];
	CGRect screenBounds	= [UIApplication myMainScreenBoundsRotated];
	CGPoint start		= destination;

	// work out where view will go based on direction
	switch (swipeDirection) {
		case swipeDirectionUp:
			destination.y	= -(bounds.size.height + 5.0);
			start.y			= bounds.size.height + screenBounds.size.height + 5.0;
			break;

		case swipeDirectionDown:
			destination.y	= bounds.size.height + screenBounds.size.height + 5.0;
			start.y			= -(bounds.size.height + 5.0);
			break;

		case swipeDirectionLeft:
			destination.x	= -(bounds.size.width + 5.0);
			start.x			= bounds.size.width + screenBounds.size.height + 5.0;
			break;

		case swipeDirectionRight:
			destination.x	= bounds.size.width + screenBounds.size.height + 5.0;
			start.x			= -(bounds.size.width + 5.0);
			break;
		default:
			break;
	}

	float animationTime = (swipeDirection == swipeDirectionNone) ? 0.0 : 0.45;
	[
		UIView
		animateWithDuration:animationTime
		animations:^{
			// Slide current desktop out
			[baseView setCenter:destination];
		}
		completion:^(BOOL didFinish) {
			// Desktop hidden, make all our changes
			[self rotateWidgets:UIInterfaceOrientationPortrait animated:NO];
			[self updateDatabaseFromCurrentDesktop];
			[self updateDesktopBackgroundImage:nil];
			[self destroyViewsFromWidgets];

			[baseView setCenter:start];
			[baseView setAlpha:1.0];
			[desktopController setCurrentDesktopIndex:desktopIndex];
			DTDesktop* desktop = [desktopController getCurrentDesktop];
			[self setDesktop:desktop];
//			DT_DEBUG_LOG("moveToDesktopBG (backgroundView#1): %@", NSStringFromCGRect([[self backgroundView] bounds]));

			// Create views for the widgets in the new desktop
			[self createViewsFromWidgets];
			[self setLastOrientation:UIInterfaceOrientationPortrait];
			[self rotateWidgets:[UIApplication myRotation] animated:NO];
			[self updateDesktopBackgroundImage:[desktop backgroundImage]];
			
			[
				// Slide the new desktop back into the centre
				UIView
				animateWithDuration:animationTime
				animations:^{
					[baseView setCenter:[UIApplication myMainScreenCentre]];
				}
				completion:^(BOOL didFinish2) {
					[self showDesktopName:desktopIndex];
				}
			];
		}
	];
}

//-----------------------------------------------------------------
- (void) moveToDesktop:(int)desktopIndex withSwipeDirection:(swipeDirection)swipeDirection
{
	DT_DEBUG_LOG(@"Moving from desktop %d to %d", [[self desktopController] currentDesktopIndex], desktopIndex);

	DTDesktopController* desktopController = [self desktopController];

	// Off end of desktops, do nothing...
	if (desktopIndex >= [desktopController getDesktopCount]) {
		return;
	}
	// Off beginning of desktops, do nothing...
	if (desktopIndex < 0) {
		return;
	}
	// changing to the desktop we already have - do nothing
	if ([desktopController currentDesktopIndex] == desktopIndex) {
		return;
	}

	MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:[self view] animated:YES];
	[HUD setLabelText:@"Loading..."];

	// Do the actual work
	[self moveToDesktopBG:desktopIndex withSwipeDirection:swipeDirection];
}

//-----------------------------------------------------------------
- (void) forwardButtonAction:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"forward button");
	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];

	if (currentDesktopIndex >= DTDesktopIndexUser) {
		[self moveToDesktop:(currentDesktopIndex + 1) withSwipeDirection:swipeDirectionLeft];
	}
}

//-----------------------------------------------------------------
- (void) backButtonAction:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"back button");
	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];

	if (currentDesktopIndex > DTDesktopIndexUser) {
		[self moveToDesktop:(currentDesktopIndex - 1) withSwipeDirection:swipeDirectionRight];
	}
}

//-----------------------------------------------------------------
- (void) deleteOrphanedImageFiles
{
	// Get all files in docs directory
	NSArray* files = [FileHelper getFilesInDocuments];

	NSMutableArray* validFileList = [[NSMutableArray alloc] init];

	// Pick out only image files, add to validFileList array
	NSString* extension = [UIImage getDefaultImageFileExtension];

	for (NSString* file in files) {
		if ([[file pathExtension] compare:extension] == NSOrderedSame) {
			NSString* filePath = [FileHelper getPathForFileInDocuments:file];
			[validFileList addObject:filePath];
		}
	}

	// Go through every desktop we have...
	DTDesktopController* controller = [self desktopController];

	for (int desktopIndex = 0; desktopIndex < [controller getDesktopCount]; desktopIndex++) {

		DTDesktop* desktop = [controller getDesktopFromIndex:desktopIndex];

		// Look thro all widgets for the files it owns
		for (int i = 0; i < [desktop getWidgetCount]; i++) {

			// Check background
/*			if (_backgroundFilePath) {
				for (NSString* file in validFileList) {
					if ([_backgroundFilePath compare:file] == NSOrderedSame) {
	//					DT_DEBUG_LOG(@"Matched to background:%@", file);
						[validFileList removeObject:file];
						break;
					}
				}
			}
*/
			DTWidget* widget = [desktop getWidgetFromIndex:i];

			// Check regular file path
			NSString* imageFilePath = [widget imageFilePath];
			if (imageFilePath) {
				for (NSString* file in validFileList) {
					if ([imageFilePath compare:file] == NSOrderedSame) {
	//					DT_DEBUG_LOG(@"Matched to widget:%@", file);
						[validFileList removeObject:file];
						break;
					}
				}
			}
			// Check hi-res file path
			imageFilePath = [widget getHighResolutionImageFilePath];
			if (imageFilePath) {
				for (NSString* file in validFileList) {
					if ([imageFilePath compare:file] == NSOrderedSame) {
	//					DT_DEBUG_LOG(@"Matched to widget:%@", file);
						[validFileList removeObject:file];
						break;
					}
				}
			}
		}
	}

	// validFileList contains files not referenced by any widget
	DT_DEBUG_LOG(@"What's left...");
	for (NSString* file in validFileList) {
		DT_DEBUG_LOG(@"%@,", file);
		[FileHelper sendFileToBin:file];
	}
}

//-----------------------------------------------------------------
#define kDataFile       	@"data.plist"
#define kOptions			@"options"

- (void) saveState
{
	[self updateDatabaseFromCurrentDesktop];

	FMDatabase* database = [self database];
	[database setIntVariable:@"lastDesktopIndex" withValue:[self lastDesktopIndex]];

	NSString*        path          = [FileHelper getPathForFileInDocuments:kDataFile];
	NSMutableData*   settingsData  = [NSMutableData data];
	NSKeyedArchiver* encoder       = [[NSKeyedArchiver alloc] initForWritingWithMutableData:settingsData];

	[encoder encodeObject:[self myOptions] forKey:kOptions];

	[encoder finishEncoding];
	[settingsData writeToFile:path atomically:YES];
}

//-----------------------------------------------------------------
- (void) restoreState
{
	LOG_FN();
	DTDesktopController* desktopController = [self desktopController];

	if (! desktopController) {
		desktopController = [DTDesktopController createDesktopControllerFromDatabase:[self database]];
		[self setDesktopController:desktopController];
	}

	FMDatabase* database = [self database];
	[self setLastDesktopIndex:[database getIntVariable:@"lastDesktopIndex" withDefault:DTDesktopIndexUser]];

	NSString* path         = [FileHelper getPathForFileInDocuments:kDataFile];
	NSData*   settingsData = [[NSMutableData alloc] initWithContentsOfFile:path];

	if ([FileHelper doesFileExist:path]) {
		// Load data...
		NSKeyedUnarchiver* decoder = [[NSKeyedUnarchiver alloc] initForReadingWithData:settingsData];

		[self setMyOptions:[decoder decodeObjectForKey:kOptions]];

		[decoder finishDecoding];
	}

	DTDesktop* desktop = [desktopController getCurrentDesktop];
	[self setDesktop:desktop];

	if (! [self myOptions]) {
		// No options - set defaults
		DTOptions* options = [[DTOptions alloc] init];
		[self setMyOptions:options];
	}

	[self createViewsFromWidgets];

//	[self deleteOrphanedImageFiles];
}

//-----------------------------------------------------------------
- (void) showBackgroundImageSelector:(id)sender
{
	// give me a view controller for my photo albums
	UIImagePickerController* pickerController = [UIImagePickerController initPhotoAlbumViewController:self];

	if (pickerController != nil) {

		[UIApplication PushNavigationBarDarkGreyColour];

		if ([UIApplication isPad]) {
			// Thumbnails exist in a popover
			UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:pickerController];

			[UIPopoverController setActiveController:popover];
			[popover setDelegate:self];

			CGRect size = [UIApplication myMainScreenBounds];

			CGRect position = CGRectMake(size.size.width / 2.0, 120.0, 1, 1);

			[popover presentPopoverFromRect:position inView:[UIApplication myWindow]
				   permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
		}
		else {
			[[UIApplication myPresentedViewController] presentViewController:pickerController animated:YES completion:nil];
		}
	}
}

//-----------------------------------------------------------------
- (void) addDesktopBackgroundImageBG:(UIImage*)imageToUse
{
	TESTFLIGHT_CHECKPOINT(@"add background image");

	// Scale the photo image to something a bit more usable for the desktop
	CGRect fullScreenRect    = [UIApplication myMainScreenBounds];
	UIImage* scaledImage	 = [imageToUse imageFromImageByScaling:fullScreenRect.size keepAspectRatio:NO];

	[[self backgroundView] setImage:scaledImage];
	[[self desktop] setBackgroundImage:scaledImage];
	[self updateDatabaseFromCurrentDesktop];

	[MBProgressHUD hideAllOnTopWindow];
}

//-----------------------------------------------------------------
- (void) addDesktopBackgroundImage:(UIImage*)imageToUse
{
	UIWindow* window = [UIApplication myWindow];
	[MBProgressHUD showProgressWhilePerformingSelector:
		@selector(addDesktopBackgroundImageBG:)
		onTarget:self usingData:imageToUse onView:window
	];
}

//-----------------------------------------------------------------
- (void) updateDesktopBackgroundImage:(UIImage*)newImage
{
	// Scale the photo image to something a bit more usable for the desktop
	CGRect fullScreenRect = [UIApplication myMainScreenBounds];

	if (newImage == nil) {
		[[self backgroundView] setImage:nil];
	}
	else {
		UIImage* scaledImage = [newImage imageFromImageByScaling:fullScreenRect.size keepAspectRatio:NO];
		[[self backgroundView] setImage:scaledImage];
	}
}

//-----------------------------------------------------------------
- (void) imagePickerController:(UIImagePickerController*)imagePicker
						didFinishPickingMediaWithInfo:(NSDictionary*)MediaInfo
{
	id popover = [UIPopoverController getActivePopoverController];

	if (popover) {
		[popover dismissPopoverAnimated:YES];
		[UIPopoverController setActiveController:nil];
	}

	[imagePicker dismissViewControllerAnimated:YES completion:nil];
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[UIApplication PopNavigationBarColour];

	// Make sure we have an image...
	NSString* mediaType = [MediaInfo objectForKey:UIImagePickerControllerMediaType];

	if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {

		UIImage* imageToUse = (UIImage*) [MediaInfo objectForKey:UIImagePickerControllerOriginalImage];
		[self addDesktopBackgroundImage:imageToUse];
	}
}

//-----------------------------------------------------------------
- (void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[UIApplication PopNavigationBarColour];

	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
static const CGFloat kToolBarHeight = 44.0f;

//-----------------------------------------------------------------
- (void) createToolbars
{
	UIView* view = [self view];
	DT_ASSERT_NOT_NIL(view);

	// add the bottom toolbar
	UIToolbar* toolbar = [[UIToolbar alloc] init];
	DT_ASSERT_NOT_NIL(toolbar);
	//	[toolbar setBarStyle:UIBarStyleDefault];

	CGRect viewSize = [view bounds];

	// kToolBarHeight pixels high, full width of screen
	[toolbar setFrame:CGRectMake(0, viewSize.size.height - kToolBarHeight + 1, viewSize.size.width, kToolBarHeight)];
	[toolbar setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin)];

	// Make style match other toolbars
	[toolbar setBarStyle:UIBarStyleBlack];
	[toolbar setTranslucent:YES];

	//-------------------------------------
	// view mode...
	UIBarButtonItem* selectButton		= [UIBarButtonItem createSelectSpaceButton:@selector(selectDesktopButtonPressed:) forTarget:self];
	UIBarButtonItem* newNoteButton		= [UIBarButtonItem createNewNoteButton:@selector(newNoteButtonPressed:)			  forTarget:self];
	UIBarButtonItem* newCameraButton	= [UIBarButtonItem createNewCameraButton:@selector(newCameraButtonPressed:)		  forTarget:self];
	UIBarButtonItem* newWebButton		= [UIBarButtonItem createNewWebButton:@selector(newWebButtonPressed:)			  forTarget:self];
	UIBarButtonItem* newPictureButton	= [UIBarButtonItem createNewImageButton:@selector(newImageButtonPressed:)		  forTarget:self];
	UIBarButtonItem* settingsButton		= [UIBarButtonItem createSettingsButton:@selector(settingsButtonAction:)		  forTarget:self];

	UIBarButtonItem* fixedSpace    = [UIBarButtonItem createFixedSpace:5.0f];
	UIBarButtonItem* flexibleSpace = [UIBarButtonItem createFlexibleSpace];

	[self setSelectDesktopButton:selectButton];

	NSArray* items;
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

		// Real phone with camera
		[self setCameraButton:newCameraButton];

		items = [NSArray arrayWithObjects:
			fixedSpace,
			selectButton,
			flexibleSpace,
			newWebButton,
			flexibleSpace,
			newNoteButton,
			flexibleSpace,
			newCameraButton,
			flexibleSpace,
			settingsButton,
			fixedSpace,
			nil
		];
	}
	else {
		[self setCameraButton:newPictureButton];

		// Sim without camera
		items = [NSArray arrayWithObjects:
			fixedSpace,
			selectButton,
			flexibleSpace,
			newWebButton,
			flexibleSpace,
			newNoteButton,
			flexibleSpace,
			newPictureButton,
			flexibleSpace,
			settingsButton,
			fixedSpace,
			nil
		];
	}

	//-------------------
	// Long press on select gives arrange menu
	UILongPressGestureRecognizer* arrangeLongPress = [
	[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleArrangeMenu:)
	];
	[arrangeLongPress setMinimumPressDuration:0.3];
	[selectButton addGesture:arrangeLongPress];

	//-------------------
	// Long press on add web gives add menu
#if 0
	UILongPressGestureRecognizer* addLongPress = [
		[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(addButtonPressed:)
	];
	[addLongPress setMinimumPressDuration:0.3];
	[newWebButton addGesture:addLongPress];
#endif
	//-------------------
	// Long press on camera gives new image
	UILongPressGestureRecognizer* imageLongPress = [
	[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(newImageHeld:)
	];
	[imageLongPress setMinimumPressDuration:0.3];
	[newCameraButton addGesture:imageLongPress];
	

	[toolbar setItems:items animated:NO];
	[self setViewToolbarItems:items];

	//-------------------------------------
	// Edit mode....
	UIBarButtonItem* actionButtonEdit		= [UIBarButtonItem createActionButton:@selector(actionButtonEditPressed:)		forTarget:self];
	UIBarButtonItem* upButtonEdit			= [UIBarButtonItem createUpButton:@selector(upButtonEditAction:)				forTarget:self];
	UIBarButtonItem* downButtonEdit			= [UIBarButtonItem createDownButton:@selector(downButtonEditAction:)			forTarget:self];
	UIBarButtonItem* deleteButtonEdit		= [UIBarButtonItem createDeleteButton:@selector(deleteButtonEditAction:)		forTarget:self];
	UIBarButtonItem* duplicateButtonEdit	= [UIBarButtonItem createDuplicateButton:@selector(duplicateButtonEditAction:)	forTarget:self];
	UIBarButtonItem* organiseButtonEdit		= [UIBarButtonItem createOrganiseButton:@selector(orderZButtonEditAction:)		forTarget:self];

	NSArray* editItems = [NSArray arrayWithObjects:
		fixedSpace,
		actionButtonEdit,
		flexibleSpace,
		organiseButtonEdit,
		flexibleSpace,
		duplicateButtonEdit,
		flexibleSpace,
		downButtonEdit,
		flexibleSpace,
		deleteButtonEdit,
		fixedSpace,
		nil
	];
	[self setEditToolbarItems:editItems];

	NSArray* editInboxItems = [NSArray arrayWithObjects:
		fixedSpace,
		actionButtonEdit,
		flexibleSpace,
		organiseButtonEdit,
		flexibleSpace,
		duplicateButtonEdit,
		flexibleSpace,
		upButtonEdit,
		flexibleSpace,
		deleteButtonEdit,
		fixedSpace,
		nil
	];
	[self setEditInboxToolbarItems:editInboxItems];

	//-------------------
	// Long press on move down gives move down all
	UILongPressGestureRecognizer* moveDownLongPress = [
		[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(downAllButtonPressed:)
	];
	[moveDownLongPress setMinimumPressDuration:0.4];
	[downButtonEdit addGesture:moveDownLongPress];

	//-------------------
	// Long press on move up gives move up all
	UILongPressGestureRecognizer* moveUpLongPress = [
		[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(upAllButtonPressed:)
	];
	[moveUpLongPress setMinimumPressDuration:0.4];
	[upButtonEdit addGesture:moveUpLongPress];

	//-------------------
	// Long press on delete gives delete all
	UILongPressGestureRecognizer* deleteLongPress = [
		[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(deleteAllButtonPressed:)
	];
	[imageLongPress setMinimumPressDuration:0.4];
	[deleteButtonEdit addGesture:deleteLongPress];

	//-------------------------------------
	[view addSubview:toolbar];
	[self setToolbar:toolbar];
}

//-----------------------------------------------------------------
- (void) createDatabase
{
	// Open/create database
	NSString* databasePath = [FileHelper getDatabasePath];

	FMDatabase* database = [FMDatabase databaseWithPath:databasePath];
	if (![database open]) {
		DT_ASSERT_MSG(NO, @"Could not open/create database");
		return;
	}
	[self setDatabase:database];

	#ifdef DEBUG_SQL
	[database setLogsErrors:YES];
	[database setTraceExecution:YES];
	#endif

	#ifdef DEBUG_CLEAN_DATABASE
	// Uncomment to allow the code to create the defaults...
	[database executeUpdate:@"DROP TABLE styles"];
	[database executeUpdate:@"DROP TABLE search"];
	#endif

	#ifdef DEBUG_HELP
	[database setBOOLVariable:@"showHelp" withValue:YES];
	[database setBOOLVariable:@"showNoteHelp" withValue:YES];
	[database setBOOLVariable:@"showBrowserHelp" withValue:YES];
	#endif
	
	//------------
	NSString* desktopTable = @"CREATE TABLE IF NOT EXISTS desktops (";
	desktopTable = [desktopTable stringByAppendingString:@"desktopID INTEGER  UNIQUE,"];
	desktopTable = [desktopTable stringByAppendingString:@"snapshot BLOB,"];
	desktopTable = [desktopTable stringByAppendingString:@"background BLOB)"];

	//------------
	NSString* widgetTable = @"CREATE TABLE IF NOT EXISTS widgets (";
	widgetTable = [widgetTable stringByAppendingString:@"widgetID  INTEGER PRIMARY KEY UNIQUE,"];
	widgetTable = [widgetTable stringByAppendingString:@"desktopID INTEGER,"];
	widgetTable = [widgetTable stringByAppendingString:@"widgetData BLOB)"];

	//------------
	NSString* varsTable = @"CREATE TABLE IF NOT EXISTS vars (";
	varsTable = [varsTable stringByAppendingString:@"name TEXT UNIQUE,"];
	varsTable = [varsTable stringByAppendingString:@" value TEXT)"];

	//------------
	NSString* shortcutTable = @"CREATE TABLE IF NOT EXISTS shortcuts (";
	shortcutTable = [shortcutTable stringByAppendingString:@"shortcutID INTEGER UNIQUE,"];
	shortcutTable = [shortcutTable stringByAppendingString:@"shortcutLabel TEXT,"];
	shortcutTable = [shortcutTable stringByAppendingString:@"shortcutText TEXT)"];

	//------------
	NSString* styleTable = @"CREATE TABLE IF NOT EXISTS styles (";
	styleTable = [styleTable stringByAppendingString:@"shortcutID INTEGER UNIQUE,"];
	styleTable = [styleTable stringByAppendingString:@"shortcutLabel TEXT,"];
	styleTable = [styleTable stringByAppendingString:@"shortcutText TEXT)"];

	//------------
	NSString* searchTable = @"CREATE TABLE IF NOT EXISTS search (";
	searchTable = [searchTable stringByAppendingString:@"shortcutID INTEGER UNIQUE,"];
	searchTable = [searchTable stringByAppendingString:@"shortcutLabel TEXT,"];
	searchTable = [searchTable stringByAppendingString:@"shortcutText TEXT)"];

	//------------
	[database executeUpdate:desktopTable];
	[database executeUpdate:widgetTable];
	[database executeUpdate:shortcutTable];
	[database executeUpdate:styleTable];
	[database executeUpdate:searchTable];
	[database executeUpdate:varsTable];
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
	LOG_FN();
    [super viewDidLoad];

	// Hide the status bar
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

	UIView* view = [self view];
	[view setTag:desktopTag];

	// Set black background
	UIColor* black = [UIColor blackColor];
	[view setBackgroundColor: black];

	[self createDatabase];

	UIView* zoomView = [[UIView alloc] initWithFrame:[[self view] bounds]];
	[zoomView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
	[self setZoomView:zoomView];

	// Create the controls used for widget editing
	[DTWidgetControl createEditControls:zoomView];

	// Create the desktop model
	[self restoreState];

	[[self view] setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];


	UIImageView* backgroundView = [[UIImageView alloc] initWithFrame:[[self view] bounds]];
	[backgroundView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];

	[backgroundView setUserInteractionEnabled:YES];
	[self setBackgroundView:backgroundView];

	[view addSubview:backgroundView];
	[backgroundView addSubview:zoomView];
//	DT_DEBUG_LOG("viewDidLoad (backgroundView#3): %@", NSStringFromCGRect([[self backgroundView] bounds]));

	[self createToolbars];

	// Remember orientation...
	UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	[self setLastOrientation:currentOrientation];

	// Create an arrow image that we use to display when switching desktops
	NSString* fileName = [FileHelper getIconFileNamed:@"arrowgray"];
	UIImage* image = [UIImage createFromImageFile:fileName];
	UIImageView* desktopSwitchArrow = [[UIImageView alloc] initWithImage:image];
	[self setDesktopSwitchArrow:desktopSwitchArrow];
	[[self view] addSubview:desktopSwitchArrow];
	[desktopSwitchArrow setHidden:YES];

	// Add desktop gestures to this view
	//--------------------------
	UILongPressGestureRecognizer* longPress = [
		[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleLongPress:)
	];
	[longPress setMinimumPressDuration:0.15];
	[zoomView addGestureRecognizer:longPress];

	//--------------------------
/*	UILongPressGestureRecognizer* longPress2 = [
		[UILongPressGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleLongPressCancel:)
	];
	[longPress2 setMinimumPressDuration:0.4];
	[longPress2 setNumberOfTouchesRequired:2];
	[zoomView addGestureRecognizer:longPress2];
*/
	//--------------------------
	UITapGestureRecognizer* twoFingerTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSingleTap2Fingers:)
	];
	[twoFingerTapRecognizer setNumberOfTapsRequired:1];
	[twoFingerTapRecognizer setNumberOfTouchesRequired:2];
	[zoomView addGestureRecognizer:twoFingerTapRecognizer];

	//--------------------------
	UIPinchGestureRecognizer* pinchRecognizer = [
		[UIPinchGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleScale:)
	];
    [zoomView addGestureRecognizer:pinchRecognizer];

	//--------------------------
	UIPanGestureRecognizer* panGesture = [
		[UIPanGestureRecognizer alloc]
		initWithTarget:self action:@selector(handlePan:)
	];
	[panGesture setMinimumNumberOfTouches:1];
	[panGesture setMaximumNumberOfTouches:1];
	[panGesture setDelegate:self];
	[view addGestureRecognizer:panGesture];

	//--------------------------
	UITapGestureRecognizer* doubleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleDoubleTap:)
	];
	[doubleTapRecognizer setNumberOfTapsRequired:2];
	[zoomView addGestureRecognizer:doubleTapRecognizer];

	//--------------------------
	UITapGestureRecognizer* singleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(dispatchSingleTap:)
	];
	[singleTapRecognizer setNumberOfTapsRequired:1];
	[singleTapRecognizer requireGestureRecognizerToFail:doubleTapRecognizer];
	[zoomView addGestureRecognizer:singleTapRecognizer];

	//--------------------------
	UISwipeGestureRecognizer* swipeLeftRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeLeft:)
	];
	[swipeLeftRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[swipeLeftRecognizer setNumberOfTouchesRequired:2];
	[swipeLeftRecognizer setDelegate:self];
	[zoomView addGestureRecognizer:swipeLeftRecognizer];

	//--------------------------
/*	UISwipeGestureRecognizer* swipeLeftRecognizerTwoFinger = [
	[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeLeftTwoFinger:)
	];
	[swipeLeftRecognizerTwoFinger setDirection:UISwipeGestureRecognizerDirectionLeft];
	[swipeLeftRecognizerTwoFinger setNumberOfTouchesRequired:2];
	[zoomView addGestureRecognizer:swipeLeftRecognizerTwoFinger];
*/

	//--------------------------
	UISwipeGestureRecognizer* swipeRightRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeRight:)
	];
	[swipeRightRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	[swipeRightRecognizer setNumberOfTouchesRequired:2];
	[swipeRightRecognizer setDelegate:self];
	[zoomView addGestureRecognizer:swipeRightRecognizer];

	//--------------------------
/*	UISwipeGestureRecognizer* swipeRightRecognizerTwoFinger = [
	[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeRightTwoFinger:)
	];
	[swipeRightRecognizerTwoFinger setDirection:UISwipeGestureRecognizerDirectionRight];
	[swipeRightRecognizerTwoFinger setNumberOfTouchesRequired:2];
	[zoomView addGestureRecognizer:swipeRightRecognizerTwoFinger];
*/

	//--------------------------
	UISwipeGestureRecognizer* swipeDownRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeDown:)
	];
	[swipeDownRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
	[swipeDownRecognizer setNumberOfTouchesRequired:2];
	[swipeDownRecognizer setDelegate:self];
	[zoomView addGestureRecognizer:swipeDownRecognizer];

	//--------------------------
/*	UISwipeGestureRecognizer* swipeDownRecognizerTwoFinger = [
	[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeDownTwoFinger:)
	];
	[swipeDownRecognizerTwoFinger setDirection:UISwipeGestureRecognizerDirectionDown];
	[swipeDownRecognizerTwoFinger setNumberOfTouchesRequired:2];
	[zoomView addGestureRecognizer:swipeDownRecognizerTwoFinger];
*/

	//--------------------------
	UISwipeGestureRecognizer* swipeUpRecognizer = [
		[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeUp:)
	];
	[swipeUpRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
	[swipeUpRecognizer setNumberOfTouchesRequired:2];
	[swipeUpRecognizer setDelegate:self];
	[zoomView addGestureRecognizer:swipeUpRecognizer];

	//--------------------------
#if 0
	UIPinchGestureRecognizer* pinchRecogniser = [
		[UIPinchGestureRecognizer alloc]
		initWithTarget:self action:@selector(handlePinch:)
	];
	[pinchRecogniser setDelegate:self];
	[zoomView addGestureRecognizer:pinchRecogniser];
#endif
	/*	UISwipeGestureRecognizer* swipeUpRecognizerTwoFinger = [
	[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeUpTwoFinger:)
	];
	[swipeUpRecognizerTwoFinger setDirection:UISwipeGestureRecognizerDirectionUp];
	[swipeUpRecognizerTwoFinger setNumberOfTouchesRequired:2];
	[zoomView addGestureRecognizer:swipeUpRecognizerTwoFinger];
*/
	// are we opening a file from another app?
	[self handleDocumentOpenURL:[self inputFileURL]];
	
//	DT_DEBUG_LOG("viewDidLoad (backgroundView#4): %@", NSStringFromCGRect([[self backgroundView] bounds]));
}

//-----------------------------------------------------------------
- (void) viewDidAppear:(BOOL)animated
{
	BOOL displayHelp = [[self database] getBOOLVariable:@"showHelp" withDefault:YES];

//	DT_DEBUG_LOG("viewDidAppear (backgroundView#1): %@", NSStringFromCGRect([[self backgroundView] bounds]));

	[super viewDidAppear:animated];
	[self showDesktopName:[[self desktopController] currentDesktopIndex]];
	[self updateDesktopBackgroundImage:[[self desktop] backgroundImage]];

//	DT_DEBUG_LOG("viewDidAppear (backgroundView#2): %@", NSStringFromCGRect([[self backgroundView] bounds]));

	if (displayHelp) {
		[DTHelpViewController showMainHelp:self];
		[[self database] setBOOLVariable:@"showHelp" withValue:NO];
	}
}

//-----------------------------------------------------------------
- (id) FindViewFromGestureRecognizer:(UIGestureRecognizer*)recogniser
{
	UIView* view      = [self zoomView];
	CGPoint location  = [recogniser locationInView:view];		// Get point in view where press happened

	return [view hitTest:location withEvent:nil];				// Find any view that might be hit by that location
}

//-----------------------------------------------------------------
- (id) FindWidgetFromGestureRecognizer:(UIGestureRecognizer*)recogniser
{
	UIView* view      = [self zoomView];
	CGPoint location  = [recogniser locationInView:view];				// Get point in view where press happened
	id viewHit   	  = [view hitTest:location withEvent:nil];			// Find any view that might be hit by that location

	if ([viewHit respondsToSelector:@selector(widget)]) {
		return [viewHit widget];
	}

	return nil;
}

//-----------------------------------------------------------------
- (void) animateWidgetToDesktop:(DTWidget*)widget withDirection:(enum swipeDirection)swipeDirection toDesktop:(enum DTDesktopIndex)desktopIndex
{
	LOG_FN();

	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];
	DTWidgetImageView* view = [widget view];

	// Move the widget to new desktop now...
	CGPoint destination = [view center];
	CGRect bounds		= [view bounds];
	CGRect screenBounds	= [UIApplication myMainScreenBounds];

	// Turns out the new desktop is where we are anyway...
	if (desktopIndex == currentDesktopIndex) {
		return;
	}

	switch (swipeDirection) {

		case swipeDirectionUp:
			destination.y = -(bounds.size.height + 5.0);
			break;

		case swipeDirectionDown:
			destination.y = bounds.size.height + screenBounds.size.height + 5.0;
			break;

		case swipeDirectionLeft:
			destination.x = -(bounds.size.width + 5.0);
			break;

		case swipeDirectionRight:
			destination.x = bounds.size.width + screenBounds.size.height + 5.0;
			break;

		default:
			break;
	}

	[self selectWidgetForEdit:nil];

	// Send the widget slding off in the direction we wanted
	[UIView animateWithDuration:0.5
		animations:^{
			[view setCenter:destination];
		}
		completion:^(BOOL didFinish) {
			// Anim finished, destroy the view
			[view removeFromSuperview];
			// And actually move the widget
			[self moveWidget:widget toDesktop:desktopIndex];
		}
	];
}

//-----------------------------------------------------------------
-(BOOL) handleSwipeWidget:(UISwipeGestureRecognizer*)recogniser withDirection:(enum swipeDirection)swipeDirection
{
	LOG_FN();
	// Can only swipe when editing
	DTWidget* widget = [[self desktop] selectedWidget];
	if (! widget) {
		return NO;
	}

	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];
	int newDesktop			= currentDesktopIndex;

	switch (swipeDirection) {
		case swipeDirectionUp:
			switch (currentDesktopIndex) {

				case DTDesktopIndexScrap:
					newDesktop = [self lastDesktopIndex];
					break;

				case DTDesktopIndexBin:
					break;

				default:
					newDesktop = DTDesktopIndexBin;
					break;
			}
			break;

		case swipeDirectionDown:
			switch (currentDesktopIndex) {

				case DTDesktopIndexScrap:
					break;

				case DTDesktopIndexBin:
					newDesktop = [self lastDesktopIndex];
					break;

				default:
					newDesktop = DTDesktopIndexScrap;
					break;
			}
			break;

		case swipeDirectionLeft:
			if (newDesktop > DTDesktopIndexUser) {
				newDesktop = currentDesktopIndex - 1;
			}
			break;

		case swipeDirectionRight:
			if ((newDesktop >= DTDesktopIndexUser) && (newDesktop < ([[self desktopController] getDesktopCount] - 1))) {
				newDesktop = currentDesktopIndex + 1;
			}
			break;

		default:
			break;
	}


	[self animateWidgetToDesktop:widget withDirection:swipeDirection toDesktop:newDesktop];

	return YES;
}


//-----------------------------------------------------------------
- (void) tileWidgets
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"Tile widgets");

	DTDesktop* desktop = [self desktop];
	
	NSMutableArray* sorted = [desktop getWidgetsOrderedBySize];

	CGRect  screen = [UIApplication myMainScreenBoundsRotated];

	float x = 0;
	float y = 0;
	
	__block float maxHeight = 0.0;
	
	float scale = 1.0;
	BOOL retry  = YES;
	
	while (retry) {
		
		retry = NO;
		x = 0;
		y = 0;
		maxHeight = 0.0;
		
		for (DTWidget* widget in sorted) {

			CGRect bounds = [[widget view] bounds];
			
			float h = bounds.size.height * scale;
			float w = bounds.size.width  * scale;
			
			if ( (x + w) > (screen.size.width)) {
				x = 0.0;
				y += maxHeight;
				maxHeight = 0.0;
				
				if ((y + h) > screen.size.height) {
					retry = YES;
					scale /= 1.2;
					break;
				}
			}
			
			if (h > maxHeight) {
				maxHeight = h;
			}
			
			x += w;
			if ( x > screen.size.width) {
				x = 0;
				y += maxHeight;
				maxHeight = 0.0;
				
				if ((y + h) > screen.size.height) {
					retry = YES;
					scale /= 1.2;
					break;
				}
			}
		}
	}
	
	x = 0;
	y = 0;
	maxHeight = 0.0;
	
	for (DTWidget* widget in sorted) {

		CGRect bounds = [[widget view] bounds];
		
		float w = bounds.size.width * scale;
		
		if ( (x + w) > (screen.size.width)) {
			x = 0;
			y += maxHeight;
			maxHeight = 0.0;
		}
		
		[
		 UIView animateWithDuration:0.3 animations:^{

			 CGRect myBounds = [[widget view] bounds];
			 
			 myBounds.size.height *= scale;
			 myBounds.size.width  *= scale;
			 
			 myBounds.origin.x = x + (myBounds.size.width / 2.0);
			 myBounds.origin.y = y + (myBounds.size.height / 2.0);
			 
			 if (myBounds.size.height > maxHeight) {
				 maxHeight = myBounds.size.height;
			 }

			 [widget setPreviousScreenPosition:[widget screenPosition]];
			 [widget setPreviousSize:[widget size]];

			 [widget setScreenPosition:myBounds.origin];
			 [widget setSize:myBounds.size];
		 }
		 completion:^(BOOL finished) {
		 }
		 ];
		
		x += w;
		if ( x > screen.size.width) {
			x = 0.0;
			y += maxHeight;
			maxHeight = 0.0;
			
		}
	}
}

//-----------------------------------------------------------------
- (void) undoTileWidgetsAnimated:(BOOL)animated
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"Tile widgets - undo");

	DTDesktop* desktop = [self desktop];
	
	for (DTWidget* widget in [desktop widgets]) {

		[
		 UIView animateWithDuration:(animated ? 0.3 : 0) animations:^{
				[widget setScreenPosition:[widget previousScreenPosition]];
				[widget setSize:[widget previousSize]];
			}
			completion:^(BOOL finished) {
			}
		 ];
	}
}

//-----------------------------------------------------------------
- (void) rotateWidgets:(UIInterfaceOrientation)newOrientation animated:(BOOL)animated
{
	LOG_FN();

	UIInterfaceOrientation lastOrientation = [self lastOrientation];

	// Nothing to do?
	if (newOrientation == lastOrientation) {
		return;
	}
//	DT_DEBUG_LOG("rotateWidgets (backgroundView#1): %@", NSStringFromCGRect([[self backgroundView] bounds]));

	CGRect			screen		= [UIApplication myMainScreenBounds];
	DTDesktop*		desktop	    = [self desktop];
	NSMutableArray* widgets		= [desktop widgets];

	for (DTWidget* widget in widgets)
	{
		// first of all rotate the widget back to portrait...
		CGPoint currentPosition = [widget screenPosition];
		CGPoint portraitPosition;

		switch (lastOrientation) {
			case UIInterfaceOrientationLandscapeLeft:
			case UIInterfaceOrientationLandscapeRight:

				portraitPosition.x  = currentPosition.y;
				portraitPosition.y  = screen.size.height - currentPosition.x;

				[widget setScreenPosition:portraitPosition];
				[widget setIsDirty:NO];
				break;

			case UIInterfaceOrientationPortrait:
			case UIInterfaceOrientationPortraitUpsideDown:
				break;

			default:
				break;
		}

		// Then rotate from portrait to new rotation
		// Possibly with animation
		[
			UIView animateWithDuration:(animated ? 0.3 : 0.0) animations:^{

				CGPoint position = [widget screenPosition];
				CGPoint newPosition;

				switch (newOrientation) {

					case UIInterfaceOrientationLandscapeLeft:
					case UIInterfaceOrientationLandscapeRight:

						newPosition.y  = position.x;
						newPosition.x  = screen.size.height - position.y;

						[widget setScreenPosition:newPosition];
						[widget setIsDirty:NO];
						break;


					case UIInterfaceOrientationPortrait:
					case UIInterfaceOrientationPortraitUpsideDown:
						break;

					default:
						break;
				}
			}
			completion:^(BOOL finished) {
			}
		];
	}

	[DTWidgetControl updateEditControls:[desktop selectedWidget]];

	[self setLastOrientation:[UIApplication myRotation]];
}

//-----------------------------------------------------------------
- (void) handleSwipeUpTwoFinger:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];

	switch (currentDesktopIndex) {

		case DTDesktopIndexScrap:
			break;

		case DTDesktopIndexBin:
			[self moveToDesktop:[self lastDesktopIndex] withSwipeDirection:swipeDirectionUp];
			break;

		default:
			[self setLastDesktopIndex:currentDesktopIndex];
			[self moveToDesktop:DTDesktopIndexScrap withSwipeDirection:swipeDirectionUp];
			break;
	}
}

//-----------------------------------------------------------------
-(void) handleSwipeUp:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"swipe up");

	if ([[self desktop] isEditing]) {
		(void) [self handleSwipeWidget:recogniser withDirection:swipeDirectionUp];
	}
}

//-----------------------------------------------------------------
-(void) handleSwipeDownTwoFinger:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];

	switch (currentDesktopIndex) {

		case DTDesktopIndexScrap:
			[self moveToDesktop:[self lastDesktopIndex] withSwipeDirection:swipeDirectionDown];
			break;

		case DTDesktopIndexBin:
			break;

		default:
			[self setLastDesktopIndex:currentDesktopIndex];
			[self moveToDesktop:DTDesktopIndexBin withSwipeDirection:swipeDirectionDown];
			break;
	}
}

//-----------------------------------------------------------------
-(void) handleSwipeDown:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"swipe down");

	if ([[self desktop] isEditing]) {
		(void) [self handleSwipeWidget:recogniser withDirection:swipeDirectionDown];
	}
}

//-----------------------------------------------------------------
-(void) handleSwipeLeft:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"swipe left");
	if ([[self desktop] isEditing]) {
		(void) [self handleSwipeWidget:recogniser withDirection:swipeDirectionLeft];
	}
}

//-----------------------------------------------------------------
-(void) handleSwipeLeftTwoFinger:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	[self forwardButtonAction:nil];
}

//-----------------------------------------------------------------
-(void) handleSwipeRight:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	if ([[self desktop] isEditing]) {
		(void) [self handleSwipeWidget:recogniser withDirection:swipeDirectionRight];
	}
}

//-----------------------------------------------------------------
-(void) handleSwipeRightTwoFinger:(UISwipeGestureRecognizer*)recogniser
{
	LOG_FN();
	[self backButtonAction:nil];
}

//-----------------------------------------------------------------
- (id) createWidgetFromClassName:(NSString*)className
{
	LOG_FN();
	id widget = [DTWidget createWidgetFromClass:NSClassFromString(className)];
	[[self desktop] addWidget:widget];

	CGPoint centre = [UIApplication myMainScreenCentre];
	[widget setScreenPosition:centre];
	[self addWidgetToView:widget];
	[self setWidgetInProgress:widget];

	return widget;
}

//-----------------------------------------------------------------
- (void) destroyWidgetInProgress
{
	[self deleteWidget:[self widgetInProgress] animated:NO];
}

//-----------------------------------------------------------------
- (void) completeWidgetInProgress
{
	[self setWidgetInProgress:nil];
}

//-----------------------------------------------------------------
- (id) duplicateWidget:(id)fromWidget
{
	LOG_FN();
	DTDesktop* desktop = [self desktop];
	id widget = [fromWidget copy];

	// Randomisze the position slightly, so that new widgets tile
	CGPoint initialPoint = [widget screenPosition];
	//	DT_DEBUG_LOG(@"Initial pt: %f,%f", initialPoint.x, initialPoint.y);

	float n = (float) (((int)arc4random() % 15) - 8);
	//	DT_DEBUG_LOG(@"rnd1: %f", n);
	initialPoint.x += n;

	n = (float) (((int)arc4random() % 15) - 8);
	//	DT_DEBUG_LOG(@"rnd2: %f", n);
	initialPoint.y += n;
	
	//	DT_DEBUG_LOG(@"After random: %f,%f", initialPoint.x, initialPoint.y);
	[widget setScreenPosition:initialPoint];
	
	[desktop addWidget:widget];
	[self addWidgetToView:widget];

	[widget setIsDirty:YES];
	[widget updateDatabaseFromWidget:[self database]];

	[[self desktop] setIsDirty:YES];

	return widget;
}

//-----------------------------------------------------------------
- (void) duplicateButtonEditAction:(id)sender
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"duplicate widget");

	[self duplicateWidget:[[self desktop] selectedWidget]];
}

//-----------------------------------------------------------------
- (BOOL) iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender
{
	UIButton* button = (UIButton*)sender;
	int tag			 = (int)[button tag];

	if ([imagePicker userObject]) {
		// picking a desktop to go to
		[[imagePicker navigationController] startActivityIndicator];
		[self moveToDesktop:tag withSwipeDirection:swipeDirectionNone];

		id popover = [UIPopoverController getActivePopoverController];
		if (popover) {
			[popover dismissPopoverAnimated:YES];
			[UIPopoverController setActiveController:nil];
		}
	}
	else {
		[imagePicker dismissViewControllerAnimated:NO completion:^{
			// Picking a new widget
			NSArray* classNames = [DTWidget getWidgetClassNameArray];
			DTWidget* widget    = [self createWidgetFromClassName:[classNames objectAtIndex:tag]];

			[widget showCreationSettings:self fromSender:(id)sender];
		}];
	}

	return YES;
}

//-----------------------------------------------------------------
- (void) addButtonPressed:(id)sender
{
	LOG_FN();
	[DTWidgetControl hideEditControls];

	CustomImagePicker* imagePicker = [[CustomImagePicker alloc] initFromWidgetList];
	[imagePicker setDelegate:self];
	[imagePicker setUserObject:nil];

	DTNavigationController* navigationController =
		[[DTNavigationController alloc] initWithRootViewController:imagePicker];

	[navigationController setBottomToolbarHidden:YES animated:NO];

	[imagePicker setNavigationController:navigationController];

	[self presentViewController:navigationController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) popoverControllerDidDismissPopover:(UIPopoverController*)popoverController
{
	if (popoverController == [UIPopoverController getActivePopoverController]) {
		[UIPopoverController setActiveController:nil];
	}
}

//-----------------------------------------------------------------
- (void) selectDesktopButtonPressed:(id)sender
{
	BOOL isPad = [UIApplication isPad];
	
	// If we have the popup list shown, a second click simply dismsses it
	id activePopover = [UIPopoverController getActivePopoverController];
	if (isPad && activePopover) {
		[activePopover dismissPopoverAnimated:NO];
		[UIPopoverController setActiveController:nil];
		return;
	}

	const float iconWidth  = 86;
	const float iconHeight = iconWidth*3/2;

	LOG_FN();
	[DTWidgetControl hideEditControls];
	[self updateDatabaseFromCurrentDesktop];

	CustomImagePicker* imagePicker = [[CustomImagePicker alloc] init];
	[imagePicker setDelegate:self];
	[imagePicker setUserObject:self];
	[imagePicker setIconWidth:iconWidth];
	[imagePicker setIconHeight:iconHeight];

	CGSize viewSize = [imagePicker sizeForIconGridWithWidth:4 height:3];
	if (isPad) {
		if ([UIApplication isLandscape]) {
			viewSize = [imagePicker sizeForIconGridWithWidth:5 height:3];
		}
		[imagePicker setViewSize:viewSize];
		[imagePicker setScrollEnabled:NO];
	}

	[imagePicker setTitle:NSLocalizedString(@"selectSpace", nil)];
	[imagePicker setBackgroundColour:[UIColor colorWithWhite:0.2f alpha:1.0f]];

	// Collect a thumbnail image from each desktop
	UIImage* blank = nil;
	DTDesktopController* controller = [self desktopController];
	for (int desktopIndex = 0; desktopIndex < [controller getDesktopCount]; desktopIndex++) {

		DTDesktop* desktop = [controller getDesktopFromIndex:desktopIndex];
		UIImage* image = [desktop snapshot];

		if (! image) {
			// If there's no thumbnail for this desktop yet, just draw a blank square
			if (! blank) {
				CGSize size	= CGSizeMake(iconWidth, iconHeight);
				blank 		= [UIImage createColouredRectangle:size colour:[UIColor lightGrayColor]];
			}
			image = blank;
		}

		[imagePicker addImage:image resizeToFit:NO];
	}

	if (isPad) {
		// Thumbnails exist in a popover
		UIPopoverController* thumbnailPopover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
		[UIPopoverController setActiveController:thumbnailPopover];
		[thumbnailPopover setPopoverContentSize:viewSize];
		[thumbnailPopover setDelegate:self];
		[thumbnailPopover presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
	}
	else {
		// Thumbnails exist in a navigation controller
		DTNavigationController* navigationController =
			[[DTNavigationController alloc] initWithRootViewController:imagePicker];

		[navigationController setBottomToolbarHidden:YES animated:NO];

		[imagePicker setNavigationController:navigationController];
		[[navigationController navigationBar] setBarStyle:UIBarStyleBlack];
		[self presentViewController:navigationController animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) newNoteButtonPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"new note button");

	[DTWidgetControl hideEditControls];
	DTWidget* widget = [self createWidgetFromClassName:@"DTPostItNoteWidget"];
	[widget showCreationSettings:self fromSender:sender];
}

//-----------------------------------------------------------------
- (void) newCameraButtonPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"new camera button");

	[DTWidgetControl hideEditControls];
	DTWidget* widget = [self createWidgetFromClassName:@"DTPhotoWidget"];
	[widget showCreationSettings:self fromSender:sender];
}

//-----------------------------------------------------------------
- (void) newWebButtonPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"new webpage button");

	[DTWidgetControl hideEditControls];
	DTWidget* widget = [self createWidgetFromClassName:@"DTShortcutWidget"];
	[widget showCreationSettings:self fromSender:sender];
}

//-----------------------------------------------------------------
- (void) newImageButtonPressed:(id)sender
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"new image button");

	if ([UIPopoverController dismissActiveController]) {
		[self destroyWidgetInProgress];
		return;
	}

	[DTWidgetControl hideEditControls];

	DTWidget* widget = [self createWidgetFromClassName:@"DTPictureWidget"];
	[widget showCreationSettings:self fromSender:sender];
}

//-----------------------------------------------------------------
- (void) newImageHeld:(UILongPressGestureRecognizer*)recogniser
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"new image - held button");

	if ([recogniser state] == UIGestureRecognizerStateBegan) {

		[DTWidgetControl hideEditControls];
		DTWidget* widget = [self createWidgetFromClassName:@"DTPictureWidget"];
		[widget showCreationSettings:self fromSender:[self cameraButton]];
	}
}

//-----------------------------------------------------------------
- (void) settingsButtonEditAction:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"settings button");
	
	[DTWidgetControl hideEditControls];
	DTWidget* selectedWidget = [[self desktop] selectedWidget];
	[selectedWidget showSettingsModal:[UIApplication myRootViewController] fromSender:sender];
}

//-----------------------------------------------------------------
- (void) actionButtonEditPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"action button");
	
	[DTWidgetControl hideEditControls];
	DTWidget* selectedWidget = [[self desktop] selectedWidget];
	[selectedWidget actionButtonAction:sender];
}

//-----------------------------------------------------------------
- (void) settingsButtonAction:(id)sender
{
	[self showSettings];
}

//-----------------------------------------------------------------
- (void) deleteWidget:(DTWidget*)widget animated:(BOOL)animated
{
	LOG_FN();
	// Don't edit a deleted widget
	[self selectWidgetForEdit:nil];

	// Remove from Desktop
	[[self desktop] removeWidget:widget];

	DT_ASSERT([widget index] >= 0);

	// Remove from database
	[[self database] executeUpdate:@"DELETE FROM widgets WHERE widgetID=?", [NSNumber numberWithInt:[widget index]]];

	// Let widget destroy itself and anything else it needs to do
	[widget destroy:animated];

	[[self desktop] setThumbnailIsDirty:YES];
}

//-----------------------------------------------------------------
- (void) deleteButtonEditAction:(id)sender
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"delete button");

	DTWidget* widget		= [[self desktop] selectedWidget];
	int currentDesktopIndex = [[self desktopController] currentDesktopIndex];

	if (widget) {
		switch (currentDesktopIndex) {

			case DTDesktopIndexBin:
				[self deleteWidget:widget animated:YES];
				break;

			case DTDesktopIndexScrap:
			default:
				[self animateWidgetToDesktop:widget withDirection:swipeDirectionUp toDesktop:DTDesktopIndexBin];
				break;
		}
	}
}

//-----------------------------------------------------------------
static swipeDirection swipeDirectionAll = swipeDirectionNone;
static BOOL isArrange = NO;

- (void) actionSheet:(UIActionSheet*)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (isArrange) {

		switch ([actionSheet getMenuIndexFromButtonIndex:(int)buttonIndex]) {
			case 1:
				[self tileWidgets];
				break;

			case 2:
				[self undoTileWidgetsAnimated:YES];
				break;

			default:
				break;
		}

	}
	else {
		// Delete all??
		if (buttonIndex == 0) {

			[self selectWidgetForEdit:nil];

			DTDesktop* desktop		= [self desktop];
			int currentDesktopIndex = [[self desktopController] currentDesktopIndex];
			int cWidgets			= [desktop getWidgetCount];

			while (cWidgets) {
				// direction none = deleting
				if (swipeDirectionAll == swipeDirectionNone) {

					if (currentDesktopIndex == DTDesktopIndexBin) {
						// If it's the bin, delete for ever

						// Always pick the first widget in the list, the list will
						// shrink as we remove each one from the front (done when it's deleted)
						DTWidget* widget = [desktop getWidgetFromIndex:0];
						[self deleteWidget:widget animated:YES];

						// How many are now left?
						cWidgets = [desktop getWidgetCount];
					}
					else {
						// Else just move them to the recycle bin
						DTWidget* widget = [desktop getWidgetFromIndex:(cWidgets - 1)];
						[self animateWidgetToDesktop:widget withDirection:swipeDirectionUp toDesktop:DTDesktopIndexBin];
						cWidgets--;
					}
				}
				else {
					DTWidget* widget = [desktop getWidgetFromIndex:(cWidgets - 1)];
					cWidgets--;

					switch (swipeDirectionAll) {

						case swipeDirectionDown:
							if (currentDesktopIndex != DTDesktopIndexScrap) {
								[self animateWidgetToDesktop:widget withDirection:swipeDirectionDown toDesktop:DTDesktopIndexScrap];
							}
							break;

						case swipeDirectionUp:
							if (currentDesktopIndex == DTDesktopIndexScrap) {
								[self animateWidgetToDesktop:widget withDirection:swipeDirectionUp toDesktop:[self lastDesktopIndex]];
							}
							break;

						default:
							break;
					}
				}
			}
		}
	}
}

//-----------------------------------------------------------------
- (void) handleMoveOrDeleteConfirmation:(UILongPressGestureRecognizer*)recogniser
						  withDirection:(swipeDirection)direction buttonTitle:(NSString*)title
{
	if ([recogniser state] == UIGestureRecognizerStateBegan) {

		swipeDirectionAll = direction;
		isArrange = NO;

		UIActionSheet* sheet = [
			[UIActionSheet alloc] initWithTitle:nil
			delegate:self
			cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
			destructiveButtonTitle:title
			otherButtonTitles:
			nil
		];
		[sheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];

		[sheet showInView:[self view]];
	}
}

//-----------------------------------------------------------------
- (void) deleteAllButtonPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"delete all");
	UILongPressGestureRecognizer* recogniser = (UILongPressGestureRecognizer*) sender;

	[self handleMoveOrDeleteConfirmation:recogniser withDirection:swipeDirectionNone buttonTitle:@"Delete All"];
}

//-----------------------------------------------------------------
- (void) downAllButtonPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"down all down");
	UILongPressGestureRecognizer* recogniser = (UILongPressGestureRecognizer*) sender;

	[self handleMoveOrDeleteConfirmation:recogniser withDirection:swipeDirectionDown buttonTitle:@"Move All"];
}

//-----------------------------------------------------------------
- (void) upAllButtonPressed:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"move all up");
	UILongPressGestureRecognizer* recogniser = (UILongPressGestureRecognizer*) sender;

	[self handleMoveOrDeleteConfirmation:recogniser withDirection:swipeDirectionUp buttonTitle:@"Move All"];
}

//-----------------------------------------------------------------
- (void) handleArrangeMenu:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"arrange menu");
	
	UILongPressGestureRecognizer* recogniser = (UILongPressGestureRecognizer*) sender;

	if ([recogniser state] == UIGestureRecognizerStateBegan) {

		isArrange = YES;

		UIActionSheet* sheet = [
			[UIActionSheet alloc] initWithTitle:nil
			delegate:self
			cancelButtonTitle:nil
			destructiveButtonTitle:nil
			otherButtonTitles:
			nil
		];
		[sheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];

		[sheet addButtonWithTitle:NSLocalizedString(@"Tile", nil) 	menuIndex:1];
		[sheet addButtonWithTitle:NSLocalizedString(@"Undo", nil) 	menuIndex:2];
		[sheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)	menuIndex:3];
		[sheet setCancelButtonIndex:[sheet getButtonIndexFromMenuIndex:3]];

		[sheet showFromBarButtonItem:sender animated:YES];
	}
}

//-----------------------------------------------------------------
- (void) downButtonEditAction:(id)sender
{
	LOG_FN();
	DTWidget* widget = [[self desktop] selectedWidget];

	switch ([[self desktopController] currentDesktopIndex]) {
			
		case DTDesktopIndexScrap:
			break;
		
		case DTDesktopIndexBin:
			[self animateWidgetToDesktop:widget withDirection:swipeDirectionDown toDesktop:[self lastDesktopIndex]];
			break;
			
		default:
			[self animateWidgetToDesktop:widget withDirection:swipeDirectionDown toDesktop:DTDesktopIndexScrap];
			break;
	}
}

//-----------------------------------------------------------------
- (void) upButtonEditAction:(id)sender
{
	LOG_FN();
	DTWidget* widget = [[self desktop] selectedWidget];
	
	if ([[self desktopController] currentDesktopIndex] == DTDesktopIndexScrap) {
		[self animateWidgetToDesktop:widget withDirection:swipeDirectionUp toDesktop:[self lastDesktopIndex]];
	}
}

//-----------------------------------------------------------------
-(void) selectWidgetForEdit:(DTWidget*)widget
{
	DTDesktop*	desktop		= [self desktop];
	BOOL		wasEditing	= [desktop isEditing];

	// Remove any widget being edited
	DTWidget* oldWidget = [desktop selectedWidget];

	if (oldWidget == widget) {
	}
	else {
		[oldWidget updateDatabaseFromWidget:[self database]];
		[[oldWidget view] unSelect];

		[desktop selectWidgetForEdit:widget];

		if (widget) {
			[[self zoomView] bringSubviewToFront:[widget view]];
			[widget setZPosition:0];
			[[self desktop] setIsDirty:YES];

			[[widget view] selectForEdit];

			[DTWidgetControl updateEditControlsForWidget:widget];
			[DTWidgetControl showEditControls];
		}
		else {
			[DTWidgetControl removeEditControls];
		}

		// Decide which toolbar we use
		if (wasEditing != [desktop isEditing]) {
			
			NSArray* items = nil;
			
			if (wasEditing) {
				items = [self viewToolbarItems];
			}
			else {
				if ([[self desktopController] currentDesktopIndex] == DTDesktopIndexScrap) {
					items = [self editInboxToolbarItems];
				}
				else {
					items = [self editToolbarItems];
				}
			}
			
			[[self toolbar] setItems:items animated:NO];
		}
	}
}

//-----------------------------------------------------------------
-(void) panWidget:(UIGestureRecognizer*)recogniser forWidget:(DTWidget*)widget withPoint:(CGPoint)newCentre
{
	LOG_FN();
	if (widget) {
		switch ([recogniser state]) {

			case UIGestureRecognizerStateBegan:
				[self selectWidgetForEdit:widget];
				[DTWidgetControl hideEditControls];
				break;

			case UIGestureRecognizerStateChanged:
				[widget setScreenPosition:newCentre];
				[DTWidgetControl updateEditControls:widget];
				break;

			case UIGestureRecognizerStateEnded:
				[widget setScreenPosition:newCentre];
				[DTWidgetControl updateEditControls:widget];
				[DTWidgetControl showEditControls];
				break;

			default:
				break;
		}
	}
}

//-----------------------------------------------------------------
-(void) handleDoubleTap:(UILongPressGestureRecognizer*)recogniser
{
	LOG_FN();
	if  ([recogniser state] == UIGestureRecognizerStateEnded) {

		id widget = [self FindWidgetFromGestureRecognizer:recogniser];
		[widget handleDoubleTap:self];
	}
}

//-----------------------------------------------------------------
-(void) selectedWidgetChangeZ
{
	LOG_FN();
	DTWidgetImageView* view;
	
	DTDesktop* desktop = [self desktop];
	int desktopCount   = [desktop getWidgetCount];

		if ([desktop isEditing]) {
			DTWidget* widget = [desktop selectedWidget];
			if (widget) {

				view = [widget view];

				if ([widget zPosition]) {

					[DTWidgetControl hideEditControls];

					// animate all other widgets out the way
					[UIView animateWithDuration:0.3 animations:^{
						for (int i = 0; i < desktopCount; i++) {
							DTWidget* w = [desktop getWidgetFromIndex:i];
							if (w != widget) {
								[[w view] setScale:0.1];
							}
						}
					}];
					
					// Animate the active widget
					[UIView animateWithDuration:0.3
					 animations:^{
						 [view setScale:1.3];
					 }
					 completion:^(BOOL finished) {
						 [[self zoomView] bringSubviewToFront:view];
						 
						 [UIView animateWithDuration:0.3 animations:^{
							 for (int i = 0; i < desktopCount; i++) {
								 DTWidget* w = [desktop getWidgetFromIndex:i];
								 if (w != widget) {
									 [[w view] setScale:1.0];
								 }
							 }
						 }];

						 [UIView animateWithDuration:0.3
							  animations:^{
								  [view setScale:1.0];
							  }
							  completion:^(BOOL finished2) {
								  [view setScale:1.0];
								  [widget setZPosition:0];
								  [DTWidgetControl updateEditControls:widget];
								  [DTWidgetControl showEditControls];
							  }
						  ];
					 }
					 ];
				}
				else {
					[DTWidgetControl hideEditControls];

					[UIView animateWithDuration:0.3 animations:^{
						for (int i = 0; i < desktopCount; i++) {
							DTWidget* w = [desktop getWidgetFromIndex:i];
							if (w != widget) {
								[[w view] setScale:0.1];
							}
						}
					}];
					
					[UIView animateWithDuration:0.3
					 animations:^{
						 [view setScale:0.7];
						 [view setAlpha:0.5];
					 }
					 completion:^(BOOL finished) {
						 [[self zoomView] sendSubviewToBack:[widget view]];

						 [UIView animateWithDuration:0.3 animations:^{
							 for (int i = 0; i < desktopCount; i++) {
								 DTWidget* w = [desktop getWidgetFromIndex:i];
								 if (w != widget) {
									 [[w view] setScale:1.0];
								 }
							 }
						 }];

						 [UIView animateWithDuration:0.3
							  animations:^{
								  [view setScale:1.0];
								  [view setAlpha:1.0];
							  }
							  completion:^(BOOL finished2) {
								  [view setScale:1.0];
								  [view setAlpha:1.0];
								  [widget setZPosition:1];
								  [DTWidgetControl updateEditControls:widget];
								  [DTWidgetControl showEditControls];
							  }
						  ];
					 }
					 ];
				}
			}
		}
	//	}
}

//-----------------------------------------------------------------
-(void) handleSingleTap2Fingers:(UILongPressGestureRecognizer*)recogniser
{
	LOG_FN();
	TESTFLIGHT_CHECKPOINT(@"single tap - 2 fingers");
	if ([recogniser state] == UIGestureRecognizerStateEnded) {
		[self selectedWidgetChangeZ];
	}
}

//-----------------------------------------------------------------
- (void) orderZButtonEditAction:(id)sender
{
	TESTFLIGHT_CHECKPOINT(@"change Z order");
	[self selectedWidgetChangeZ];
}

//-----------------------------------------------------------------
-(void) dispatchSingleTap:(UILongPressGestureRecognizer*)recogniser
{
	LOG_FN();
	if ([recogniser state] == UIGestureRecognizerStateEnded) {

		// find what we tapped
		id objectHit = [self FindViewFromGestureRecognizer:recogniser];

		DTWidget* widget = [[self desktop] selectedWidget];

		if (([widget view] == objectHit) && [[self desktop] isEditing]) {
			[self selectWidgetForEdit:nil];
		}
		else {
			// if it doesn't have a widget, we can't do anything....
			if ([objectHit respondsToSelector:@selector(handleSingleTap:)]) {
				[DTWidgetControl hideEditControls];
				[objectHit handleSingleTap:self];
			}
			else {
				// Tapped on something other than a widget or control (E.g. desktop)
				// This cancels edit mode
				[self selectWidgetForEdit:nil];
			}
		}
	}
}

//-----------------------------------------------------------------
-(void) handleFingerDown:(UIGestureRecognizer*)recogniser atPoint:(CGPoint)newCentre hitObject:(id)objectHit
{
	static bool		isResizing			= NO;
	static CGSize	initialWidgetSize   = {0, 0};
	static CGPoint	initialWidgetCentre = {0, 0};
	static CGPoint	initialFinger       = {0, 0};
	static CGRect	initialBounds		= {{0, 0}, {0, 0}};
	static id		initialObjectHit	= nil;

	LOG_FN();

	if ([recogniser state] == UIGestureRecognizerStateEnded) {
		DTWidget* widget = [[self desktop] selectedWidget];
		[[widget view] selectForEdit];
	}

	if ([recogniser state] == UIGestureRecognizerStateBegan) {
		DTWidget* widget = [self FindWidgetFromGestureRecognizer:recogniser];
		[self selectWidgetForEdit:widget];

		CGPoint widgetCentre = [widget screenPosition];

		isResizing		 = [objectHit isKindOfClass:[DTWidgetControl class]];
		initialObjectHit = objectHit;

		initialFinger		= newCentre;
		initialWidgetCentre	= widgetCentre;
		initialBounds		= [widget getBounds];
		initialWidgetSize	= [widget size];

		if (isResizing) {
			[[self zoomView] bringSubviewToFront:[widget view]];
			[widget setZPosition:0];

			[((DTWidgetControl*)objectHit) selectForEdit];
		}
		else {
			[DTWidgetControl hideEditControls];
		}
		[[widget view] selectForDragging];
	}
	else {
		DTWidget* widget = [[self desktop] selectedWidget];

		if (isResizing) {
			// Calculate the difference between where we were, and where we are now
			CGPoint difference = CGPointMake(initialFinger.x - newCentre.x, initialFinger.y - newCentre.y);
			[DTWidgetControl resizeWidget:widget usingControl:initialObjectHit sizeOffset:difference initialBounds:initialBounds];
			[DTWidgetControl updateEditControlsForWidget:widget];

			if ([recogniser state] == UIGestureRecognizerStateEnded) {
				// Let widget know the resizing has finished
				[widget hasFinishedResizing:[widget size]];
				[((DTWidgetControl*)initialObjectHit) unselectForEdit];
				[DTWidgetControl showEditControls];
				[[self desktop] setIsDirty:YES];
			}
		}
		else {
			CGPoint difference   = CGPointMake(initialFinger.x - newCentre.x, initialFinger.y - newCentre.y);
			CGPoint centre       = CGPointMake(initialWidgetCentre.x - difference.x, initialWidgetCentre.y - difference.y);

			if ([recogniser state] == UIGestureRecognizerStateEnded) {
				if ([[self myOptions] alignIcons]) {
					centre.x = roundToNearest(centre.x, 32.0);
					centre.y = roundToNearest(centre.y, 32.0);
				}
				[[self desktop] setIsDirty:YES];
			}

			[self panWidget:recogniser forWidget:widget withPoint:centre];
		}
	}
}


//-----------------------------------------------------------------
-(void) handleLongPressCancel:(UILongPressGestureRecognizer*)recogniser
{
	if ([recogniser state] == UIGestureRecognizerStateEnded) {
		[self selectWidgetForEdit:nil];
	}
}

//-----------------------------------------------------------------
-(void) handleLongPress:(UILongPressGestureRecognizer*)recogniser
{
	CGPoint newCentre = [recogniser locationInView:[self zoomView]];
	id objectHit      = [self FindViewFromGestureRecognizer:recogniser];

	[self handleFingerDown:recogniser atPoint:newCentre hitObject:objectHit];
}

//-----------------------------------------------------------------
-(void) handleScale:(UIPinchGestureRecognizer*)recogniser
{
	static CGSize	initialSize;

	if ([[self desktop] isEditing]) {

		DTWidget* widget  = [[self desktop] selectedWidget];

		if ([recogniser state] == UIGestureRecognizerStateBegan) {
			initialSize	= [widget size];
		}

		// Give me a scale that's not quite so harsh...
		float scale = [recogniser scale] * 0.8;

		CGSize newSize = CGSizeMake(initialSize.width * scale, initialSize.height * scale);

		// Don't make widget too small
		if ((newSize.width > 56.0) && (newSize.height > 56.0)) {
			[widget setSize:newSize];
		}

		[DTWidgetControl updateEditControlsForWidget:widget];
	}
}

//-----------------------------------------------------------------
- (void) handlePan:(UIPanGestureRecognizer*)recogniser
{
	UIView* zoomView = [self zoomView];
	UIView* baseView = [self backgroundView];

	static CGPoint start;
	static CGPoint startZoomPoint;
	static swipeDirection direction = swipeDirectionNone;

	float	offsetX;
	float	offsetY;
	float	absOffsetX;
	float	absOffsetY;
	CGPoint currentOffset;
	CGPoint newCentre = [recogniser translationInView:zoomView];

	if ([recogniser state] == UIGestureRecognizerStateBegan) {
		start			= newCentre;
		direction		= swipeDirectionNone;
		startZoomPoint	= [baseView center];
		return;
	}

	offsetX		= (newCentre.x - start.x);
	absOffsetX	= fabsf(offsetX);
	offsetY		= (newCentre.y - start.y);
	absOffsetY	= fabsf(offsetY);

	if (direction == swipeDirectionNone) {
		// Decide which direction we are trying to swipe in, and validate it
		int desktopIndex = [[self desktopController] currentDesktopIndex];

		switch (desktopIndex) {

			case DTDesktopIndexBin:
				direction = swipeDirectionDown;
				break;

			case DTDesktopIndexScrap:
				direction = swipeDirectionUp;
				break;

			default:
				if ((absOffsetX > 0.0) || (absOffsetY > 0.0)) {
					direction = (absOffsetX > absOffsetY) ? swipeDirectionLeftRight : swipeDirectionUpDown;
				}

				if ((desktopIndex <= DTDesktopIndexUser) && (direction == swipeDirectionLeftRight)) {
					direction = swipeDirectionLeft;
				}
				if ((desktopIndex >= ([[self desktopController] getDesktopCount] - 1)) && (direction == swipeDirectionLeftRight)) {
					direction = swipeDirectionRight;
				}
				break;
		}
	}

	//	DT_DEBUG_LOG(@"Offset: %f,%f, (%d)", offsetX, offsetY, direction);
	switch (direction) {

		case swipeDirectionDown:
			if (offsetY > 0.0) {
				offsetY = 0.0;
			}
			// Fall through break;

		case swipeDirectionUpDown:
			offsetX = 0.0;
			break;

		case swipeDirectionUp:
			offsetX = 0.0;
			if (offsetY < 0.0) {
				offsetY = 0.0;
			}
			break;

		case swipeDirectionLeft:
			offsetY = 0.0;
			if (offsetX > 0.0) {
				offsetX = 0.0;
			}
			break;

		case swipeDirectionRight:
			if (offsetX < 0.0) {
				offsetX = 0.0;
			}
			// fall through break;

		case swipeDirectionLeftRight:
			offsetY = 0.0;
			break;

		default:
			break;
	}

	if (offsetX == 0.0) {
		start.x = newCentre.x;
	}
	if (offsetY == 0.0) {
		start.y = newCentre.y;
	}

	//	DT_DEBUG_LOG(@"Corrected Offset: %f,%f", offsetX, offsetY);

	currentOffset = CGPointMake(startZoomPoint.x + offsetX, startZoomPoint.y + offsetY);
	[baseView setCenter:currentOffset];

	CGPoint offset = [UIApplication myMainScreenCentre];
	offset.x /= 2.0;
	offset.y /= 2.0;

	swipeDirection possibleDirection = swipeDirectionNone;

	if (offsetX > offset.x) {
		possibleDirection = swipeDirectionRight;
	}
	else {
		if (offsetX < -offset.x) {
			possibleDirection = swipeDirectionLeft;
		}
		else {
			if (offsetY > offset.y) {
				possibleDirection = swipeDirectionDown;
			}
			else {
				if (offsetY < -offset.y) {
					possibleDirection = swipeDirectionUp;
				}
			}
		}
	}

	// Display an arrow in the correct direction if the desktop is able to switch
	UIImageView* desktopSwitchArrow = [self desktopSwitchArrow];

	CGRect  screen = [UIApplication myMainScreenBoundsRotated];
	CGPoint centre = [UIApplication myMainScreenCentre];
	CGRect  bounds = [desktopSwitchArrow bounds];

	switch (possibleDirection) {

		case swipeDirectionDown:
			[desktopSwitchArrow setCenter:CGPointMake(centre.x, bounds.size.height)];
			[desktopSwitchArrow setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0.0))];
			[desktopSwitchArrow showWithAnimation];
			break;

		case swipeDirectionUp:
			[desktopSwitchArrow setCenter:CGPointMake(centre.x, screen.size.height - bounds.size.height - kToolBarHeight)];
			[desktopSwitchArrow setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180.0))];
			[desktopSwitchArrow showWithAnimation];
			break;

		case swipeDirectionRight:
			[desktopSwitchArrow setCenter:CGPointMake(bounds.size.width, centre.y)];
			[desktopSwitchArrow setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(270.0))];
			[desktopSwitchArrow showWithAnimation];
			break;

		case swipeDirectionLeft:
			[desktopSwitchArrow setCenter:CGPointMake(screen.size.width - bounds.size.width, centre.y)];
			[desktopSwitchArrow setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90.0))];
			[desktopSwitchArrow showWithAnimation];
			break;

		default:
			[desktopSwitchArrow hideWithAnimation];
			break;
	}

	if ([recogniser state] == UIGestureRecognizerStateEnded) {

		[desktopSwitchArrow hideWithAnimation];

		// do the actual desktop switch, or simply animate the display back into place
		switch (possibleDirection) {

			case swipeDirectionDown:
				TESTFLIGHT_CHECKPOINT(@"swipe down");
				[self handleSwipeDownTwoFinger:nil];
				break;

			case swipeDirectionUp:
				TESTFLIGHT_CHECKPOINT(@"swipe up");
				[self handleSwipeUpTwoFinger:nil];
				break;

			case swipeDirectionLeft:
				TESTFLIGHT_CHECKPOINT(@"swipe left");
				[self handleSwipeLeftTwoFinger:nil];
				break;

			case swipeDirectionRight:
				TESTFLIGHT_CHECKPOINT(@"swipe right");
				[self handleSwipeRightTwoFinger:nil];
				break;

			default:
				[
					UIView
					animateWithDuration:0.3
					animations:^{
						[baseView setCenter:startZoomPoint];
						[baseView setAlpha:1.0];
					}
				  ];
				break;
		}
	}
}

//-----------------------------------------------------------------
- (void) viewDidUnload
{
	LOG_FN();
	[super viewDidUnload];

	// Save state of app...
	[self saveState];

	[[self database] close];
}

//-----------------------------------------------------------------
// iOS 5
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	LOG_FN();
	return YES;
}

//-----------------------------------------------------------------
// IOS X
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
}

//-----------------------------------------------------------------
// IOS X
- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	//	[self setLastOrientation:[UIApplication myRotation]];
	//	[self rotateWidgets:interfaceOrientation animated:YES];
}

//-----------------------------------------------------------------
// IOS X
- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	// If we have the popup list shown, dismiss and re-do
	id activePopover = [UIPopoverController getActivePopoverController];
	if ([UIApplication isPad] && activePopover) {

		// is the popup list the list of thumbnails?
		if ([[activePopover contentViewController] isKindOfClass:[CustomImagePicker class]]) {

			// Yes - remove it
			[activePopover dismissPopoverAnimated:NO];
			[UIPopoverController setActiveController:nil];

			// and re-create in new postion
			[self selectDesktopButtonPressed:[self selectDesktopButton]];
		}
	}

	[self rotateWidgets:interfaceOrientation animated:YES];
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return YES;
}

//-----------------------------------------------------------------
- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

	[self rotateWidgets:[UIApplication myRotation] animated:NO];
}

//-----------------------------------------------------------------
- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];

	[self setLastOrientation:[UIApplication myRotation]];
}

@end

//-----------------------------------------------------------------
