//
//  DTHTMLViewController.m
//  DeskTopper
//
//  Created by David Shea on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "macros.h"
#import "DTHTMLViewController.h"
#import "NSString+Markdown.h"
#import "NSString+AttributedString.h"
#import "DTPostItNoteWidget.h"
#import "SVProgressHUD.h"
#import "DTNavigationController.h"
#import "UIBarButtonItem+StandardButtons.h"
#import "DTBrowserController.h"
#import "DTShortcutWidget.h"
#import "UIApplication+Helpers.h"
#import "DTViewController.h"
#import "UIImageExtras.h"
#import "MBProgressHUD.h"
#import "MBProgressHUD+Extras.h"
#import "UIColor+StandardColours.h"
#import "DTPostItNoteWidget.h"
#import "NSString+URL.h"
#import "DTHelpViewController.h"
#import "FMDatabase+Variables.h"

//-----------------------------------------------------------------
static const float DefaultTextScale = 1.2f;
static const float MinimumTextScale = 0.5f;
static const float MaximumTextScale = 2.5f;
//static const float ZoomTextScale	= 2.1f;

//-----------------------------------------------------------------
@implementation DTHTMLViewController

@synthesize webView					= _webView;
@synthesize textView				= _textView;
@synthesize HTML					= _HTML;
@synthesize CSS						= _CSS;
@synthesize generatedCSS			= _generatedCSS;
@synthesize shortString				= _shortString;
@synthesize isResizing				= _isResizing;
@synthesize scale					= _scale;
@synthesize textScale				= _textScale;
@synthesize navigationController	= _navigationController;
@synthesize backgroundColour		= _backgroundColour;
@synthesize textColour				= _textColour;
@synthesize linkColour				= _linkColour;
@synthesize fontName				= _fontName;
@synthesize fontSize				= _fontSize;
@synthesize browserViewController	= _browserViewController;
@synthesize linkWidget				= _linkWidget;
@synthesize clickedLink				= _clickedLink;

@synthesize attributedTextViewDelegate	= _attributedTextViewDelegate;

//-----------------------------------------------------------------
- (void) dealloc
{
	DT_DEBUG_LOG(@"HTML Controller dealloc");
}

//-----------------------------------------------------------------
- (void) buildCSS
{
	NSString* css = @"<head><style type=\"text/css\">";

	css = [css stringByAppendingString:@"body {"];
	css = [css stringByAppendingString:[NSString stringWithFormat:@"background-color:%@;",	[[self backgroundColour] HTMLName]]];
	css = [css stringByAppendingString:[NSString stringWithFormat:@"color:%@;",				[[self textColour] HTMLName]]];
	css = [css stringByAppendingString:[NSString stringWithFormat:@"font-family:%@;",		[self fontName]]];
	css = [css stringByAppendingString:[NSString stringWithFormat:@"font-size:12pt;"]];
	css = [css stringByAppendingString:[NSString stringWithFormat:@"font-size:%@;",			[self fontSize]]];
	css = [css stringByAppendingString:@"}"];
	css = [css stringByAppendingString:[NSString stringWithFormat:@"a:link {color:%@;}",	[[self linkColour] HTMLName]]];

	if ([self CSS]) {
		css = [css stringByAppendingString:[self CSS]];
	}

	css = [css stringByAppendingString:@"</style>"];

	css = [css stringByAppendingString:[NSString stringWithFormat:@"</<base href=\"file://%@\">", [FileHelper getDocumentPath]]];

	css = [css stringByAppendingString:@"</head>"];

	css = [css stringByAppendingString:@"<meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=4.0;\">"];

	[self setGeneratedCSS:css];

	DT_DEBUG_LOG(@"CSS:%@", [self generatedCSS]);
}

//-----------------------------------------------------------------
- (void) setBackgroundColour:(UIColor*)colour
{
	[[self webView] setBackgroundColor:colour];

	_backgroundColour = colour;
	[self buildCSS];
}

//-----------------------------------------------------------------
- (void) setTextColour:(UIColor*)colour
{
	_textColour = colour;
	[self buildCSS];
}

//-----------------------------------------------------------------
- (void) setFontName:(NSString*)fontName
{
	_fontName = fontName;
	[self buildCSS];
}

//-----------------------------------------------------------------
- (void) setFontSize:(NSString*)fontSize
{
	_fontSize = fontSize;
	[self buildCSS];
}

//-----------------------------------------------------------------
- (void) setFontSizeFromInt:(int)size
{
	[self setFontSize:[NSString stringWithFormat:@"%d%%", size]];
}

//-----------------------------------------------------------------
- (int) getFontSizeAsInt
{
	NSString* percent = [[self fontSize] stringByReplacingOccurrencesOfString:@"%" withString:@""];
	return [percent intValue];
}

//-----------------------------------------------------------------
- (void) setCSS:(NSString*)css
{
	_CSS = css;
	[self buildCSS];
}

//-----------------------------------------------------------------
- (void) setTextScale:(float)textScale
{
	if (textScale < MinimumTextScale) {
		_textScale = MinimumTextScale;
	}
	else {
		if (textScale > MaximumTextScale) {
			_textScale = MaximumTextScale;
		}
		else {
			_textScale = textScale;
		}
	}
}

//-----------------------------------------------------------------
- (id) initWithString:(NSString*)HTMLText
{
	if (self = [super init]) {
		[self setTextScale:DefaultTextScale];
	}
	return self;
}

//-----------------------------------------------------------------
- (void) loadView
{
	LOG_FN();
	[super loadView];

	//	UIView* view = [self view];
	//	CGRect frame = [view frame];
	CGRect frame = [UIApplication myMainScreenBounds];

	UIView* contentView = [[UIView alloc] initWithFrame:[UIApplication myMainScreenBounds]];
	[contentView setBackgroundColor:[UIColor blackColor]];
	[contentView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
	[contentView setAutoresizesSubviews:YES];
	[self setView:contentView];

	UIWebView* webView = [[UIWebView alloc] initWithFrame:frame];
	 // Top, left, bottom, right
	[[webView scrollView] setContentInset:UIEdgeInsetsMake(0.0, 0.0, 32.0, 0.0)];
	[[webView scrollView] setScrollIndicatorInsets:UIEdgeInsetsMake(24.0, 0.0, 0.0, 0.0)];
	[webView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
	[webView setAutoresizesSubviews:YES];
	[webView setDelegate:self];

	[self setWebView:webView];
	[contentView addSubview:webView];

	UIButton* fullScreenButton = [UIButton createFullScreenButton:@selector(toggleFullScreenButtonAction:) forTarget:self];
	[contentView addSubview:fullScreenButton];
	[fullScreenButton setHidden:YES];
	[self setFullScreenButton:fullScreenButton];

	//	[webView setBackgroundColor:[UIColor blackColor]];
	[webView setBackgroundColor:[self backgroundColour]];

	[webView setAllowsInlineMediaPlayback:YES];
	[webView setScalesPageToFit:YES];

	//	UIView* guestureView = contentView;

	//-----------------
/*	UITapGestureRecognizer* doubleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleDoubleTap:)
	];
	[doubleTapRecognizer setNumberOfTapsRequired:2];
	[doubleTapRecognizer setDelegate:self];
	[guestureView addGestureRecognizer:doubleTapRecognizer];
*/
	//-----------------
	UITapGestureRecognizer* singleTapRecognizer = [
		[UITapGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSingleTap:)
	];
	[singleTapRecognizer setNumberOfTapsRequired:1];
	[singleTapRecognizer setNumberOfTouchesRequired:2];
	[singleTapRecognizer setDelegate:self];
	[contentView addGestureRecognizer:singleTapRecognizer];

	UISwipeGestureRecognizer* swipeLeftRecognizer = [
	[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeLeft:)
	];
	[swipeLeftRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
	[swipeLeftRecognizer setNumberOfTouchesRequired:2];
	[swipeLeftRecognizer setDelegate:self];
	[contentView addGestureRecognizer:swipeLeftRecognizer];
	
	// Gesture recogniser -------
	UISwipeGestureRecognizer* swipeRightRecognizer = [
	[UISwipeGestureRecognizer alloc]
		initWithTarget:self action:@selector(handleSwipeRight:)
	];
	[swipeRightRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
	[swipeRightRecognizer setNumberOfTouchesRequired:2];
	[swipeRightRecognizer setDelegate:self];
	[contentView addGestureRecognizer:swipeRightRecognizer];

	// Only pan around with a single finger
	UIPanGestureRecognizer* scrollGesture = [[[self webView] scrollView] panGestureRecognizer];
	//	UIPinchGestureRecognizer* zoomGesture = [[[self webView] scrollView] pinchGestureRecognizer];
	
	[scrollGesture setMaximumNumberOfTouches:1];
	[scrollGesture setMinimumNumberOfTouches:1];
	
	[scrollGesture requireGestureRecognizerToFail:swipeLeftRecognizer];
	[scrollGesture requireGestureRecognizerToFail:swipeRightRecognizer];
	
	[singleTapRecognizer requireGestureRecognizerToFail:scrollGesture];
}

//-----------------------------------------------------------------
- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];

	FMDatabase* database = [UIApplication myDatabase];

	BOOL displayHelp = [database getBOOLVariable:@"showNoteHelp" withDefault:YES];

	if (displayHelp) {
		[DTHelpViewController showNoteHelp:self];
		[database setBOOLVariable:@"showNoteHelp" withValue:NO];
	}
}

//-----------------------------------------------------------------
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
	shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

//-----------------------------------------------------------------
- (void) removeLinkWidget
{
	DTShortcutWidget* linkWidget = [self linkWidget];

	if (linkWidget) {
		[[UIApplication myRootViewController] deleteWidget:linkWidget animated:NO];
		[self setLinkWidget:nil];
	}
}

//-----------------------------------------------------------------
- (BOOL) webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request
  navigationType:(UIWebViewNavigationType)navigationType
{
	LOG_FN();
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {

		DT_DEBUG_LOG(@"Link clicked");

		DTShortcutWidget* linkWidget = [self linkWidget];

		if (! linkWidget) {
			linkWidget = [[DTShortcutWidget alloc] init];

			[linkWidget setUpdateImage:	NO];
			[linkWidget setUpdateURL:	YES];
			[linkWidget setIsReadOnly:	YES];
			[linkWidget setScreenPosition:[UIApplication myMainScreenCentre]];

			[self setLinkWidget:linkWidget];
		}

		[self setClickedLink:YES];

		// Get link text from button, then try and create a valid URL from it
		//		NSString* URLFromButton = [[button URL] absoluteString];
		NSURL* URL      	= [request URL];
		NSString* validURL	= [NSString checkForValidURL:[URL absoluteString]];

		if (validURL) {
			DT_DEBUG_LOG(@"Web page....");
			// Try opening the web page if we think it's a valid link
			[linkWidget setURL:validURL];
			[linkWidget displayWebPage:validURL parentController:self animated:YES];
		}

		return NO;
	}

	return YES;
}

//-----------------------------------------------------------------
- (void) refreshView
{
	// HTML content with the CSS header...
	NSString* totalHTML = [[self generatedCSS] stringByAppendingString:[self HTML]];

	[[self webView] loadHTMLString:totalHTML baseURL:nil];
}

//-----------------------------------------------------------------
- (void) handleDoubleTap:(UITapGestureRecognizer*)recogniser
{
}

//-----------------------------------------------------------------
- (void) handleSingleTap:(UITapGestureRecognizer*)recogniser
{
	if ([recogniser state] == UIGestureRecognizerStateEnded) {

		// If we have single clicked a link...
		if (![self clickedLink]) {
			[self toggleFullScreenButtonAction:nil];
		}
		[self setClickedLink:NO];
	}
}

//-----------------------------------------------------------------
- (void) handleSwipeLeft:(UITapGestureRecognizer*)recogniser
{
	int size = [self getFontSizeAsInt];
	if (size > 50) {
		size -= 50;
	}

	[self setFontSizeFromInt:size];

	[MBProgressHUD showInformation:[self fontSize]];

	[self refreshView];
}

//-----------------------------------------------------------------
- (void) handleSwipeRight:(UITapGestureRecognizer*)recogniser
{
	int size = [self getFontSizeAsInt];
	if (size < 400) {
		size += 50;
	}
	[self setFontSizeFromInt:size];

	[MBProgressHUD showInformation:[self fontSize]];
	
	[self refreshView];
}

//-----------------------------------------------------------------
- (void) setText:(NSString*)HTMLText
{
	[self setHTML:HTMLText];
	[self refreshView];
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
	[super viewDidLoad];

	// Fill up the bottom toolbar
    UIBarButtonItem* fixedSpace    = [UIBarButtonItem createFixedSpace:5.0f];
	UIBarButtonItem* flexibleSpace = [UIBarButtonItem createFlexibleSpace];

	UIBarButtonItem* fullScreenBarButtonItem = [UIBarButtonItem createFullScreenButton:@selector(fullScreenButtonAction:) forTarget:self];
	UIBarButtonItem* actionBarButtonItem	 = [UIBarButtonItem createActionButton:@selector(actionButtonAction:)         forTarget:self];
	UIBarButtonItem* settingsBarButtonItem	 = [UIBarButtonItem createSettingsButton:@selector(settingsButtonAction:)     forTarget:self];

	NSArray* items = [NSArray arrayWithObjects:
		fixedSpace,
		fullScreenBarButtonItem,
		flexibleSpace,
		actionBarButtonItem,
		flexibleSpace,
		settingsBarButtonItem,
		fixedSpace,
		nil
	];

	[self setToolbarItems:items animated:NO];

	[self refreshView];
}

//-----------------------------------------------------------------
- (void) toggleFullScreenButtonAction:(UIButton*)sender
{
	[[self navigationController] toggleToolbars];
	[[self fullScreenButton] setHidden:![[self navigationController] isNavigationBarHidden]];
}

//-----------------------------------------------------------------
- (void) fullScreenButtonAction:(UIBarButtonItem*)sender
{
	[self toggleFullScreenButtonAction:nil];
}

//-----------------------------------------------------------------
- (void) actionButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];
	[widget actionButtonAction:sender];
}

//-----------------------------------------------------------------
- (void) settingsButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];
	[widget showSettingsModal:self fromSender:sender];
}

//-----------------------------------------------------------------
#if 0
- (void) fontButtonAction:(UIBarButtonItem*)sender
{
	DTWidget* widget = [[self navigationController] widget];

	NSArray* fontNames = [DTPostItNoteWidget getFontNameArray];
	NSMutableArray* selectedIndexes	= nil;

	// Try and find font name in font list
	NSUInteger selectedIndex = [fontNames indexOfObject:[self fontName]];

	if (selectedIndex != NSNotFound) {
		// got it, create "selected" array with that number in
		// to highlight the font in use
		selectedIndexes = [NSMutableArray array];
		[selectedIndexes addObject:[NSNumber numberWithUnsignedInteger:selectedIndex]];
	}

	//	QSection* fontList = [styleSection addNewPageInSectionWithTitle:@"Font"];

	QSelectSection* fontSection = [[QSelectSection alloc]
								   initWithItems:fontNames selectedIndexes:selectedIndexes title:@"Font"];

	//	[fontSection setSelectedItem:[self fontName]];

	QRootElement* fontRoot = [QRootElement rootWithTitle:@"Font"];

	//	QRootElement* fontRoot = [QRootElement rootWithTitle:@"Font"];
	//	QSection* section  = [fontRoot addNewSection];

	[fontSection setMultipleAllowed:NO];
	[fontRoot addSection:fontSection];

	//	[styleSection addElement:fontRoot];




	[widget showModalMenu:fontRoot onViewController:self];
}
#endif

//-----------------------------------------------------------------
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

//-----------------------------------------------------------------
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[[self fullScreenButton] repositionFullScreenButton];
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return YES;
}

//-----------------------------------------------------------------
@end

