//
//  DTNavigationController.m
//  DeskTopper
//
//  Created by David Shea on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTNavigationController.h"
#import "QuickdialogHelper.h"
#import "DTOptionsControllerWithDone.h"
#import "UIImagePickerController+Helper.h"
#import "CustomImagePickerController.h"
#import "UIApplication+Helpers.h"
#import "UIImageExtras.h"
//#import "SVModalWebViewController.h"
#import "UIBarButtonItem+StandardButtons.h"

//-----------------------------------------------------------------
@implementation DTNavigationController

@synthesize widget				= _widget;
@synthesize timeTillHide		= _timeTillHide;
@synthesize activityIndicator	= _activityIndicator;
@synthesize bottomToolbarHidden = _bottomToolbarHidden;

//static const CGFloat kToolBarHeight = 44.0f;

//-----------------------------------------------------------------
- (DTNavigationController*) init
{
	self = [super init];

	return self;
}
//-----------------------------------------------------------------
- (void) viewDidLoad
{
    [super viewDidLoad];

	// Style navigation bar
	UINavigationBar* bar = [self navigationBar];
	[bar setBarStyle:UIBarStyleBlack];


	UIToolbar* toolbar = [self toolbar];

	//	[toolbar setTintColor:[UIColor blackColor]];


	[toolbar setBarStyle:UIBarStyleBlack];

	[toolbar setTranslucent:YES];
	[bar setTranslucent:YES];

	[self showToolbars];
}

//-----------------------------------------------------------------
- (DTNavigationController*) initWithRootViewController:(UIViewController*)viewController
{
	self = [super initWithRootViewController:viewController];

	return self;
}

//-----------------------------------------------------------------
- (void) setTranslucent:(BOOL)translucent
{
	[[self navigationBar] setTranslucent:translucent];
	[[self toolbar] setTranslucent:translucent];
}

//-----------------------------------------------------------------
- (void) hideToolbarAfterDelay
{
	LOG_FN();
}

//-----------------------------------------------------------------
- (void) setTimeTillHide:(float)timeTillHide
{
	_timeTillHide = timeTillHide;
	[self hideToolbarAfterDelay];
}

//-----------------------------------------------------------------
- (void) setBottomToolbarHidden:(BOOL)hidden animated:(BOOL)animated
{
	[self setToolbarHidden:hidden animated:animated];
	[self setBottomToolbarHidden:hidden];
}

//-----------------------------------------------------------------
- (void) hideToolbars
{
	LOG_FN();
	[self setNavigationBarHidden:YES animated:YES];

	if (! [self bottomToolbarHidden]) {
		[self setToolbarHidden:YES animated:YES];
	}
}

//-----------------------------------------------------------------
- (void) showToolbars
{
	LOG_FN();
	[self setNavigationBarHidden:NO animated:YES];

	if (! [self bottomToolbarHidden]) {
		[self setToolbarHidden:NO animated:YES];
	}
}

//-----------------------------------------------------------------
- (void) toggleToolbars
{
	LOG_FN();
	if ([self isNavigationBarHidden]) {
		[self showToolbars];
	}
	else {
		[self hideToolbars];
	}
}

//-----------------------------------------------------------------
- (void) doneButtonPressed
{
	LOG_FN();
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) addItemToLeftOfBar:(UIBarButtonItem*)newButton
{
	UINavigationBar* bar   = [self navigationBar];
	UINavigationItem* item = [bar topItem];

	NSArray* leftItems	  = [item leftBarButtonItems];
	NSArray* newLeftItems = [leftItems arrayByAddingObject:newButton];
	[item setLeftBarButtonItems:newLeftItems];
}

//-----------------------------------------------------------------
- (void) removeItemFromLeftOfBar
{
	UINavigationBar* bar   = [self navigationBar];
	UINavigationItem* item = [bar topItem];

	NSArray* leftItems = [item leftBarButtonItems];

	// -1 since array = (0,count-1)
	NSRange range = NSMakeRange(0, [leftItems count] - 1);
	NSArray* newLeftItems = [leftItems subarrayWithRange:range];

	[item setLeftBarButtonItems:newLeftItems];
}

//-----------------------------------------------------------------
- (void) addActivityIndicator
{
	if (! [self activityIndicator]) {

		//Create an instance of activity indicator view
		UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
		[self setActivityIndicator:activityIndicator];

		//set the initial property
		[activityIndicator hidesWhenStopped];
		[activityIndicator stopAnimating];

		//Create an instance of Bar button item with custome view which is of activity indicator
		UIBarButtonItem* activityButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];

		[self addItemToLeftOfBar:activityButton];
	}
}

//-----------------------------------------------------------------
- (void) removeActivityIndicator
{
	if ([self activityIndicator]) {
		[self removeItemFromLeftOfBar];
		[self setActivityIndicator:nil];
	}
}

//-----------------------------------------------------------------
- (void) startActivityIndicator
{
	[self addActivityIndicator];
	[[self activityIndicator] startAnimating];
}

//-----------------------------------------------------------------
- (void) stopActivityIndicator
{
	[[self activityIndicator] stopAnimating];
	[self removeActivityIndicator];
}

//-----------------------------------------------------------------
- (void) addToLeft:(UIBarButtonItem*)button
{
	// give the parent navigation controller a done button
	UINavigationBar*  bar  = [self navigationBar];
	UINavigationItem* item = [bar topItem];
	
	NSMutableArray* leftItems = [NSMutableArray arrayWithArray:[item leftBarButtonItems]];
	[leftItems addObject:button];
	[item setLeftBarButtonItems:leftItems];
}

//-----------------------------------------------------------------
- (void) addToRight:(UIBarButtonItem*)button
{
	// give the parent navigation controller a done button
	UINavigationBar*  bar  = [self navigationBar];
	UINavigationItem* item = [bar topItem];
	
	NSMutableArray* rightItems = [NSMutableArray arrayWithArray:[item rightBarButtonItems]];
	[rightItems addObject:button];
	[item setRightBarButtonItems:rightItems];
}

//-----------------------------------------------------------------
- (void) addDoneButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	// Add a 'done' button to the root left-side edge
    UIBarButtonItem* doneButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Done"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action ? action : @selector(doneButtonPressed))
	];

	[doneButton tint];

	[self addToLeft:doneButton];
}

//-----------------------------------------------------------------
- (void) addSaveButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	DT_ASSERT(action);

	// Add a 'done' button to the root left-side edge
    UIBarButtonItem* saveButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Save"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action)
	];

	[saveButton tint];

	[self addToLeft:saveButton];
}

//-----------------------------------------------------------------
- (void) cancelButtonPressed
{
	LOG_FN();
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) addLeftCancelButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();

	// Add a 'cancel' button to the root left-side edge
	UIBarButtonItem* cancelButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Cancel"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action ? action : @selector(cancelButtonPressed))
	];

	[cancelButton tint];

	[self addToLeft:cancelButton];
}

//-----------------------------------------------------------------
- (void) addCancelButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	// Add a 'cancel' button to the root right-side edge
	UIBarButtonItem* cancelButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Cancel"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action ? action : @selector(cancelButtonPressed))
	];

	[cancelButton tint];

	[self addToRight:cancelButton];
}

//-----------------------------------------------------------------
- (void) addEditButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	DT_ASSERT(action);

	// Add a 'cancel' button to the root right-side edge
	UIBarButtonItem* editButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Edit"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action)
	];

	[editButton tint];

	[self addToRight:editButton];
}

//-----------------------------------------------------------------
- (void) addDefaultButtonsForTarget:(id)buttonTarget
{
	[self addCancelButton:@selector(cancelButtonPressed) 	forTarget:buttonTarget];
	[self addDoneButton:@selector(doneButtonPressed)   		forTarget:buttonTarget];
}

//-----------------------------------------------------------------
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	LOG_FN();
    // Return YES for supported orientations
	return YES;
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return YES;
}
//-----------------------------------------------------------------
@end

