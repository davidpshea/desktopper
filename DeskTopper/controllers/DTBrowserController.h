//
//  DTBrowserController.h
//  DeskTopper
//
//  Created by David Shea on 19/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <WebKit/WKWebView.h>
@import WebKit;

@class DTNavigationController;

@interface DTBrowserViewController : UIViewController <WKNavigationDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) WKWebView* 				webView;
@property (nonatomic, strong) UIImageView*				imageBeforeLoadView;
@property (nonatomic, strong) NSString*  				pageSource;
@property (nonatomic, strong) NSString*  				pageSourceURL;
@property (nonatomic, weak)	  DTNavigationController*	navigationController;
@property (nonatomic)	  	  BOOL						isReadOnly;
@property (nonatomic, strong) NSTimer*					finishedTimer;
@property (nonatomic, strong) UITapGestureRecognizer*	singleTapRecognizer;
@property (nonatomic, strong) UIButton*					fullScreenButton;
@property (nonatomic)		  UIImage*					loadingImage;

- (NSString*)	getURL;
- (void)		refresh;

@end
