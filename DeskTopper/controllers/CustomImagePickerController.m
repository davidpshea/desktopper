//
//  CustomImagePicker.m
//  CustomImagePicker
//
//  Created by Ray Wenderlich on 1/27/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import "CustomImagePickerController.h"
#import "UIImageExtras.h"
#import "DTBlockButton.h"
#import "FileHelper.h"
#import "UIColor+StandardColours.h"
#import "UIApplication+Helpers.h"
#import "UIImageView+ImageViewShadow.h"
#import "UIImageView+ImageViewBorder.h"
#import "DTNavigationController.h"
#import "UIBarButtonItem+StandardButtons.h"

//-----------------------------------------------------------------
@interface ImagePickerItem : NSObject

@property (nonatomic, strong)	UIImage*	image;
@property (nonatomic, strong)	UIButton*	button;

@end

//-----------------------------------------------------------------
@implementation ImagePickerItem

@synthesize	image	= _image;
@synthesize	button	= _button;

@end

//-----------------------------------------------------------------
@implementation CustomImagePicker

@synthesize delegate			 = _delegate;
@synthesize userObject			 = _userObject;
@synthesize navigationController = _navigationController;
@synthesize items				 = _items;
@synthesize widget				 = _widget;
@synthesize backgroundColour	 = _backgroundColour;
@synthesize	iconWidth			 = _iconWidth;
@synthesize	iconHeight			 = _iconHeight;
@synthesize	iconMargin			 = _iconMargin;
@synthesize leftMargin			 = _leftMargin;
@synthesize	topMargin			 = _topMargin;
@synthesize viewSize			 = _viewSize;

//-----------------------------------------------------------------
- (id) init
{
	if ((self = [super init])) {
		_items  =  [[NSMutableArray alloc] init];

		// Setup property defaults
		[self setBackgroundColour:[UIColor whiteColor]];

		_iconWidth  = 64.0;
		_iconHeight = 64.0;
		_iconMargin = 8.0;
		_leftMargin = 8.0;
		_topMargin  = 8.0;

		_scrollEnabled = YES;

		_viewSize = CGSizeMake(
		   [UIApplication myMainScreenBoundsRotated].size.width,
		   [UIApplication myMainScreenBoundsRotated].size.height
	   );
	}

	return self;
}

//----------------------------------------------------------------
- (void) addItem:(UIImage*)image andButton:(UIButton*)button resizeToFit:(BOOL)resize
{
	// Force image to size (if we have a size)
	if (resize) {
		image = [image imageFromImageByScaling:CGSizeMake(_iconWidth, _iconHeight) keepAspectRatio:NO];
	}

	ImagePickerItem* item = [[ImagePickerItem alloc] init];
	[item setImage:image];
	[item setButton:button];

	[_items addObject:item];
}

//-----------------------------------------------------------------
- (void) addImage:(UIImage*)image resizeToFit:(BOOL)resize
{
	[self addItem:image andButton:nil resizeToFit:resize];
}

//-----------------------------------------------------------------
- (UIButton*) getButtonAtIndex:(int)buttonIndex
{
	ImagePickerItem* item = [_items objectAtIndex:buttonIndex];
	return [item button];
}

//-----------------------------------------------------------------
- (BOOL) iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender
{
	return YES;
}

//-----------------------------------------------------------------
- (void) buttonPressed:(id)sender
{
	if ([[self delegate] iconIsSelected:self from:sender]) {
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) addButton:(UIButton*)button withImage:(UIImage*)image resizeToFit:(BOOL)resize
{
	[button addTarget:self action:@selector(buttonPressed:)
		forControlEvents:UIControlEventTouchUpInside
	];

	[self addItem:image andButton:button resizeToFit:resize];
}

//-----------------------------------------------------------------
- (float) widthForIcons:(int)NumberOfIcons
{
	float iconTotalWidth  = _iconWidth  + (_iconMargin * 2.0f);
	return NumberOfIcons * iconTotalWidth;
}

//-----------------------------------------------------------------
- (CGSize) sizeForIconGridWithWidth:(int)numberOfIconsWidth height:(int)height
{
	float iconTotalWidth  = _iconWidth  + (_iconMargin * 2.0f);
	float iconTotalHeight = _iconHeight + (_iconMargin * 2.0f);
	
	return CGSizeMake(
		( ((float)numberOfIconsWidth) * iconTotalWidth)  + 6.0f,
		( ((float)height)             * iconTotalHeight) + 4.0f
	);
}

//-----------------------------------------------------------------
- (void) refreshDisplay
{
	UIScrollView* view = (UIScrollView*)[self view];
	CGRect frame = [view frame];

	float iconWidth  = [UIApplication isPortrait] ? _iconWidth  : _iconHeight;
	float iconHeight = [UIApplication isPortrait] ? _iconHeight : _iconWidth;

	float iconTotalWidth  = iconWidth  + (_iconMargin * 2.0f);
	float iconTotalHeight = iconHeight + (_iconMargin * 2.0f);

	int columns = (int)((frame.size.width) / iconTotalWidth);

	float remaining  = ( frame.size.width - ((float)columns * iconTotalWidth) );
	float leftMargin = remaining / 2.0;

	int row    = 0;
	int column = 0;

	for (int i = 0; i < [_items count]; i++) {

		float x = ( ((float)column) * iconTotalWidth  ) + 6.0 + leftMargin;
		float y = ( ((float)row)    * iconTotalHeight ) + 4.0;

		ImagePickerItem* item = [_items objectAtIndex:i];
		UIImage*  image		  = [item image];
		UIButton* button	  = [item button];

		if (! button) {
			button = [UIButton buttonWithType:UIButtonTypeCustom];
			[item setButton:button];
			[button
				addTarget:self
				action:@selector(buttonPressed:)
				forControlEvents:UIControlEventTouchUpInside
			 ];
		}

		[button setFrame:CGRectMake(x, y, iconWidth, iconHeight)];
		[button setBackgroundImage:image forState:UIControlStateNormal];
		[button setTag:i];
		UIImageView* imageView = [button imageView];
		[imageView configureImageViewBorder:2.0];
		[imageView addImageShadow:8.0];
		[view addSubview:button];

		column++;
		if (column >= columns) {
			column = 0;
			if (i != ([_items count] - 1)) {
				row++;
			}
		}
	}

	[view setContentSize:CGSizeMake([view bounds].size.width, (row + 1) * iconTotalHeight + 30)];
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
	[super viewDidLoad];

	CGRect viewSize    = CGRectMake(0, 0, [self viewSize].width, [self viewSize].height);
	UIScrollView* view = [[UIScrollView alloc] initWithFrame:viewSize];

	[view setBackgroundColor:[self backgroundColour]];
	[view setPagingEnabled:NO];
	[view setShowsHorizontalScrollIndicator:NO];
	[view setCanCancelContentTouches:YES];
	[view setScrollEnabled:[self scrollEnabled]];
	[self setView:view];

	[self refreshDisplay];

	// Customise navigation bar - style like photo picker
	UINavigationBar* bar = [[self navigationController] navigationBar];
	[bar setBarStyle:UIBarStyleBlack];
	[bar setTranslucent:YES];

	// Create cancel button
	UIBarButtonItem* cancelButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Cancel"
		style:UIBarButtonItemStyleBordered
		target:self
		action:@selector(cancelIconSelection:)
	];

	[cancelButton tint];

	[[self navigationItem] setRightBarButtonItem:cancelButton];
}

//-----------------------------------------------------------------
- (void) cancelIconSelection:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (CustomImagePicker*) initFromWidgetList
{
	// Do standard init first...
	self = [self init];

	// Initialize image picker
	[self setTitle:@"Choose Widget"];
	[self setIconWidth:64.0];
	[self setIconHeight:64.0];

	UIColor* colour = [UIColor colorWithWhite:0.0f alpha:1.0f];
	[self setBackgroundColour:colour];

	NSArray* classNames = [DTWidget getWidgetClassNameArray];

	for (NSString* className in classNames) {

		Class widgetClass  = NSClassFromString(className);

		NSString* filename = [FileHelper getIconFileNamed:[widgetClass getIconName]];
		UIImage* iconImage = [[UIImage alloc] initWithContentsOfFile:filename];

		UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];

		[button setShowsTouchWhenHighlighted:YES];
		[button setTitle:[widgetClass getUserName] forState:UIControlStateNormal];
		//UIColor* colour = [UIColor colorWithWhite:1.0f alpha:1.0f];
		[button setTitleColor:colour forState:UIControlStateNormal];

		UILabel* label = [button titleLabel];
		[label setFont:[UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]]];

		// make the buttons content appear in the top-left
		// the space between the image and text
		CGFloat spacing = 70.0;

		// get the size of the elements here for readability
		CGSize imageSize = button.imageView.frame.size;
		// the text width might have changed (in case it was shortened before due to
		// lack of space and isn't anymore now), so we get the frame size again
		CGSize titleSize = button.titleLabel.frame.size;

		// lower the text and push it left to center it
		button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);

		//		titleSize = button.titleLabel.frame.size;

		// raise the image and push it right to center it
		button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, - titleSize.width);

		[self addButton:button withImage:iconImage resizeToFit:NO];
	}

	return self;
}

//-----------------------------------------------------------------
- (void) createFromColourListWithTitle:(NSString*)title andViewSize:(CGSize)viewSize
{
	[self setTitle:title];
	[self setIconWidth:48.0];
	[self setIconHeight:48.0];
	[self setIconMargin:6.0];
	[self setViewSize:viewSize];

	[self setBackgroundColour:[UIColor colorWithWhite:0.0f alpha:1.0f]];

	NSArray* colours = [UIColor getStandardColours];
	CGSize size		 = CGSizeMake([self iconWidth], [self iconHeight]);

	for (UIColor* colour in colours) {
		UIImage* colouredSquare = [UIImage createColouredRectangleWithContrastingBorder:size colour:colour];
		[self addImage:colouredSquare resizeToFit:NO];
	}
}

//-----------------------------------------------------------------
- (CustomImagePicker*) initFromIconList
{
	self = [self init];

	[self setTitle:@"Choose Icon"];

	NSArray* icons = [FileHelper getIconFileArray];
	for (NSString* path in icons) {
		UIImage* iconImage = [[UIImage alloc] initWithContentsOfFile:path];
		[self addImage:iconImage resizeToFit:NO];
	}

	return self;
}

//-----------------------------------------------------------------
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	LOG_FN();
	return YES;
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return YES;
}

//-----------------------------------------------------------------
// IOS X
- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	// Remove all the icons from the view...
	[[[self view] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

	// Recreate the view with the new orientation
	[self refreshDisplay];
}

//-----------------------------------------------------------------
/*
 - (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
}
*/
//-----------------------------------------------------------------

@end

