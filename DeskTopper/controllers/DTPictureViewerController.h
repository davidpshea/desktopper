//
//  DTPictureViewerController.h
//  DeskTopper
//
//  Created by David Shea on 10/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DTPhotoScrollView;
@class DTNavigationController;

@interface DTPictureViewerController : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong)	UIImage*				image;
@property (nonatomic, strong)	UIImageView*			imageView;
@property (nonatomic, strong)	DTPhotoScrollView*		scrollView;
@property (nonatomic)			BOOL					toolbarHidden;
@property (nonatomic, weak)		DTNavigationController*	navigationController;

- (DTPictureViewerController*)	initWithImage:(UIImage*)image;

@end

