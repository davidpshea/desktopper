//
//  CustomImagePicker.h
//  CustomImagePicker
//
//  Created by Ray Wenderlich on 1/27/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTWidget.h"
#import "DTBlockButton.h"

//-----------------------------------------------------------------
@class CustomImagePicker;
@class DTNavigationController;

@protocol CustomImagePickerDelegate <NSObject>

@optional
- (BOOL) iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender;

@end

//-----------------------------------------------------------------
@interface CustomImagePicker : UIViewController <CustomImagePickerDelegate>

@property (nonatomic, strong)	id<CustomImagePickerDelegate>	delegate;

@property (nonatomic, strong)	NSMutableArray*	items;
@property (nonatomic, weak)		DTWidget*		widget;
@property (nonatomic, strong)	UIColor*		backgroundColour;
@property (nonatomic, weak)		id				userObject;

@property (nonatomic, weak)		DTNavigationController* navigationController;

@property (nonatomic)			float	iconWidth;
@property (nonatomic)			float	iconHeight;
@property (nonatomic)			float	iconMargin;
@property (nonatomic)			float	leftMargin;
@property (nonatomic)			float	topMargin;
@property (nonatomic)			CGSize	viewSize;
@property (nonatomic)			BOOL	scrollEnabled;

- (CustomImagePicker*) initFromWidgetList;
- (CustomImagePicker*) initFromIconList;

- (void)		createFromColourListWithTitle:(NSString*)title andViewSize:(CGSize)viewSize;

- (void)		addImage:(UIImage*)image resizeToFit:(BOOL)resize;
- (void)		addButton:(UIButton*)button withImage:(UIImage*)image resizeToFit:(BOOL)resize;
- (UIButton*)	getButtonAtIndex:(int)buttonIndex;
- (float)		widthForIcons:(int)NumberOfIcons;
- (CGSize)		sizeForIconGridWithWidth:(int)numberOfIconsWidth height:(int)height;

//-----------------------------------------------------------------
@end


