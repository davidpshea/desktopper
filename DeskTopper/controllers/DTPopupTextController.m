//
//  DTPopupTextController.m
//  DeskTopper
//
//  Created by David Shea on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTPostItNoteWidget.h"
#import "DTPopupTextController.h"
#import "UIApplication+Helpers.h"
#import "DTNavigationController.h"
#import "YIPopupTextView.h"
#import "UIColor+CopyWithZone.h"
#import "UIBarButtonItem+StandardButtons.h"
#import "UIImageExtras.h"
#import "UIApplication+Helpers.h"
#import "DTOptions.h"
#import "NSString+Word.h"
#import "NSString+Macros.h"
#import "UIGlossyButton.h"
#import "UIView+LayerEffects.h"

//-----------------------------------------------------------------
static const float shortcutButtonWidth			= 44.0;
static const float shortcutButtonHeight			= 44.0;
static const float shortcutButtonSeparation		= 4.0;
//static const float shortcutButtonGranularity	= 32.0;

//-----------------------------------------------------------------
@interface DTCustomKeyboard : NSObject

@property (nonatomic, strong)	UIInputView*	view;
@property (nonatomic, strong)	NSArray*		labels;

@end

//-----------------------------------------------------------------
@implementation DTCustomKeyboard

@synthesize view	= _view;
@synthesize labels	= _labels;

@end


//-----------------------------------------------------------------
@interface DTPopupTextController ()

@property (nonatomic, strong) YIPopupTextView*		popupView;
@property (nonatomic)		  keyboardID			currentKeyboardID;
@property (nonatomic, strong) NSMutableDictionary*	keyboardList;

@end

//-----------------------------------------------------------------
@implementation DTPopupTextController
/*
@synthesize popupView				= _popupView;
//@synthesize backgroundColour		= _backgroundColourName;
//@synthesize textColour				= _textColourName;
@synthesize textScale				= _textScale;
//@synthesize font					= _font;
@synthesize target					= _target;
@synthesize userObject				= _userObject;
@synthesize currentKeyboardID		= _currentKeyboardID;
@synthesize keyboardList			= _keyboardList;
@synthesize cursorPosition			= _cursorPosition;
@synthesize enabledKeyboards		= _enabledKeyboards;
*/

//-----------------------------------------------------------------
- (id) init
{
	if (self = [super init]) {
		YIPopupTextView* popup = [[YIPopupTextView alloc] initWithPlaceHolder:@"" maxCount:0];
		[self setPopupView:popup];

		[self setEnabledKeyboards:
			keyboardIDControl | keyboardIDMarkdown | keyboardIDShortcut | keyboardIDHTML
		];

		if ([UIApplication isIOS7]) {
			[self setExtendedLayoutIncludesOpaqueBars:NO];
		}

	}
	return self;
}

//-----------------------------------------------------------------
- (void) dealloc
{
	[self setUserObject:nil];
	[self setTarget:nil];
	[self setPopupView:nil];
}

//-----------------------------------------------------------------
- (int) cursorPosition
{
	NSRange range = [[self popupView] selectedRange];
	return (int)range.location;
}

//-----------------------------------------------------------------
- (void) setText:(NSString*)newText
{
	[_popupView setText:newText];
}

//-----------------------------------------------------------------
- (NSString*) text
{
	return [_popupView text];
}

//-----------------------------------------------------------------
- (UITextView*) getTextView
{
	return [self popupView];
}

//-----------------------------------------------------------------
-(void) setTextColour:(UIColor *)textColour
{
	[_popupView setTextColor:textColour];
}

//-----------------------------------------------------------------
- (UIColor*) textColor
{
	return [_popupView textColor];
}

//-----------------------------------------------------------------
- (void) setBackgroundColour:(UIColor *)backgroundColour
{
	[_popupView setBackgroundColor:backgroundColour];
}

//-----------------------------------------------------------------
- (UIColor*) backgroundColour
{
	return [_popupView backgroundColor];
}

//-----------------------------------------------------------------
- (void) setFont:(UIFont *)font
{
	[_popupView setFont:font];
}

//-----------------------------------------------------------------
- (UIFont*) font
{
	return [_popupView font];
}

//-----------------------------------------------------------------
- (void) dismissKeyboard
{
	[_popupView dismiss];
}

//-----------------------------------------------------------------
- (NSMutableArray*) createURLLabels
{
	NSMutableArray* shortcutList = [[NSMutableArray alloc] init];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"http"		index:0  andShortcut:@"http://"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"www."		index:1  andShortcut:@"www."]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".com"		index:2  andShortcut:@".com"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".net"		index:3  andShortcut:@".net"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".org"		index:4  andShortcut:@".org"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".gov"		index:5  andShortcut:@".gov"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".co"		index:6  andShortcut:@".co"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".info"		index:7  andShortcut:@".info"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".biz"		index:8  andShortcut:@".biz"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".au"		index:9  andShortcut:@".au"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".eu"		index:10 andShortcut:@".eu"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".asia"		index:11 andShortcut:@".asia"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@".uk"		index:12 andShortcut:@".uk"]];

	return shortcutList;
}

//-----------------------------------------------------------------
- (NSMutableArray*) createHTMLLabels
{
	NSMutableArray* shortcutList = [[NSMutableArray alloc] init];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H1"			index:0 andShortcut:@"<h1>$s$c</h1>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H2"			index:1 andShortcut:@"<h2>$s$c</h2>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H3"			index:2 andShortcut:@"<h3>$s$c</h3>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"B"			index:3 andShortcut:@"<b>$s$c</b>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"I"			index:4 andShortcut:@"<i>$s$c</i>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"P"			index:5 andShortcut:@"<p>$s$c</p>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Div"		index:6 andShortcut:@"<div>$s$c</div>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Span"		index:7 andShortcut:@"<span>$s$c</span>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Sub"		index:8 andShortcut:@"<sub>$s$c</sub>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Sup"		index:9 andShortcut:@"<sup>$s$c</sup>"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Style"		index:10 andShortcut:@" style=\"$s$c\"" ]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Comment"	index:11 andShortcut:@"<!--$s$c-->"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Center"		index:12 andShortcut:@"style=\"text-align:center;\""]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Font Size"	index:13 andShortcut:@"style=\"font-size:$c;\""]];

	return shortcutList;
}

//-----------------------------------------------------------------
- (NSMutableArray*) createMarkdownLabels
{
	NSMutableArray* shortcutList = [[NSMutableArray alloc] init];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H1"		index:0 andShortcut:@"$l# $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H2"		index:1 andShortcut:@"$l## $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H3"		index:2 andShortcut:@"$l### $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"H4"		index:2 andShortcut:@"$l#### $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"1."		index:3 andShortcut:@"$l1. $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"*"		index:4 andShortcut:@"$l* $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Link"	index:5 andShortcut:@"[$s]($c)"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"---"	index:6 andShortcut:@"$l---$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"I"		index:7 andShortcut:@"$w*$s$c*"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"B"		index:8 andShortcut:@"$w**$s$c**"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Strike"	index:9 andShortcut:@"$w~~$s$c~~"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Code"	index:10 andShortcut:@"$w`$s$c`"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Quote"	index:11 andShortcut:@"$l> $s$c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Table"	index:12 andShortcut:@"$c1|2\n:---:|:---:\na|b\n"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Block Code"	index:13 andShortcut:@"~~~\n$s$c\n~~~\n"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Def"	index:14 andShortcut:@"$w$s\n: $c"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Sup"	index:15 andShortcut:@"^$w$s$c"]];

	return shortcutList;
}

//-----------------------------------------------------------------
- (NSMutableArray*) createControlLabels
{
	NSMutableArray* shortcutList = [[NSMutableArray alloc] init];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"<"		index:2 andShortcut:@"2"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"<<"		index:1 andShortcut:@"1"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"<<<"	index:0 andShortcut:@"6"]];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@">"		index:3 andShortcut:@"3"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@">>"		index:4 andShortcut:@"4"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@">>>"	index:5 andShortcut:@"5"]];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"[Word]"	index:6  andShortcut:@"10"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"[Line]"	index:7  andShortcut:@"11"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"[]"		index:8  andShortcut:@"12"]];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Copy"	index:9  andShortcut:@"13"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Cut"	index:10 andShortcut:@"15"]];
	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Paste"	index:11 andShortcut:@"14"]];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"Del"	index:12 andShortcut:@"16"]];

	[shortcutList addObject:[[DTShortcut alloc] initWithLabel:@"TAB"	index:13 andShortcut:@"17"]];

	return shortcutList;
}

//-----------------------------------------------------------------
- (NSRange) addText:(NSString*)textToAdd
{
	NSString* addBefore = nil;
	NSString* addAfter  = nil;

	YIPopupTextView *popup	= [self popupView];
	NSRange range   		= [popup selectedRange];
	NSString* fullText		= [popup text];

	// Now selection specified, does the replacement string offer
	// an automatic selection?
	if (range.length == 0) {

		// $l in string means "select line"
		NSRange selectLineMarker = [textToAdd rangeOfString:@"$l"];
		if (selectLineMarker.location != NSNotFound) {
			textToAdd = [textToAdd stringByReplacingOccurrencesOfString:@"$l" withString:@""];
			range     = [fullText lineRangeForRange:range];
		}

		// $w in string means "select word"
		NSRange selectwordMarker = [textToAdd rangeOfString:@"$w"];
		if (selectwordMarker.location != NSNotFound) {
			textToAdd = [textToAdd stringByReplacingOccurrencesOfString:@"$w" withString:@""];
			range     = [fullText rangeOfWordInRange:range];
		}
	}
	else {
		textToAdd = [textToAdd stringByReplacingOccurrencesOfString:@"$l" withString:@""];
		textToAdd = [textToAdd stringByReplacingOccurrencesOfString:@"$w" withString:@""];
	}

	// split up text to add into before $$ and after $$
	NSRange start = [textToAdd rangeOfString:@"$s"];
	NSRange end   = [textToAdd rangeOfString:@"$s" options:(NSBackwardsSearch)];

	if (start.location == NSNotFound) {
		addBefore = textToAdd;
		addAfter  = @"";
	}
	else {
		addBefore = [textToAdd substringToIndex:start.location];
		if (addBefore == nil) {
			addBefore = @"";
		}

		addAfter = [textToAdd substringFromIndex:(end.location + end.length)];
		if (addAfter == nil) {
			addAfter = @"";
		}
	}

	addBefore = [addBefore expandMacros];
	addAfter  = [addAfter expandMacros];

	// Split main string into before selection, selection text (if any) and after selection
	NSString* beforeSelection = [fullText substringToIndex:range.location];
	NSString* afterSelection  = [fullText substringFromIndex:(range.location + range.length)];
	NSString* selection		  = [fullText substringWithRange:range];
	
	if (beforeSelection == nil) {
		beforeSelection = @"";
	}
	if (afterSelection == nil) {
		afterSelection = @"";
	}
	if (selection == nil) {
		selection = @"";
	}

	NSString* newSelectionText = [
		NSString stringWithFormat:@"%@%@%@",
		addBefore, selection, addAfter
	];

	// do we have a cursor marker?
	NSRange cursorPosition = [newSelectionText rangeOfString:@"$c"];
	if (cursorPosition.location != NSNotFound) {
		// If so, record position and remove $c tag
		newSelectionText = [newSelectionText stringByReplacingOccurrencesOfString:@"$c" withString:@""];
	}

	// build up new string, wrapping the shortcut around any selection
	NSString* newText = [NSString stringWithFormat:@"%@%@%@",
		beforeSelection, newSelectionText, afterSelection
	];

	[popup setText:newText];

	if (cursorPosition.location == NSNotFound) {
		// Move cursor to after the insertion point
		range.location += range.length + [addBefore length] + [addAfter length];
	}
	else {
		// Move cursor to where the $c marker was
		range.location += cursorPosition.location;
	}
	// remove any existing selection since it doesn't look right
	range.length = 0;

	[popup setSelectedRange:range];

	return range;
}

//-----------------------------------------------------------------
- (UIInputView*) createKeyboardView
{
	CGRect popupFrame = [[self popupView] frame];

	// note it will be resized later...
	UIInputView* newView = [[UIInputView alloc] initWithFrame: popupFrame inputViewStyle:UIInputViewStyleKeyboard];

	UIColor* background = [UIApplication isIOS7] ?
		[UIColor colorWithRed:166.0/256.0 green:169.0/256.0 blue:172.0/256.0 alpha:0.8] :
		[UIColor colorWithRed:110.0/256.0 green:115.0/256.0 blue:127.0/256.0 alpha:1.0];

	[newView setBackgroundColor:background];

	// important for view orientation rotation
	[newView setAutoresizingMask: (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
	[newView setAutoresizesSubviews: YES];
	
	[newView setHidden:YES];

	return newView;
}

//-----------------------------------------------------------------
static const CGFloat kToolBarHeight = 48.0f;

- (UIToolbar*) getToolbar:(UIView*)contentView
{
	UIToolbar* toolbar = [[UIToolbar alloc] init];
	DT_ASSERT_NOT_NIL(toolbar);
	CGRect viewSize = [contentView bounds];

	// kToolBarHeight pixels high, full width of screen
	[toolbar setFrame:CGRectMake(0, viewSize.size.height - kToolBarHeight + 1, viewSize.size.width, kToolBarHeight)];
	[toolbar setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin)];

	// Make style match other toolbars
	[toolbar setBarStyle:UIBarStyleBlack];
	[toolbar setTranslucent:[UIApplication isIOS7]];

	UIBarButtonItem* flexibleSpace	= [UIBarButtonItem createFlexibleSpace];

	NSMutableArray* items = [[NSMutableArray alloc] init];

	UIBarButtonItem* saveButton = [
		UIBarButtonItem
		createSaveButton:NSSelectorFromString(@"popupTextControllerSaveButtonPressed")
		forTarget:[self target]
	];
	[items addObject:saveButton];

	[items addObject:flexibleSpace];

	if ([self enabledKeyboards] & keyboardIDControl) {
		UIBarButtonItem* controlButton	= [UIBarButtonItem
			createButtonWithText:@"<>" action:@selector(popupTextControllerControlButtonPressed) forTarget:self];
		[items addObject:controlButton];
		[items addObject:flexibleSpace];
	}

	if ([self enabledKeyboards] & keyboardIDShortcut) {
		UIBarButtonItem* shortcutButton	= [UIBarButtonItem
			createButtonWithText:@"My" action:@selector(popupTextControllerShortcutButtonPressed) forTarget:self];
		[items addObject:shortcutButton];
		[items addObject:flexibleSpace];
	}

	if ([self enabledKeyboards] & keyboardIDMarkdown) {
		UIBarButtonItem* markdownButton	= [UIBarButtonItem
			createButtonWithText:@" # " action:@selector(popupTextControllerMarkdownButtonPressed) forTarget:self];
		[items addObject:markdownButton];
		[items addObject:flexibleSpace];
	}

	if ([self enabledKeyboards] & keyboardIDHTML) {
		UIBarButtonItem* HTMLButton	= [UIBarButtonItem
			createButtonWithText:@"HTML" action:@selector(popupTextControllerHTMLButtonPressed) forTarget:self];
		[items addObject:HTMLButton];
		[items addObject:flexibleSpace];
	}

	if ([self enabledKeyboards] & keyboardIDURL) {
		UIBarButtonItem* URLButton	= [UIBarButtonItem
			createButtonWithText:@"WWW" action:@selector(popupTextControllerURLButtonPressed) forTarget:self];
		[items addObject:URLButton];
		[items addObject:flexibleSpace];
	}

	UIBarButtonItem* cancelButton =
		[UIBarButtonItem
		createCancelButton:NSSelectorFromString(@"popupTextControllerCancelButtonPressed")
		 forTarget:[self target]
	];
	[items addObject:cancelButton];

	[toolbar setItems:items animated:NO];

	return toolbar;
}

//-----------------------------------------------------------------
- (CGPoint) getPositionForControlButtonIndex:(int)index
{
	static CGPoint controlButtons[] = {

		{ 5.0,  5.0   },	// cursor keys
		{ 5.0,  55.0  },
		{ 5.0,  105.0 },
		{ 64.0, 5.0   },
		{ 64.0, 55.0  },
		{ 64.0, 105.0 },

		{ 136.0, 5.0 },		// select keys
		{ 205.0, 5.0 },
		{ 265.0, 5.0 },

		{ 136.0, 55.0 },	// cut, copy, paste
		{ 205.0, 55.0 },
		{ 265.0, 55.0 },

		{ 265.0, 105.0 },	// Delete
		{ 205.0, 105.0 }	// TAB
	};

	return controlButtons[index];
}

//-----------------------------------------------------------------
- (void) rearrangeKeyboardButtons:(keyboardID)keyboardID
{
	if (keyboardID == keyboardIDRegular) {
		return;
	}

	DTCustomKeyboard* keyboard = [_keyboardList objectForKey:[NSNumber numberWithInt:keyboardID]];
	if (!keyboard) {
		return;
	}

	UIView* shortcutView = [keyboard view];
	if (!shortcutView) {
		return;
	}

	BOOL isControl = (keyboardID == keyboardIDControl);

	CGRect frame = [UIApplication myMainScreenBoundsRotated];

	int   i = 0;
	float x = 6.0;
	float y = 8.0;

	NSArray* subviews = [shortcutView subviews];

	NSMutableArray* thisRow = [[NSMutableArray alloc] init];

	for (UIView* button in subviews) {

		if ([button isKindOfClass:[UIButton class]]) {

			CGSize size = [button frame].size;

			if (isControl) {

				CGPoint newPosition = [self getPositionForControlButtonIndex:i];
				x = newPosition.x;
				y = newPosition.y;
				i++;

			} else {
				if (x > (frame.size.width - size.width - shortcutButtonSeparation)) {

					float remaining = (frame.size.width - x) / 2.0;
					for (UIView* view in thisRow) {
						CGRect bounds = [view frame];
						bounds.origin.x += remaining;
						[view setFrame:bounds];
					}
					[thisRow removeAllObjects];

					x = 6.0;
					y += (size.height + (2.0 * shortcutButtonSeparation));
				}
			}

			CGRect box = CGRectMake(x, y, size.width, size.height);
			[button setFrame:box];
			[thisRow addObject:button];

			x += size.width + (2.0 * shortcutButtonSeparation);
		}
	}

	if ((! isControl) && ([thisRow count] > 0)) {
		float remaining = (frame.size.width - x) / 2.0;

		for (UIView* view in thisRow) {
			CGRect bounds = [view frame];
			bounds.origin.x += remaining;
			[view setFrame:bounds];
		}
	}

	[thisRow removeAllObjects];
}

//-----------------------------------------------------------------
- (void) addButtonsToShortcutKeyboard:(UIView*)view withKeyboardID:(keyboardID)keyboardID shortcuts:(NSMutableArray*)shortcuts
{
	UIView* shortcutView = view;

	UIFont* font = [UIApplication isIOS7] ?
		[UIFont fontWithName:@"Helvetica-Light" size: 17] :
		[UIFont systemFontOfSize: 17];

	//	UIFont* font = [UIFont fontWithName:@"Helvetica Neue" size:17];

	int tag = (keyboardID << 8) & 0xff00;

	for (DTShortcut* shortcut in shortcuts) {

		NSString* label	= [shortcut label];
		if ([label length] > 0) {

			//			CGSize size = [label sizeWithFont:font];
			CGSize size = [label sizeWithAttributes:@{NSFontAttributeName : font}];
			size.width += 12.0;

			if (size.width < shortcutButtonWidth) {
				size.width = shortcutButtonWidth;
			}
			if (size.height < shortcutButtonHeight) {
				size.height = shortcutButtonHeight;
			}

			CGRect box = CGRectMake(0, 0, size.width, size.height);
			UIGlossyButton* button = [[UIGlossyButton alloc] initWithFrame:box];
			[button setTitle:label forState:UIControlStateNormal];
			[button setTag:tag];

			[[button titleLabel] setFont:font];
			[button useBlackLabel:YES];

			if ([UIApplication isIOS7]) {
				[button setGradientType: kUIGlossyButtonGradientTypeSolid];
				[button setStrokeType:kUIGlossyButtonStrokeTypeSolid];
			}
			else {
				[button setShadow:[UIColor blackColor] opacity:1.0 offset:CGSizeMake(0, 1) blurRadius: 3];
				[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
				[button setTintColor:[UIColor whiteColor]];
			}
			
			[button setShowsTouchWhenHighlighted:YES];

			[button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
			[shortcutView addSubview:button];

			tag++;
		}
	}

	[self rearrangeKeyboardButtons:keyboardID];
}

//-----------------------------------------------------------------
- (void) showKeyboard:(keyboardID)newKeyboardID
{
	LOG_FN();
	
	keyboardID currentKeyboardID = [self currentKeyboardID];

	if (currentKeyboardID == newKeyboardID) {
		newKeyboardID = keyboardIDRegular;
	}

	YIPopupTextView* popup	= [self popupView];
	
	DTCustomKeyboard* currentKeyboard	= [_keyboardList objectForKey:[NSNumber numberWithInt:currentKeyboardID]];
	DTCustomKeyboard* newKeyboard		= [_keyboardList objectForKey:[NSNumber numberWithInt:newKeyboardID]];
	
	UIView* currentKeyboardView = (currentKeyboardID == keyboardIDRegular) ? nil : [currentKeyboard view];
	UIView* newKeyboardView		= (newKeyboardID == keyboardIDRegular)     ? nil : [newKeyboard view];

	[self rearrangeKeyboardButtons:newKeyboardID];

	[popup setRespondsToNotifications:NO];

	[popup setInputView:newKeyboardView];
	[self setCurrentKeyboardID:newKeyboardID];

	[currentKeyboardView removeFromSuperview];
	[newKeyboardView removeFromSuperview];

	[currentKeyboardView setHidden:YES];
	[newKeyboardView setHidden:NO];

	[popup reloadInputViews];
	[popup setRespondsToNotifications:YES];
}

//-----------------------------------------------------------------
- (void) popupTextControllerShortcutButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"shortcut keyboard selected");
	[self showKeyboard:keyboardIDShortcut];
}

//-----------------------------------------------------------------
- (void) popupTextControllerHTMLButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"HTML keyboard selected");
	[self showKeyboard:keyboardIDHTML];
}

//-----------------------------------------------------------------
- (void) popupTextControllerMarkdownButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"markdown keyboard selected");
	[self showKeyboard:keyboardIDMarkdown];
}

//-----------------------------------------------------------------
- (void) popupTextControllerURLButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"URL keyboard selected");
	[self showKeyboard:keyboardIDURL];
}

//-----------------------------------------------------------------
- (void) popupTextControllerControlButtonPressed
{
	TESTFLIGHT_CHECKPOINT(@"controller keyboard selected");
	[self showKeyboard:keyboardIDControl];
}

//-----------------------------------------------------------------
- (NSRange) cursorMovement:(int)offsetToMoveBy textView:(YIPopupTextView*)popup
{
	NSString* fullText	= [popup text];
	NSRange range		= [popup selectedRange];
	long fullTextLength	= [fullText length];

	if (range.location == NSNotFound) {
		return range;
	}

	// Move cursor
	long newLocation = (range.location) + offsetToMoveBy;

	// Limit cursor to string
	if (newLocation < 0) {
		newLocation = 0;
	}
	if (newLocation > fullTextLength) {
		newLocation = fullTextLength;
	}

	range.location = newLocation;

	return range;
}

//-----------------------------------------------------------------
- (void) handleControlKey:(NSString*)shortcut
{
	YIPopupTextView* popup	= [self popupView];

	NSString* fullText		= [popup text];
	NSRange range			= [popup selectedRange];
	BOOL hasSelection		= range.length > 0;
	NSRange existingRange	= range;
	BOOL updatesCursor		= YES;

	NSRange			newRange;
	NSUInteger		n;
	UIPasteboard*	clipboard;
	NSString*		pasteString;
	NSString*		before;
	NSString*		after;

	switch ([shortcut intValue]) {

		//-----------------------
		case 3:
			// forward one char
			range = [self cursorMovement:1 textView:popup];
			break;

		//-----------------------
		case 2:
			// back one char
			range = [self cursorMovement:-1 textView:popup];
			break;
			
		//-----------------------
		case 1:
			// back one word
			if (range.location > 0) {
				range.location--;
			}
			newRange = [fullText rangeOfWordInRange:range];
			range.location = newRange.location;
			break;

		//-----------------------
		case 4:
			// forward one word
			if (range.location < [fullText length]) {
				range.location++;
			}
			newRange = [fullText rangeOfWordInRange:range];
			range.location = newRange.location + newRange.length;
			break;

		//-----------------------
		case 5:
			// To end of line
			[fullText getLineStart:NULL end:&n contentsEnd:NULL forRange:range];
			if ((range.location == n) && (range.location < [fullText length])) {
				range.location++;
				[fullText getLineStart:NULL end:&n contentsEnd:NULL forRange:range];
			}
			range.location = n;
			break;

		//-----------------------
		case 6:
			// To start of line
			[fullText getLineStart:&n end:NULL contentsEnd:NULL forRange:range];
			if ((range.location == n) && (range.location > 0)) {
				range.location--;
				[fullText getLineStart:&n end:NULL contentsEnd:NULL forRange:range];
			}
			range.location = n;
			break;

		//-----------------------
		case 10:
			// Select word
			range = [fullText rangeOfWordInRange:range];
			existingRange = range;
			break;
			
		//-----------------------
		case 11:
			// Select line
			range = [fullText lineRangeForRange:range];
			existingRange = range;
			break;

		//-----------------------
		case 12:
			// Select nothing
			range.length  = 0;
			existingRange = range;
			hasSelection  = NO;
			break;
		
		//-----------------------
		case 13:
			// Copy
			if (range.length > 0) {
				clipboard = [UIPasteboard generalPasteboard];
				pasteString = [fullText substringWithRange:range];
				[clipboard setString:pasteString];

				range.length  = 0;
				hasSelection  = NO;
				updatesCursor = YES;
			}
			break;
			
		//-----------------------
		case 15:
			// Cut
			if (range.length > 0) {
				[popup setScrollEnabled:NO];
				clipboard = [UIPasteboard generalPasteboard];
				pasteString = [fullText substringWithRange:range];
				[clipboard setString:pasteString];

				before = [fullText substringToIndex:range.location];
				after  = [fullText substringFromIndex:(range.location + range.length)];
				[popup setText:[before stringByAppendingString:after]];

				range.length  = 0;
				hasSelection  = NO;
				updatesCursor = YES;
			}
			break;

		//-----------------------
		case 14:
			// Paste
			clipboard = [UIPasteboard generalPasteboard];
			pasteString = [clipboard string];

			if (pasteString != nil) {
				[popup setScrollEnabled:NO];

				range = [self addText:pasteString];
				hasSelection  = NO;
				updatesCursor = YES;
			}
			break;

			//-----------------------
		case 16:
			// Del
			if ((range.length == 0) && (range.location > 0)) {
				range.location--;
				range.length = 1;
			}

			if (range.length > 0) {
				[popup setScrollEnabled:NO];

				before = [fullText substringToIndex:range.location];
				after  = [fullText substringFromIndex:(range.location + range.length)];
				[popup setText:[before stringByAppendingString:after]];

				range.length  = 0;
				hasSelection  = NO;
				updatesCursor = YES;
			}
			break;

		case 17:
			// TAB
			[popup setScrollEnabled:NO];
			range = [self addText:@"\t"];
			hasSelection  = NO;
			updatesCursor = YES;
			break;

		//-----------------------
		default:
			break;
	}

	if (updatesCursor) {
		if (hasSelection) {
			if (range.location < existingRange.location) {
				unsigned long newLength = range.length + (existingRange.location - range.location);
				if ((range.location + newLength) < [fullText length]) {
					range.length = newLength;
				}
			}
			else {
				range.length -= (range.location - existingRange.location);
			}
		}

		[popup setSelectedRange:range];
		[popup setScrollEnabled:YES];
	}
}

//-----------------------------------------------------------------
- (void) buttonPressed:(id)sender
{
	UIButton* button		= (UIButton*) sender;
	int			tag			= (int)[button tag];
	keyboardID	keyboardID	= (enum keyboardID) ((tag >> 8) & 0xff);
	int			keyIndex	= tag & 0xff;
	
	DTCustomKeyboard* keyboard = [_keyboardList objectForKey:[NSNumber numberWithInt:keyboardID]];

	DTShortcut* shortcut = [[keyboard labels] objectAtIndex:keyIndex];

	switch (keyboardID) {
		case keyboardIDControl:
			[self handleControlKey:[shortcut text]];
			break;
			
		default:
			[self addText:[shortcut text]];
			[self showKeyboard:keyboardIDRegular];
			break;
	}
}

//-----------------------------------------------------------------
- (void) popupTextView:(YIPopupTextView*)textView willDismissWithText:(NSString*)text
{
    if ([[self popupTextControllerDelegate] respondsToSelector:@selector(popupTextController:willDismissWithText:)]) {
        [[self popupTextControllerDelegate] popupTextController:self willDismissWithText:text];
    }
}

//-----------------------------------------------------------------
- (void) popupTextView:(YIPopupTextView*)textView didDismissWithText:(NSString*)text
{
    if ([[self popupTextControllerDelegate] respondsToSelector:@selector(popupTextController:didDismissWithText:)]) {
        [[self popupTextControllerDelegate] popupTextController:self didDismissWithText:text];
    }
}

//-----------------------------------------------------------------
- (void) popupTextView:(YIPopupTextView*)textView textDidChange:(NSString*)text
{
    if ([[self popupTextControllerDelegate] respondsToSelector:@selector(popupTextController:textDidChange:)]) {
        [[self popupTextControllerDelegate] popupTextController:self textDidChange:text];
    }
}

//-----------------------------------------------------------------
- (void) viewDidLoad
{
	LOG_FN();

	[super viewDidLoad];

	YIPopupTextView* popup = [self popupView];
	UIView* view = [self view];
	[popup setTextViewDelegate:self];

	// Add toolbar
	id accessoryView = [self getToolbar:view];
	[popup setInputAccessoryView:accessoryView];

	//--------------
	DTCustomKeyboard* keyboardShortcut = [[DTCustomKeyboard alloc] init];
	
	UIInputView* keyboardViewShortcut = [self createKeyboardView];
	[keyboardShortcut setView:keyboardViewShortcut];
	
	DTOptionArray* shortcuts = [[UIApplication myOptions] shortcuts];
	[keyboardShortcut setLabels:[shortcuts array]];

	[self addButtonsToShortcutKeyboard:keyboardViewShortcut withKeyboardID:keyboardIDShortcut shortcuts:[shortcuts array]];

	//--------------
	DTCustomKeyboard* keyboardHTML = [[DTCustomKeyboard alloc] init];
	
	UIInputView* keyboardViewHTML = [self createKeyboardView];
	[keyboardHTML setView:keyboardViewHTML];

	NSMutableArray* labels = [self createHTMLLabels];
	[keyboardHTML setLabels:labels];
	[self addButtonsToShortcutKeyboard:keyboardViewHTML withKeyboardID:keyboardIDHTML shortcuts:labels];

	//--------------
	DTCustomKeyboard* keyboardMarkdown = [[DTCustomKeyboard alloc] init];

	UIInputView* keyboardViewMarkdown = [self createKeyboardView];
	[keyboardMarkdown setView:keyboardViewMarkdown];
	
	labels = [self createMarkdownLabels];
	[keyboardMarkdown setLabels:labels];
	[self addButtonsToShortcutKeyboard:keyboardViewMarkdown withKeyboardID:keyboardIDMarkdown shortcuts:labels];

	//--------------
	DTCustomKeyboard* keyboardURL = [[DTCustomKeyboard alloc] init];
	
	UIInputView* keyboardViewURL = [self createKeyboardView];
	[keyboardURL setView:keyboardViewURL];

	labels = [self createURLLabels];
	[keyboardURL setLabels:labels];
	[self addButtonsToShortcutKeyboard:keyboardViewURL withKeyboardID:keyboardIDURL shortcuts:labels];

	//--------------
	DTCustomKeyboard* keyboardControl = [[DTCustomKeyboard alloc] init];

	UIInputView* keyboardViewControl = [self createKeyboardView];
	[keyboardControl setView:keyboardViewControl];

	labels = [self createControlLabels];
	[keyboardControl setLabels:labels];
	[self addButtonsToShortcutKeyboard:keyboardViewControl withKeyboardID:keyboardIDControl shortcuts:labels];

	//--------------
	_keyboardList = [NSMutableDictionary dictionaryWithObjectsAndKeys:
					 
		[NSNull null],		[NSNumber numberWithInt:keyboardIDRegular],
		keyboardHTML,		[NSNumber numberWithInt:keyboardIDHTML],
		keyboardMarkdown,	[NSNumber numberWithInt:keyboardIDMarkdown],
		keyboardControl,	[NSNumber numberWithInt:keyboardIDControl],
		keyboardShortcut,	[NSNumber numberWithInt:keyboardIDShortcut],
		keyboardURL,		[NSNumber numberWithInt:keyboardIDURL],
		nil
	];
	
	[popup setInputView:nil];
	[self setCurrentKeyboardID:keyboardIDRegular];

	[popup showInView:view HTMLView:nil];

	// Restore cursor to where it was when we stopped editing
	NSRange cursorRange = {_cursorPosition, 0};
	[popup setSelectedRange:cursorRange];
}

//-----------------------------------------------------------------
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

//-----------------------------------------------------------------
- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[self rearrangeKeyboardButtons:[self currentKeyboardID]];
}

//-----------------------------------------------------------------
// iOS 6
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

//-----------------------------------------------------------------
// iOS 6
- (BOOL) shouldAutorotate
{
	return YES;
}

//-----------------------------------------------------------------
@end
