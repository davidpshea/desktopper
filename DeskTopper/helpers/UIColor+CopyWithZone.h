//
//  UIColor+CopyWithZone.h
//  DeskTopper
//
//  Created by David Shea on 10/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//-----------------------------------------------------------------
@interface UIColor (CopyWithZone)

-(id)copyWithZone:(NSZone*)zone;

@end
//-----------------------------------------------------------------
