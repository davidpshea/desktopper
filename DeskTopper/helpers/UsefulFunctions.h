﻿//-----------------------------------------------------------------
#if 0
inline void CGPointAssign(CGPoint* const assignTo, const CGPoint* const from)
{
	assignTo->x = from->x;
	assignTo->y = from->y;
}

//-----------------------------------------------------------------
inline float roundToNearest(const float N, const float roundTo)
{
	return ((int)((N + (roundTo / 2)) / roundTo)) * roundTo;
}

//-----------------------------------------------------------------
inline void roundToNearestPoint(CGPoint* const point, const int roundTo)
{
	roundToNearest(point->x, roundTo);
	roundToNearest(point->y, roundTo);
}

//-----------------------------------------------------------------
inline void CGSizeScale(CGSize* const size, const float scaleFactor)
{
	size->width  *= scaleFactor;
	size->height *= scaleFactor;
}
#endif

//-----------------------------------------------------------------

