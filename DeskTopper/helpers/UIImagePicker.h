//
//  UIImagePickerController+Helper.h
//  DeskTopper
//
//  Created by David Shea on 27/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "DTWidget.h"
#import "QuickDialogHelper.h"
//#import "DTOptions.h"
#import "CustomImagePickerController.h"
/*
//@interface UIImagePickerController (Helper)
@interface UIImagePicker : DTOptionsControllerWithDone
		<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) DTWidget* widget;
*/
//-----------------------------------------------------------------
@class DTWidget;

@interface UIImagePicker : UIViewController
	<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, weak) DTWidget* widget;

- (void) showPhotoPicker:(DTWidget*)widget rootViewController:(UIViewController*)rootViewController;
/*- (void) viewDidLoad;
- (void) doneButtonPressed;
- (void) cancelButtonPressed;
- (void) showPhotoPicker;
- (void) showCamera;
- (void) showIconPicker;
*/
@end

//@end

