//
//  UIApplication+Helpers.m
//  DeskTopper
//
//  Created by David Shea on 18/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netdb.h>
#import <SystemConfiguration/SCNetworkReachability.h>

#import "UIApplication+Helpers.h"
#import "DTAppDelegate.h"
#import "DTViewController.h"
#import "FMDatabase.h"
#import "DTOptions.h"
#import "DTDesktop.h"

@implementation UIApplication (Helpers)

//-----------------------------------------------------------------
+ (UIApplication*) myApp
{
	return [UIApplication sharedApplication];
}

//-----------------------------------------------------------------
+ (NSString*) myAppVersion
{
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

//-----------------------------------------------------------------
+ (id) myAppDelegate
{
	return [[UIApplication sharedApplication] delegate];
}

//-----------------------------------------------------------------
+ (id) myRootViewController
{
	return [[[UIApplication sharedApplication] keyWindow] rootViewController];
}

//-----------------------------------------------------------------
+ (id) myPresentedViewController
{
	UIViewController* viewController = [[UIApplication myRootViewController] presentedViewController];
	return viewController ? viewController : [UIApplication myRootViewController];
}

//-----------------------------------------------------------------
+ (id) myNavigationController
{
	return [[UIApplication myRootViewController] navigationController];
}

//-----------------------------------------------------------------
+ (CGRect) myMainScreenBounds
{
	id<UICoordinateSpace> screenSpace = [[UIScreen mainScreen] fixedCoordinateSpace];
	return [screenSpace bounds];
}

//-----------------------------------------------------------------
+ (UIWindow*) myWindow
{
	return [[UIApplication sharedApplication] keyWindow];
}

//-----------------------------------------------------------------
+ (UIInterfaceOrientation) myRotation
{
	return [[UIApplication sharedApplication] statusBarOrientation];
}

//-----------------------------------------------------------------
+ (BOOL) isPortrait
{
	return UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]);
}

//-----------------------------------------------------------------
+ (BOOL) isLandscape
{
	return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}

//-----------------------------------------------------------------
+ (CGPoint) myMainScreenCentre
{
	CGRect bounds = [UIApplication myMainScreenBoundsRotated];
	
	return CGPointMake(
		bounds.origin.x + (bounds.size.width / 2.0),
		bounds.origin.y + (bounds.size.height / 2.0)
	);
}

//-----------------------------------------------------------------
+ (CGRect) myMainScreenBoundsRotated
{
	id<UICoordinateSpace> screenSpace = [[UIScreen mainScreen] coordinateSpace];
	return [screenSpace bounds];
}

//-----------------------------------------------------------------
+ (NSOperationQueue*) myOperationQueue
{
	DTAppDelegate* delegate = (DTAppDelegate*)[UIApplication myAppDelegate];
	return [delegate operationQueue];
}

//-----------------------------------------------------------------
+ (FMDatabase*) myDatabase
{
	return [[UIApplication myRootViewController] database];
}

//-----------------------------------------------------------------
+ (DTOptions*) myOptions
{
	return [[UIApplication myRootViewController] myOptions];
}

//-----------------------------------------------------------------
+ (void) addOperationToMyOperationQueue:(NSOperation*)operation
{
	DTAppDelegate* delegate = (DTAppDelegate*)[UIApplication myAppDelegate];
	[[delegate operationQueue] addOperation:operation];
}

//-----------------------------------------------------------------
+ (void) dismissToRootViewController:(BOOL)animated
{
    [[UIApplication myRootViewController] dismissViewControllerAnimated:animated completion:nil];
}

//-----------------------------------------------------------------
+ (BOOL) isConnectedToInternet
{
    NSURL *url = [NSURL URLWithString:@"http://www.google.com/"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
	
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
	
    return ([response statusCode] == 200) ? YES : NO;
}

//-----------------------------------------------------------------
+ (BOOL) isConnectedToNetwork
{
	// Create zero addy
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	
	if (!didRetrieveFlags) {
		return NO;
	}
	
	BOOL isReachable = flags & kSCNetworkFlagsReachable;
	BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
	return (isReachable && !needsConnection) ? YES : NO;
}

//-----------------------------------------------------------------
+ (BOOL) isIOS7
{
	return [[[UIDevice currentDevice] systemVersion] floatValue] >= 7;
}

//-----------------------------------------------------------------
+ (BOOL) isPad
{
	#ifdef UI_USER_INTERFACE_IDIOM
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
	#else
    return NO;
	#endif
}

//-----------------------------------------------------------------
+ (BOOL) isPhone
{
	#ifdef UI_USER_INTERFACE_IDIOM
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
	#else
    return NO;
	#endif
}

//-----------------------------------------------------------------
static UIColor* oldBarColour = nil;

+ (void) PushNavigationBarColour:(UIColor*)barColour
{
	if ([UIApplication isIOS7]) {
		oldBarColour = [[UINavigationBar appearance] barTintColor];
		 [[UINavigationBar appearance] setBarTintColor:barColour];
	}
}

//-----------------------------------------------------------------
+ (void) PopNavigationBarColour
{
	if ([UIApplication isIOS7] && oldBarColour) {
		[[UINavigationBar appearance] setBarTintColor:oldBarColour];
		oldBarColour = nil;
	}
}

//-----------------------------------------------------------------
+ (void) PushNavigationBarDarkGreyColour
{
	[UIApplication PushNavigationBarColour:[UIColor darkGrayColor]];
}

//-----------------------------------------------------------------
@end
