//
//  NSCoder+DefaultDecoding.h
//  DeskTopper
//
//  Created by David Shea on 16/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCoder (DefaultDecoding)

- (id)		decodeObject:(NSString*)key withDefault:(id)defaultObject;
- (float)	decodeFloat: (NSString*)key withDefault:(float)defaultFloat;
- (int)		decodeInt:	 (NSString*)key withDefault:(int)defaultInt;
- (BOOL)	decodeBOOL:  (NSString*)key withDefault:(BOOL)defaultBOOL;
- (CGRect)	decodeRect:  (NSString*)key withDefault:(CGRect)defaultRect;

@end
