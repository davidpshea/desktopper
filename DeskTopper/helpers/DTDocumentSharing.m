//
//  DTDocumentSharing.m
//  DeskTopper
//
//  Created by David Shea on 31/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIApplication+Helpers.h"
#import "DTDocumentSharing.h"
#import "SVProgressHUD.h"
#import "FileHelper.h"
#import "UIImageExtras.h"

//-----------------------------------------------------------------
#define SUPPORT_EMAIL	@"support@bigorsmallapps.com"
#define SUPPORT_WEB		@"http://www.bigorsmallapps.com"

//-----------------------------------------------------------------
@implementation DTDocumentSharing

@synthesize strongSelf				= _strongSelf;
@synthesize ownsFile				= _ownsFile;
@synthesize interactionController	= _interactionController;

//-----------------------------------------------------------------
- (DTDocumentSharing*) init:(BOOL)ownsFile
{
	self = [super init];
	[self setStrongSelf:self];
	[self setOwnsFile:ownsFile];

	return self;
}

//-----------------------------------------------------------------
- (void) dealloc
{
	LOG_FN();
}

//-----------------------------------------------------------------
+ (void) emailSupport:(NSString*)subject body:(NSString*)body
{
	DTDocumentSharing* sharing = [[DTDocumentSharing alloc] init:NO];
	
	NSDictionary* plist = [[NSBundle mainBundle] infoDictionary];
	NSString*     info  = @"\n\n---------\n";

	#if 0
	for (NSString* s in plist) {
		info = [info stringByAppendingString:s];
		info = [info stringByAppendingString:@":"];
		info = [info stringByAppendingString:@"\n"];
	}
	#endif
	
	info = [info stringByAppendingFormat:@"App Version: %@\n", [plist valueForKey:@"CFBundleVersion"]];
	info = [info stringByAppendingFormat:@"Platform: %@\n",    [plist valueForKey:@"DTPlatformName"]];

	UIDevice* device = [UIDevice currentDevice];
	info = [info stringByAppendingFormat:@"Model: %@\n", [device model]];
	info = [info stringByAppendingFormat:@"OS: %@:%@\n", [device systemName], [device systemVersion]];
	
	body = [body stringByAppendingString:info];

	MFMailComposeViewController* mailViewController = [[MFMailComposeViewController alloc] init];

	NSArray* to = [[NSArray alloc] initWithObjects:SUPPORT_EMAIL, nil];
	[mailViewController setMailComposeDelegate:sharing];
	[mailViewController setSubject:subject];
	[mailViewController setToRecipients:to];
	[mailViewController setMessageBody:body isHTML:NO];
	[mailViewController setModalPresentationStyle:UIModalPresentationFormSheet];

	[[UIApplication myPresentedViewController]
		presentViewController:mailViewController animated:YES completion:nil];
}

//-----------------------------------------------------------------
+ (void) sendEmail:(NSString*)subject body:(NSString*)body isHTML:(BOOL)isHTML imageFileName:(NSString*)fileName
{
	DTDocumentSharing* sharing = [[DTDocumentSharing alloc] init:NO];

	MFMailComposeViewController* mailViewController = [[MFMailComposeViewController alloc] init];

	body = isHTML ? [body stringByAppendingString:@"<br><br><hr><br>"] :
					[body stringByAppendingString:@"\n\n-------\n"];

	body = [body stringByAppendingString:@"Sent from Total Notes on iPhone"];

	[mailViewController setMailComposeDelegate:sharing];
	[mailViewController setSubject:subject];
	[mailViewController setMessageBody:body isHTML:isHTML];
	[mailViewController setModalPresentationStyle:UIModalPresentationFormSheet];

	// Special case - include a file as an attachment - Always PNG...
	if (fileName) {
		UIImage* myImage  = [UIImage createFromImageFile:fileName];
		NSData* imageData = UIImagePNGRepresentation(myImage);
		[mailViewController addAttachmentData:imageData mimeType:@"image/png" fileName:@"TotalNotes"];
	}

	[[UIApplication myPresentedViewController]
		presentViewController:mailViewController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) mailComposeController:(MFMailComposeViewController*)controller
		   didFinishWithResult:(MFMailComposeResult)result
						 error:(NSError*)error
{
	[controller
	 dismissViewControllerAnimated:YES completion:^{ [self setStrongSelf:nil]; }];
}

//-----------------------------------------------------------------
- (void) documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController*)controller
{
	LOG_FN();
}

//-----------------------------------------------------------------
- (void) documentInteractionController:(UIDocumentInteractionController*)controller
	willBeginSendingToApplication:(NSString*)application
{
	LOG_FN();
	[SVProgressHUD show];
}

//-----------------------------------------------------------------
- (void) documentInteractionController:(UIDocumentInteractionController*)controller
	didEndSendingToApplication:(NSString*)application
{
	LOG_FN();
	if ([self ownsFile]) {
		NSURL* URL = [controller URL];
		[FileHelper deleteFile:[URL path]];
	}

	[SVProgressHUD dismiss];
	[self setStrongSelf:nil];
}

//-----------------------------------------------------------------
+ (void) exportString:(NSString*)text as:(NSString*)uti
{
	DTDocumentSharing* sharing = [[DTDocumentSharing alloc] init:YES];

	NSString* fileName = @"TotalNotes.txt";
	DT_DEBUG_LOG(@"Export text name:%@", fileName);
	NSString* filePath = [FileHelper getPathForFileInDocuments:fileName];
	DT_DEBUG_LOG(@"Export text path:%@", filePath);

	NSError* error = nil;
	BOOL written = [FileHelper writeStringToFile:filePath content:text error:(& error)];
	DT_DEBUG_LOG(@"Text file written Error:%d, %@", written, [error localizedDescription]);

	if (written) {
		NSURL* URL = [NSURL fileURLWithPath:filePath];

		UIDocumentInteractionController* interaction =
			[UIDocumentInteractionController interactionControllerWithURL:URL];

		[sharing setInteractionController:interaction];
		[interaction setDelegate:sharing];
		if (uti) {
			[interaction setUTI:uti];
		}

		UIViewController* viewController = [UIApplication myPresentedViewController];
		DT_ASSERT_NOT_NIL(viewController);

		BOOL presentedMenu = [interaction
			presentOpenInMenuFromRect:[[viewController view] bounds]
			inView:[viewController view]
			animated:YES
		];

		if (! presentedMenu) {
			[FileHelper deleteFile:filePath];
			[sharing setStrongSelf:nil];
		}
	}
}

//-----------------------------------------------------------------
+ (void) exportPhoto:(NSString*)filePath
{
	DTDocumentSharing* sharing = [[DTDocumentSharing alloc] init:NO];

	UIViewController* viewController = [UIApplication myPresentedViewController];
	DT_ASSERT_NOT_NIL(viewController);

	DT_DEBUG_LOG(@"Export photo path:%@", filePath);
	NSURL* URL = [NSURL fileURLWithPath:filePath];

	UIDocumentInteractionController* interaction =
		[UIDocumentInteractionController interactionControllerWithURL:URL];

	[sharing setInteractionController:interaction];
	[interaction setDelegate:sharing];

	(void) [
		interaction
		presentOpenInMenuFromRect:[[viewController view] bounds]
		inView:[viewController view]
		animated:YES
	];
}

//-----------------------------------------------------------------
+ (BOOL) isChromeInstalled
{
	return [[UIApplication sharedApplication]
		canOpenURL:[NSURL URLWithString:@"googlechrome://"]];
}

//-----------------------------------------------------------------
+ (void) openURLWithChrome:(NSURL*)inputURL
{
	NSString *scheme = inputURL.scheme;

	// Replace the URL Scheme with the Chrome equivalent.
	NSString *chromeScheme = nil;
	if ([scheme isEqualToString:@"http"]) {
	  chromeScheme = @"googlechrome";
	}
	else {
		if ([scheme isEqualToString:@"https"]) {
		  chromeScheme = @"googlechromes";
		}
	}

	// Proceed only if a valid Google Chrome URI Scheme is available.
	if (chromeScheme) {
	  NSString* absoluteString	= [inputURL absoluteString];
	  NSRange rangeForScheme	= [absoluteString rangeOfString:@":"];
	  NSString* urlNoScheme		= [absoluteString substringFromIndex:rangeForScheme.location];
	  NSString* chromeURLString	= [chromeScheme stringByAppendingString:urlNoScheme];
	  NSURL* chromeURL			= [NSURL URLWithString:chromeURLString];

	  // Open the URL with Chrome.
	  [[UIApplication sharedApplication] openURL:chromeURL];
	}
}

//-----------------------------------------------------------------
@end

