//
//  UIBarButtonItem+StandardButtons.m
//  DeskTopper
//
//  Created by David Shea on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIBarButtonItem+StandardButtons.h"
#import "UIApplication+Helpers.h"
#import "UIImageExtras.h"
#import "FileHelper.h"

//-----------------------------------------------------------------
@implementation UIButton (FullScreenButton)

+ (UIButton*) createFullScreenButton:(SEL)action forTarget:(id)target
{
	CGRect bounds = [UIApplication myMainScreenBounds];
	CGSize size   = CGSizeMake(32.0, 32.0);
	CGRect box    = CGRectMake(bounds.size.width - size.width - 8.0, bounds.size.height - size.height - 8.0, size.width, size.height);

	NSString* buttonName = [FileHelper getIconFileNamed:@"no_fullscreen1"];
	UIImage* buttonImage = [UIImage createFromImageFile:buttonName];

	UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.frame = box;

	//	[button setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];

	[button setBackgroundImage:buttonImage forState:UIControlStateNormal];
	
	[button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
	
	return button;
}

//-----------------------------------------------------------------
- (void) repositionFullScreenButton
{
	CGRect bounds = [UIApplication myMainScreenBoundsRotated];
	CGSize size   = CGSizeMake(32.0, 32.0);
	CGRect box    = CGRectMake(bounds.size.width - size.width - 8.0, bounds.size.height - size.height - 8.0, size.width, size.height);

	[self setFrame:box];
}

@end

//-----------------------------------------------------------------
@implementation UIBarButtonItem (tint)

- (void) tint
{
	UIColor* colour;

	if ([UIApplication isIOS7]) {
		colour = [UIColor colorWithWhite:1.0f alpha:0.8f];
	}
	else {
		// Style button like the photo picker
		colour = [UIColor colorWithWhite:0.0f alpha:0.3f];
	}

	[self setTintColor:colour];
}

@end

//-----------------------------------------------------------------
@implementation UIBarButtonItem (AddGesture)

//-----------------------------------------------------------------
- (void) addGesture:(UIGestureRecognizer*)gesture
{
	[[self customView] addGestureRecognizer:gesture];
}

//-----------------------------------------------------------------
- (void) addTapGestureForAction:(SEL)action forTarget:(id)target
{
	UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
	[self addGesture:tap];
}

//-----------------------------------------------------------------
@end


//-----------------------------------------------------------------
@implementation UIBarButtonItem (StandardButtons)

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createButtonWithImageName:(NSString*)imageName
{
	UIImage* image = [UIImage imageFromResource:imageName];
	CGRect frame   = CGRectMake(0, 0, [image size].width,[image size].height);
	
	UIButton* customButton = [[UIButton alloc] initWithFrame:frame];
	[customButton setBackgroundImage:image forState:UIControlStateNormal];
	[customButton setShowsTouchWhenHighlighted:YES];
	
	UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithCustomView:customButton];
	
	return button;
}

//-----------------------------------------------------------------
/*
 + (UIBarButtonItem*) createStandardButtonWithImageName:(NSString*)imageName action:(SEL)action forTarget:(id)target
{
	UIBarButtonItem* button = [
		[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:imageName]
		style:UIBarButtonItemStylePlain target:target action:action
	];
	
	return button;
}
*/
//-----------------------------------------------------------------
+ (UIBarButtonItem*) createButtonWithImageName:(NSString*)imageName action:(SEL)action forTarget:(id)target
{
	UIBarButtonItem* button = [UIBarButtonItem createButtonWithImageName:imageName];
	[button addTapGestureForAction:action forTarget:target];
	return button;
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createButtonWithText:(NSString*)text action:(SEL)action forTarget:(id)target
{
	UIBarButtonItem* button =
	[
		[UIBarButtonItem alloc]
		initWithTitle:text
		style:UIBarButtonItemStyleBordered
		target:target
		action:action
	];
	[button tint];

	return button;
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createDoneButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	UIBarButtonItem* doneButton =
	[
		[UIBarButtonItem alloc]
		initWithTitle:@"Done"
		style:UIBarButtonItemStyleDone
		target:target
		action:(action)
	];

	[doneButton tint];

	return doneButton;
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createCancelButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	UIBarButtonItem* cancelButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Cancel"
		style:UIBarButtonItemStyleDone
		target:target
		action:(action)
	];

	[cancelButton tint];

	return cancelButton;
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createSaveButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	UIBarButtonItem* saveButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Save"
		style:UIBarButtonItemStyleDone
		target:target
		action:(action)
	];

	[saveButton tint];

	return saveButton;
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createNewNoteButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			//createButtonWithImageName:@"/resource/toolbar/0088.png"
			createButtonWithImageName:@"/resource/toolbar/edit.png"
			action:(action)
			forTarget:target
	];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createNewWebButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/globe.png"
			action:(action)
			forTarget:target
		];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createNewImageButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0095.png"
			action:(action)
			forTarget:target
		];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createNewCameraButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0042.png"
			action:(action)
			forTarget:target
		];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createBackButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0281.png"
			action:(action)
			forTarget:target
		];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createForwardButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0282.png"
			action:(action)
			forTarget:target
		];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createFullScreenButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
		createButtonWithImageName:@"/resource/toolbar/0289.png"
		action:(action)
		forTarget:target
	];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createSettingsButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
		createButtonWithImageName:@"/resource/toolbar/0158.png"
		action:(action)
		forTarget:target
	];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createRefreshButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
		createButtonWithImageName:@"/resource/toolbar/0259.png"
		action:(action)
		forTarget:target
	];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createStopButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/stop.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createActionButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0238.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createAddButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/add-item2.png"
			action:(action)
			forTarget:target
	];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createDeleteButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0210.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createOrganiseButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0279.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createDuplicateButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0096.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createInboxButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0283.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createUpButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0284.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createDownButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0283.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createSelectSpaceButton:(SEL)action forTarget:(id)target
{
	DT_ASSERT(action);

	return [UIBarButtonItem
			createButtonWithImageName:@"/resource/toolbar/0032.png"
			action:(action)
			forTarget:target
			];
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createFlexibleSpace
{
	UIBarButtonItem* flexibleSpace = [
		[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil
	];

	return flexibleSpace;
}

//-----------------------------------------------------------------
+ (UIBarButtonItem*) createFixedSpace:(float)width
{
	UIBarButtonItem* fixedSpace = [
		[UIBarButtonItem alloc]
		initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
		target:nil action:nil
	];
	[fixedSpace setWidth:width];

	return fixedSpace;
}

//-----------------------------------------------------------------
@end

