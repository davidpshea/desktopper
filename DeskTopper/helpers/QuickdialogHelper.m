//
//  QuickdialogHelper.m
//  DeskTopper
//
//  Created by David Shea on 20/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <MobileCoreServices/MobileCoreServices.h>

#import "UINavigationController+Extras.h"
#import "QuickdialogHelper.h"
#import "UIImageExtras.h"
#import "DTNavigationController.h"
#import "CustomImagePickerController.h"
#import "UIImagePickerController+Helper.h"

//-----------------------------------------------------------------
@implementation QRootElement (HelperFunctions)

//-----------------------------------------------------------------
+ (QRootElement*) rootWithTitle:(NSString*)title
{
	QRootElement* root	= [[QRootElement alloc] init];
	[root setTitle:title];
	[root setGrouped:YES];

	return root;
}

//-----------------------------------------------------------------
- (QSection*) addNewSection
{
	QSection* section = [[QSection alloc] init];
	[self addSection:section];
	return section;
}

//-----------------------------------------------------------------
- (QSection*) addNewSectionWithTitle:(NSString*)title
{
	QSection* section = [[QSection alloc] init];
	[section setTitle:title];
	[self addSection:section];

	return section;
}

//-----------------------------------------------------------------
@end

//-----------------------------------------------------------------
@implementation QSection (HelperFunctions)

- (QSection*) addNewPageInSectionWithTitle:(NSString*)title
{
	QRootElement* root = [QRootElement rootWithTitle:title];
	QSection* section  = [root addNewSection];

	[self addElement:root];
	
	return section;
}

//-----------------------------------------------------------------
@end

//-----------------------------------------------------------------
@implementation QSection (HeaderImageFunctions)

- (void) setNewHeaderImage:(UIImage*)image
{
	[self setHeaderView:nil];

	// Create a thumbnail view to show
	CGSize thumbnailSize	= CGSizeMake(96, 96);
	UIImage* scaledImage	= [image imageFromImageByScaling:thumbnailSize keepAspectRatio:NO];
    UIImageView* imageView	= [[UIImageView alloc] initWithImage:scaledImage];

	// Make thumbnail appear in centre of view, not stretched
	// and give it 8 pixels space all around
	[imageView setContentMode:UIViewContentModeCenter];
	CGRect bounds = [imageView bounds];
	bounds.size.width += 16;
	bounds.size.height += 16;
	[imageView setBounds:bounds];

//	UIView *aView = [[UIView alloc] initWithFrame:CGRectZero];
//	[aView addSubview:imageView];

//	[self setHeaderView:aView];
	[self setHeaderView:imageView];
}

@end

//-----------------------------------------------------------------
static id globalStyleProvider = nil;

@implementation DTWidgetQuickDialogController

@synthesize styleProvider = _styleProvider;

+ (void) setGlobalStyleProvider:(id)provider
{
	globalStyleProvider = provider;
}

/*- (DTWidgetQuickDialogController*) init
{
	self = [super init];
	return self;
}
*/

- (DTWidgetQuickDialogController*) initWithRoot:(QRootElement*)root
{
	self = [super initWithRoot:root];
	[self setStyleProvider:globalStyleProvider];
	return self;
}

//-----------------------------------------------------------------
- (void) setQuickDialogTableView:(QuickDialogTableView*) aQuickDialogTableView
{
	[super setQuickDialogTableView:aQuickDialogTableView];

	//	[[self quickDialogTableView] setStyleProvider:[self styleProvider]];
}

@end
//-----------------------------------------------------------------

@implementation QRootElement (GetElement)

//-----------------------------------------------------------------
- (NSString*) getStringWithKey:(NSString*)key
{
	LOG_FN();
	id element = [self elementWithKey:key];

//    if ([element isKindOfClass:[QSelectItemElement class]]) {
    if ([element isKindOfClass:[QSelectSection class]]) {
		LOG_FN();
		// Work out which string is selected for QSelectSection items
		NSArray* selected = [element selectedItems];
		if ([selected count] > 0) {
			LOG_FN();
			DT_DEBUG_LOG(@"%@", [selected objectAtIndex:0]);
			return [selected objectAtIndex:0];
		}
	}
    if ([element isKindOfClass:[QRadioItemElement class]]) {
		return [element title];
	}

	// Fallback if nothing else found
	return nil;
}

//-----------------------------------------------------------------
- (BOOL) getBOOLWithKey:(NSString*)key
{
	id element = [self elementWithKey:key];
	return [element boolValue];
}

@end

//-----------------------------------------------------------------
@implementation QuickDialogController (HelperFunctions)

//-----------------------------------------------------------------
- (void) showPhotoPicker:(QElement*)element
{
	// give me a view controller for my photo album
	UIImagePickerController* pickerController = [UIImagePickerController initPhotoAlbumViewController:self];

	// none? - use normal settings
	if (pickerController != nil) {
		// show photo album...
		[self presentViewController:pickerController animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) showCamera
{
	// give me a view controller for my photo album
	UIImagePickerController* pickerController = [UIImagePickerController initCameraViewController:self];

	// none? - use normal settings
	if (pickerController != nil) {
		// show photo album...
		[self presentViewController:pickerController animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) imagePickerController:(UIImagePickerController*)pImagePicker
 didFinishPickingMediaWithInfo:(NSDictionary*)MediaInfo
{
//	DTWidget* widget = [self object];
	DTWidget* widget = [DTWidget getActiveWidget];
	[UIImagePickerController didFinishPickingMediaWithWidget:MediaInfo widget:widget];

	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
-(void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (BOOL) iconIsSelected:(CustomImagePicker*)imagePicker from:(id)sender
{
	DT_DEBUG_LOG(@"icon is selected in view controller");

	UIButton* button = (UIButton*)sender;
	int tag			 = (int)[button tag] + 1;

	DTWidget* widget = [DTWidget getActiveWidget];

	[widget setImageFilePath:nil];
	[widget setSize:CGSizeMake(64, 64)];
	[widget setIconIndex:tag];

	return YES;
}

//-----------------------------------------------------------------
- (void) showIconPicker
{
	CustomImagePicker* iconPicker = [[CustomImagePicker alloc] init];
	[iconPicker setDelegate:self];

	// Initialize image picker
	[iconPicker setTitle:@"Choose Icon"];

	NSArray* icons = [FileHelper getIconFileArray];
	for (NSString* path in icons) {
		UIImage* iconImage = [[UIImage alloc] initWithContentsOfFile:path];
		[iconPicker addImage:iconImage resizeToFit:NO];
	}

	DTNavigationController* navigationController = [[DTNavigationController alloc] initWithRootViewController:iconPicker];

	// Create the enclosing navigation controller, so we get the 'cancel' button
	//	UINavigationController* navigationController = [UINavigationController styledControllerWithRoot:iconPicker];
	[self presentViewController:navigationController animated:YES completion:nil];
}

#if 0
//-----------------------------------------------------------------
- (void) showImageEditor
{
//	DTWidget* widget = [self object];
//	DTWidget* widget = [self widget];
	DTWidget* widget = [DTWidget getActiveWidget];

	AFPhotoEditorController* editorController = [[AFPhotoEditorController alloc] initWithImage:[widget image]];
	[editorController setDelegate:self];

	[self presentViewController:editorController animated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) photoEditor:(AFPhotoEditorController*)editor finishedWithImage:(UIImage*) image
{
//	DTWidget* widget = [self widget];
//	DTWidget* widget = [self object];
	DTWidget* widget = [DTWidget getActiveWidget];

	// Send the correct image to the widget being edited
	NSString* newFilePath = [image saveToUniqueImageFile:nil];
	[widget setImageFilePath:newFilePath withImage:image];

	[editor dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
- (void) photoEditorCanceled:(AFPhotoEditorController*)editor
{
	[editor dismissViewControllerAnimated:YES completion:nil];
}
#endif

//-----------------------------------------------------------------
@end

