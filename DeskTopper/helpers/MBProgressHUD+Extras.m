//
//  MBProgressHUD+Extras.m
//  DeskTopper
//
//  Created by David Shea on 17/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MBProgressHUD+Extras.h"
#import "UIApplication+Helpers.h"
#import "UIImageExtras.h"

//-----------------------------------------------------------------
@interface HUDTimerData : NSObject

@property (nonatomic, strong)	id	target;
@property (nonatomic) 			SEL	action;
@property (nonatomic, strong)	id	data;

@end

//-----------------------------------------------------------------
@implementation HUDTimerData

@synthesize target	= _target;
@synthesize action	= _action;
@synthesize data    = _data;

@end

//-----------------------------------------------------------------
@implementation MBProgressHUD (Extras)

//-----------------------------------------------------------------
+ (MBProgressHUD*) showInformation:(NSString*)message onView:(UIView*)view withDelay:(float)delay
{
	MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];

	[HUD setMode:MBProgressHUDModeText];
	[HUD setLabelText:message];
	[HUD setYOffset:-190];
	[HUD setAnimationDurationShow:0.2];
	[HUD setAnimationDurationHide:1.0];
	[HUD setBlockTouches:NO];
	[HUD setUserInteractionEnabled:NO];
	[HUD setAnimationType:MBProgressHUDAnimationFade];
	[HUD hide:YES afterDelay:delay];
	[HUD setRemoveFromSuperViewOnHide:YES];

	return HUD;
}

//-----------------------------------------------------------------
+ (MBProgressHUD*) showProgressOnView:(UIView*)view
{
	[MBProgressHUD hideAllHUDsForView:view animated:NO];
	
	MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
	
	[HUD setMode:MBProgressHUDModeIndeterminate];
	[HUD setAnimationDurationShow:0.0];
	[HUD setAnimationDurationHide:0.2];
	[HUD setBlockTouches:NO];
	[HUD setUserInteractionEnabled:NO];
	[HUD setAnimationType:MBProgressHUDAnimationFade];
	//	[HUD hide:N afterDelay:0.];
	[HUD setRemoveFromSuperViewOnHide:YES];
	
	return HUD;
}

//-----------------------------------------------------------------
+ (MBProgressHUD*) showInformation:(NSString*)message withDelay:(float)delay
{
	return [MBProgressHUD showInformation:message onView:[[UIApplication myRootViewController] view] withDelay:delay];
}

//-----------------------------------------------------------------
+ (MBProgressHUD*) showInformation:(NSString*)message
{
	return [MBProgressHUD showInformation:message onView:[[UIApplication myRootViewController] view] withDelay:2.0];
}

//-----------------------------------------------------------------
- (void) HUDTimerFire:(NSTimer*)timer
{
	HUDTimerData* timerData = (HUDTimerData*)[timer userInfo];
	
	//	BOOL valid = [timer isValid];
	SEL action = [timerData action];
	id  target = [timerData target];
	id  data   = [timerData data];

	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
	[target performSelector:action withObject:data];
	#pragma clang diagnostic pop
}

//-----------------------------------------------------------------
+ (void) hideAllOnTopWindow
{
	[MBProgressHUD hideAllHUDsForView:[UIApplication myWindow] animated:YES];
}

//-----------------------------------------------------------------
+ (MBProgressHUD*) showProgressWhilePerformingSelector:(SEL)action onTarget:(id)target usingData:(id)data onView:(UIView*)view
{
	[MBProgressHUD hideAllHUDsForView:view animated:NO];
	
	MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
	
	[HUD setMode:MBProgressHUDModeIndeterminate];
	[HUD setAnimationDurationShow:0.0];
	[HUD setAnimationDurationHide:0.2];
	[HUD setBlockTouches:NO];
	[HUD setUserInteractionEnabled:NO];
	[HUD setAnimationType:MBProgressHUDAnimationFade];
	//	[HUD hide:N afterDelay:0.];
	[HUD setRemoveFromSuperViewOnHide:YES];

	HUDTimerData* timerData = [[HUDTimerData alloc] init];
	[timerData setAction:action];
	[timerData setTarget:target];
	[timerData setData:data];

	(void) [
		NSTimer
		scheduledTimerWithTimeInterval:0.005
		target:HUD
		selector:@selector(HUDTimerFire:)
		userInfo:timerData
		repeats:NO
	];

	return HUD;
}

//-----------------------------------------------------------------
+ (MBProgressHUD*) showDoneOnView:(UIView*)view
{
	[MBProgressHUD hideAllHUDsForView:view animated:NO];
	
	MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
	
	[HUD setMode:MBProgressHUDModeText];
	[HUD setLabelText:@"Done"];
	[HUD setAnimationDurationShow:0.1];
	[HUD setAnimationDurationHide:0.4];
	[HUD setBlockTouches:NO];
	[HUD setUserInteractionEnabled:NO];
	[HUD setAnimationType:MBProgressHUDAnimationFade];
	[HUD hide:YES afterDelay:1.0];
	[HUD setRemoveFromSuperViewOnHide:YES];
	
	return HUD;
}

//-----------------------------------------------------------------
+ (MBProgressHUD*) showGesture:(MBProgressHUDGesture)gestureID onView:(UIView*)view
{
	static UIImage* backImage    = nil;
	static UIImage* forwardImage = nil;
	
	[MBProgressHUD hideAllHUDsForView:view animated:NO];
	
	MBProgressHUD* HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];

	UIImage* image = nil;

	switch (gestureID) {

		case navigateBack:
			if (!backImage){
				backImage = [UIImage imageFromResource:@"/resource/toolbar/0281.png"];
			}
			image = backImage;
			break;
			
		case navigateForward:
			if (!forwardImage) {
				forwardImage = [UIImage imageFromResource:@"/resource/toolbar/0282.png"];
			}
			image = forwardImage;
			break;
			
		default:
			break;
	}

	UIImageView* imageView = [[UIImageView alloc] initWithImage:image];

	[HUD setCustomView:imageView];
	[HUD setMode:MBProgressHUDModeCustomView];
	[HUD setYOffset:-60];
	[HUD setAnimationDurationShow:0.1];
	[HUD setAnimationDurationHide:0.2];
	[HUD setBlockTouches:NO];
	[HUD setUserInteractionEnabled:NO];
	[HUD setAnimationType:MBProgressHUDAnimationFade];
	[HUD hide:YES afterDelay:0.7];
	[HUD setRemoveFromSuperViewOnHide:YES];

	return HUD;
}

//-----------------------------------------------------------------
@end

