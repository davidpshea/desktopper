//
//  NSString+Word.m
//  DeskTopper
//
//  Created by David Shea on 20/11/12.
//
//

#import "NSString+Word.h"

//-----------------------------------------------------------------
@implementation NSString (Word)

- (NSRange) rangeOfWordInRange:(NSRange)range
{
	// Range from cursor to end of string
	NSRange toEnd     = {range.location, [self length] - range.location};
	// Range from start of string to cursor
	NSRange fromStart = {0, range.location};

	// Find first whitespace character going from cursor to end of string
	NSRange endRange	= [self rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] options:0 range:toEnd];
	// Find first whitespace characher going from cursor to start of string
	NSRange startRange	= [self rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] options:NSBackwardsSearch range:fromStart];

	if (startRange.location == NSNotFound) {
		startRange.location = 0;
	}
	if (endRange.location == NSNotFound) {
		endRange.location = [self length];
	}

	// start is pointing at the first whitespace character - so move it back
	// so it points at the first character of the word
	if (startRange.location > 0) {
		startRange.location++;
	}

	// Calculate range for word
	NSRange wordRange = {startRange.location, endRange.location - startRange.location};

	return wordRange;
}

@end
