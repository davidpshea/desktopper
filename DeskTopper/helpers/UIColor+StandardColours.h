//
//  UIColor+StandardColours.h
//  DeskTopper
//
//  Created by David Shea on 16/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (StandardColours)

+ (NSArray*) getStandardColourNames;
+ (NSArray*) getStandardColours;

- (NSString*) HTMLName;

@end
