//
//  UIColor+StandardColours.m
//  DeskTopper
//
//  Created by David Shea on 16/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIColor+StandardColours.h"
#import "DTColor+HTML.h"

#define HTML_STANDARD_COLOURS	1

@implementation UIColor (StandardColours)

//-----------------------------------------------------------------
static NSArray* colourNames		= nil;
static NSArray* standardColours	= nil;

//-----------------------------------------------------------------
+ (NSArray*) getStandardColourNames
{
	if (!colourNames) {
		colourNames = [NSArray arrayWithObjects:

		// Basic HTML colour names
		@"White",
		@"Silver",
		@"Gray",
		@"Black",
		@"Red",
		@"Maroon",
		@"Yellow",
		@"Olive",
		@"Lime",
		@"Green",
		@"Aqua",
		@"Teal",
		@"Blue",
		@"Navy",
		@"Fuchsia",
		@"Purple",

		// Additional useful colours
		@"Brown",
		@"Orange",
		@"Pink",
		@"DarkGray",

		// Red colors
		@"IndianRed",
		@"LightCoral",
		@"Salmon",
		@"DarkSalmon",
		@"LightSalmon",
		@"Crimson",
		@"FireBrick",
		@"DarkRed",

		// Pink colors
		@"LightPink",
		@"HotPink",
		@"DeepPink",
		@"MediumVioletRed",
		@"PaleVioletRed",

		// Orange colors
		@"LightSalmon",
		@"Coral",
		@"Tomato",
		@"OrangeRed",
		@"DarkOrange",

		// Yellow colors
		@"Gold",
		@"LightYellow",
					   //		@"LemonChiffon",
//		@"LightGoldenrodYellow",
//		@"PapayaWhip",
//		@"Moccasin",
		@"PeachPuff",
		@"PaleGoldenrod",
		@"Khaki",
		@"DarkKhaki",

		// Purple colors
		@"Lavender",
		@"Thistle",
		@"Plum",
		@"Violet",
		@"Orchid",
		@"Magenta",
		@"MediumOrchid",
		@"MediumPurple",
		@"BlueViolet",
		@"DarkViolet",
		@"DarkOrchid",
		@"DarkMagenta",
		@"Indigo",
		@"DarkSlateBlue",
		@"SlateBlue",
//		@"MediumSlateBlue",

		// Green colors
		@"GreenYellow",
		@"Chartreuse",
		@"LawnGreen",
		@"LimeGreen",
		@"PaleGreen",
		@"LightGreen",
		@"MediumSpringGreen",
		@"SpringGreen",
		@"MediumSeaGreen",
		@"SeaGreen",
		@"ForestGreen",
		@"DarkGreen",
		@"YellowGreen",
		@"OliveDrab",
		@"DarkOliveGreen",
		@"MediumAquamarine",
		@"DarkSeaGreen",
		@"LightSeaGreen",
		@"DarkCyan",
		// Blue/Cyan colors
		@"Cyan",
		@"LightCyan",
		@"PaleTurquoise",
		@"Aquamarine",
		@"Turquoise",
//		@"MediumTurquoise",
		@"DarkTurquoise",
		@"CadetBlue",
		@"SteelBlue",
//		@"LightSteelBlue",
		@"PowderBlue",
//		@"LightBlue",
		@"SkyBlue",
//		@"LightSkyBlue",
		@"DeepSkyBlue",
		@"DodgerBlue",
//		@"CornflowerBlue",
		@"RoyalBlue",
		@"MediumBlue",
		@"DarkBlue",
		@"MidnightBlue",

		// Brown colors
//		@"Cornsilk",
		@"BlanchedAlmond",
//		@"Bisque",
		@"NavajoWhite",
//		@"Wheat",
		@"BurlyWood",
//		@"Tan",
		@"RosyBrown",
		@"SandyBrown",
		@"Goldenrod",
		@"DarkGoldenrod",
		@"Peru",
		@"Chocolate",
		@"SaddleBrown",
		@"Sienna",
		// White colors
//		@"Snow",
//		@"Honeydew",
//		@"MintCream",
//		@"Azure",
//		@"AliceBlue",
//		@"GhostWhite",
//		@"WhiteSmoke",
//		@"Seashell",
//		@"Beige",
//		@"OldLace",
//		@"FloralWhite",
//		@"Ivory",
//		@"AntiqueWhite",
//		@"Linen",
//		@"LavenderBlush",
//		@"MistyRose",

		// Gray colors
		@"Gainsboro",
		@"LightGray",
		@"DimGray",
//		@"LightSlateGray",
		@"SlateGray",
		@"DarkSlateGray",
		nil
		];
	}

	return colourNames;
}

//-----------------------------------------------------------------
#ifdef HTML_STANDARD_COLOURS
+ (NSArray*) getStandardColours
{
	if (!standardColours) {

		NSArray*		standardColourNames	= [UIColor getStandardColourNames];
		NSMutableArray* colourArray         = [[NSMutableArray alloc] initWithCapacity:[colourNames count]];

		for (NSString* colourName in standardColourNames) {
			[colourArray addObject:[UIColor colorWithHTMLName:colourName]];
		}

		standardColours = (NSArray*)colourArray;
	}

	return standardColours;
}
#else
//-----------------------------------------------------------------
+ (NSArray*) getStandardColours
{
	if (!standardColours) {
		NSMutableArray* colourArray = [[NSMutableArray alloc] init];

		// Add some standard colours at start of list
		[colourArray addObject:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]];
		[colourArray addObject:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
		[colourArray addObject:[UIColor redColor]];
		[colourArray addObject:[UIColor blueColor]];
		[colourArray addObject:[UIColor greenColor]];
		[colourArray addObject:[UIColor yellowColor]];
		[colourArray addObject:[UIColor brownColor]];
		[colourArray addObject:[UIColor cyanColor]];
		[colourArray addObject:[UIColor magentaColor]];
		[colourArray addObject:[UIColor orangeColor]];
		[colourArray addObject:[UIColor purpleColor]];

		// and now some tints....
		for (int red = 16; red <= 256; red += 48) {
			for (int green = 16; green <= 256; green += 48) {
				for (int blue = 16; blue <= 256; blue += 48) {

					[colourArray addObject:[UIColor colorWithRed:((float)red / 256.0)
						green:((float)green / 256.0) blue:((float)blue / 256.0) alpha:1.0]];
				}
			}
		}

		standardColours = (NSArray*)colourArray;
	}

	return standardColours;
}
#endif

//-----------------------------------------------------------------
- (NSString*) HTMLName
{
	NSArray* standardColourNames = [UIColor getStandardColourNames];
	NSArray* colourArray         = [UIColor getStandardColours];

	int i = 0;
	for (UIColor* color in colourArray) {

		if ([self isEqual:color]) {
			return [standardColourNames objectAtIndex:i];
		}
		i++;
	}
	return nil;
}

//-----------------------------------------------------------------

@end

