//
//  FileHelper.h
//  DeskTopper
//
//  Created by David Shea on 12/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileHelper : NSObject

+ (NSString*)getDocumentPath;

+ (NSArray*) getFilesInDocuments;

/**
 * Return the full path for a file in the document directory.
 *
 * @param  pFileName the NSString containing the filename.
 *
 * @return A NSString containing the full path to the document
 *  	   directory containing the filename.
*/
+ (NSString*)getPathForFileInDocuments:(NSString*)pFileName;

/**
 * Return the full path for a file in the bundle directory.
 *
 * @param  fullFileName the NSString containing the filename.
 *
 * @return A NSString containing the full path to the main
 *  	   bundle directory containing the filename.
*/
+ (NSString*) getPathForFileInBundle:(NSString*)fullFileName;

/**
 * Given a name mask, return a unique filename based around the
 * time and date.
 *
 * @param  pNameMask the NSString that contains the mask. The
 *  				 mask is a string, with %@ to indicate where
 *  				 the time and date will go. E.g. "pic%@.png"
 *  				 will expand to "pic20120421-235940.png"
 *
 * @return A NSString containing the new filename.
*/
+ (NSString*) getUniqueFileNameFromTime:(NSString*) pNameMask;

+ (NSString*) getDatabasePath;

+ (void) createDirectory:	(NSString*)directoryName;
+ (void) deleteFile:		(NSString*)pFilePath;
+ (void) copyFile:			(NSString*)from toPath:(NSString*)to;
+ (BOOL) doesFileExist:		(NSString*)pFilePath;

+ (NSArray*)	getIconFileArray;
+ (NSString*)	getIconFileNamed:(NSString*)filename;
+ (NSString*)	getToolbarIconFileNamed:(NSString*)filename;
+ (NSString*)	getCSSFileNamed:(NSString*)filename;

+ (void) sendFileToBin:(NSString*)filename;

//+ (void) writeStringToFile:(NSString*)filePath content:(NSString*)content;
+ (BOOL) writeStringToFile:(NSString*)filePath content:(NSString*)content error:(NSError**)error;
+ (NSString*) readStringFromFile:(NSString*)filePath error:(NSError**)error;
+ (NSString*) readStringFromURL:(NSURL*)URL error:(NSError**)error;

@end

