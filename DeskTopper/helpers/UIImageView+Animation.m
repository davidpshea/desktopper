//
//  UIImageView+Animation.m
//  DESKTOPPER
//
//  Created by David Shea on 25/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImageView+Animation.h"

@implementation UIImageView (Animation)

//-----------------------------------------------------------------
- (void) hideWithAnimation
{
	if (! [self isHidden]) {
		[
			UIView
			animateWithDuration:0.3
			animations:^{
				[self setAlpha:0.0];
			}
			completion:^(BOOL didFinish) {
				[self setAlpha:0.0];
				[self setHidden:YES];
			}
		];
	}
}

//-----------------------------------------------------------------
- (void) showWithAnimation
{
	if ([self isHidden]) {

		[self setAlpha:0.0];
		[self setHidden:NO];
		[
			UIView
			animateWithDuration:0.3
			animations:^{
				[self setAlpha:1.0];
			}
			completion:^(BOOL didFinish) {
				[self setAlpha:1.0];
			}
		];
	}
}

//-----------------------------------------------------------------
@end
