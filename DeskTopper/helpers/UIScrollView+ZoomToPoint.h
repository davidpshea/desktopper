//
//  UIScrollView+ZoomToPoint.h
//  DeskTopper
//
//  Created by David Shea on 20/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (ZoomToPoint)

- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)scale animated:(BOOL)animated;

- (CGPoint) getScreenCentre;
- (CGRect)  getVisibleRect;

@end
