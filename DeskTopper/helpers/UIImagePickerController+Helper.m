//
//  UIImagePickerController+Helper.m
//  DeskTopper
//
//  Created by David Shea on 27/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DTWidget.h"
#import "DTPictureWidget.h"
#import "UIImageExtras.h"
#import "SVProgressHUD.h"
#import "UIImagePickerController+Helper.h"

//-----------------------------------------------------------------
// This is the size for the full-size versions from the camera
//static const float hiResScaledPhotoWidth  = 800;
//static const float scaledPhotoHeight 	= (480/2);

// This is the size for the thumbnail versions on the desktop
//static const float scaledPhotoWidth  = (320/2);
//static const float scaledPhotoHeight = (480/2);

//-----------------------------------------------------------------
@implementation UIImagePickerController (Helper)

//-----------------------------------------------------------------
+ (UIImagePickerController*) initCameraViewController:(id<UINavigationControllerDelegate, UIImagePickerControllerDelegate>)delegate
{
	// Need a camera...
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {

		UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];

		[imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
		[imagePicker setAllowsEditing:NO];
		[imagePicker setDelegate:delegate];

		return imagePicker;
	}

	return nil;
}

//-----------------------------------------------------------------
+ (UIImagePickerController*) initPhotoAlbumViewController:(id<UINavigationControllerDelegate, UIImagePickerControllerDelegate>)delegate
{
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {

		UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];

		[imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
		[imagePicker setAllowsEditing:NO];
		[imagePicker setDelegate:delegate];

		return imagePicker;
	}

	return nil;
}

//-----------------------------------------------------------------
+ (void) didFinishPickingMediaWithWidget:(NSDictionary*)MediaInfo widget:(DTWidget*)widget
{
	// Make sure we have an image...
	NSString* mediaType = [MediaInfo objectForKey: UIImagePickerControllerMediaType];

	if ([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
		UIImage* imageToUse = (UIImage*) [MediaInfo objectForKey:UIImagePickerControllerOriginalImage];

		// ensure hi-res version is the right way round
		CGSize hiResScaledSize = [imageToUse getSizeToFitIntoSize:[UIImage getScaledPhotoSize]];
//		CGSize hiResScaledSize	= [imageToUse getSizeFromNewWidth:hiResScaledPhotoWidth];
		UIImage* hiResImage 	= [imageToUse imageFromImageByScaling:hiResScaledSize keepAspectRatio:YES];

		// Save the large image straight from the camera/library
		[widget setHighResolutionImageBackground:hiResImage];

		// Scale the camera/photo image to something a bit more usable for the desktop
		CGSize scaledSize = [hiResImage getSizeToFitIntoSize:[UIImage getThumbnailPhotoSize]];

		
//		CGSize scaledSize		 = [hiResImage getSizeFromNewWidth:scaledPhotoWidth];
		UIImage* scaledImage	 = [hiResImage imageFromImageByScaling:scaledSize keepAspectRatio:YES];
		NSString* scaledFilePath = [scaledImage saveToUniqueImageFile:nil];

		// Send the correct image to the widget being edited
		[widget setImageFilePath:scaledFilePath withImage:scaledImage];
		[widget setSize:scaledSize];
	}
}

//-----------------------------------------------------------------

@end

