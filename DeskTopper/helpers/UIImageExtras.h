//#import "DTAttributedTextView.h"

//-----------------------------------------------------------------
#if 0
@interface DTAttributedTextViewDelegate : NSObject <DTAttributedTextContentViewDelegate>

@property (nonatomic, strong)	id	linkTarget;

- (DTAttributedTextViewDelegate*) initWithViewController:(id)viewController;
@end
#endif

//-----------------------------------------------------------------
@interface UIImage (Extras)

+ (UIImage*) imageFromResource:(NSString*)imagePath;

+ (CGSize)		getScaledPhotoSize;
+ (CGSize)		getThumbnailPhotoSize;
+ (CGSize)		getStandardPopoverSize;

+ (NSString*)	getDefaultImageFileExtension;
+ (NSString*)	addDefaultImageFileExtension:(NSString*)filePath;

+ (UIImage*) createFromText:(NSString*)text intoSize:(CGSize)box;
/*+ (UIImage*) createFromHTML:(NSString*)HTMLtext
				intoSize:(CGSize)box
				backgroundColour:(UIColor*)backgroundColour
				textColour:(UIColor*)textColour
				linkColour:(UIColor*)linkColour
				scale:(float)scale
				fontName:(NSString*)fontName;
*/
+ (UIImage*) createFromView:(UIView*)view;
+ (UIImage*) createFromViewWithScale:(UIView*)view scaledBy:(float)scale;
+ (UIImage*) createFromImageFile:(NSString*)filePath;

- (UIImage*) maskImageWithMask:(UIImage*)maskImage;
- (UIImage*) maskImageWithArea:(CGRect)area;

+ (UIImage*) createColouredRectangle:(CGSize)size colour:(UIColor*)colour;
+ (UIImage*) createColouredRectangle:(CGSize)size colour:(UIColor*)colour borderColour:(UIColor*)borderColour;
+ (UIImage*) createColouredRectangleWithContrastingBorder:(CGSize)size colour:(UIColor*)colour;

- (NSString*)	saveToUniqueImageFile:(NSString*)filenameMask;
- (BOOL)		saveToImageFile:(NSString*)filePath;
- (void)		saveToCameraRoll;
- (UIImage*)	imageFromImageByScaling:(CGSize)targetSize keepAspectRatio:(BOOL)keepAspectRatio;
- (float)		getAspectRatio;
- (CGSize)		getSizeFromNewWidth:(float)newWidth;
- (CGSize)		getSizeToFitIntoSize:(CGSize)boundingSize;
- (UIImage*)    greyScaleImage;


@end

//-----------------------------------------------------------------
@interface UIImageView (Extras)

- (void)		setScale:(CGFloat)scale;

@end

//-----------------------------------------------------------------

