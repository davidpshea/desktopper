//
//  NSString+Word.h
//  DeskTopper
//
//  Created by David Shea on 20/11/12.
//
//

#import <Foundation/Foundation.h>

//-----------------------------------------------------------------
@interface NSString (Word)

- (NSRange) rangeOfWordInRange:(NSRange)range;

//-----------------------------------------------------------------
@end

