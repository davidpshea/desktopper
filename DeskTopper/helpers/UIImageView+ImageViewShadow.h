//
//  UIImageView+ImageViewShadow.h
//  DeskTopper
//
//  Created by David Shea on 20/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

@interface UIImageView (ImageViewShadow)

-(void) addImageShadow:(CGFloat)shadowSize;
-(void) removeImageShadow;

@end

