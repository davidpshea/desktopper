//
//  QImageElement.m
//  DeskTopper
//
//  Created by David Shea on 12/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QImageElement.h"
#import "UIImageExtras.h"
#import "DTWidget.h"
#import "UIApplication+Helpers.h"

#if 0

const float topBorderDefault	= 3.0f;
const float bottomBorderDefault	= 3.0f;

@implementation QImageElement

//-----------------------------------------------------------------
@synthesize imageView		 = _imageView;
@synthesize cellView		 = _cellView;
@synthesize backgroundColour = _backgroundColour;
@synthesize topBorder		 = _topBorder;
@synthesize bottomBorder	 = _bottomBorder;


//-----------------------------------------------------------------
+ (void) addThumbnailElementToSection:(QSection*)section forWidget:(DTWidget*)widget
{
/*	CGRect frame = [UIApplication myMainScreenBounds];
	frame.size.width  -= 64;
	frame.size.height = 64;
	frame.origin.x += 16;
*/
	CGRect frame = CGRectMake(32, 3, 96, 96);
//	CGRect frame = [_imageView bounds];
	QImageElement* imageElement = [[QImageElement alloc] initWithFrame:frame];

	[imageElement setTopBorder:3.0];
	[imageElement setBottomBorder:3.0];
	[imageElement setBackgroundColour:[UIColor blackColor]];
	[imageElement setImage:[widget image]];
	[imageElement setKey:@"thumbnail"];

	[section addElement:imageElement];
}

//-----------------------------------------------------------------
- (QImageElement*) initWithFrame:(CGRect)frame
{
	self = [super init];

	_imageView = [[UIImageView alloc] initWithFrame:frame];
	[_imageView setTag:1];
	[_imageView setContentMode:UIViewContentModeCenter];

	return self;
}

//-----------------------------------------------------------------
- (UITableViewCell *)getCellForTableView:(QuickDialogTableView *)tableView controller:(QuickDialogController *)controller
{
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"QuickformImage"]];
	if (cell == nil){
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"QuickformImage"];

		[cell setBackgroundColor:[self backgroundColour]];
	}

	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];

	// Save the root view used by this cell
	UIView* contentView = [cell contentView];
	UIView* subView		= [contentView viewWithTag:1];

	[self setCellView:contentView];

	if (! subView) {
		[self setCellView:contentView];
		[contentView addSubview:_imageView];
		[contentView setNeedsDisplay];
	}

    return cell;
}

//-----------------------------------------------------------------
- (void) setImage:(UIImage*)image
{
	UIImageView* imageView = [self imageView];

	if (!imageView) {
		return;
	}

	CGRect frame		 = [imageView frame];
	CGSize size			 = CGSizeMake(frame.size.width, frame.size.height - 4);
	UIImage* scaledImage = [image imageFromImageByScaling:size keepAspectRatio:YES];

	[imageView setImage:scaledImage];
	[imageView setNeedsDisplay];

	if ([self cellView]) {
		[[self cellView] setNeedsDisplay];
	}
}

//-----------------------------------------------------------------
- (CGFloat)getRowHeightForTableView:(QuickDialogTableView *)tableView;
{
	CGRect frame = [[self imageView] frame];
	return  frame.size.height + [self topBorder] + [self bottomBorder];
}

//-----------------------------------------------------------------
@end

#endif

