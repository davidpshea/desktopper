//
//  UIImageView+ImageViewShadow.m
//  DeskTopper
//
//  Created by David Shea on 20/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImageView+ImageViewShadow.h"

@implementation UIImageView (ImageViewShadow)

//-----------------------------------------------------------------
-(void) removeImageShadow
{
    CALayer* layer = [self layer];
	[layer setShadowOffset:CGSizeMake(0, 0)];
    [layer setShadowColor:[UIColor clearColor].CGColor];
    [layer setShadowRadius:0.0];
    [layer setShadowOpacity:0.0f];
	[layer setMasksToBounds:NO];
	[layer setShadowPath:nil];
}

//-----------------------------------------------------------------
-(void) addImageShadow:(CGFloat)shadowSize
{
	if (shadowSize <= 0.0) {
		[self removeImageShadow];
	}
	else {
		CALayer* layer = [self layer];
		[layer setShadowOffset:CGSizeMake(shadowSize, shadowSize)];
		[layer setShadowColor:[UIColor blackColor].CGColor];
		[layer setShadowRadius:shadowSize];
		[layer setShadowOpacity:1.0f];
		[layer setMasksToBounds:NO];

		UIBezierPath* path = [UIBezierPath bezierPathWithRect:[self bounds]];
		[layer setShadowPath:[path CGPath]];
	}
}


//-----------------------------------------------------------------
@end

