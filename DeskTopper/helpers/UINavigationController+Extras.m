//
//  UINavigationController+Extras.m
//  DeskTopper
//
//  Created by David Shea on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuickdialogHelper.h"
#import "DTWidget.h"
#import "UIImageExtras.h"
#import "DTWidgetControl.h"
#import "CustomImagePickerController.h"
#import "UIApplication+Helpers.h"
#import "UINavigationController+Extras.h"

//-----------------------------------------------------------------
@implementation UINavigationController (Extras)

+ (UINavigationController*) styledControllerWithRoot:(UIViewController*)rootViewController
{
	UINavigationController* navigationController =
	[[UINavigationController alloc] initWithRootViewController:rootViewController];
	// Customise
	[navigationController style];

	return navigationController;
}

//-----------------------------------------------------------------
- (void) style
{
	// Customise navigation bar - style like photo picker
	UINavigationBar* bar = [self navigationBar];
	[bar setBarStyle:UIBarStyleBlack];
	//	[bar setTranslucent:YES];
}

//-----------------------------------------------------------------
- (void) addDoneButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	DT_ASSERT(action);

	UINavigationBar* bar = [self navigationBar];

	// Add a 'done' button to the root left-side edge
    UIBarButtonItem* doneButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Done"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action)
	];
	if (! [UIApplication isIOS7]) {
		// Style button like the photo picker
		UIColor* colour = [UIColor colorWithWhite:0.0f alpha:0.3f];
		[doneButton setTintColor:colour];
	}

	// give the parent navigation controller a done button
	UINavigationItem* item = [bar topItem];
	[item setLeftBarButtonItem:doneButton];
}

//-----------------------------------------------------------------
- (void) addCancelButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	DT_ASSERT(action);

	UINavigationBar* bar = [self navigationBar];

	// Add a 'cancel' button to the root right-side edge
	UIBarButtonItem* cancelButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Cancel"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action)
	];
	if (! [UIApplication isIOS7]) {
		// Style button like the photo picker
		UIColor* colour = [UIColor colorWithWhite:0.0f alpha:0.3f];
		[cancelButton setTintColor:colour];
	}

	// give the parent navigation controller a done button
	UINavigationItem* item = [bar topItem];
	[item setRightBarButtonItem:cancelButton];
}

//-----------------------------------------------------------------
- (void) addEditButton:(SEL)action forTarget:(id)buttonTarget
{
	LOG_FN();
	DT_ASSERT(action);

	UINavigationBar* bar = [self navigationBar];

	// Add a 'cancel' button to the root right-side edge
	UIBarButtonItem* editButton = [
		[UIBarButtonItem alloc]
		initWithTitle:@"Edit"
		style:UIBarButtonItemStyleDone
		target:buttonTarget
		action:(action)
	];
	if (! [UIApplication isIOS7]) {
		// Style button like the photo picker
		UIColor* colour = [UIColor colorWithWhite:0.0f alpha:0.3f];
		[editButton setTintColor:colour];
	}

	// give the parent navigation controller a done button
	UINavigationItem* item = [bar topItem];
	[item setRightBarButtonItem:editButton];
}

//-----------------------------------------------------------------
- (void) addDefaultButtons:(SEL)action forTarget:(id)buttonTarget
{
	[self addCancelButton:action forTarget:buttonTarget];
	[self addDoneButton:action   forTarget:buttonTarget];
}

//-----------------------------------------------------------------
@end

