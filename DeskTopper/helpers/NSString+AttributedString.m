//
//  NSString+AttributedString.m
//  DeskTopper
//
//  Created by David Shea on 26/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+AttributedString.h"
#import "NSString+Markdown.h"
//#import "DTCSSStylesheet.h"
#import "FileHelper.h"

#if 0

@implementation NSString (AttributedString)

//-----------------------------------------------------------------
+ (NSMutableDictionary*) getDefaultAttributedStringOptions
{
	NSMutableDictionary* options = [
		NSMutableDictionary dictionaryWithObjectsAndKeys:

		[NSNumber numberWithFloat:1.5], 	NSTextSizeMultiplierDocumentOption,
		@"Times New Roman",					DTDefaultFontFamily,
		@"purple",							DTDefaultLinkColor,
		[NSNumber numberWithBool:YES],		DTPreserveNewlinesAttribute,
		[NSString getDefaultStyleSheet],	DTDefaultStyleSheet,
		nil
	];

	return options;
}

//-----------------------------------------------------------------
static BOOL				hasLookedForStyleSheet	= NO;
static DTCSSStylesheet* myStyleSheet			= nil;
static NSString*		defaultCSSFileName		= @"default.css";

+ (DTCSSStylesheet*) getDefaultStyleSheet
{
	if (myStyleSheet || hasLookedForStyleSheet) {
		return myStyleSheet;
	}

	hasLookedForStyleSheet = YES;

	NSString* filePath = [FileHelper getPathForFileInDocuments:defaultCSSFileName];

	if (! [FileHelper doesFileExist:filePath]) {
		return nil;
	}

	NSString* CSS = [FileHelper readStringFromFile:filePath error:nil];

	myStyleSheet = [[DTCSSStylesheet alloc] initWithStyleBlock:CSS];

	return myStyleSheet;
}

//-----------------------------------------------------------------
- (NSAttributedString*) createAttributedStringFromHTMLStringWithOptions:(NSDictionary*)options
{
	if (options == nil) {
		options = [NSString getDefaultAttributedStringOptions];
	}

	NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];

	NSAttributedString* string = [
		[NSAttributedString alloc]
		initWithHTMLData:data
		options:options
		documentAttributes:NULL
	];

	return string;
}

//-----------------------------------------------------------------
- (NSAttributedString*) createAttributedStringFromMarkdownStringWithOptions:(NSDictionary*)options
{
	NSString* HTML = [self createHTMLFromMarkdown];

	return [HTML createAttributedStringFromHTMLStringWithOptions:options];
}

//-----------------------------------------------------------------
@end
#endif

