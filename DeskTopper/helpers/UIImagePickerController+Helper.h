//
//  UIImagePickerController+Helper.h
//  DeskTopper
//
//  Created by David Shea on 27/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "UIImagePickerController+Helper.h"

@class DTWidget;

@interface UIImagePickerController (Helper)

+ (UIImagePickerController*) initCameraViewController:     (id<UINavigationControllerDelegate, UIImagePickerControllerDelegate>)delegate;
+ (UIImagePickerController*) initPhotoAlbumViewController: (id<UINavigationControllerDelegate, UIImagePickerControllerDelegate>)delegate;

+ (void) didFinishPickingMediaWithWidget:(NSDictionary*)MediaInfo widget:(DTWidget*)widget;

@end

