//
//  UIImageView+ImageViewBorder.h
//  DeskTopper
//
//  Created by David Shea on 20/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"

@interface UIImageView (ImageViewBorder)

-(void) configureImageViewBorder:(CGFloat)borderWidth;
-(void) configureImageViewBorder:(CGFloat)borderWidth withColour:(UIColor*)colour;
-(void) setImage:(UIImage*)image withBorder:(CGFloat)borderWidth andShadow:(CGFloat)shadowSize;

@end

