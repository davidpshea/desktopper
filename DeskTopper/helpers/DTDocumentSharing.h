//
//  DTDocumentSharing.h
//  DeskTopper
//
//  Created by David Shea on 31/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface DTDocumentSharing : NSObject <
MFMailComposeViewControllerDelegate,
UIDocumentInteractionControllerDelegate
>

@property (strong, nonatomic) DTDocumentSharing* strongSelf;
@property (nonatomic)		  BOOL				 ownsFile;
@property (nonatomic, strong) UIDocumentInteractionController* interactionController;

+ (void) emailSupport:	(NSString*)subject body:(NSString*)body;
+ (void) sendEmail:		(NSString*)subject body:(NSString*)body isHTML:(BOOL)isHTML imageFileName:(NSString*)fileName;
+ (void) exportString:	(NSString*)text as:(NSString*)uti;
+ (void) exportPhoto:	(NSString*)filePath;

+ (BOOL) isChromeInstalled;
+ (void) openURLWithChrome:(NSURL*)inputURL;

@end

