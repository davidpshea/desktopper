//
//  UIPopoverController+Stack.m
//  DeskTopper
//
//  Created by David Shea on 12/04/13.
//
//

#import "UIApplication+Helpers.h"
#import "UIPopoverController+Stack.h"

//-----------------------------------------------------------------
@implementation UIPopoverController (Stack)

static NSMutableArray*	viewStack		 = nil;
static id				activeController = nil;

//-----------------------------------------------------------------
- (void) pushViewController:(id)viewController
{
	if ([UIApplication isPad]) {

		if (! viewStack) {
			viewStack = [[NSMutableArray alloc] init];
		}

		// Push current view controller
		[viewStack addObject:[self contentViewController]];

		// Set new view controller
		[self setContentViewController:viewController];
	}
}

//-----------------------------------------------------------------
- (id) popViewController
{
	if ([UIApplication isPhone]) {
		return nil;
	}

	if ( (!viewStack) || ([viewStack count] == 0) ) {
		return nil;
	}

	id lastController = [viewStack lastObject];
	[viewStack removeLastObject];

	[self setContentViewController:lastController];

	return lastController;
}

//-----------------------------------------------------------------
+ (BOOL) dismissActiveController
{
	BOOL wasActive = NO;

	if (activeController) {

		wasActive = YES;

		// Dismiss an action sheet...
		if ([activeController isKindOfClass:[UIActionSheet class]]) {
			[activeController dismissWithClickedButtonIndex:-1 animated:YES];
		}
		else {
			// dismiss a regular popover
			[activeController dismissPopoverAnimated:YES];
		}

		activeController = nil;
	}

	return wasActive;
}

//-----------------------------------------------------------------
+ (id) getActiveController
{
	return activeController;
}

//-----------------------------------------------------------------
+ (UIPopoverController*) getActivePopoverController
{
	return (UIPopoverController*)activeController;
}

//-----------------------------------------------------------------
+ (void) setActiveController:(id)newController
{
	// We always set if nil....
	if (newController == nil) {
		activeController = nil;
		[viewStack removeAllObjects];
	}
	else {
		if ([UIApplication isPad]) {
			activeController = newController;
		}
	}
}

//-----------------------------------------------------------------

@end
