//
//  NSString+Markdown.m
//  DeskTopper
//
//  Created by David Shea on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSString+Markdown.h"

#import "markdown.h"
#import "html.h"

//-----------------------------------------------------------------
@implementation NSString (Markdown)

//-----------------------------------------------------------------
- (NSString*) createHTMLFromMarkdown
{
	const char* prose	= [self UTF8String];
	int length			= (int)[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding] + 1;

	struct buf* ib;
	struct buf* ob;

	ib = bufnew(length);
	bufgrow(ib, length);
	memcpy(ib->data, prose, length);
	ib->size = length;

	ob = bufnew(64);

	struct sd_callbacks		callbacks;
	struct html_renderopt	options;
	struct sd_markdown*		markdown;

	sdhtml_renderer(&callbacks, &options, HTML_HARD_WRAP);
//	sdhtml_renderer(&callbacks, &options, 0);
	markdown = sd_markdown_new(
		MKDEXT_NO_INTRA_EMPHASIS |
		MKDEXT_TABLES |
		MKDEXT_FENCED_CODE | MKDEXT_STRIKETHROUGH | MKDEXT_AUTOLINK |
		MKDEXT_LAX_SPACING | MKDEXT_SUPERSCRIPT,
		16, &callbacks, &options
	);

	sd_markdown_render(ob, ib->data, ib->size, markdown);
	sd_markdown_free(markdown);

	NSString* newHTML = [NSString stringWithUTF8String:(const char*)(ob->data)];

	bufrelease(ib);
	bufrelease(ob);

	return newHTML;
}

//-----------------------------------------------------------------
@end

