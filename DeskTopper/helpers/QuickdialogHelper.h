//
//  QuickdialogHelper.h
//  DeskTopper
//
//  Created by David Shea on 20/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "CustomImagePickerController.h"


//-----------------------------------------------------------------
@interface QRootElement (HelperFunctions)

+ (QRootElement*)	rootWithTitle:(NSString*)title;
- (QSection*)		addNewSection;
- (QSection*)		addNewSectionWithTitle:(NSString*)title;

@end

//-----------------------------------------------------------------
@interface QRootElement (GetElement)

- (NSString*)	getStringWithKey:	(NSString*)key;
- (BOOL)		getBOOLWithKey:		(NSString*)key;

@end

//-----------------------------------------------------------------
@interface QSection (HelperFunctions)

- (QSection*) addNewPageInSectionWithTitle:(NSString*)title;

@end

//-----------------------------------------------------------------
@interface QSection (HeaderImageFunctions)

- (void) setNewHeaderImage:(UIImage*)image;

@end

//-----------------------------------------------------------------
@interface DTWidgetQuickDialogController: QuickDialogController

@property (nonatomic, strong) id styleProvider;

+ (void) setGlobalStyleProvider:(id)provider;

@end

//-----------------------------------------------------------------
//@class DTWidget;

@interface QuickDialogController (HelperFunctions) <
	UINavigationControllerDelegate,
	UIImagePickerControllerDelegate,
	CustomImagePickerDelegate
>

//@property (nonatomic, weak) DTWidget* widget;

//- (void) viewDidLoad;
//- (void) doneButtonPressed;
//- (void) cancelButtonPressed;

- (void) showPhotoPicker:(QElement*)element;
- (void) showCamera;
//- (void) showImageEditor;
- (void) showIconPicker;
//- (void) displayWebPage:(QElement*)element;
//- (void) displayWebPage:(NSString*)URL parentController:(UIViewController*)parentController;

@end

//-----------------------------------------------------------------

