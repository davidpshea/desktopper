//
//  NSCoder+DefaultDecoding.m
//  DeskTopper
//
//  Created by David Shea on 16/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSCoder+DefaultDecoding.h"

@implementation NSCoder (DefaultDecoding)

//-----------------------------------------------------------------
- (id) decodeObject:(NSString*)key withDefault:(id)defaultObject
{
	return [self containsValueForKey:key] ? [self decodeObjectForKey:key] : defaultObject;
}

//-----------------------------------------------------------------
- (float) decodeFloat:(NSString*)key withDefault:(float)defaultFloat
{
	return [self containsValueForKey:key] ? [self decodeFloatForKey:key] : defaultFloat;
}

//-----------------------------------------------------------------
- (int) decodeInt:(NSString*)key withDefault:(int)defaultInt
{
	return [self containsValueForKey:key] ? [self decodeIntForKey:key] : defaultInt;
}

//-----------------------------------------------------------------
- (BOOL) decodeBOOL:(NSString*)key withDefault:(BOOL)defaultBOOL
{
	return [self containsValueForKey:key] ? [self decodeBoolForKey:key] : defaultBOOL;
}

//-----------------------------------------------------------------
- (CGRect) decodeRect:(NSString*)key withDefault:(CGRect)defaultRect
{
	return [self containsValueForKey:key] ? [self decodeCGRectForKey:key] : defaultRect;
}

//-----------------------------------------------------------------
@end
