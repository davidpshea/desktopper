//
//  UIActionSheet+Tools.m
//  DeskTopper
//
//  Created by David Shea on 4/10/13.
//
//

#import "UIActionSheet+Tools.h"

//-----------------------------------------------------------------
@implementation UIActionSheet (launchFrom)

- (void) launchFrom:(id)sender animated:(BOOL)animated
{
	if ([sender isKindOfClass:[UIBarButtonItem class]]) {

        [self showFromBarButtonItem:sender animated:animated];
    }
    else {
       	if ([sender isKindOfClass:[UITapGestureRecognizer class]]) {

            UIView* view     = [sender view];
            CGPoint TapPoint = [sender locationInView:view];
            CGRect box       = CGRectMake(TapPoint.x, TapPoint.y, 2, 2);

            [self showFromRect:box inView:view animated:animated];
        }
    }
}

@end

//-----------------------------------------------------------------
@implementation UIActionSheet (ButtonIndex)

static const int maximumActionSheetItems = 16;

static int buttonToMenuIndex[maximumActionSheetItems] = {0};
static int menuToButtonIndex[maximumActionSheetItems] = {0};

//-----------------------------------------------------------------
- (void) addButtonWithTitle:(NSString*)title menuIndex:(int)index
{
	int buttonIndex = (int)[self addButtonWithTitle:title];
	DT_ASSERT(buttonIndex < maximumActionSheetItems);

	buttonToMenuIndex[buttonIndex]	= index;
	menuToButtonIndex[index]		= buttonIndex;
}

//-----------------------------------------------------------------
- (int) getMenuIndexFromButtonIndex:(int)buttonIndex
{
	DT_ASSERT(buttonIndex < maximumActionSheetItems);
	return buttonToMenuIndex[buttonIndex];
}

//-----------------------------------------------------------------
- (int) getButtonIndexFromMenuIndex:(int)menuIndex
{
	DT_ASSERT(menuIndex < maximumActionSheetItems);
	return menuToButtonIndex[menuIndex];
}

//-----------------------------------------------------------------
@end
