//
//  NSString+Markdown.h
//  DeskTopper
//
//  Created by David Shea on 24/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Markdown)

- (NSString*) createHTMLFromMarkdown;

@end
