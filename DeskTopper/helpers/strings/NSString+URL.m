//
//  NSString+URL.m
//  DeskTopper
//
//  Created by David Shea on 5/12/12.
//
//

#import "NSString+URL.h"
#import "UIApplication+Helpers.h"
#import "DTOptions.h"
#import "NSString+Word.h"

@implementation NSString (URL)

//-----------------------------------------------------------------
+ (NSString*) checkForValidURL:(NSString*)stringToCheck
{
	// Check for http at start of string
	NSRange rangeHTTP = [stringToCheck rangeOfString:@"http" options:NSCaseInsensitiveSearch | NSAnchoredSearch];

	// no....
	if (rangeHTTP.length == 0) {
		// check to see if there's a www there...
		NSRange rangeWWW = [stringToCheck rangeOfString:@"www" options:NSCaseInsensitiveSearch | NSAnchoredSearch];
		if (rangeWWW.length != 0) {
			// Yes.. so add the missing http...
			stringToCheck = [NSString stringWithFormat:@"http://%@", stringToCheck];
		}
	}

	NSURL* candidateURL = [NSURL URLWithString:stringToCheck];
	// WARNING > "test" is an URL according to RFCs, being just a path
	// so you still should check scheme and all other NSURL attributes you need

	if (candidateURL && [candidateURL scheme] && [candidateURL host]) {
		// candidate is a well-formed url with:
		//  - a scheme (like http://)
		//  - a host (like stackoverflow.com)
		return stringToCheck;
	}

	return nil;
}

//-----------------------------------------------------------------
+ (NSString*) createValidURLFromString:(NSString*)stringToProcess
{
	LOG_FN();

	// did we type in a URL?
	NSString* URL = [NSString checkForValidURL:stringToProcess];

	//	NSArray* queries = [DTShortcutWidget getQueryArray];

	NSMutableArray* searchEngines = [[[UIApplication myOptions] searchEngines] array];

	// No, perhaps it was a search string
	if (! URL) {
		// Extract seach identifier
		NSRange labelRange    = {0, 0};
		labelRange            = [stringToProcess rangeOfWordInRange:labelRange];
		NSString* searchLabel = [stringToProcess substringWithRange:labelRange];

		NSRange searchTextRange = {labelRange.length, [stringToProcess length] - labelRange.length};
		NSString* searchText    = [stringToProcess substringWithRange:searchTextRange];
		searchText 			    = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

		// Look through all search queries defined
		for (DTShortcut* shortcut in searchEngines) {

			NSString* query      = [shortcut text];
			NSString* queryLabel = [shortcut label];

			// Found a query?
			if ([searchLabel caseInsensitiveCompare:queryLabel] == NSOrderedSame) {

				NSString* searchResult = [query stringByReplacingOccurrencesOfString:@"$s" withString:searchText];

				// remove any white space off the ends, and convert to a web friendly string
				searchResult = [searchResult stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
				searchResult = [searchResult stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				// create url from seach string and query
				//				URL	= [NSString stringWithFormat:queryResult, searchResult];
				URL = searchResult;
			}
		}
	}

	if (! URL) {
		// Last resort - take first search and use that...
		DTShortcut* firstShortcut = [searchEngines objectAtIndex:0];
		if (firstShortcut) {
			URL = [NSString createValidURLFromString:[NSString stringWithFormat:@"%@ %@", [firstShortcut label], stringToProcess]];
		}
	}
	
	return URL;
}

//-----------------------------------------------------------------

@end
