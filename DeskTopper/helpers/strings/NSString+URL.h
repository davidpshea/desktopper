//
//  NSString+URL.h
//  DeskTopper
//
//  Created by David Shea on 5/12/12.
//
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

+ (NSString*) checkForValidURL:(NSString*)stringToCheck;
+ (NSString*) createValidURLFromString:(NSString*)stringToProcess;

@end
