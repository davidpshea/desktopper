//
//  NSString+Macros.m
//  DeskTopper
//
//  Created by David Shea on 5/12/12.
//
//

#import "NSString+Macros.h"

@implementation NSString (Macros)

//-----------------------------------------------------------------
- (NSString*) expandMacros
{
	// Stsrt by preserving $$ ($4 gets translated to a single one)
	// We use the HTML representation of $ to ensure it doesn't affect
	// any other macros
	NSString* newString = [self stringByReplacingOccurrencesOfString:@"$$"  withString:@"&#36;"];

	NSDate* now = [NSDate date];
	// create a data object, then string...
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	NSString* dateMedium = [dateFormatter stringFromDate:now];

	[dateFormatter setDateStyle:NSDateFormatterShortStyle];
	NSString* dateShort = [dateFormatter stringFromDate:now];

	[dateFormatter setDateStyle:NSDateFormatterLongStyle];
	NSString* dateLong = [dateFormatter stringFromDate:now];

	// Expand date...
	newString = [newString stringByReplacingOccurrencesOfString: @"$dateshort"  withString:dateShort];
	newString = [newString stringByReplacingOccurrencesOfString: @"$datemedium" withString:dateMedium];
	newString = [newString stringByReplacingOccurrencesOfString: @"$datelong"   withString:dateLong];


	// Expand time
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	[dateFormatter setDateStyle:NSDateFormatterNoStyle];
	NSString* timeShort = [dateFormatter stringFromDate:now];

	[dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
	NSString* timeMedium = [dateFormatter stringFromDate:now];

	[dateFormatter setTimeStyle:NSDateFormatterLongStyle];
	NSString* timeLong = [dateFormatter stringFromDate:now];

	newString = [newString stringByReplacingOccurrencesOfString:@"$timeshort"  withString:timeShort];
	newString = [newString stringByReplacingOccurrencesOfString:@"$timemedium" withString:timeMedium];
	newString = [newString stringByReplacingOccurrencesOfString:@"$timelong"  withString:timeLong];

	return newString;
}

@end
