//
//  NSString+Macros.h
//  DeskTopper
//
//  Created by David Shea on 5/12/12.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Macros)

- (NSString*) expandMacros;

@end
