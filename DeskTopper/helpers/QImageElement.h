//
//  QImageElement.h
//  DeskTopper
//
//  Created by David Shea on 12/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//#import "QElement.h"
#if 0

@class QuickDialogTableView;
@class QuickDialogController;
@class QSection;
@class DTWidget;

@interface QImageElement : QElement

//@property (nonatomic, strong)	UIImage* image;
@property (nonatomic, weak)		UIView*			cellView;
@property (nonatomic, strong)	UIImageView*	imageView;
@property (nonatomic, strong)	UIColor*		backgroundColour;
@property (nonatomic)			float			topBorder;
@property (nonatomic)			float			bottomBorder;

+ (void) addThumbnailElementToSection:(QSection*)section forWidget:(DTWidget*)widget;

- (void)			setImage:(UIImage *)image;
- (QImageElement*)	initWithFrame:(CGRect)frame;

@end

#endif
