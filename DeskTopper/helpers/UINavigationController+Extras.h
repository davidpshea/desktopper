//
//  UINavigationController+Extras.h
//  DeskTopper
//
//  Created by David Shea on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//-----------------------------------------------------------------
@interface UINavigationController (Extras)

+ (UINavigationController*) styledControllerWithRoot:(UIViewController*)rootViewController;

- (void) style;
- (void) addDoneButton:    (SEL)action forTarget:(id)buttonTarget;
- (void) addEditButton:    (SEL)action forTarget:(id)buttonTarget;
- (void) addCancelButton:  (SEL)action forTarget:(id)buttonTarget;
- (void) addDefaultButtons:(SEL)action forTarget:(id)buttonTarget;

//-----------------------------------------------------------------
@end
