//
//  UIPopoverController+Stack.h
//  DeskTopper
//
//  Created by David Shea on 12/04/13.
//
//

#import <UIKit/UIKit.h>

@interface UIPopoverController (Stack)

- (void)	pushViewController:(id)viewController;
- (id)		popViewController;

+ (id)						getActiveController;
+ (UIPopoverController*)	getActivePopoverController;
+ (BOOL)					dismissActiveController;
+ (void)					setActiveController:(id)newController;

@end
