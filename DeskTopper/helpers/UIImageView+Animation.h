//
//  UIImageView+Animation.h
//  DESKTOPPER
//
//  Created by David Shea on 25/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Animation)

- (void) hideWithAnimation;
- (void) showWithAnimation;

@end
