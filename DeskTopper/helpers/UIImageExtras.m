#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "UIImageExtras.h"
#import "FileHelper.h"
#import "NSString+Markdown.h"
//#import "DTAttributedTextView.h"
#import "NSString+AttributedString.h"
#import "SVProgressHUD.h"
#import "UIApplication+Helpers.h"

//-----------------------------------------------------------------
#if 0
@implementation DTAttributedTextViewDelegate

@synthesize linkTarget = _linkTarget;

//-----------------------------------------------------------------
- (DTAttributedTextViewDelegate*) initWithViewController:(id)viewController
{
	self = [super init];
	[self setLinkTarget:viewController];
	return self;
}

//-----------------------------------------------------------------
- (UIView*) attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView
				viewForLink:(NSURL*)url identifier:(NSString*)identifier frame:(CGRect)frame
{
//	LOG_FN();
	DTLinkButton* button = [[DTLinkButton alloc] initWithFrame:frame];
	[button setURL:url];
	[button setMinimumHitSize:CGSizeMake(25, 25)]; // adjusts it's bounds so that button is always large enough
	[button setGUID:identifier];

	// use normal push action for opening URL
	[button addTarget:[self linkTarget] action:@selector(linkPushed:) forControlEvents:UIControlEventTouchUpInside];

	// demonstrate combination with long press
	UILongPressGestureRecognizer* longPress =
		[[UILongPressGestureRecognizer alloc] initWithTarget:[self linkTarget] action:@selector(linkLongPressed:)];
	[button addGestureRecognizer:longPress];

	return button;
}

//-----------------------------------------------------------------
- (BOOL) attributedTextContentView:(DTAttributedTextContentView*)attributedTextContentView
	shouldDrawBackgroundForTextBlock:(DTTextBlock*)textBlock frame:(CGRect)frame context:(CGContextRef)context
		forLayoutFrame:(DTCoreTextLayoutFrame*)layoutFrame
{
//	LOG_FN();
	CGColorRef colour = [[textBlock backgroundColor] CGColor];
	if (colour) {
		CGContextSetFillColorWithColor(context, colour);
		CGContextFillRect(context, frame);
		return NO;
	}

	return YES; // draw standard background
}

@end
#endif

//-----------------------------------------------------------------
#define USE_JPEG	1

//-----------------------------------------------------------------
@implementation UIImage (Extras)

//-----------------------------------------------------------------
+ (NSString*) getDefaultImageFileExtension
{
	#ifdef USE_JPEG
	return @"jpg";
	#else
	return @"png";
	#endif
}

//-----------------------------------------------------------------
+ (NSString*) addDefaultImageFileExtension:(NSString*)filePath
{
	// If we have an extension, leave it in place
	if ([[filePath pathExtension] length] > 0) {
		return filePath;
	}

	// Otherwise stick the default one on...
	return [filePath stringByAppendingPathExtension:[UIImage getDefaultImageFileExtension]];
}

//-----------------------------------------------------------------
+ (CGSize) getScaledPhotoSize
{
	return CGSizeMake(768, 1024);
}

//-----------------------------------------------------------------
+ (CGSize) getThumbnailPhotoSize
{
	return [UIApplication isPad] ? CGSizeMake(320, 480) : CGSizeMake(320/2, 480/2);
}

//-----------------------------------------------------------------
+ (CGSize) getStandardPopoverSize
{
	return CGSizeMake(320, 480);
}

//-----------------------------------------------------------------
- (float) getAspectRatio
{
	CGSize size = [self size];
	return size.width / size.height;
}

//-----------------------------------------------------------------
- (CGSize) getSizeFromNewWidth:(float)newWidth
{
	CGSize size = [self size];
	float aspectRatio  = newWidth / size.width;
	float scaledHeight = size.height * aspectRatio;

	return CGSizeMake(newWidth, scaledHeight);
}

//-----------------------------------------------------------------
- (CGSize) getSizeToFitIntoSize:(CGSize)boundingSize
{
	CGSize size    = [self size];
	CGSize newSize = size;

	float aspectRatio  = size.width / size.height;

	if (size.width > boundingSize.width) {
		newSize.width  = boundingSize.width;
		newSize.height = newSize.width / aspectRatio;
	}
/*
	if (size.height > boundingSize.height) {
		newSize.height = boundingSize.height;
		newSize.width  = newSize.height * aspectRatio;
	}
*/
	return newSize;
}

//-----------------------------------------------------------------
static inline double radians(double degrees)
{
	return degrees * M_PI/180;
}

//-----------------------------------------------------------------
- (UIImage*) imageByScalingToSize:(CGSize)targetSize
{
	//-(UIImage *)resizeImage:(UIImage *)image :(NSInteger) width :(NSInteger) height {
	UIImage* image = self;
	CGImageRef imageRef = [self CGImage];
	CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(imageRef);
	CGColorSpaceRef colorSpaceInfo = CGColorSpaceCreateDeviceRGB();

	int width  = targetSize.width;
	int height = targetSize.height;

	if (alphaInfo == kCGImageAlphaNone)
		alphaInfo = kCGImageAlphaNoneSkipLast;

	CGContextRef bitmap;

	if (image.imageOrientation == UIImageOrientationUp || image.imageOrientation == UIImageOrientationDown) {
		bitmap = CGBitmapContextCreate(
			NULL, width, height,
			CGImageGetBitsPerComponent(imageRef),
			CGImageGetBytesPerRow(imageRef),
			colorSpaceInfo,
			(CGBitmapInfo)alphaInfo
		);

	} else {
		bitmap = CGBitmapContextCreate(
			NULL, height, width,
			CGImageGetBitsPerComponent(imageRef),
			CGImageGetBytesPerRow(imageRef),
			colorSpaceInfo,
			(CGBitmapInfo)alphaInfo
		);
	}

	if (image.imageOrientation == UIImageOrientationLeft) {
		NSLog(@"image orientation left");
		CGContextRotateCTM (bitmap, radians(90));
		CGContextTranslateCTM (bitmap, 0, -height);

	} else if (image.imageOrientation == UIImageOrientationRight) {
		NSLog(@"image orientation right");
		CGContextRotateCTM (bitmap, radians(-90));
		CGContextTranslateCTM (bitmap, -width, 0);

	} else if (image.imageOrientation == UIImageOrientationUp) {
		NSLog(@"image orientation up");

	} else if (image.imageOrientation == UIImageOrientationDown) {
		NSLog(@"image orientation down");
		CGContextTranslateCTM (bitmap, width,height);
		CGContextRotateCTM (bitmap, radians(-180.));

	}

	CGContextDrawImage(bitmap, CGRectMake(0, 0, width, height), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage *result = [UIImage imageWithCGImage:ref];

	CGColorSpaceRelease(colorSpaceInfo);
	CGContextRelease(bitmap);
	CGImageRelease(ref);

	return result;
}

//-----------------------------------------------------------------
- (UIImage*) imageFromImageByScaling:(CGSize)targetSize keepAspectRatio:(BOOL)keepAspectRatio
{
//	LOG_FN();
	if (keepAspectRatio) {
		float currentAspectRatio = [self getAspectRatio];
		targetSize.height  = targetSize.width / currentAspectRatio;
	}

	DT_ASSERT((targetSize.width > 0.0) && (targetSize.height > 0.0));

#if 1
	UIGraphicsBeginImageContext(targetSize); // this will crop

	CGRect thumbnailRect = CGRectMake(0, 0, targetSize.width, targetSize.height);

	[self drawInRect:thumbnailRect];

	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	DT_ASSERT_NOT_NIL(newImage);
	if(newImage == nil) {
		DT_DEBUG_LOG(@"could not scale image");
	}

	//pop the context to get back to the default
	UIGraphicsEndImageContext();
	return newImage;
#else
	return [self imageByScalingToSize:targetSize];
#endif
}

//-----------------------------------------------------------------
- (UIImage*) imageFromImageByScalingRatio:(CGSize)targetSize
{
//	LOG_FN();
	UIImage* sourceImage   = self;
	UIImage* newImage      = nil;
	CGSize imageSize       = sourceImage.size;
	CGFloat width          = imageSize.width;
	CGFloat height         = imageSize.height;
	CGFloat targetWidth    = targetSize.width;
	CGFloat targetHeight   = targetSize.height;
	CGFloat scaleFactor    = 0.0f;
	CGFloat scaledWidth    = targetWidth;
	CGFloat scaledHeight   = targetHeight;
	CGPoint thumbnailPoint = CGPointMake(0.0f, 0.0f);

	if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
		CGFloat widthFactor  = targetWidth / width;
		CGFloat heightFactor = targetHeight / height;

		scaleFactor  = (widthFactor > heightFactor) ? widthFactor : heightFactor;
		scaledWidth  = width * scaleFactor;
		scaledHeight = height * scaleFactor;

		// center the image
		if (widthFactor > heightFactor) {
			thumbnailPoint.y = (targetHeight - scaledHeight) / 2.0;
		}
		else {
			if (widthFactor < heightFactor) {
				thumbnailPoint.x = (targetWidth - scaledWidth) / 2.0;
			}
		}
	}

	DT_ASSERT((targetSize.width > 0.0) && (targetSize.height > 0.0));
	UIGraphicsBeginImageContext(targetSize); // this will crop

	CGRect thumbnailRect      = CGRectZero;
	thumbnailRect.origin      = thumbnailPoint;
	thumbnailRect.size.width  = scaledWidth;
	thumbnailRect.size.height = scaledHeight;

	[sourceImage drawInRect:thumbnailRect];

	newImage = UIGraphicsGetImageFromCurrentImageContext();
	if(newImage == nil) {
		DT_DEBUG_LOG(@"could not scale image");
	}

	//pop the context to get back to the default
	UIGraphicsEndImageContext();
	return newImage;
}

//-----------------------------------------------------------------
+ (UIImage*) createFromText:(NSString*)text intoSize:(CGSize)box
{
//	LOG_FN();
	// set the font type and size
	UIFont* font = [UIFont systemFontOfSize:12.0];

	CGSize sizeRect = CGSizeMake(box.width, box.height);
	DT_ASSERT((sizeRect.width > 0.0) && (sizeRect.height > 0.0));

	UIGraphicsBeginImageContextWithOptions(sizeRect, YES, 0.0);

	// get the context for CoreGraphics
	CGContextRef context = UIGraphicsGetCurrentContext();
	DT_ASSERT_NOT_NIL(context);

	CGRect fillRect = CGRectMake(0, 0, sizeRect.width, sizeRect.height);
	CGContextSetRGBFillColor(context, 128, 128, 0, 1);
	UIRectFill(fillRect);

	CGRect textRect = CGRectMake(3, 3, sizeRect.width - (3*2), sizeRect.height - (3*2));

	CGContextSetTextDrawingMode(context, kCGTextFill);
	CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1.0);

	// draw in context, you can use also drawInRect:withFont:
//	[text drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
	DT_ASSERT_NOT_NIL(text);
	NSDictionary* attributes = @{NSFontAttributeName : font};
	[text drawInRect:CGRectIntegral(textRect) withAttributes:attributes];

	// transfer image
	UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	DT_ASSERT_NOT_NIL(image);

	return image;
}

//-----------------------------------------------------------------
+ (UIImage*) createColouredRectangle:(CGSize)size colour:(UIColor*)colour borderColour:(UIColor*)borderColour
{
//	LOG_FN();
	DT_ASSERT((size.width > 0.0) && (size.height > 0.0));
	DT_ASSERT_NOT_NIL(colour);

	UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
	DT_ASSERT_NOT_NIL(UIGraphicsGetCurrentContext());

	CGRect fillRect = CGRectMake(0, 0, size.width, size.height);

	// Draw the square
	[colour setFill];
	UIRectFill(fillRect);

	if (borderColour) {
		// Draw border if we've specified one
		[borderColour setStroke];
		UIRectFrame(fillRect);
	}

	// transfer image
	UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
	DT_ASSERT_NOT_NIL(image);
	UIGraphicsEndImageContext();

	return image;
}

//-----------------------------------------------------------------
+ (UIImage*) createColouredRectangle:(CGSize)size colour:(UIColor*)colour
{
	return [UIImage createColouredRectangle:size colour:colour borderColour:nil];
}

//-----------------------------------------------------------------
+ (UIImage*) createColouredRectangleWithContrastingBorder:(CGSize)size colour:(UIColor*)colour
{
	// Note that getRed might fail (and not set any vars) if the colour is
	// Not in RGB colourspace, so we assume a default of full-white
	CGFloat red = 1.0, green = 1.0, blue = 1.0, alpha = 1.0;
	[colour getRed:&red green:&green blue:&blue alpha:&alpha];

	BOOL isDark				= (red < 0.3) && (green < 0.3) && (blue < 0.3);
	UIColor* borderColour	= isDark ? [UIColor colorWithWhite:0.4 alpha:1.0] : [UIColor blackColor];

	return [UIImage createColouredRectangle:size colour:colour borderColour:borderColour];
}

//-----------------------------------------------------------------
#if 0
+ (UIImage*) createFromHTML:(NSString*)HTMLtext
		intoSize:(CGSize)box
		backgroundColour:(UIColor*)backgroundColour
		textColour:(UIColor*)textColour
		linkColour:(UIColor*)linkColour
		scale:(float)scale
		fontName:(NSString*)fontName
{
	static DTAttributedTextViewDelegate* attributedTextViewDelegate = nil;

//	LOG_FN();
	DT_ASSERT_NOT_NIL(HTMLtext);
	DT_ASSERT_NOT_NIL(backgroundColour);
	DT_ASSERT_NOT_NIL(textColour);
	DT_ASSERT((box.width > 0.0) && (box.height > 0.0));

	CGRect frame = CGRectMake(0, 0, box.width, box.height);

	NSDictionary* options = [
		NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithFloat:scale],	NSTextSizeMultiplierDocumentOption,
		fontName,							DTDefaultFontFamily,
		linkColour,							DTDefaultLinkColor,
		textColour,							DTDefaultTextColor,
		fontName,							DTDefaultFontFamily,
		[NSNumber numberWithBool:YES],		DTPreserveNewlinesAttribute,
		[NSString getDefaultStyleSheet],	DTDefaultStyleSheet,
		nil
	];

	NSAttributedString* attributedString = [HTMLtext createAttributedStringFromHTMLStringWithOptions:options];
	DT_ASSERT_NOT_NIL(attributedString);

	DTAttributedTextView* HTMLView = [[DTAttributedTextView alloc] initWithFrame:frame];
	DT_ASSERT_NOT_NIL(HTMLView);

	if (!attributedTextViewDelegate) {
		attributedTextViewDelegate = [[DTAttributedTextViewDelegate alloc] init];
	}
	[[HTMLView contentView] setDelegate:attributedTextViewDelegate];
	[HTMLView setBackgroundColor:backgroundColour];
	[HTMLView setAttributedString:attributedString];

	UIImage* newImage = [UIImage createFromView:HTMLView];
//	DT_ASSERT_NOT_NIL(newImage);
	return newImage;
}
#endif

//-----------------------------------------------------------------
#if 0
UIImage* UIImageFromLayer(CGLayerRef layer)
{
	// Create the bitmap context
	CGContextRef    bitmapContext = NULL;
	void *          bitmapData;
	int             bitmapByteCount;
	int             bitmapBytesPerRow;
	CGSize          size = CGLayerGetSize(layer);
	
	// Declare the number of bytes per row. Each pixel in the bitmap in this
	// example is represented by 4 bytes; 8 bits each of red, green, blue, and
	// alpha.
	bitmapBytesPerRow   = (size.width * 4);
	bitmapByteCount     = (bitmapBytesPerRow * size.height);
	
	// Allocate memory for image data. This is the destination in memory
	// where any drawing to the bitmap context will be rendered.
	bitmapData = malloc( bitmapByteCount );
	if (bitmapData == NULL)
	{
		return nil;
	}
	
	// Create the bitmap context. We want pre-multiplied ARGB, 8-bits
	// per component. Regardless of what the source image format is
	// (CMYK, Grayscale, and so on) it will be converted over to the format
	// specified here by CGBitmapContextCreate.
	bitmapContext = CGBitmapContextCreate (bitmapData, size.width, size.height,8,bitmapBytesPerRow,
										   CGColorSpaceCreateDeviceRGB(),kCGImageAlphaNoneSkipFirst);
	
	if (bitmapContext == NULL)
		// error creating context
		return nil;
	
	CGContextScaleCTM(bitmapContext, 1, -1);
	CGContextTranslateCTM(bitmapContext, 0, -size.height);
	
	// Draw the image to the bitmap context. Once we draw, the memory
	// allocated for the context for rendering will then contain the
	// raw image data in the specified color space.
	CGContextDrawLayerAtPoint(bitmapContext, CGPointZero, layer);
	CGImageRef   img = CGBitmapContextCreateImage(bitmapContext);
	UIImage*     ui_img = [UIImage imageWithCGImage: img];
	
	CGImageRelease(img);
	CGContextRelease(bitmapContext);
	free(bitmapData);
	
	return ui_img;
	
}

//-----------------------------------------------------------------
/*
 Create the CGLayer
Get the layer's context by calling CGLayerGetContext().
Draw the view to the layer's graphics context with [view.layer renderInContext:].
*/
+ (UIImage*) createFromView3:(UIView*)view // outputSize:(CGSize)outputSize
{
	CGSize size = [view bounds].size;
	//	LOG_FN();
	DT_ASSERT_NOT_NIL(view);

	UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
	CGLayerRef ref = CGLayerCreateWithContext(UIGraphicsGetCurrentContext(), size, NULL);

	UIImage* p = UIImageFromLayer(ref);

	CGLayerRelease(ref);

	return p;

#if 0
	CGSize size = [view bounds].size;

	size.width /= 4.0;
	size.height /= 4.0;

	DT_ASSERT((size.width > 0.0) && (size.height > 0.0));

	UIGraphicsBeginImageContextWithOptions(size, YES, 1.0 / 4.0);
	DT_ASSERT_NOT_NIL([view layer]);
	[[view layer] renderInContext:UIGraphicsGetCurrentContext()];
	UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
	DT_ASSERT_NOT_NIL(image);
	UIGraphicsEndImageContext();

	return image;
#endif
}
#endif

//-----------------------------------------------------------------
+ (UIImage*) createFromView:(UIView*)view
{
//	LOG_FN();
	DT_ASSERT_NOT_NIL(view);

	CGSize size = [view bounds].size;
	DT_ASSERT((size.width > 0.0) && (size.height > 0.0));

	UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
	DT_ASSERT_NOT_NIL([view layer]);

	[[view layer] renderInContext:UIGraphicsGetCurrentContext()];
	UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
	DT_ASSERT_NOT_NIL(image);
	UIGraphicsEndImageContext();

	return image;
}

//-----------------------------------------------------------------
+ (UIImage*) createFromViewWithScale:(UIView*)view scaledBy:(float)scale
{
	//	LOG_FN();
	DT_ASSERT_NOT_NIL(view);

	CGSize size = [view bounds].size;
	DT_ASSERT((size.width > 0.0) && (size.height > 0.0));

	size.width  *= scale;
	size.height *= scale;

	UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
	DT_ASSERT_NOT_NIL([view layer]);

	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextScaleCTM(context, scale, scale);

	[[view layer] renderInContext:context];
	UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
	DT_ASSERT_NOT_NIL(image);
	UIGraphicsEndImageContext();

	return image;
}

//-----------------------------------------------------------------
- (UIImage*) maskImageWithMask:(UIImage*)maskImage
{
	CGImageRef maskRef = maskImage.CGImage;

	CGImageRef mask = CGImageMaskCreate(
		CGImageGetWidth(maskRef),
		CGImageGetHeight(maskRef),
		CGImageGetBitsPerComponent(maskRef),
		CGImageGetBitsPerPixel(maskRef),
		CGImageGetBytesPerRow(maskRef),
		CGImageGetDataProvider(maskRef),
		NULL,
		false
	);

	CGImageRef masked = CGImageCreateWithMask([self CGImage], mask);

	return [UIImage imageWithCGImage:masked];
}

//-----------------------------------------------------------------
- (UIImage*) maskImageWithArea:(CGRect)area
{
	UIImage* mask = [UIImage createColouredRectangle:area.size colour:[UIColor blackColor]];

	return [self maskImageWithMask:mask];
}

//-----------------------------------------------------------------
- (BOOL) saveToImageFile:(NSString*)filePath
{
	DT_ASSERT_NOT_NIL(filePath);

	NSData* imageData = nil;

	if ([[filePath pathExtension] compare:@"png"] == NSOrderedSame) {
		imageData = UIImagePNGRepresentation(self);
	}
	else {
		imageData = UIImageJPEGRepresentation(self, 1.0f);
	}

	DT_ASSERT_NOT_NIL(imageData);
	[imageData writeToFile:filePath atomically:YES];

	return YES;
}

//-----------------------------------------------------------------
- (NSString*) saveToUniqueImageFile:(NSString*)filenameMask
{
	NSString* mask     = (filenameMask == nil) ? @"image%@" : filenameMask;
	NSString* fileName = [UIImage addDefaultImageFileExtension:[FileHelper getUniqueFileNameFromTime:mask]];
	NSString* filePath = [FileHelper getPathForFileInDocuments:fileName];

	[self saveToImageFile:filePath];

	return filePath;
}

//-----------------------------------------------------------------
- (void) thisImage:(UIImage*)image hasBeenSavedInPhotoAlbumWithError:(NSError*)error usingContextInfo:(void*)contextInfo
{
	if (error) {
		[SVProgressHUD showErrorWithStatus:@"Failed to save"];
	}
	else {
		[SVProgressHUD showSuccessWithStatus:@"Ok"];
	}
}

//-----------------------------------------------------------------
- (void) saveToCameraRoll
{
	UIImageWriteToSavedPhotosAlbum(self, self, @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), nil);
}

//-----------------------------------------------------------------
+ (UIImage*) createFromImageFile:(NSString*)filePath
{
	if (!filePath) {
		return nil;
	}

	DT_DEBUG_LOG(@"%@", filePath);
	UIImage* image = [[UIImage alloc] initWithContentsOfFile:filePath];
	//DT_ASSERT_NOT_NIL(image);
	return image;
}

//-----------------------------------------------------------------
+ (UIImage*) imageFromResource:(NSString*)imagePath
{
	NSString* imageFilename = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], imagePath];
	return [UIImage imageNamed:imageFilename];
}

//-----------------------------------------------------------------
- (UIImage*) greyScaleImage
{
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, self.size.width, self.size.height);

    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();

    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, self.size.width, self.size.height, 8, 0, colorSpace, kCGImageAlphaNone);

    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [self CGImage]);

    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);

    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];

    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}

//-----------------------------------------------------------------

@end

//-----------------------------------------------------------------
@implementation UIImageView (Extras)

- (void) setScale:(CGFloat)scale
{
    CGAffineTransform newTransform = CGAffineTransformMakeScale(scale, scale);
    [self setTransform:newTransform];
}

//-----------------------------------------------------------------

@end

