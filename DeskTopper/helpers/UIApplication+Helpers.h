//
//  UIApplication+Helpers.h
//  DeskTopper
//
//  Created by David Shea on 18/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FMDatabase;
@class DTOptions;

@interface UIApplication (Helpers)

+ (UIApplication*)		myApp;
+ (NSString*)			myAppVersion;
+ (id)					myAppDelegate;
+ (id)					myRootViewController;
+ (id)					myNavigationController;
+ (id)					myPresentedViewController;
+ (CGRect)				myMainScreenBounds;
+ (CGPoint)				myMainScreenCentre;
+ (CGRect)				myMainScreenBoundsRotated;
+ (FMDatabase*)			myDatabase;
+ (DTOptions*)			myOptions;
+ (UIWindow*)			myWindow;
+ (NSOperationQueue*)	myOperationQueue;
+ (void)				addOperationToMyOperationQueue:(NSOperation*)operation;
+ (void)				dismissToRootViewController:(BOOL)animated;
+ (BOOL)				isConnectedToInternet;
+ (BOOL)				isConnectedToNetwork;
+ (BOOL)				isPad;
+ (BOOL)				isPhone;
+ (BOOL)				isIOS7;

+ (UIInterfaceOrientation)	myRotation;
+ (BOOL)					isPortrait;
+ (BOOL)					isLandscape;

+ (void) PushNavigationBarColour:(UIColor*)barColour;
+ (void) PushNavigationBarDarkGreyColour;
+ (void) PopNavigationBarColour;

@end

