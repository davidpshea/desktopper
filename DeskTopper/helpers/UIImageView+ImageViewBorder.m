//
//  UIImageView+ImageViewBorder.m
//  DeskTopper
//
//  Created by David Shea on 20/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImageView+ImageViewBorder.h"
#import "UIImageView+ImageViewShadow.h"

@implementation UIImageView (ImageViewBorder)

//-----------------------------------------------------------------
-(void) configureImageViewBorder:(CGFloat)borderWidth withColour:(UIColor*)colour
{
	CALayer* layer = [self layer];
    [layer setBorderWidth:borderWidth];
//    [self setContentMode:UIViewContentModeScaleToFill];
    [layer setBorderColor:colour.CGColor];
/*    [layer setShadowOffset:CGSizeMake(-5.0, 5.0)];
    [layer setShadowRadius:5.0];
    [layer setShadowOpacity:1.0];
*/
}

//-----------------------------------------------------------------
-(void) configureImageViewBorder:(CGFloat)borderWidth
{
	[self configureImageViewBorder:borderWidth withColour:[UIColor whiteColor]];
}

//-----------------------------------------------------------------
- (UIImage*) rescaleImage:(UIImage*)image
{
    UIImage* scaledImage = image;
	
    CALayer* layer      = [self layer];
    CGFloat borderWidth = layer.borderWidth;
	
    //if border is defined
    if (borderWidth > 0) {
		CGRect bounds = [self bounds];
        //rectangle in which we want to draw the image.
        CGRect imageRect = CGRectMake(0.0, 0.0, bounds.size.width - 2 * borderWidth,bounds.size.height - 2 * borderWidth);
		
		//Only draw image if its size is bigger than the image rect size.
        if (image.size.width > imageRect.size.width || image.size.height > imageRect.size.height)
        {
            UIGraphicsBeginImageContext(imageRect.size);
            [image drawInRect:imageRect];
            scaledImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
        }
    }
    return scaledImage;
}

//-----------------------------------------------------------------
-(void)setImage:(UIImage*)image withBorder:(CGFloat)borderWidth andShadow:(CGFloat)shadowSize
{
    [self configureImageViewBorder:borderWidth];
    [self addImageShadow:shadowSize];

	UIImage* scaledImage = [self rescaleImage:image];
    self.image = scaledImage;
}

//-----------------------------------------------------------------

@end

