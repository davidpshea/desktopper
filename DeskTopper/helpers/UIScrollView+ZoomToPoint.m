//
//  UIScrollView+ZoomToPoint.m
//  DeskTopper
//
//  Created by David Shea on 20/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIScrollView+ZoomToPoint.h"
#import "UIApplication+Helpers.h"

@implementation UIScrollView (ZoomToPoint)

//-----------------------------------------------------------------
- (void)zoomToPoint:(CGPoint)zoomPoint withScale:(CGFloat)scale animated:(BOOL)animated
{
    CGRect bounds		= [self bounds];
	float  zoomScale	= [self zoomScale];

	// Normalize current content size back to content scale of 1.0f
    CGSize contentSize;
    contentSize.width  = (self.contentSize.width / zoomScale);
    contentSize.height = (self.contentSize.height / zoomScale);

    // translate the zoom point to relative to the content rect
    zoomPoint.x = (zoomPoint.x / bounds.size.width)  * contentSize.width;
    zoomPoint.y = (zoomPoint.y / bounds.size.height) * contentSize.height;

    // derive the size of the region to zoom to
    CGSize zoomSize;
    zoomSize.width  = bounds.size.width / scale;
    zoomSize.height = bounds.size.height / scale;

    // offset the zoom rect so the actual zoom point is in the middle of the rectangle
    CGRect zoomRect;
    zoomRect.origin.x    = zoomPoint.x - zoomSize.width / 2.0f;
    zoomRect.origin.y    = zoomPoint.y - zoomSize.height / 2.0f;
    zoomRect.size.width  = zoomSize.width;
    zoomRect.size.height = zoomSize.height;

    // apply the resize
    [self zoomToRect:zoomRect animated:animated];
}

//-----------------------------------------------------------------
- (CGRect) getVisibleRect
{
//	CGRect  fullScreenRect = [UIApplication myMainScreenBounds];
	CGRect  fullScreenRect = [self frame];

	CGRect visibleRect;
	visibleRect.origin = [self contentOffset];
	visibleRect.size   = fullScreenRect.size;

	CGFloat scale = [self zoomScale];

	visibleRect.origin.x	/= scale;
	visibleRect.origin.y	/= scale;
	visibleRect.size.width	/= scale;
	visibleRect.size.height /= scale;

//	DT_DEBUG_LOG( @"Visible rect: %@", NSStringFromCGRect(visibleRect) );

	return visibleRect;
}

//-----------------------------------------------------------------
- (CGPoint) getScreenCentre
{
	CGRect visible = [self getVisibleRect];

	return CGPointMake(
		visible.origin.x + (visible.size.width / 2),
		visible.origin.y + (visible.size.height / 2)
	);
}

//-----------------------------------------------------------------
@end

