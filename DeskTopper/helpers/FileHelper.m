//
//  FileHelper.m
//  DeskTopper
//
//  Created by David Shea on 12/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FileHelper.h"

@implementation FileHelper

#define iconResourcePath		@"resource/icons3"
#define toolbarIconResourcePath	@"resource/toolbar"
#define CSSResourcePath			@"resource/css"

#define databaseName			@"database.sq3"

//-----------------------------------------------------------------
static NSArray* sortedIconFileArray = nil;

//-----------------------------------------------------------------
+ (NSString*) getDocumentPath
{
	NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [documentDirectories objectAtIndex:0];
}

//-----------------------------------------------------------------
+ (NSString*) getPathForFileInDocuments:(NSString*)fileName
{
	NSArray*  documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

	NSString* documentDirectory   = [documentDirectories objectAtIndex:0];
	NSString* filePath            = [documentDirectory stringByAppendingPathComponent:fileName];

	return filePath;
}

//-----------------------------------------------------------------
+ (NSString*) getDatabasePath
{
	return [FileHelper getPathForFileInDocuments:databaseName];
}

//-----------------------------------------------------------------
+ (NSArray*) getFilesInDocuments
{
	return [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[FileHelper getDocumentPath] error:nil];
}

//-----------------------------------------------------------------
+ (NSString*) getPathForFileInBundle:(NSString*)fullFileName
{
	NSString* fileName  = [[fullFileName lastPathComponent] stringByDeletingPathExtension];
	NSString* extension = [fullFileName pathExtension];

	NSBundle* mainBundle = [NSBundle mainBundle];
	NSString* path       = [mainBundle pathForResource:fileName ofType:extension inDirectory:@"resource"];

	return path;
}

//-----------------------------------------------------------------
+ (NSString*) getUniqueFileNameFromTime:(NSString*)nameMask
{
	LOG_FN();
	NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];

	// Create filename with date and time
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setLocale:locale];
	[dateFormatter setDateFormat:@"yyyyMMdd-HHmmss"];
	[dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];

	// trail it with a random number so if we create 2 in one second
	// They're still unique filenames
	int randomNumber = (int)arc4random_uniform(1000);
	NSString* fullName = [dateFormatter stringFromDate:[NSDate date]];
	fullName = [fullName stringByAppendingFormat:@"-%3.3d", randomNumber];
	
	NSString* name = [NSString stringWithFormat:nameMask, fullName];

	DT_DEBUG_LOG(@"%@", name);

	return name;
}

//-----------------------------------------------------------------
+ (BOOL) writeStringToFile:(NSString*)filePath content:(NSString*)content error:(NSError**)error
{
	//save content to the documents directory
	return [
		content
		writeToFile:filePath
		atomically:NO
		encoding:NSUTF8StringEncoding
		error:error
	];
}

//-----------------------------------------------------------------
+ (NSString*) readStringFromFile:(NSString*)filePath error:(NSError**)error
{
	return [
		NSString
		stringWithContentsOfFile:filePath
		encoding:NSUTF8StringEncoding
		error:error
	 ];
}

//-----------------------------------------------------------------
+ (NSString*) readStringFromURL:(NSURL*)URL error:(NSError**)error
{
	return [
		NSString
		stringWithContentsOfURL:URL
		encoding:NSUTF8StringEncoding
		error:error
	];
}

//-----------------------------------------------------------------
+ (void) createDirectory:(NSString*)directoryName
{
	LOG_FN();
	[[NSFileManager defaultManager] createDirectoryAtPath:directoryName
		withIntermediateDirectories:YES attributes:nil error:nil];
}

//-----------------------------------------------------------------
+ (void) deleteFile:(NSString*)filePath
{
	LOG_FN();
	DT_DEBUG_LOG(@"DELETE:%@", filePath);

	DT_ASSERT_NOT_NIL(filePath);
	DT_ASSERT([filePath length] > 0);
	
	NSString* last   = [filePath lastPathComponent];
	BOOL isDocuments = [last compare:@"documents" options:NSCaseInsensitiveSearch] == NSOrderedSame;

	if (! isDocuments) {
		[[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
	}
	else {
		DT_DEBUG_LOG("**** DOCUMENT FAIL ****");
	}
}

//-----------------------------------------------------------------
+ (void) copyFile:(NSString*)from toPath:(NSString*)to
{
	LOG_FN();
	[[NSFileManager defaultManager] copyItemAtPath:from toPath:to error:nil];
}

//-----------------------------------------------------------------
+ (BOOL) doesFileExist:(NSString*)filePath
{
	return [[NSFileManager defaultManager] fileExistsAtPath:filePath];
}


//-----------------------------------------------------------------
+ (NSArray*) getIconFileArray
{
	LOG_FN();
	if (!sortedIconFileArray) {
		NSBundle* mainBundle = [NSBundle mainBundle];
		NSArray* icons       = [mainBundle pathsForResourcesOfType:@"png" inDirectory:iconResourcePath];
		sortedIconFileArray  = [icons sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
	}

	return sortedIconFileArray;
}

//-----------------------------------------------------------------
+ (NSString*) getIconFileNamed:(NSString*)filename
{
	LOG_FN();
	NSBundle* mainBundle = [NSBundle mainBundle];
	return [mainBundle pathForResource:filename ofType:@"png" inDirectory:iconResourcePath];
}

//-----------------------------------------------------------------
+ (NSString*) getToolbarIconFileNamed:(NSString*)filename
{
	LOG_FN();
	NSBundle* mainBundle = [NSBundle mainBundle];
	return [mainBundle pathForResource:filename ofType:@"png" inDirectory:toolbarIconResourcePath];
}

//-----------------------------------------------------------------
+ (NSString*) getCSSFileNamed:(NSString*)filename
{
	LOG_FN();
	NSBundle* mainBundle = [NSBundle mainBundle];
	return [mainBundle pathForResource:filename ofType:@"css" inDirectory:CSSResourcePath];
}

//-----------------------------------------------------------------
+ (void) sendFileToBin:(NSString*)filename
{
	LOG_FN();

	#ifdef SAVE_FILE_IN_BIN_DIRECTORY
	NSString* name		= [filename lastPathComponent];
	NSString* bin		= [FileHelper getPathForFileInDocuments:@"bin"];
	NSString* toPath	= [bin stringByAppendingPathComponent:name];
	
	[FileHelper copyFile:filename toPath:toPath];
	#endif
	
	[FileHelper deleteFile:filename];
}

//-----------------------------------------------------------------
@end

