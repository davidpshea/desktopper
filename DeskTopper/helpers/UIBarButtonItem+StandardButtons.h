//
//  UIBarButtonItem+StandardButtons.h
//  DeskTopper
//
//  Created by David Shea on 20/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

//-----------------------------------------------------------------
@interface UIButton (FullScreenButton)

+ (UIButton*)	createFullScreenButton:(SEL)action forTarget:(id)target;
- (void)		repositionFullScreenButton;

@end

//-----------------------------------------------------------------
@interface UIBarButtonItem (tint)

- (void) tint;

@end

//-----------------------------------------------------------------
@interface UIBarButtonItem (AddGesture)

- (void) addGesture:(UIGestureRecognizer*)gesture;
- (void) addTapGestureForAction:(SEL)action forTarget:(id)target;

@end

//-----------------------------------------------------------------
@interface UIBarButtonItem (StandardButtons)

+ (UIBarButtonItem*) createDoneButton:	(SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createCancelButton:(SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createSaveButton:	(SEL)action forTarget:(id)target;

+ (UIBarButtonItem*) createButtonWithText:(NSString*)text action:(SEL)action forTarget:(id)target;

+ (UIBarButtonItem*) createBackButton:		 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createForwardButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createFullScreenButton: (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createRefreshButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createStopButton:		 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createActionButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createAddButton:		 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createOrganiseButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createDuplicateButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createSettingsButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createDeleteButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createNewWebButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createNewImageButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createNewNoteButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createNewCameraButton:	 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createSelectSpaceButton:(SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createInboxButton:		 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createUpButton:		 (SEL)action forTarget:(id)target;
+ (UIBarButtonItem*) createDownButton:		 (SEL)action forTarget:(id)target;

+ (UIBarButtonItem*) createFlexibleSpace;
+ (UIBarButtonItem*) createFixedSpace:(float)width;

//-----------------------------------------------------------------
@end
