//
//  FMDatabase+Variables.h
//  DeskTopper
//
//  Created by David Shea on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FMDatabase.h"

@interface FMDatabase (Variables)

- (int)  getIntVariable:(NSString*)name withDefault:(int)defaultInt;
- (void) setIntVariable:(NSString*)name withValue:(int)value;

- (BOOL) getBOOLVariable:(NSString*)name withDefault:(BOOL)defaultBOOL;
- (void) setBOOLVariable:(NSString*)name withValue:(BOOL)value;

@end

