//
//  UIActionSheet+Tools.h
//  DeskTopper
//
//  Created by David Shea on 4/10/13.
//
//

#import <UIKit/UIKit.h>

//-----------------------------------------------------------------
@interface UIActionSheet (launchFrom)

- (void) launchFrom:(id)sender animated:(BOOL)animated;

@end

//-----------------------------------------------------------------
@interface UIActionSheet (ButtonIndex)

- (void)	addButtonWithTitle:(NSString*)title menuIndex:(int)index;

- (int)		getButtonIndexFromMenuIndex:(int)menuIndex;
- (int)		getMenuIndexFromButtonIndex:(int)buttonIndex;

@end

//-----------------------------------------------------------------
