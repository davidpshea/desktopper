//
//  UIColor+CopyWithZone.m
//  DeskTopper
//
//  Created by David Shea on 10/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIColor+CopyWithZone.h"

#ifdef UNUSED
@implementation UIColor (CopyWithZone)

//-----------------------------------------------------------------
/*  
 Adding the following code in a category on UIColor seems to have fied the problem: */
-(id)copyWithZone:(NSZone*)zone
{
	CGColorRef cgcopy = CGColorCreateCopy([self CGColor]);
	UIColor *copy = [[[self class] allocWithZone:zone] initWithCGColor:cgcopy];
	return copy;
}

//-----------------------------------------------------------------
@end
#endif
