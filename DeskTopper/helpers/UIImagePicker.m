//
//  UIImagePickerController+Helper.m
//  DeskTopper
//
//  Created by David Shea on 27/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIImageExtras.h"
#import "UIApplication+Helpers.h"
#import "UIImagePicker.h"
#import "UIImagePickerController+Helper.h"

@implementation UIImagePicker

//-----------------------------------------------------------------
@synthesize widget = _widget;

//-----------------------------------------------------------------
- (id) init
{
	self = [super init];
	return self;
}

//-----------------------------------------------------------------
- (void) showPhotoPicker:(DTWidget*)widget rootViewController:(UIViewController*)rootViewController
{
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {

		static UIImagePickerController* imagePicker;
		imagePicker = [[UIImagePickerController alloc] init];

		[imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
		[imagePicker setDelegate: self];
		[imagePicker setAllowsEditing:NO];
		[self setWidget:widget];

		[rootViewController presentViewController:imagePicker animated:YES completion:nil];
	}
}

//-----------------------------------------------------------------
- (void) imagePickerController:(UIImagePickerController*)imagePicker
						didFinishPickingMediaWithInfo:(NSDictionary*)mediaInfo
{
	[UIImagePickerController didFinishPickingMediaWithWidget:mediaInfo widget:[self widget]];

	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------
-(void) imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

//-----------------------------------------------------------------

@end
