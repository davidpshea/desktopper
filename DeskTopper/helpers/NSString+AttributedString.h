//
//  NSString+AttributedString.h
//  DeskTopper
//
//  Created by David Shea on 26/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DTCSSStylesheet;

@interface NSString (AttributedString)

+ (NSMutableDictionary*)	getDefaultAttributedStringOptions;
+ (DTCSSStylesheet*)		getDefaultStyleSheet;

- (NSAttributedString*) createAttributedStringFromHTMLStringWithOptions:    (NSDictionary*)options;
- (NSAttributedString*) createAttributedStringFromMarkdownStringWithOptions:(NSDictionary*)options;

@end

