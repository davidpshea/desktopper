//
//  FMDatabase+Variables.m
//  DeskTopper
//
//  Created by David Shea on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FMDatabase+Variables.h"

@implementation FMDatabase (Variables)

//-----------------------------------------------------------------
- (int) getIntVariable:(NSString*)name withDefault:(int)defaultInt
{
	//TODO	DT_ASSERT(![self isExecutingStatement]);
	FMResultSet* results = [self executeQuery:@"SELECT value FROM vars where name=?", name];
	//TODO	DT_ASSERT(![self isExecutingStatement]);

	int result = defaultInt;

	while ([results next]) {
		NSString* s = [results stringForColumn:@"value"];
		result = [s intValue];
	}

	return result;
}

//-----------------------------------------------------------------
- (void) setIntVariable:(NSString*)name withValue:(int)value
{
	NSString* stringValue = [NSString stringWithFormat:@"%d", value];
	//TODO	DT_ASSERT(![self isExecutingStatement]);
	[self executeUpdate:@"INSERT OR REPLACE INTO vars (name,value) VALUES(?,?)", name, stringValue];
	//TODO	DT_ASSERT(![self isExecutingStatement]);
}

//-----------------------------------------------------------------
- (BOOL) getBOOLVariable:(NSString*)name withDefault:(BOOL)defaultBOOL
{
	return [self getIntVariable:name withDefault:(defaultBOOL ? 1 : 0)] == 1;
}

//-----------------------------------------------------------------
- (void) setBOOLVariable:(NSString*)name withValue:(BOOL)value
{
	[self setIntVariable:name withValue:(value ? 1 : 0)];
}

//-----------------------------------------------------------------
@end

