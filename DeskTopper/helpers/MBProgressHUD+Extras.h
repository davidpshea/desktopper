//
//  MBProgressHUD+Extras.h
//  DeskTopper
//
//  Created by David Shea on 17/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MBProgressHUD.h"

typedef enum _MBProgressHUDGesture
{
	navigateBack,
	navigateForward
	
} MBProgressHUDGesture;

@interface MBProgressHUD (Extras)

+ (void) hideAllOnTopWindow;

+ (MBProgressHUD*) showInformation:(NSString*)message onView:(UIView*)view withDelay:(float)delay;
+ (MBProgressHUD*) showInformation:(NSString*)message withDelay:(float)delay;
+ (MBProgressHUD*) showInformation:(NSString*)message;

+ (MBProgressHUD*) showGesture:(MBProgressHUDGesture)gestureID onView:(UIView*)view;

+ (MBProgressHUD*) showDoneOnView:(UIView*)view;
+ (MBProgressHUD*) showProgressOnView:(UIView*)view;
//+ (MBProgressHUD*) showProgressWhilePerformingSelector:(SEL)action onTarget:(id)target onView:(UIView*)view;
+ (MBProgressHUD*) showProgressWhilePerformingSelector:(SEL)action onTarget:(id)target usingData:(id)data onView:(UIView*)view;

@end
