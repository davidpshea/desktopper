//
//  Total_NotesUITestsSwift.swift
//  Total NotesUITestsSwift
//
//  Created by David Shea on 17/03/2016.
//
//

import XCTest

class Total_NotesUITestsSwift: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
		
		let app = XCUIApplication()
		app.toolbars.buttons["0095"].tap()
/*		app.alerts["\U201cTotal Notes\U201d Would Like to Access Your Photos"].collectionViews.buttons["OK"].tap()*/
		app.tables.buttons["Camera Roll"].tap()
		app.collectionViews.cells["Photo, Landscape, March 12, 2011, 4:17 PM"].tap()
		
		let imagesQuery = app.images
		let usersDavidsheaLibraryDeveloperCoresimulatorDevices8f4946e2846f4c83A13b09dde0aa8676DataContainersBundleApplicationBd42ddff002d44c5B15e16430b6dc8c3TotalNotesAppResourceBadgePngElementsQuery = imagesQuery.otherElements.containingType(.Image, identifier:"/Users/davidshea/Library/Developer/CoreSimulator/Devices/8F4946E2-846F-4C83-A13B-09DDE0AA8676/data/Containers/Bundle/Application/BD42DDFF-002D-44C5-B15E-16430B6DC8C3/Total Notes.app//resource/badge.png")
		usersDavidsheaLibraryDeveloperCoresimulatorDevices8f4946e2846f4c83A13b09dde0aa8676DataContainersBundleApplicationBd42ddff002d44c5B15e16430b6dc8c3TotalNotesAppResourceBadgePngElementsQuery.childrenMatchingType(.Image).elementBoundByIndex(10).tap()
		usersDavidsheaLibraryDeveloperCoresimulatorDevices8f4946e2846f4c83A13b09dde0aa8676DataContainersBundleApplicationBd42ddff002d44c5B15e16430b6dc8c3TotalNotesAppResourceBadgePngElementsQuery.childrenMatchingType(.Image).matchingIdentifier("/Users/davidshea/Library/Developer/CoreSimulator/Devices/8F4946E2-846F-4C83-A13B-09DDE0AA8676/data/Containers/Bundle/Application/BD42DDFF-002D-44C5-B15E-16430B6DC8C3/Total Notes.app//resource/badge.png").elementBoundByIndex(7).tap()
		imagesQuery.otherElements.containingType(.Image, identifier:"/Users/davidshea/Library/Developer/CoreSimulator/Devices/8F4946E2-846F-4C83-A13B-09DDE0AA8676/data/Containers/Bundle/Application/BD42DDFF-002D-44C5-B15E-16430B6DC8C3/Total Notes.app//resource/badge.png").element.tap()
		// Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
}
