//
//  Total_NotesUITests.m
//  Total NotesUITests
//
//  Created by David Shea on 17/03/2016.
//
//

#import <XCTest/XCTest.h>

@interface Total_NotesUITests : XCTestCase

@end

@implementation Total_NotesUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
	
	XCUIApplication *app = [[XCUIApplication alloc] init];
	XCUIElementQuery *toolbarsQuery = app.toolbars;
	[toolbarsQuery.buttons[@"edit"] tap];
	[[[[[[[[app childrenMatchingType:XCUIElementTypeWindow] elementBoundByIndex:0] childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeOther].element childrenMatchingType:XCUIElementTypeTextView].element typeText:@"New note"];
	[toolbarsQuery.buttons[@"Save"] tap];
	[app.navigationBars[@"DTHTMLView"].buttons[@"Done"] tap];
	// Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

@end
