Note Taking Goes Visual.

- Want a way of storing all those notes, photos, pictures and websites without losing them in multiple apps?
- Ever had a need to group random stuff together in a way that's easy to remember?
- Just need to gather information now, for later sorting?
- Find staring at lists of text leaves you cold?
- Often take photos of book covers, posters, landmarks, maps and hand written notes for visual reminders?
- Want to make the process of creating and organising notes as painless as possible?

Then Total Notes is your app.

Organise notes, websites and pictures in a fast, visual and simple graphical interface.

Having tried just-about every kind of note taking app out there, and finding them lacking, we decided to build our own.
We wanted something that was simple, visual and useful, yet powerful.

It's Organised

- Multiple screen spaces allow you to organise notes your own way. Work? Home? Project based? Your choice.
- Set your own background for each space to easily identify it.
- Innovative scrap and trash spaces allow you to move notes around easily.
- Notes can be moved, resized, layered and tiled with simple gestures.
- Export notes via email, open in other supported apps via "open in", or via clipboard.
- Import from other apps that support "open in"

It's Visual

- Powerful HTML-based style, color and font control allow you to dramatically alter note appearance instantly.
- Notes can be written in any combination of text, HTML and extended markdown, allowing headings, tables, lists and more.
- View notes full screen without distractions.
- Clean, minimalist, modern user interface.

It's Fast

- Additional keyboards allow Markdown and user-defined shortcuts to facilitate faster note taking.
- One tap note creation from keyboard, web, camera and photo roll.
- Streamlined, gesture-based control to cut down on endless menu taps.
- Enter the information you need quickly, organise later.
- Editable web search engines allow quicker access to web content.
