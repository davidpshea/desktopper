----
Headline (max 72 chars):  remaining [ Info ]
Craft a compelling headline. Keep it short, active, and descriptive.
Write headlines directed to journalists, not consumers.
Avoid the use of uppercase letters.
Use Google Keywords to help craft your Headline. [ Hide ]

Visual note taking comes to the iPhone with the release of Total Notes


----
Summary (max 500 chars):  remaining [ Info ]
Convey your message quickly and concisely by using a powerful opening summary.
If journalists read only this portion of your release, there should be enough summary information to understand what the release is about.
Do not use URL links in your summary!
Use Google Keywords to help craft your Summary. [ Hide ]

BigOrSmallApps have released their latest iPhone app, Total Notes, designed to organise text notes, websites and photos in a fast, clean, graphical interface. In sharp contrast to the many list based apps already available, note contents are shown visually, and can be moved, resized and edited with gestures. New content can be added with a single tap, and notes can be organised over multiple screens.

----
City/Country of Origin (max 55 chars):  remaining [ Info ]
The City/Country of Origin is REQUIRED info in all press communications.
Unless your company is located in another city, use the same city and province as entered in your Account Profile.

----
Body (max 9500 chars):  remaining [ Info ]


As lead developer David Shea states "The iPhone market is saturated with apps for note taking, organising thoughts and managing those snippets of information that life throws at you, and yet we couldn't find an app that suited the way we work. So we decided to develop our own. We wanted something that could quickly capture random bits of information, then help us remember later why we did."

Total Notes has been in development for almost nine months, and has been in everyday use by the developers all that time. "We developed something we actually use ourselves. Every gesture, button and feature was thought about, tested and evaluated. If something didn't work, we threw it out and went back to the drawing board. We tried to minimise everything, the interface, the time needed to create notes, the time spent organising."

Total Notes also contains features that power users expect from a note taking app.

* Multiple spaces allow the user to organise notes however they wish. Two special spaces called trash and scrap, aid the user in organising notes.
* Notes can be written in any combination of text, HTML and extended markdown, allowing headings, tables, lists and more.
* Additional keyboards allow Markdown and user-defined shortcuts.
* Notes can be styled with font changes, colours and user defined styles.
* Editable internet search engines allow quick access to web content.
* Export notes via email, open in other supported apps via "open in", or via clipboard.
* Import from other apps that support "open in"
* View notes full screen.

Total Notes is available now on 








- Want a way of storing all those notes, photos, pictures and websites without loosing them in multiple apps?
- Ever had a need to group random stuff together in a way that's easy to remember?
- Just need to gather information now, for later sorting?
- Find staring at lists of text leaves you cold?
- Often take photos of book covers, posters, landmarks, maps and hand written notes for visual reminders?
- Want to make the process of creating and organising notes as painless as possible?

Then Total Notes is your app.






- Multiple screen spaces allow you to organise notes your own way. Work? Home? Project based? Your choice.
- Set your own background for each space to easily identify it.
- Innovative scrap and trash spaces allow you to move notes around easily.
- Notes can be moved, resized, layered and tiled with simple gestures.
- Export notes via email, open in other supported apps via "open in", or via clipboard.
- Import from other apps that support "open in"

It's Visual

- Powerful HTML-based style, color and font control allow you to dramatically alter note appearance instantly.
- Notes can be written in any combination of text, HTML and extended markdown, allowing headings, tables, lists and more.
- View notes full screen without distractions.
- Clean, minimalist, modern user interface.

It's Fast

- Additional keyboards allow Markdown and user-defined shortcuts to facilitate faster note taking.
- One tap note creation from keyboard, web, camera and photo roll.
- Streamlined, gesture-based control to cut down on endless menu taps.
- Enter the information you need quickly, organise later.
- Editable web search engines allow quicker access to web content.




The iPhone market is saturated with apps for note taking, organising thoughts and managing those snippits of information that life throws at you.
BigOrSmallApps has released their latest iPhone app, Total Notes, as a direct response to evaluating many note taking apps currently on the market.



Submit New Press Release

Extra Links: [ Info ] iPhone Apps click this First
It's important to make additional information available online.
The industry recognizes the value of including multimedia elements, such as photos, charts, and graphics.
Journalists are always looking for these.

We have provided the means for additional links where the URL link to your product may be different
from your normal website address: (http://www.yourwebsite.com/yourproduct.htm). You may leave these blank if not required. Note: Your website address will display automatically once your release has been published.

iPhone Developers: You MUST include at a minimum the relevant product link to your website, as well as the link to your product on the App Store. Your release will be failed without including these crucial links!

[ Hide ]

Link One Text:
Link One URL:    →


-------
Sent from Total Notes on iPhone






BigOrSmallApps have released their latest iPhone app, Total Notes, designed to
organise text notes, websites and photos in a fast, clean, graphical interface.
In sharp contrast to the many list based apps already available, note contents
are shown visually, and can be moved, resized and edited with gestures. New
content can be added with a single tap, and notes can be organised over multiple
screens.

As lead developer for BigOrSmallApps David Shea states "The iPhone market is
saturated with apps for note taking, organising thoughts and managing those
snippets of information that life throws at you, and yet we couldn't find an app
that suited the way we work. So we decided to develop our own. We wanted
something that could quickly capture random bits of information, then help us
remember later why we did."

Total Notes also contains features that power users expect from a note taking
app: * Multiple spaces allow the user to organise notes however they wish. Two
special spaces called trash and scrap, aid the user in organising notes * Notes
can be written in any combination of text, HTML and extended markdown, allowing
headings, tables, lists and more * Additional keyboards allow Markdown and
user-defined shortcuts * Notes can be styled with font changes, colours and user
defined styles * Editable internet search engines allow quick access to web
content * Export notes via email, open in other supported apps via "open in", or
via clipboard * Import from other apps that support "open in" * View notes full
screen

Total Notes has been in development for almost nine months, and has been in
everyday use by the developers all that time. As David explains, "We developed
something we actually use ourselves. Every gesture, button and feature was
thought about, tested and evaluated. If something didn't work, we threw it out
and went back to the drawing board. We tried to minimise everything, the
interface, the time needed to create notes, the time spent organising."

Device Requirements: * iPhone 3GS, 4, 4S, iPhone 5, iPod touch (3/4/5 gen) and
iPad * Requires iOS 5.0 or later * 6.3 MB

Pricing and Availability: Total Notes 1.1.0 is $1.99 USD (or equivalent amount
in other currencies) and available worldwide exclusively through the App Store
in the Productivity category.

Total Notes 1.1.0 Purchase and Download Screenshot App Icon BigOrSmallApps is a
small independent software developer based in Brisbane, Australia, specialising
in mobile applications. BigOrSmallApps has over thirty years experience in
software development, and specialises in providing well designed apps that they
actually use on a day-to-day basis. Copyright (C) 2013 BigOrSmallApps. All
Rights Reserved. Apple, the Apple logo, iPhone, iPod and iPad are registered
trademarks of Apple Inc. in the U.S. and/or other countries.

